# Used by "mix format"
[
  import_deps: [:phoenix, :ecto],
  inputs: ["*.{ex,exs}", "{config,lib,test}/**/*.{ex,exs}"]
]
