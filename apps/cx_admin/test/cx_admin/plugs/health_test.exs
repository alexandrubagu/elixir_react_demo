defmodule CXAdmin.Plugs.HealthTest do
  @moduledoc false

  use CXAdmin.ConnCase

  test "returns 200 ok", %{conn: conn} do
    assert %{"status" => "ok"} ==
             conn
             |> get("/cx/admin/_healthz")
             |> json_response(200)
  end
end
