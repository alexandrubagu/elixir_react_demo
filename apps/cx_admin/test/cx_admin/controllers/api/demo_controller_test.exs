defmodule CXAdmin.API.DemoControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase

  alias CX.AI.Categorizations

  setup do
    s = insert_survey(verbatim_questions: ["q1"], expire_after: 999_999_999)
    i = insert_invite(survey: s, expire_at: ~U[2022-10-14 12:36:54.864835Z])

    {:ok, p} = CX.InviteService.claim(i, %{})
    {:ok, p} = CX.InviteService.complete_participation(p, %{"q1" => "1"})
    :ok = CX.Verbatims.Processing.sentiment(p)
    %{verbatims: [v]} = CX.Participations.get_participation!(p.id, preload: :verbatims)

    {:ok, c} =
      Categorizations.create_verbatim_categorization(v, %{
        topic_probabilities: [
          %{"topic" => "Customer", "probability" => 0.3321312},
          %{"topic" => "Dealer", "probability" => 0.5321321},
          %{"topic" => "Unknown", "probability" => 0.1312321}
        ],
        topic_selections: [
          %{topic: "Customer", probability: 0.3321312},
          %{topic: "Dealer", probability: 0.5321321}
        ],
        word_selections: [
          %{word: "hello"},
          %{word: "hello"},
          %{word: "world"}
        ]
      })

    {:ok, verbatim_categorization: CX.Repo.preload(c, [:topic_selections])}
  end

  describe "index" do
    test "lists all verbatim categorization", %{conn: conn, verbatim_categorization: c} do
      assert %{"data" => [serialized]} =
               conn
               |> get(Routes.api_demo_path(conn, :index))
               |> json_response(200)

      assert %{
               "id" => c.id,
               "verbatim_id" => c.verbatim_id,
               "participation_id" => c.participation_id,
               "text_en" => c.text_en,
               "topic_probabilities" => [
                 %{"probability" => "0.3321312", "topic" => "Customer"},
                 %{"probability" => "0.5321321", "topic" => "Dealer"},
                 %{"probability" => "0.1312321", "topic" => "Unknown"}
               ],
               "topic_selections" => ["Customer", "Dealer"],
               "inserted_at" => DateTime.to_iso8601(c.inserted_at),
               "updated_at" => DateTime.to_iso8601(c.updated_at)
             } == serialized
    end
  end
end
