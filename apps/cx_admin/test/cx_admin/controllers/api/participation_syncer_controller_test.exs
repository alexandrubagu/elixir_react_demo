defmodule CXAdmin.API.ParticipationSyncerControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase

  alias CX.Errors.NotFoundError
  alias CX.ParticipationSyncers
  alias CX.ParticipationSyncers.Process

  setup do
    start_supervised!(Process.Registry)
    start_supervised!(Process.Supervisor)

    :ok
  end

  describe "index" do
    setup do
      a = fixture(:participation_syncer, name: "a")
      b = fixture(:participation_syncer, name: "b")
      {:ok, syncers: [a, b]}
    end

    test "lists all participation_syncers", %{conn: conn, syncers: [a, b]} do
      assert [serialized(b), serialized(a)] ==
               conn
               |> get(Routes.api_participation_syncer_path(conn, :index))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists sorted participation_syncers", %{conn: conn, syncers: [a, b]} do
      assert [serialized(b), serialized(a)] ==
               conn
               |> get(
                 Routes.api_participation_syncer_path(conn, :index,
                   sort: %{field: :name, order: :desc}
                 )
               )
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists searched participation_syncers", %{conn: conn, syncers: [a, _b]} do
      assert [serialized(a)] ==
               conn
               |> get(Routes.api_participation_syncer_path(conn, :index, search: "a"))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "create" do
    @create_attrs %{
      name: "opel_deepdive",
      enabled: false,
      settings: %{
        url: "https://dev.intersofthub.com/api/",
        interval_sec: 3600,
        invites_after: "2020-07-31T10:45:21.885923Z"
      }
    }

    test "creates a new participation_syncer", %{conn: conn} do
      assert %{"data" => data} =
               conn
               |> post(Routes.api_participation_syncer_path(conn, :create),
                 syncer: @create_attrs
               )
               |> assert_operation()
               |> json_response(201)

      assert {:ok, %{entries: [participation_syncer]}} = ParticipationSyncers.list_syncers()

      assert serialized(participation_syncer) == data
      assert participation_syncer.name == @create_attrs.name
      assert participation_syncer.enabled == @create_attrs.enabled
      assert participation_syncer.settings.url == @create_attrs.settings.url
      assert participation_syncer.settings.interval_sec == @create_attrs.settings.interval_sec

      assert DateTime.to_iso8601(participation_syncer.settings.invites_after) ==
               @create_attrs.settings.invites_after
    end

    test "renders errors when data is invalid", %{conn: conn} do
      assert conn
             |> post(Routes.api_participation_syncer_path(conn, :create),
               syncer: %{}
             )
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("name", "can't be blank")
             |> has_validation_error("settings", "can't be blank")
    end
  end

  describe "show" do
    setup do
      {:ok, syncer: fixture(:participation_syncer)}
    end

    test "returns a participation_syncer", %{conn: conn, syncer: syncer} do
      assert %{"data" => serialized(syncer)} ==
               conn
               |> get(Routes.api_participation_syncer_path(conn, :show, syncer))
               |> assert_operation()
               |> json_response(200)
    end

    test "returns a not found error", %{conn: conn} do
      assert conn
             |> get(Routes.api_participation_syncer_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "update" do
    @update_attrs %{
      settings: %{
        url: "https://dev.intersofthub.com/api/v2/",
        interval_sec: 7200,
        invites_after: "2020-09-09T10:45:21.885923Z"
      }
    }

    setup do
      settings = %{url: "https://url", enabled: true, interval_sec: 3600}
      {:ok, syncer: fixture(:participation_syncer, settings: settings)}
    end

    test "updates a syncer and then starts it if was not started before", %{
      conn: conn,
      syncer: syncer
    } do
      assert %{"data" => data} =
               conn
               |> put(Routes.api_participation_syncer_path(conn, :update, syncer),
                 syncer: @update_attrs
               )
               |> assert_operation()
               |> json_response(200)

      {:ok, updated} = ParticipationSyncers.get_syncer(syncer.id)

      assert serialized(updated) == data
      assert updated.settings.url == @update_attrs.settings.url
      assert updated.settings.interval_sec == @update_attrs.settings.interval_sec

      assert DateTime.to_iso8601(updated.settings.invites_after) ==
               @update_attrs.settings.invites_after

      refute Process.Registry.get_syncer(updated.id)
    end

    test "renders errors when data is invalid", %{conn: conn, syncer: syncer} do
      invalid = Map.put(@update_attrs, :settings, nil)

      assert conn
             |> put(Routes.api_participation_syncer_path(conn, :update, syncer),
               syncer: invalid
             )
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("settings", "can't be blank")
    end

    test "renders error when participation_syncer is not found", %{conn: conn} do
      assert conn
             |> put(Routes.api_participation_syncer_path(conn, :update, uuid()),
               syncer: @update_attrs
             )
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "delete" do
    setup do
      {:ok, syncer: fixture(:participation_syncer)}
    end

    test "deletes chosen participation_syncer", %{conn: conn, syncer: syncer} do
      assert conn
             |> delete(Routes.api_participation_syncer_path(conn, :delete, syncer))
             |> assert_operation()
             |> response(204)

      assert {:error, %NotFoundError{}} = ParticipationSyncers.get_syncer(syncer.id)
    end

    test "renders an error when participation_syncer cannot be found", %{conn: conn} do
      assert conn
             |> delete(Routes.api_participation_syncer_path(conn, :delete, uuid()))
             |> assert_operation()
             |> response(404)
    end
  end
end
