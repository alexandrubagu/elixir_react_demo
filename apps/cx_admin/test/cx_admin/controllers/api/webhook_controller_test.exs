defmodule CXAdmin.API.WebhookControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase

  describe "index" do
    setup do
      a = insert_webhook(event: "a", status: :delivered)
      b = insert_webhook(event: "b", status: :failed)
      c = insert_webhook(event: "c", status: :deferred)

      {:ok, webhooks: [a, b, c]}
    end

    test "lists all webhook", %{conn: conn, webhooks: [a, b, c]} do
      assert [serialized(c), serialized(b), serialized(a)] ==
               conn
               |> get(Routes.api_webhook_path(conn, :index))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "allows sorting", %{conn: conn, webhooks: [a, b, c]} do
      assert [serialized(a), serialized(b), serialized(c)] ==
               conn
               |> get(Routes.api_webhook_path(conn, :index, sort: %{field: :event, order: :asc}))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "allows filtering", %{conn: conn, webhooks: [a, _b, c]} do
      assert [serialized(c), serialized(a)] ==
               conn
               |> get(
                 Routes.api_webhook_path(conn, :index,
                   filters: %{"statuses" => ["delivered", "deferred"]}
                 )
               )
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "show" do
    setup _, do: {:ok, webhook: insert_webhook()}

    test "returns a webhook", %{conn: conn, webhook: webhook} do
      assert %{"data" => serialized(webhook)} ==
               conn
               |> get(Routes.api_webhook_path(conn, :show, webhook))
               |> assert_operation()
               |> json_response(200)
    end

    test "returns a not found error", %{conn: conn} do
      assert conn
             |> get(Routes.api_webhook_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end
end
