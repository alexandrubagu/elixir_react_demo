defmodule CXAdmin.API.Notifications.Emails.WebhookControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase

  alias CX.Notifications
  alias CX.Notifications.EmailWebhookRetryProcessor
  alias CX.Notifications.Supervisor, as: NotificationSupervisor

  setup_all do
    :ok = Supervisor.terminate_child(NotificationSupervisor, EmailWebhookRetryProcessor)

    on_exit(fn ->
      Supervisor.restart_child(NotificationSupervisor, EmailWebhookRetryProcessor)
    end)
  end

  describe "index" do
    setup do
      w1 = insert_webhook_event(data: %{"email" => "e1@test.local"}, search: "a")
      w2 = insert_webhook_event(data: %{"email" => "e2@test.local"}, search: "b")

      {:ok, webhooks: [w1, w2]}
    end

    test "lists all email webhooks", %{conn: conn, webhooks: [w1, w2]} do
      assert [serialized(w2), serialized(w1)] ==
               conn
               |> get(Routes.api_email_webhook_event_path(conn, :index))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "allows searching", %{conn: conn, webhooks: [_w1, w2]} do
      assert [serialized(w2)] ==
               conn
               |> get(Routes.api_email_webhook_event_path(conn, :index), search: "b")
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "allows pagination and sorting", %{conn: conn, webhooks: [w1, w2]} do
      query_param = [sort: %{field: "inserted_at", order: "asc"}]

      assert [serialized(w1), serialized(w2)] ==
               conn
               |> get(Routes.api_email_webhook_event_path(conn, :index), query_param)
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "show" do
    setup _, do: {:ok, webhook: insert_webhook_event()}

    test "returns a mail webhook event", %{conn: conn, webhook: w} do
      assert %{"data" => serialized_email_webhook} =
               conn
               |> get(Routes.api_email_webhook_event_path(conn, :show, w))
               |> assert_operation()
               |> json_response(200)

      assert serialized_email_webhook == serialized(w)
    end

    test "renders errors when mail webhook event doesn't exists", %{conn: conn} do
      assert conn
             |> get(Routes.api_email_webhook_event_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "manually_process" do
    @valid_attrs %{
      mail_id: uuid(),
      event_type: "dropped",
      drop_reason: "invalid_address"
    }

    @invalid_attrs %{
      mail_id: nil,
      event_type: "exploded"
    }

    setup do
      mail = insert_mail_message(id: @valid_attrs.mail_id)
      event = insert_webhook_event(status: :requires_manual_intervention)
      {:ok, mail: mail, event: event}
    end

    test "manually processes the webhook event", %{conn: conn, event: event} do
      assert %{"data" => result} =
               conn
               |> put(Routes.api_email_webhook_event_path(conn, :manually_process, event), %{
                 "params" => @valid_attrs
               })
               |> assert_operation()
               |> json_response(200)

      {:ok, %{status: :manually_processed} = processed} =
        Notifications.get_email_webhook_event(event.id)

      assert result == serialized(processed)
    end

    test "errors when given invalid parameters", %{conn: conn, event: event} do
      assert conn
             |> put(Routes.api_email_webhook_event_path(conn, :manually_process, event), %{
               "params" => @invalid_attrs
             })
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("mail_id", "can't be blank")
             |> has_validation_error("event_type", "invalid")
    end

    test "errors when event not found", %{conn: conn} do
      assert conn
             |> put(Routes.api_email_webhook_event_path(conn, :manually_process, uuid()), %{
               "params" => @valid_attrs
             })
             |> assert_operation()
             |> json_response(404)
    end

    test "errors when mail not found", %{conn: conn, event: event} do
      assert conn
             |> put(Routes.api_email_webhook_event_path(conn, :manually_process, event), %{
               "params" => %{@valid_attrs | mail_id: uuid()}
             })
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "manually_ignore" do
    setup do
      event = insert_webhook_event(status: :requires_manual_intervention)
      {:ok, event: event}
    end

    test "manually ignores the webhook event", %{conn: conn, event: event} do
      assert %{"data" => result} =
               conn
               |> put(Routes.api_email_webhook_event_path(conn, :manually_ignore, event))
               |> assert_operation()
               |> json_response(200)

      {:ok, %{status: :manually_ignored} = processed} =
        Notifications.get_email_webhook_event(event.id)

      assert result == serialized(processed)
    end

    test "errors when event not found", %{conn: conn} do
      assert conn
             |> put(Routes.api_email_webhook_event_path(conn, :manually_ignore, uuid()))
             |> assert_operation()
             |> json_response(404)
    end

    test "errors when status does not allow ignoring", %{conn: conn, event: event} do
      {:ok, _already_ignored} = Notifications.manually_ignore_email_webhook_event(event.id)

      assert conn
             |> put(Routes.api_email_webhook_event_path(conn, :manually_ignore, event))
             |> assert_operation()
             |> json_response(409)
    end
  end

  describe "retry_processing_stats" do
    setup do
      start_supervised!(EmailWebhookRetryProcessor)

      insert_webhook_event(status: :requires_manual_intervention)
      :ok = EmailWebhookRetryProcessor.retry_processing()
      run_count_reaches(_log_count = 1, _timeout = 50)

      :ok
    end

    test "return the stats for email webhook retry processing", %{conn: conn} do
      assert %{"data" => data} =
               conn
               |> get(Routes.api_email_webhook_event_path(conn, :retry_processing_stats))
               |> assert_operation()
               |> json_response(200)

      assert %{
               "logs" => [%{"at" => _, "resolved" => 0, "unresolved" => 1}],
               "status" => "idle"
             } = data
    end
  end

  describe "retry_processing" do
    setup do
      start_supervised!(EmailWebhookRetryProcessor)
      :ok
    end

    test "retries to reprocess the email webhook events in requires for manual intervention", %{
      conn: conn
    } do
      assert %{"data" => "ok"} =
               conn
               |> get(Routes.api_email_webhook_event_path(conn, :retry_processing))
               |> assert_operation()
               |> json_response(200)
    end
  end

  defp run_count_reaches(log_count, timeout) do
    becomes_true(timeout, fn ->
      Enum.count(EmailWebhookRetryProcessor.get_stats()[:logs]) >= log_count
    end)
  end
end
