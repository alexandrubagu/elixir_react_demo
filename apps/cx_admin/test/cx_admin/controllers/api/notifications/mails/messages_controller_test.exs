defmodule CXAdmin.API.Notifications.Emails.MessageControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase, async: false

  alias CX.Notifications

  describe "index" do
    setup do
      m1 = insert_mail_message(to_address: "m1@test.local")
      m2 = insert_mail_message(to_address: "m2@test.local")

      {:ok, messages: [m1, m2]}
    end

    test "lists all email messages", %{conn: conn, messages: [m1, m2]} do
      assert [serialized(m2), serialized(m1)] ==
               conn
               |> get(Routes.api_email_message_path(conn, :index))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "allows searching", %{conn: conn, messages: [_m1, m2]} do
      assert [serialized(m2)] ==
               conn
               |> get(Routes.api_email_message_path(conn, :index), search: "2")
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "allows pagination and sorting", %{conn: conn, messages: [m1, m2]} do
      query_param = [sort: %{field: "to_address", order: "asc"}]

      assert [serialized(m1), serialized(m2)] ==
               conn
               |> get(Routes.api_email_message_path(conn, :index), query_param)
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "show" do
    setup do
      m = insert_mail_message()
      _e1 = insert_mail_message_event(message: m)
      _e2 = insert_mail_message_event(message: m, type: :queued)
      {:ok, message} = Notifications.get_email_message(m.id, preload: :events)

      {:ok, message: message}
    end

    test "returns a mail message alongside events", %{conn: conn, message: m} do
      assert %{"data" => serialized_email_message} =
               conn
               |> get(Routes.api_email_message_path(conn, :show, m))
               |> assert_operation()
               |> json_response(200)

      assert serialized_email_message == serialized(m)
    end

    test "renders errors when message doesn't exists", %{conn: conn} do
      assert conn
             |> get(Routes.api_email_message_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "retry_email_delivery" do
    test "returns a mail message alongside events", %{conn: conn} do
      m = insert_mail_message(delivery_status: :bounced)
      mail_id = m.id

      assert %{"data" => serialized_email_message} =
               conn
               |> get(Routes.api_email_message_path(conn, :retry_email_delivery, m))
               |> assert_operation()
               |> json_response(200)

      assert serialized_email_message["delivery_status"] == "pending"

      assert [
               %{
                 "message_id" => ^mail_id,
                 "type" => "redelivery_requested",
                 "drop_reason" => nil,
                 "info" => %{
                   "delivery_drop_reason" => nil,
                   "delivery_status" => "bounced",
                   "was_opened" => false,
                   "was_reported_as_spam" => false,
                   "was_unsubscribed" => false
                 }
               }
             ] = serialized_email_message["events"]

      assert message_is_queued(mail_id, _timeout = 200)
    end

    test "renders a conflict error when mail isn't dropped or bounced", %{conn: conn} do
      m = insert_mail_message()

      assert %{
               "error" => %{
                 "message" =>
                   "Conflict: delivery_status_must_be_dropped_or_bounced, (email_message in notifications)"
               }
             } =
               conn
               |> get(Routes.api_email_message_path(conn, :retry_email_delivery, m))
               |> assert_operation()
               |> json_response(409)
    end

    test "renders errors when message doesn't exists", %{conn: conn} do
      assert conn
             |> get(Routes.api_email_message_path(conn, :retry_email_delivery, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "update_email_address" do
    @email_address "valid@to.local"

    test "returns a mail message alongside events", %{conn: conn} do
      m =
        insert_mail_message(
          to_address: "invalid@to.local",
          delivery_status: :dropped,
          delivery_drop_reason: :invalid_address
        )

      assert %{"data" => serialized_email_message} =
               conn
               |> put(Routes.api_email_message_path(conn, :update_email_address, m), %{
                 "email_address" => @email_address
               })
               |> assert_operation()
               |> json_response(200)

      assert serialized_email_message["to_address"] == @email_address
      assert serialized_email_message["delivery_status"] == "pending"

      assert [
               %{
                 "type" => "email_address_updated",
                 "drop_reason" => nil,
                 "info" => %{
                   "to_address" => "invalid@to.local",
                   "delivery_status" => "dropped",
                   "delivery_drop_reason" => "invalid_address"
                 }
               }
             ] = serialized_email_message["events"]

      assert message_is_queued(m.id, _timeout = 200)
    end

    test "errors when given invalid parameters", %{conn: conn} do
      m = insert_mail_message(delivery_status: :bounced)

      assert conn
             |> put(Routes.api_email_message_path(conn, :update_email_address, m), %{
               "email_address" => nil
             })
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("to_address", "can't be blank")
    end

    test "errors when mail is not dropped or bounced", %{conn: conn} do
      m = insert_mail_message(delivery_status: :delivered)

      assert conn
             |> put(Routes.api_email_message_path(conn, :update_email_address, m), %{
               "email_address" => @email_address
             })
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("delivery_status", "must be dropped or bounced")
    end

    test "errors when mail is not dropped for an acceptable reason", %{conn: conn} do
      m =
        insert_mail_message(
          delivery_status: :dropped,
          delivery_drop_reason: :unsubscribed_address
        )

      assert conn
             |> put(Routes.api_email_message_path(conn, :update_email_address, m), %{
               "email_address" => @email_address
             })
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error(
               "delivery_drop_reason",
               "delivery status must be dropped with reason invalid_address or bounced_address"
             )
    end

    test "renders errors when message doesn't exists", %{conn: conn} do
      assert conn
             |> put(Routes.api_email_message_path(conn, :update_email_address, uuid()), %{
               "email_address" => @email_address
             })
             |> assert_operation()
             |> json_response(404)
    end
  end
end
