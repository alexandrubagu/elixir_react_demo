defmodule CXAdmin.API.Notifications.Blacklists.BlacklistEntryControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase

  alias CX.Notifications.Blacklists

  describe "index" do
    setup do
      e1 = insert_blacklist_entry(key: "e1@test.local")
      e2 = insert_blacklist_entry(key: "e2@test.local")

      {:ok, entries: [e1, e2]}
    end

    test "lists all blacklist entries", %{conn: conn, entries: [e1, e2]} do
      assert [serialized(e2), serialized(e1)] ==
               conn
               |> get(Routes.api_blacklist_entry_path(conn, :index))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "allows searching", %{conn: conn, entries: [_e1, e2]} do
      assert [serialized(e2)] ==
               conn
               |> get(Routes.api_blacklist_entry_path(conn, :index), search: "2")
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "allows pagination and sorting", %{conn: conn, entries: [e1, e2]} do
      query_param = [sort: %{field: "key", order: "asc"}]

      assert [serialized(e1), serialized(e2)] ==
               conn
               |> get(Routes.api_blacklist_entry_path(conn, :index), query_param)
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "create" do
    @event_id uuid()
    @valid_attrs %{
      notification_type: "mail",
      key: "test@example.com",
      reason: "unsubscribed_address",
      webhook_event_id: @event_id
    }

    @invalid_attrs %{
      notification_type: "boom",
      key: nil,
      reason: nil
    }

    setup do
      event = insert_webhook_event(id: @event_id)

      {:ok, event: event}
    end

    test "renders blacklist entry when data is valid", %{conn: conn} do
      assert %{"data" => %{"id" => id} = serialized_entry} =
               conn
               |> post(Routes.api_blacklist_entry_path(conn, :create),
                 blacklist_entry: @valid_attrs
               )
               |> assert_operation()
               |> json_response(201)

      {:ok, created} = Blacklists.get_entry(id)
      assert serialized_entry == serialized(created)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      assert conn
             |> post(Routes.api_blacklist_entry_path(conn, :create),
               blacklist_entry: @invalid_attrs
             )
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("notification_type", "is invalid")
             |> has_validation_error("key", "can't be blank")
             |> has_validation_error("reason", "can't be blank")
    end
  end

  describe "update" do
    @valid_attrs %{
      notification_type: "mail",
      key: "test@example.com",
      reason: "unsubscribed_address"
    }

    @invalid_attrs %{
      notification_type: "boom",
      key: nil,
      reason: nil
    }

    setup do
      {:ok, entry: insert_blacklist_entry()}
    end

    test "renders blacklist entry when data is valid", %{conn: conn, entry: entry} do
      assert %{"data" => %{"id" => id} = serialized_entry} =
               conn
               |> put(Routes.api_blacklist_entry_path(conn, :update, entry),
                 blacklist_entry: @valid_attrs
               )
               |> assert_operation()
               |> json_response(200)

      {:ok, updated} = Blacklists.get_entry(entry.id)
      assert serialized_entry == serialized(updated)
    end

    test "renders errors when data is invalid", %{conn: conn, entry: entry} do
      assert conn
             |> put(Routes.api_blacklist_entry_path(conn, :update, entry),
               blacklist_entry: @invalid_attrs
             )
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("notification_type", "is invalid")
             |> has_validation_error("key", "can't be blank")
             |> has_validation_error("reason", "can't be blank")
    end

    test "renders error when entry is not found", %{conn: conn} do
      assert conn
             |> put(Routes.api_blacklist_entry_path(conn, :update, uuid()), blacklist_entry: %{})
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "show" do
    setup _, do: {:ok, entry: insert_blacklist_entry()}

    test "renders blacklist entry", %{conn: conn, entry: entry} do
      assert %{"data" => serialized_entry} =
               conn
               |> get(Routes.api_blacklist_entry_path(conn, :show, entry))
               |> assert_operation()
               |> json_response(200)

      assert serialized_entry == serialized(entry)
    end

    test "renders errors when blacklist entry doesn't exists", %{conn: conn} do
      assert conn
             |> get(Routes.api_blacklist_entry_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "delete" do
    setup _, do: {:ok, entry: insert_blacklist_entry()}

    test "deletes a blacklist entry", %{conn: conn, entry: entry} do
      assert conn
             |> delete(Routes.api_blacklist_entry_path(conn, :delete, entry))
             |> assert_operation()
             |> response(204)
    end

    test "renders errors when data is invalid", %{conn: conn} do
      assert conn
             |> get(Routes.api_blacklist_entry_path(conn, :delete, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end
end
