defmodule CXAdmin.API.Notifications.SMS.MessageControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase

  alias CX.Notifications.SMS

  describe "index" do
    setup do
      m1 = fixture(:sms)
      m2 = fixture(:sms)

      {:ok, messages: [m1, m2]}
    end

    test "lists all sms messages", %{conn: conn, messages: [m1, m2]} do
      assert [serialized(m2), serialized(m1)] ==
               conn
               |> get(Routes.api_sms_message_path(conn, :index))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "allows pagination and sorting", %{conn: conn, messages: [m1, m2]} do
      query_param = [sort: %{field: "inserted_at", order: "asc"}]

      assert [serialized(m1), serialized(m2)] ==
               conn
               |> get(Routes.api_sms_message_path(conn, :index), query_param)
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "show" do
    setup do
      message = fixture(:sms)
      {:ok, with_event} = SMS.mark(message, :unsubscribed, %{"hello" => "world"})

      {:ok, message: preload(with_event)}
    end

    test "returns a sms message with preloaded events", %{conn: conn, message: message} do
      assert %{"data" => serialized_sms_message} =
               conn
               |> get(Routes.api_sms_message_path(conn, :show, message))
               |> assert_operation()
               |> json_response(200)

      assert serialized_sms_message == serialized(message)
      assert length(serialized_sms_message["events"]) == 1
    end

    test "renders errors when message doesn't exists", %{conn: conn} do
      assert conn
             |> get(Routes.api_sms_message_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  defp preload(message), do: CX.Repo.preload(message, :events)
end
