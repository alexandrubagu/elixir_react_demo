defmodule CXAdmin.API.BatchControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase

  alias CX.Errors.NotFoundError
  alias CX.Batches

  describe "index" do
    setup do
      a = fixture(:batch, name: "a")
      b = fixture(:batch, name: "b")
      {:ok, a: a, b: b}
    end

    test "lists all batches", %{conn: conn, a: a, b: b} do
      assert [serialized(b), serialized(a)] ==
               conn
               |> get(Routes.api_batch_path(conn, :index))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists sorted batches", %{conn: conn, a: a, b: b} do
      assert [serialized(b), serialized(a)] ==
               conn
               |> get(Routes.api_batch_path(conn, :index, sort: %{field: :name, order: :desc}))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists searched batches", %{conn: conn, a: a} do
      assert [serialized(a)] ==
               conn
               |> get(Routes.api_batch_path(conn, :index, search: "a"))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "create" do
    @create_attrs %{name: "some name"}

    test "creates a new batch", %{conn: conn} do
      assert %{"data" => data} =
               conn
               |> post(Routes.api_batch_path(conn, :create), batch: @create_attrs)
               |> assert_operation()
               |> json_response(201)

      assert {:ok, %{entries: [batch]}} = Batches.list_batches()
      assert serialized(batch) == data

      assert batch.name == @create_attrs.name
      assert batch.log == []
    end

    test "renders errors when data is invalid", %{conn: conn} do
      assert conn
             |> post(Routes.api_batch_path(conn, :create), batch: %{})
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("name", "can't be blank")
    end
  end

  describe "show" do
    setup do
      {:ok, batch: fixture(:batch, name: "a")}
    end

    test "returns a batch", %{conn: conn, batch: batch} do
      assert %{"data" => serialized(batch)} ==
               conn
               |> get(Routes.api_batch_path(conn, :show, batch))
               |> assert_operation()
               |> json_response(200)
    end

    test "returns a not found error", %{conn: conn} do
      assert conn
             |> get(Routes.api_batch_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "update" do
    @update_attrs %{name: "some updated name"}

    setup do
      {:ok, batch: fixture(:batch, name: "a")}
    end

    test "updates a batch", %{conn: conn, batch: batch} do
      assert %{"data" => data} =
               conn
               |> put(Routes.api_batch_path(conn, :update, batch), batch: @update_attrs)
               |> assert_operation()
               |> json_response(200)

      {:ok, updated} = Batches.get_batch(batch.id)
      assert serialized(updated) == data
      assert updated.name == @update_attrs.name
    end

    test "renders errors when data is invalid", %{conn: conn, batch: batch} do
      invalid = Map.put(@update_attrs, :name, nil)

      assert conn
             |> put(Routes.api_batch_path(conn, :update, batch), batch: invalid)
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("name", "can't be blank")
    end

    test "renders error when batch is not found", %{conn: conn} do
      assert conn
             |> put(Routes.api_batch_path(conn, :update, uuid()), batch: @update_attrs)
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "delete" do
    setup do
      {:ok, batch: fixture(:batch)}
    end

    test "deletes chosen batch", %{conn: conn, batch: batch} do
      assert conn
             |> delete(Routes.api_batch_path(conn, :delete, batch))
             |> assert_operation()
             |> response(204)

      assert {:error, %NotFoundError{}} = Batches.get_batch(batch.id)
    end

    test "renders an error when batch cannot be found", %{conn: conn} do
      assert conn
             |> delete(Routes.api_batch_path(conn, :delete, uuid()))
             |> assert_operation()
             |> response(404)
    end
  end

  describe "process" do
    setup do
      {:ok, batch: fixture(:batch)}
    end

    test "processes the entries in the chosen batch", %{conn: conn, batch: batch} do
      assert %{"data" => data} =
               conn
               |> put(Routes.api_batch_path(conn, :process, batch))
               |> assert_operation()
               |> json_response(200)

      assert {:ok, processed} = Batches.get_batch(batch.id)
      assert data == serialized(processed)
      assert [%{event: "batch_ended"} | _] = processed.log
    end

    test "renders an error when batch cannot be found", %{conn: conn} do
      assert conn
             |> put(Routes.api_batch_path(conn, :process, uuid()))
             |> assert_operation()
             |> response(404)
    end
  end

  describe "import_json" do
    setup do
      Temp.track!()
      {:ok, batch: fixture(:batch), survey: insert_survey()}
    end

    test "imports the entries in the chosen batch", %{conn: conn, batch: batch, survey: survey} do
      file =
        create_json_file([
          %{
            survey: survey.name,
            ref: "1",
            language: "de",
            notification_type: "mail",
            email_address: "a@a.local",
            params: %{a: "a"}
          }
        ])

      assert %{"data" => %{"count" => 1}} =
               conn
               |> post(Routes.api_batch_path(conn, :import_json, batch), json_file: file)
               |> assert_operation()
               |> json_response(200)

      assert {:ok, %{entries: [%{ref: "1"}]}} = Batches.list_batch_entries(batch)
    end

    test "renders an error when batch cannot be found", %{conn: conn} do
      file = create_json_file([])

      assert conn
             |> post(Routes.api_batch_path(conn, :import_json, uuid()), json_file: file)
             |> assert_operation()
             |> response(404)
    end

    defp create_json_file(data) do
      content = Jason.encode!(data)
      {:ok, path} = Temp.open("json_batch", &IO.write(&1, content))
      %Plug.Upload{path: path, filename: "batch.json"}
    end
  end
end
