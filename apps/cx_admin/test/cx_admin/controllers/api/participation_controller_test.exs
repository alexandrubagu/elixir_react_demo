defmodule CXAdmin.API.ParticipationControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase

  alias CX.Participations
  alias CX.Verbatims.Processor
  alias CX.Verbatims.Supervisor, as: VerbatimSupervisor

  setup_all do
    :ok = Supervisor.terminate_child(VerbatimSupervisor, Processor)

    on_exit(fn ->
      Supervisor.restart_child(VerbatimSupervisor, Processor)
    end)
  end

  describe "index" do
    setup do
      a = insert_participation(ref: "a-ref")
      b = insert_participation(ref: "b-ref")

      {:ok, a: a, b: b}
    end

    test "lists all participations", %{conn: conn, a: a, b: b} do
      assert [serialized(b), serialized(a)] ==
               conn
               |> get(Routes.api_participation_path(conn, :index))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists sorted participations", %{conn: conn, a: a, b: b} do
      assert [serialized(a), serialized(b)] ==
               conn
               |> get(
                 Routes.api_participation_path(conn, :index, sort: %{field: :ref, order: :asc})
               )
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists searched participations", %{conn: conn, a: a} do
      assert [serialized(a)] ==
               conn
               |> get(Routes.api_participation_path(conn, :index, search: "a-ref"))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "show" do
    setup do
      participation = insert_participation()
      verbatim = fixture(:verbatim, participation: participation)
      _sentence = fixture(:verbatim_sentence, verbatim: verbatim)
      _verbatim_log = fixture(:verbatim_log, participation: participation)
      with_event = Participations.complete_participation(participation, %{"a" => 1})
      {:ok, participation: preload(with_event)}
    end

    test "returns a participation with event details", %{conn: conn, participation: participation} do
      assert %{"data" => serialized(participation)} ==
               conn
               |> get(Routes.api_participation_path(conn, :show, participation))
               |> assert_operation()
               |> json_response(200)
    end

    test "returns a not found error", %{conn: conn} do
      assert conn
             |> get(Routes.api_participation_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "sessions" do
    setup do
      survey = insert_survey()
      invite = insert_invite(survey: survey)
      {:ok, participation} = Participations.start_participation(invite, survey)
      {:ok, participation: participation}
    end

    test "returns the list of participation sessions", %{conn: conn, participation: %{id: id}} do
      assert %{"data" => [%{"participation_id" => ^id, "pid" => pid}]} =
               conn
               |> get(Routes.api_participation_path(conn, :sessions))
               |> assert_operation()
               |> json_response(200)

      assert pid =~ "#PID<"
    end
  end

  describe "process_verbatims" do
    setup do
      p = insert_participation(status: :completed, verbatims_processed: false)
      start_supervised!({Processor, initial_delay: 0})
      processed_participation_count_reaches(_count = 1, _timeout = 100)

      {:ok, participation: p}
    end

    test "starts to process verbatims", %{conn: conn, participation: p} do
      assert %{"data" => "ok"} =
               conn
               |> post(Routes.api_participation_path(conn, :process_verbatims))
               |> assert_operation()
               |> json_response(200)

      assert {:ok, %{verbatims_processed: true}} = Participations.get_participation(p.id)
    end
  end

  describe "verbatim_processor_stats" do
    setup do
      p = insert_participation(status: :completed, verbatims_processed: false)
      start_supervised!({Processor, initial_delay: 0})
      processed_participation_count_reaches(_count = 1, _timeout = 100)

      {:ok, participation: p}
    end

    test "returns the stats of verbatim processor", %{conn: conn} = ctx do
      %{participation: %{id: participation_id}} = ctx

      assert %{"data" => data} =
               conn
               |> get(Routes.api_participation_path(conn, :verbatim_processor_stats))
               |> assert_operation()
               |> json_response(200)

      assert %{
               "logs" => [
                 %{
                   "participation" => ^participation_id,
                   "status" => "processed",
                   "ts" => _serialized_date
                 }
               ],
               "processed_participation_count" => 1,
               "run_count" => 1
             } = data
    end
  end

  defp processed_participation_count_reaches(count, timeout) do
    becomes_true(timeout, fn -> Processor.get_stats()[:processed_participation_count] >= count end)
  end

  defp preload({:ok, participation}) do
    CX.Participations.get_participation!(participation.id,
      preload: [:survey, :events, :verbatims, :verbatim_logs]
    )
  end
end
