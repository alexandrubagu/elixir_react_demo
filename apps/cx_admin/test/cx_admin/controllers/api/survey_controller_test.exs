defmodule CXAdmin.API.SurveyControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase

  alias CX.Surveys

  describe "index" do
    setup do
      a = insert_survey(name: "a", deleted: false)
      b = insert_survey(name: "b", deleted: false)
      c = insert_survey(name: "c", deleted: true)
      {:ok, surveys: [a, b, c]}
    end

    test "lists all undeleted surveys", %{conn: conn, surveys: [a, b | _]} do
      assert [serialized(a), serialized(b)] ==
               conn
               |> get(Routes.api_survey_path(conn, :index))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists all surveys", %{conn: conn, surveys: [a, b, c]} do
      assert [serialized(a), serialized(b), serialized(c)] ==
               conn
               |> get(Routes.api_survey_path(conn, :index), %{
                 "filters" => %{"include_deleted" => true}
               })
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists sorted surveys", %{conn: conn, surveys: [a, b | _]} do
      assert [serialized(b), serialized(a)] ==
               conn
               |> get(Routes.api_survey_path(conn, :index, sort: %{field: :name, order: :desc}))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists searched surveys", %{conn: conn, surveys: [a | _]} do
      assert [serialized(a)] ==
               conn
               |> get(Routes.api_survey_path(conn, :index, search: "a"))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "create" do
    @create_attrs %{
      abandonned_after: 42,
      content: %{"brand" => "test"},
      description: "some description",
      expire_after: 42,
      from_email_address: "some from_email_address",
      from_email_name: "some from_email_name",
      name: "some name",
      remind_after: 42,
      tpl_abandonned: "7488a646-e31f-11e4-aace-600308960662",
      tpl_invite: "7488a646-e31f-11e4-aace-600308960663",
      tpl_reminder: "7488a646-e31f-11e4-aace-600308960664",
      tpl_thanks: "7488a646-e31f-11e4-aace-600308960665",
      verbatim_questions: ["a", "b"],
      sms_invite_tpl: "Hello, <name>",
      sms_from_number: "+15005550006"
    }

    test "creates a new survey", %{conn: conn} do
      assert %{"data" => data} =
               conn
               |> post(Routes.api_survey_path(conn, :create), survey: @create_attrs)
               |> assert_operation()
               |> json_response(201)

      assert {:ok, %{entries: [survey]}} = Surveys.list_surveys()
      assert serialized(survey) == data

      assert survey.abandonned_after == @create_attrs.abandonned_after
      assert survey.content == @create_attrs.content
      assert survey.description == @create_attrs.description
      assert survey.expire_after == @create_attrs.expire_after
      assert survey.from_email_address == @create_attrs.from_email_address
      assert survey.from_email_name == @create_attrs.from_email_name
      assert survey.name == @create_attrs.name
      assert survey.remind_after == @create_attrs.remind_after
      assert survey.tpl_abandonned == @create_attrs.tpl_abandonned
      assert survey.tpl_invite == @create_attrs.tpl_invite
      assert survey.tpl_reminder == @create_attrs.tpl_reminder
      assert survey.tpl_thanks == @create_attrs.tpl_thanks
      assert survey.verbatim_questions == @create_attrs.verbatim_questions
      assert survey.sms_invite_tpl == @create_attrs.sms_invite_tpl
      assert survey.sms_from_number == @create_attrs.sms_from_number
    end

    test "renders errors when data is invalid", %{conn: conn} do
      assert conn
             |> post(Routes.api_survey_path(conn, :create), survey: %{})
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("name", "can't be blank")
             |> has_validation_error("description", "can't be blank")
    end
  end

  describe "show" do
    setup do
      {:ok, survey: insert_survey(name: "a")}
    end

    test "returns a survey", %{conn: conn, survey: survey} do
      assert %{"data" => serialized(survey)} ==
               conn
               |> get(Routes.api_survey_path(conn, :show, survey))
               |> assert_operation()
               |> json_response(200)
    end

    test "returns a not found error", %{conn: conn} do
      assert conn
             |> get(Routes.api_survey_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "update" do
    @update_attrs %{
      abandonned_after: 43,
      content: %{"brand" => "updated"},
      description: "some updated description",
      expire_after: 43,
      from_email_address: "some updated from_email_address",
      from_email_name: "some updated from_email_name",
      name: "some updated name",
      remind_after: 43,
      tpl_abandonned: "7488a646-e31f-11e4-aace-600308960668",
      tpl_invite: "7488a646-e31f-11e4-aace-600308960669",
      tpl_reminder: "7488a646-e31f-11e4-aace-600308960670",
      tpl_thanks: "7488a646-e31f-11e4-aace-600308960671",
      verbatim_questions: ~w(1 2 3),
      sms_invite_tpl: "Hello world",
      sms_from_number: "+15005550007"
    }

    setup do
      {:ok, survey: insert_survey(name: "a")}
    end

    test "updates a survey", %{conn: conn, survey: survey} do
      assert %{"data" => data} =
               conn
               |> put(Routes.api_survey_path(conn, :update, survey), survey: @update_attrs)
               |> assert_operation()
               |> json_response(200)

      {:ok, updated} = Surveys.get_survey(survey.id)
      assert serialized(updated) == data

      assert updated.abandonned_after == @update_attrs.abandonned_after
      assert updated.content == @update_attrs.content
      assert updated.description == @update_attrs.description
      assert updated.expire_after == @update_attrs.expire_after
      assert updated.from_email_address == @update_attrs.from_email_address
      assert updated.from_email_name == @update_attrs.from_email_name
      assert updated.name == @update_attrs.name
      assert updated.remind_after == @update_attrs.remind_after
      assert updated.tpl_abandonned == @update_attrs.tpl_abandonned
      assert updated.tpl_invite == @update_attrs.tpl_invite
      assert updated.tpl_reminder == @update_attrs.tpl_reminder
      assert updated.tpl_thanks == @update_attrs.tpl_thanks
      assert updated.verbatim_questions == @update_attrs.verbatim_questions
      assert updated.sms_invite_tpl == @update_attrs.sms_invite_tpl
      assert updated.sms_from_number == @update_attrs.sms_from_number
    end

    test "renders errors when data is invalid", %{conn: conn, survey: survey} do
      invalid = Map.put(@update_attrs, :name, nil)

      assert conn
             |> put(Routes.api_survey_path(conn, :update, survey), survey: invalid)
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("name", "can't be blank")
    end

    test "renders error when survey is not found", %{conn: conn} do
      assert conn
             |> put(Routes.api_survey_path(conn, :update, uuid()), survey: @update_attrs)
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "toggle_delete" do
    setup _, do: {:ok, survey: insert_survey(name: "a", deleted: false)}

    test "toggles delete flag on survey", %{conn: conn, survey: survey} do
      assert %{"data" => %{"deleted" => true}} =
               conn
               |> put(Routes.api_survey_path(conn, :toggle_delete, survey))
               |> assert_operation()
               |> json_response(200)

      assert %{"data" => %{"deleted" => false}} =
               conn
               |> put(Routes.api_survey_path(conn, :toggle_delete, survey))
               |> assert_operation()
               |> json_response(200)
    end

    test "renders error when survey is not found", %{conn: conn} do
      assert conn
             |> put(Routes.api_survey_path(conn, :toggle_delete, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end
end
