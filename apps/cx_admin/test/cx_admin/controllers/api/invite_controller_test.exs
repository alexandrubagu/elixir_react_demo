defmodule CXAdmin.API.InviteControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase, async: false

  alias CX.Invites
  alias CX.BrowserInfo
  alias CX.Notifications

  describe "index" do
    setup do
      a = insert_invite(ref: "a-ref")
      b = insert_invite(ref: "b-ref")
      {:ok, a: a, b: b}
    end

    test "lists all invites", %{conn: conn, a: a, b: b} do
      assert [serialized(b), serialized(a)] ==
               conn
               |> get(Routes.api_invite_path(conn, :index))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists sorted invites", %{conn: conn, a: a, b: b} do
      assert [serialized(a), serialized(b)] ==
               conn
               |> get(Routes.api_invite_path(conn, :index, sort: %{field: :ref, order: :asc}))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists searched invites", %{conn: conn, a: a} do
      assert [serialized(a)] ==
               conn
               |> get(Routes.api_invite_path(conn, :index, search: "a-ref"))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "show" do
    setup do
      participation = insert_participation()
      invite = insert_invite()
      with_event = Invites.mark_invite_claimed(invite, participation.id, BrowserInfo.defaults())
      fixture(:sms, invite: invite)
      insert_mail_message(invite: invite)
      {:ok, invite: preload(with_event)}
    end

    test "returns a invite with preloaded event details, smses and email_messages", %{
      conn: conn,
      invite: invite
    } do
      assert %{"data" => serialized(invite)} ==
               conn
               |> get(Routes.api_invite_path(conn, :show, invite))
               |> assert_operation()
               |> json_response(200)
    end

    test "returns a not found error", %{conn: conn} do
      assert conn
             |> get(Routes.api_invite_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "create_and_send" do
    @valid_mail_attrs %{
      notification_type: "mail",
      email_address: "user@intersoft.com",
      language: "some language",
      params: %{"firstname" => "Joe", "lastname" => "Doe", "unused" => 1},
      ref: "some ref",
      webhook_url: "https://intersoft-one-ex.com",
      webhook_params: %{"investigation" => "7488a646-e31f-11e4-aace-600308960662"}
    }

    @valid_sms_attrs %{
      notification_type: :sms,
      sms_dest_number: "+15005550006",
      language: "some language",
      params: %{"firstname" => "Joe", "lastname" => "Doe", "unused" => 1},
      ref: "some ref",
      webhook_url: "https://intersoft-one-ex.com",
      webhook_params: %{"investigation" => "7488a646-e31f-11e4-aace-600308960662"}
    }

    @valid_none_attrs %{
      notification_type: "none",
      language: "some language",
      params: %{"firstname" => "Joe", "lastname" => "Doe", "unused" => 1},
      ref: "some ref",
      webhook_params: %{"investigation" => "7488a646-e31f-11e4-aace-600308960662"},
      webhook_url: "https://intersoft-one-ex.com"
    }

    setup _, do: {:ok, survey: insert_survey()}

    test "with valid data creates a mail invite", %{conn: conn, survey: survey} do
      assert %{"data" => %{"id" => invite_id} = data} =
               conn
               |> post(Routes.api_invite_path(conn, :create_and_send, survey.name),
                 invite: @valid_mail_attrs
               )
               |> assert_operation()
               |> json_response(200)

      assert {:ok, invite} =
               Invites.get_invite(invite_id, preload: [:events, :smses, :email_messages])

      assert data == serialized(invite)
    end

    test "with valid data creates a sms invite", %{conn: conn, survey: survey} do
      assert %{"data" => %{"id" => invite_id} = data} =
               conn
               |> post(Routes.api_invite_path(conn, :create_and_send, survey.name),
                 invite: @valid_sms_attrs
               )
               |> assert_operation()
               |> json_response(200)

      assert {:ok, invite} =
               Invites.get_invite(invite_id, preload: [:events, :smses, :email_messages])

      assert data == serialized(invite)
    end

    test "with valid data creates a none invite", %{conn: conn, survey: survey} do
      assert %{"data" => %{"id" => invite_id} = data} =
               conn
               |> post(Routes.api_invite_path(conn, :create_and_send, survey.name),
                 invite: @valid_none_attrs
               )
               |> assert_operation()
               |> json_response(200)

      assert {:ok, invite} =
               Invites.get_invite(invite_id, preload: [:events, :smses, :email_messages])

      assert data == serialized(invite)
    end

    test "with invalid data returns error changeset", %{conn: conn, survey: survey} do
      assert conn
             |> post(Routes.api_invite_path(conn, :create_and_send, survey.name), invite: %{})
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("email_address", "can't be blank")
             |> has_validation_error("language", "can't be blank")
             |> has_validation_error("params", "can't be blank")
             |> has_validation_error("ref", "can't be blank")
    end
  end

  describe "update_email_address" do
    @email_address "updated@tld.local"
    @ten_days 3600 * 24 * 10

    setup do
      expiration = DateTime.utc_now() |> DateTime.add(@ten_days)
      invite = insert_invite(email_address: "valid@tld.local", expire_at: expiration)

      {:ok, invite: invite}
    end

    test "returns an invite alongside with email messages", %{conn: conn, invite: invite} do
      insert_mail_message(type: :invite, invite: invite, delivery_status: :bounced)

      assert email_delivery_reaches(1, fn ->
               assert %{"data" => invite} =
                        conn
                        |> put(
                          Routes.api_invite_path(conn, :update_email_address, invite),
                          %{"email_address" => @email_address}
                        )
                        |> assert_operation()
                        |> json_response(200)

               assert invite["email_address"] == @email_address

               assert {:ok, %{entries: [message]}} = Notifications.list_email_messages()
               assert message.delivery_status == :pending
               refute message.delivery_drop_reason
             end)
    end

    test "when invite expires in less than five days returns an error", %{conn: conn} do
      expiration = DateTime.utc_now() |> DateTime.add(1000)
      invite = insert_invite(email_address: "valid@tld.local", expire_at: expiration)

      assert conn
             |> put(
               Routes.api_invite_path(conn, :update_email_address, invite),
               %{"email_address" => @email_address}
             )
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("expire_at", "invite expires in less than 432000")
    end

    test "returns a conflict error if is not updatable", %{conn: conn, invite: invite} do
      insert_mail_message(type: :invite, invite: invite, delivery_status: :delivered)

      assert conn
             |> put(
               Routes.api_invite_path(conn, :update_email_address, invite),
               %{"email_address" => @email_address}
             )
             |> assert_operation()
             |> json_response(409)
             |> has_error_message("messages_already_delivered_or_interacted")
    end

    test "renders errors when invite doesn't exists", %{conn: conn} do
      assert conn
             |> put(
               Routes.api_invite_path(conn, :update_email_address, uuid()),
               %{"email_address" => @email_address}
             )
             |> assert_operation()
             |> json_response(404)
    end
  end

  defp preload({:ok, invite}),
    do: CX.Repo.preload(invite, [:events, smses: :events, email_messages: :events])
end
