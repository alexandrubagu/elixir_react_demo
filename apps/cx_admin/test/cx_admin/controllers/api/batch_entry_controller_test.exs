defmodule CXAdmin.API.BatchEntryControllerTest do
  @moduledoc false

  use CXAdmin.ConnCase
  use CX.DataCase

  alias CX.Errors.NotFoundError
  alias CX.Batches

  describe "index" do
    setup do
      batch = fixture(:batch, name: "a")
      a1 = fixture(:batch_entry, ref: "a1", batch: batch)
      a2 = fixture(:batch_entry, ref: "a2", batch: batch)
      _c = fixture(:batch_entry, ref: "c1")

      {:ok, batch: batch, a1: a1, a2: a2}
    end

    test "lists all entries", %{conn: conn, batch: batch, a1: a1, a2: a2} do
      assert [serialized(a2), serialized(a1)] ==
               conn
               |> get(Routes.api_batch_entry_path(conn, :index, batch))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists sorted entries", %{conn: conn, batch: batch, a1: a1, a2: a2} do
      params = %{sort: %{field: :ref, order: :asc}}

      assert [serialized(a1), serialized(a2)] ==
               conn
               |> get(Routes.api_batch_entry_path(conn, :index, batch, params))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end

    test "lists searched entries", %{conn: conn, batch: batch, a1: a1} do
      assert [serialized(a1)] ==
               conn
               |> get(Routes.api_batch_entry_path(conn, :index, batch, search: "a1"))
               |> assert_operation()
               |> json_response(200)
               |> paginated()
    end
  end

  describe "create" do
    @create_attrs %{
      ref: "some-ref",
      notification_type: :mail,
      params: %{"hello" => "world"},
      language: "en",
      email_address: "test@some.local"
    }

    setup do
      batch = fixture(:batch, name: "a")
      survey = insert_survey(name: "a")
      {:ok, batch: batch, survey: survey}
    end

    test "creates a new entry", %{conn: conn, batch: batch, survey: survey} do
      assert %{"data" => data} =
               conn
               |> post(Routes.api_batch_entry_path(conn, :create, batch, survey),
                 entry: @create_attrs
               )
               |> assert_operation()
               |> json_response(201)

      assert {:ok, %{entries: [entry]}} = Batches.list_batch_entries(batch)
      assert serialized(entry) == data
      assert entry.batch_id == batch.id
      assert entry.survey_id == survey.id
      assert entry.ref == @create_attrs.ref
      assert entry.notification_type == @create_attrs.notification_type
      assert entry.params == @create_attrs.params
      assert entry.language == @create_attrs.language
      assert entry.email_address == @create_attrs.email_address
    end

    test "renders errors when data is invalid", %{conn: conn, batch: batch, survey: survey} do
      assert conn
             |> post(Routes.api_batch_entry_path(conn, :create, batch, survey), entry: %{})
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("ref", "can't be blank")
             |> has_validation_error("notification_type", "can't be blank")
             |> has_validation_error("params", "can't be blank")
    end

    test "renders errors when survey cannot be found", %{conn: conn, batch: batch} do
      assert conn
             |> post(Routes.api_batch_entry_path(conn, :create, batch, uuid()),
               entry: @create_attrs
             )
             |> assert_operation()
             |> json_response(404)
    end

    test "renders errors when batch cannot be found", %{conn: conn, survey: survey} do
      assert conn
             |> post(Routes.api_batch_entry_path(conn, :create, uuid(), survey),
               entry: @create_attrs
             )
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "show" do
    setup do
      {:ok, entry: fixture(:batch_entry, name: "a")}
    end

    test "returns a entry", %{conn: conn, entry: entry} do
      assert %{"data" => serialized(entry)} ==
               conn
               |> get(Routes.api_batch_entry_path(conn, :show, entry))
               |> assert_operation()
               |> json_response(200)
    end

    test "returns a not found error", %{conn: conn} do
      assert conn
             |> get(Routes.api_batch_entry_path(conn, :show, uuid()))
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "update" do
    @update_attrs %{
      ref: "some-updated_ref",
      notification_type: :mail,
      params: %{"hello" => "updated world"},
      language: "nl",
      email_address: "test@some-updated.local"
    }

    setup do
      {:ok, entry: fixture(:batch_entry)}
    end

    test "updates a entry", %{conn: conn, entry: entry} do
      assert %{"data" => data} =
               conn
               |> put(Routes.api_batch_entry_path(conn, :update, entry), entry: @update_attrs)
               |> assert_operation()
               |> json_response(200)

      {:ok, updated} = Batches.get_batch_entry(entry.id)
      assert serialized(updated) == data

      assert updated.ref == @update_attrs.ref
      assert updated.notification_type == @update_attrs.notification_type
      assert updated.params == @update_attrs.params
      assert updated.language == @update_attrs.language
      assert updated.email_address == @update_attrs.email_address
    end

    test "renders errors when data is invalid", %{conn: conn, entry: entry} do
      invalid = Map.put(@update_attrs, :ref, nil)

      assert conn
             |> put(Routes.api_batch_entry_path(conn, :update, entry), entry: invalid)
             |> assert_operation()
             |> json_response(422)
             |> has_validation_error("ref", "can't be blank")
    end

    test "renders error when entry is not found", %{conn: conn} do
      assert conn
             |> put(Routes.api_batch_entry_path(conn, :update, uuid()), entry: @update_attrs)
             |> assert_operation()
             |> json_response(404)
    end
  end

  describe "delete" do
    setup do
      {:ok, entry: fixture(:batch_entry)}
    end

    test "deletes chosen entry", %{conn: conn, entry: entry} do
      assert conn
             |> delete(Routes.api_batch_entry_path(conn, :delete, entry))
             |> assert_operation()
             |> response(204)

      assert {:error, %NotFoundError{}} = Batches.get_batch_entry(entry.id)
    end

    test "renders an error when entry cannot be found", %{conn: conn} do
      assert conn
             |> delete(Routes.api_batch_entry_path(conn, :delete, uuid()))
             |> assert_operation()
             |> response(404)
    end
  end

  describe "process" do
    setup do
      {:ok, entry: fixture(:batch_entry)}
    end

    test "processes the entry", %{conn: conn, entry: entry} do
      assert %{"data" => data} =
               conn
               |> put(Routes.api_batch_entry_path(conn, :process, entry))
               |> assert_operation()
               |> json_response(200)

      assert {:ok, processed} = Batches.get_batch_entry(entry.id)
      assert data == serialized(processed)
      assert [%{event: "mail_sent"}, %{event: "invite_created"} | _] = processed.log
    end

    test "renders an error when entry cannot be found", %{conn: conn} do
      assert conn
             |> put(Routes.api_batch_entry_path(conn, :process, uuid()))
             |> assert_operation()
             |> response(404)
    end
  end
end
