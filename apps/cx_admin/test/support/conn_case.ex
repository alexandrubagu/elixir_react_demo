defmodule CXAdmin.ConnCase do
  @moduledoc """
  This module defines the test case to be used by
  tests that require setting up a connection.

  Such tests rely on `Phoenix.ConnTest` and also
  import other functionality to make it easier
  to build common datastructures and query the data layer.

  Finally, if the test case interacts with the database,
  it cannot be async. For this reason, every test runs
  inside a transaction which is reset at the beginning
  of the test unless the test case is marked as async.
  """

  use ExUnit.CaseTemplate

  require Logger

  using do
    quote do
      # Import conveniences for testing with connections
      import Plug.Conn
      import Phoenix.ConnTest
      import CXAdmin.ConnCase
      import OASTestHelpers
      import ControllerTestHelpers, except: [uuid: 0]

      alias CXAdmin.Router.Helpers, as: Routes

      # The default endpoint for testing
      @endpoint CXAdmin.Endpoint
    end
  end

  setup _tags do
    {:ok, conn: Phoenix.ConnTest.build_conn()}
  end

  def assert_operation(conn, opts \\ [])

  def assert_operation(conn, ignore: true) do
    Logger.warn("Ignoring openapi schema assertions")
    conn
  end

  def assert_operation(%Plug.Conn{} = conn, _opts) do
    api_spec = CXAdmin.ApiSpec.spec()
    OASTestHelpers.assert_operation(api_spec, conn)
  end
end
