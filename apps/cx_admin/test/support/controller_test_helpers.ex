defmodule ControllerTestHelpers do
  @moduledoc """
  These functions are very useful during controller tests
  """
  require Logger
  import ExUnit.Assertions

  @doc """
  Places a %Core.Performer{} on the Plug.Conn so that it becomes
  an authenticated connnection.
  """
  def authorize_conn(conn, performer) do
    Plug.Conn.assign(conn, :performer, performer)
  end

  @doc """
  Generate a uuid
  """
  def uuid(), do: Ecto.UUID.generate()

  @doc """
  Checks if the response data contains error an error with the specified type

  # Example
      assert conn
             |> post(Routes.form_definition_path(conn, :create, form), definition: @invalid_attrs)
             |> json_response(422)
             |> has_error_type("invalid_variables")
  """
  def has_error_type(response, error_type) do
    case Map.get(response, "error") do
      %{"type" => ^error_type} ->
        response

      _ ->
        flunk("expected errors type to be #{error_type}, got: #{inspect(response)}")
    end
  end

  @doc """
  Checks if the response data contains an error message that matches the passed text.

  # Example
      assert conn
             |> post(Routes.form_definition_path(conn, :create, form), definition: @invalid_attrs)
             |> json_response(422)
             |> has_error_message("some substring")
  """
  def has_error_message(response, substring) do
    with %{"message" => message} <- Map.get(response, "error"),
         true <- message =~ substring do
      response
    else
      _ ->
        flunk("expected errors message to include `#{substring}`, got: #{inspect(response)}")
    end
  end

  @doc """
  Checks if the response data contains errors

  # Example
      assert conn
             |> post(Routes.form_definition_path(conn, :create, form), definition: @invalid_attrs)
             |> json_response(422)
             |> has_validation_errors()
  """
  def has_validation_errors(response) do
    case Map.get(response, "error") do
      %{"type" => "unprocessable"} ->
        response

      _ ->
        flunk("expected validation errors to be present, got: #{inspect(response)}")
    end
  end

  @doc """
  Checks if the response data contains errors on a specific field.
  This will only work when the changeset view is being used.

  # Example
      assert conn
             |> post(Routes.form_definition_path(conn, :create, form), definition: @invalid_attrs)
             |> json_response(422)
             |> has_validation_errors("content", "is invalid")
             |> has_validation_errors(["content", 0], "is invalid")

  """
  def has_validation_error(response, field_or_location, error) do
    location = List.wrap(field_or_location)

    with %{"info" => %{"errors" => errors}} <- Map.get(response, "error"),
         [_ | _] = on_location <- Enum.filter(errors, fn err -> err["location"] == location end),
         true <- Enum.any?(on_location, fn err -> err["message"] =~ error end) do
      response
    else
      _ ->
        flunk(
          "expected validation error \"#{error}\" on field #{inspect(field_or_location)}, got: #{
            inspect(response)
          }"
        )
    end
  end

  @doc """
  Ensures the response body has been paginated.
  To verify the specifics on pagination you can give it the expected
  pagination params:

  # Example
      query_param = [page: 2, page_size: 1, order_by: ["-inserted_at", "+name"]]

      assert conn
            |> get(Routes.some_path(conn, :index, query_param))
            |> json_response(200)
            |> paginated(page_number: 2, page_size: 1)
  """
  def paginated(response_body, opts \\ [])

  def paginated(%{"page_info" => page_info, "data" => data}, opts) when is_list(data) do
    opts
    |> Enum.map(fn {k, v} -> {Atom.to_string(k), v} end)
    |> Enum.each(fn
      {key, expected} ->
        case Map.get(page_info, key, :empty) do
          ^expected ->
            :ok

          :empty ->
            flunk(
              "Expected a paginated response but #{inspect(key)} " <>
                "was not found in #{inspect(page_info)}"
            )

          actual ->
            flunk("Wrong #{inspect(key)} returned, expected: `#{expected}, but got: `#{actual}`")
        end
    end)

    data
  end
end
