defmodule OASTestHelpers do
  @moduledoc false

  import ExUnit.Assertions

  alias OpenApiSpex.Cast.Error

  defstruct [:operation, :conn, :api_spec]

  def assert_operation(api_spec, conn) do
    %__MODULE__{conn: conn, api_spec: api_spec}
    |> determine_operation()
    |> verify_parameters()
    |> verify_response()
    |> return_conn()
  end

  defp determine_operation(%{conn: conn, api_spec: api_spec} = check) do
    matching =
      for {path, operations} <- api_spec.paths,
          operation = Map.fetch!(operations, atomize_http_method(conn.method)),
          path_regex = create_path_regex(path, operation.parameters),
          Regex.match?(path_regex, URI.decode(conn.request_path)) do
        {path, operation}
      end

    case matching do
      [] -> flunk("No match for #{conn.method}: #{URI.decode(conn.request_path)}")
      [{_, operation}] -> %{check | operation: operation}
      [_op | _ops] -> flunk("Too many routes match: #{inspect(Keyword.keys(matching))}")
    end
  end

  defp atomize_http_method("GET"), do: :get
  defp atomize_http_method("POST"), do: :post
  defp atomize_http_method("PUT"), do: :put
  defp atomize_http_method("PATCH"), do: :patch
  defp atomize_http_method("DELETE"), do: :delete

  defp create_path_regex(path, parameters) do
    path =
      for %{in: :path, name: name, schema: %{type: type, format: format}} <- parameters || [],
          reduce: path do
        path -> String.replace(path, "{#{name}}", regex(type, format))
      end

    ~r/^#{path}$/
  end

  defp regex(:integer, :int64), do: "(-?[1-9][0-9]*|0)"

  defp regex(:string, :uuid),
    do: "[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}"

  defp regex(:string, :irn), do: "irn:one:[a-z_]+::.*:[a-z_]+:.*"

  defp regex(:string, nil), do: "[\\w- ()_]+"

  defp verify_parameters(%{conn: conn} = check) do
    check
    |> verify_parameters(conn.path_params, in: :path)
    |> verify_parameters(conn.query_params, in: :query)
    |> verify_required_parameter_presence()
  end

  defp verify_parameters(%{operation: operation, api_spec: api_spec} = check, params, location) do
    for {name, value} <- params do
      operation.parameters
      |> get_matching_parameter(String.to_atom(name), location)
      |> validate_parameter_schema(value, api_spec)
    end

    check
  end

  defp get_matching_parameter(parameters, name, [{:in, location}]) do
    case Enum.filter(parameters, &match?(%{in: ^location, name: ^name}, &1)) do
      [] -> unknown_parameter(name, parameters, location)
      [%{schema: nil}] -> flunk("Parameter #{name} in #{location} is missing schema")
      [parameter] -> parameter
      [_ | _] -> flunk("Duplicate parameters #{name} in #{location}")
    end
  end

  defp unknown_parameter(name, parameters, location) do
    known = for %{in: ^location, name: name} <- parameters, do: name
    flunk("Unknown parameter `#{name}` in #{location}, only accepts: #{inspect(known)}")
  end

  defp validate_parameter_schema(%{schema: schema} = param, value, api_spec) do
    case OpenApiSpex.cast_value(value, schema, api_spec) do
      {:ok, value} ->
        {:ok, value}

      {:error, errors} ->
        errors = Enum.map(errors, &"#{Error.message(&1)} at #{Error.path_to_string(&1)}")
        flunk("Invalid parameter #{param.name} in #{param.in}: #{Enum.join(errors, "\n")}")
    end
  end

  defp verify_required_parameter_presence(%{operation: operation, conn: conn} = check) do
    operation.parameters
    |> Enum.filter(& &1.required)
    |> Enum.each(fn
      %{in: :path, name: name} -> ensure_param_present(conn.path_params, :path, name)
      %{in: :query, name: name} -> ensure_param_present(conn.query_params, :query, name)
    end)

    check
  end

  defp ensure_param_present(params, location, name) do
    unless Map.has_key?(params, to_string(name)) do
      flunk("Required parameter `#{name}` in #{location} missing in request")
    end
  end

  defp verify_response(check) do
    oas_response = get_oas_response(check)
    content_type = get_content_type(check)
    schema_ref = get_content_schema(oas_response, content_type)
    conn_response = decode_response_body(content_type, check.conn.resp_body)
    verify_content_schema(schema_ref, conn_response, check.api_spec)

    check
  end

  defp get_oas_response(%{operation: operation, conn: conn}) do
    case Map.get(operation.responses, conn.status) do
      nil -> flunk("OAS operation misses response for status code #{conn.status}")
      resp -> resp
    end
  end

  defp get_content_type(%{conn: conn}) do
    case :proplists.get_value("content-type", conn.resp_headers, :no_content) do
      :no_content -> guard_empty_body(conn)
      content_type -> content_type |> String.split(";") |> hd()
    end
  end

  defp guard_empty_body(%{resp_body: ""}), do: :no_content
  defp guard_empty_body(conn), do: flunk("Expected empty response body but got #{conn.resp_body}")

  defp get_content_schema(%{content: nil}, :no_content), do: nil

  defp get_content_schema(%{content: content}, :no_content),
    do: flunk("Expected #{inspect(content)} but got :no_content")

  defp get_content_schema(%{content: content}, content_type) do
    case Map.get(content, content_type) do
      %{schema: %{"$ref": ref}} -> String.replace(ref, "#/components/schemas/", "")
      nil -> flunk("Missing content_type #{content_type} in: #{inspect(Map.keys(content))}")
    end
  end

  defp decode_response_body("application/json", body), do: Jason.decode!(body)
  defp decode_response_body(_content_type, body), do: body

  defp verify_content_schema(nil, "", _api_spec), do: ""

  defp verify_content_schema(nil, response, _api_spec),
    do: flunk("Expected no content but got #{response}")

  defp verify_content_schema(schema_ref, response, api_spec) do
    OpenApiSpex.TestAssertions.assert_schema(response, schema_ref, api_spec)
  end

  defp return_conn(check), do: check.conn
end
