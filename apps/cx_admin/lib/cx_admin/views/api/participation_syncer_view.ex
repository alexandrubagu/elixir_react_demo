defmodule CXAdmin.API.ParticipationSyncerView do
  @moduledoc false

  use CXAdmin, :view

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "participation_syncer.json")
  end

  def render("show.json", %{participation_syncer: participation_syncer}) do
    %{data: render_one(participation_syncer, __MODULE__, "participation_syncer.json")}
  end

  def render("participation_syncer.json", %{participation_syncer: participation_syncer}) do
    %{
      id: participation_syncer.id,
      name: participation_syncer.name,
      enabled: participation_syncer.enabled,
      settings:
        render_one(participation_syncer.settings, __MODULE__, "settings.json", as: :settings),
      state: render_one(participation_syncer.state, __MODULE__, "state.json", as: :state),
      inserted_at: participation_syncer.inserted_at,
      updated_at: participation_syncer.updated_at
    }
  end

  def render("settings.json", %{settings: settings}) do
    %{
      id: settings.id,
      url: settings.url,
      interval_sec: settings.interval_sec,
      invites_after: settings.invites_after
    }
  end

  def render("state.json", %{state: state}) do
    %{
      id: state.id,
      last_run: state.last_run,
      participations_after: state.participations_after
    }
  end
end
