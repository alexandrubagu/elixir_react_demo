defmodule CXAdmin.API.DemoView do
  @moduledoc false

  use CXAdmin, :view

  def render("index.json", %{verbatim_categorizations: verbatim_categorizations}) do
    %{
      data:
        render_many(verbatim_categorizations, __MODULE__, "verbatim_categorization.json",
          as: :verbatim_categorization
        )
    }
  end

  def render("verbatim_categorization.json", %{verbatim_categorization: categorization}) do
    %{
      id: categorization.id,
      verbatim_id: categorization.verbatim_id,
      participation_id: categorization.participation_id,
      text_en: categorization.text_en,
      topic_probabilities:
        render_many(categorization.topic_probabilities, __MODULE__, "topic_probability.json",
          as: :topic_probability
        ),
      inserted_at: categorization.inserted_at,
      updated_at: categorization.updated_at
    }
    |> maybe_render(
      :topic_selections,
      categorization,
      &render_many(&1, __MODULE__, "topic_selection.json", as: :topic_selection)
    )
  end

  def render("topic_probability.json", %{topic_probability: topic_probability}) do
    %{
      topic: topic_probability.topic,
      probability: topic_probability.probability
    }
  end

  def render("topic_selection.json", %{topic_selection: topic_selection}) do
    topic_selection.topic
  end

  def render("dlp_finding.json", %{dlp_finding: finding}) do
    %{
      id: finding.id,
      verbatim_id: finding.verbatim_id,
      participation_id: finding.participation_id,
      text_dlp: finding.text_dlp,
      probabilities:
        render_many(finding.probabilities, __MODULE__, "finding_probability.json",
          as: :finding_probability
        )
    }
  end

  def render("finding_probability.json", %{finding_probability: probability}) do
    %{
      id: probability.id,
      info_type: probability.info_type,
      likelihood: probability.likelihood,
      quote: probability.quote
    }
  end
end
