defmodule CXAdmin.API.InviteView do
  @moduledoc false

  use CXAdmin, :view

  alias CXAdmin.API.Notifications.Emails
  alias CXAdmin.API.Notifications.SMS

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "invite.json")
  end

  def render("show.json", %{invite: invite}) do
    %{data: render_one(invite, __MODULE__, "invite.json")}
  end

  def render("invite.json", %{invite: invite}) do
    %{
      id: invite.id,
      notification_type: invite.notification_type,
      batch_id: invite.batch_id,
      ref: invite.ref,
      remind_at: invite.remind_at,
      language: invite.language,
      params: invite.params,
      expire_at: invite.expire_at,
      reminder_triggered: invite.reminder_triggered,
      from_email_name: invite.from_email_name,
      from_email_address: invite.from_email_address,
      email_address: invite.email_address,
      tpl_invite: invite.tpl_invite,
      tpl_reminder: invite.tpl_reminder,
      tpl_thanks: invite.tpl_thanks,
      tpl_abandonned: invite.tpl_abandonned,
      sms_invite_tpl: invite.sms_invite_tpl,
      sms_dest_number: invite.sms_dest_number,
      sms_from_number: invite.sms_from_number,
      survey_id: invite.survey_id,
      participation_id: invite.participation_id,
      inserted_at: invite.inserted_at,
      updated_at: invite.updated_at,
      webhook_id: invite.webhook_id,
      webhook_params: invite.webhook_params,
      webhook_url: invite.webhook_url
    }
    |> maybe_render(
      :events,
      invite,
      &render_many(&1, __MODULE__, "invite_event.json", as: :event)
    )
    |> maybe_render(
      :email_messages,
      invite,
      &render_many(&1, Emails.MessageView, "message.json", as: :message)
    )
    |> maybe_render(
      :smses,
      invite,
      &render_many(&1, SMS.MessageView, "message.json", as: :message)
    )
  end

  def render("invite_event.json", %{event: event}) do
    %{
      id: event.id,
      invite_id: event.invite_id,
      type: event.type,
      browser_info: render_one(event.browser_info, __MODULE__, "browser_info.json", as: :info),
      inserted_at: event.inserted_at,
      updated_at: event.updated_at
    }
  end

  def render("browser_info.json", %{info: info}) do
    %{
      device: info.device,
      os: info.os,
      os_version: info.os_version,
      screen_width: info.screen_width,
      screen_height: info.screen_height,
      browser: info.browser,
      browser_version: info.browser_version,
      browser_useragent: info.browser_useragent,
      browser_width: info.browser_width,
      browser_height: info.browser_height
    }
  end
end
