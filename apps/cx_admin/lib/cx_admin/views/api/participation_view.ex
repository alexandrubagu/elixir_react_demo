defmodule CXAdmin.API.ParticipationView do
  @moduledoc false

  use CXAdmin, :view

  alias CXAdmin.API.SurveyView
  alias CXAdmin.API.DemoView

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "participation.json")
  end

  def render("show.json", %{participation: participation}) do
    %{data: render_one(participation, __MODULE__, "participation.json")}
  end

  def render("participation.json", %{participation: participation}) do
    %{
      id: participation.id,
      status: participation.status,
      ref: participation.ref,
      content: participation.content,
      language: participation.language,
      params: participation.params,
      answers: participation.answers,
      verbatims_processed: participation.verbatims_processed,
      survey_id: participation.survey_id,
      inserted_at: participation.inserted_at,
      updated_at: participation.updated_at
    }
    |> maybe_render_survey(participation)
    |> maybe_render_events(participation)
    |> maybe_render_verbatims(participation)
    |> maybe_render_verbatim_logs(participation)
  end

  def render("particiption_event.json", %{event: event}) do
    %{
      id: event.id,
      data: event.data,
      type: event.type,
      participation_id: event.participation_id,
      inserted_at: event.inserted_at,
      updated_at: event.updated_at
    }
  end

  def render("verbatim.json", %{verbatim: verbatim}) do
    %{
      id: verbatim.id,
      question: verbatim.question,
      text: verbatim.text,
      text_lang: verbatim.text_lang,
      text_en: verbatim.text_en,
      sentiment_magnt: verbatim.sentiment_magnt,
      sentiment_score: verbatim.sentiment_score,
      survey_id: verbatim.survey_id,
      participation_id: verbatim.participation_id,
      sentences: render_many(verbatim.sentences, __MODULE__, "sentence.json", as: :sentence),
      inserted_at: verbatim.inserted_at,
      updated_at: verbatim.updated_at
    }
    |> maybe_render_verbatim_categorization(verbatim)
    |> maybe_render_dlp_finding(verbatim)
  end

  def render("sentence.json", %{sentence: sentence}) do
    %{
      id: sentence.id,
      text: sentence.text,
      offset: sentence.offset,
      sentiment_magnt: sentence.sentiment_magnt,
      sentiment_score: sentence.sentiment_score,
      verbatim_id: sentence.verbatim_id,
      inserted_at: sentence.inserted_at,
      updated_at: sentence.updated_at
    }
  end

  def render("verbatim_log.json", %{log: log}) do
    %{
      id: log.id,
      question: log.question,
      event: log.event,
      lvl: log.lvl,
      message: log.message,
      participation_id: log.participation_id,
      inserted_at: log.inserted_at,
      updated_at: log.updated_at
    }
  end

  def render("sessions.json", %{sessions: sessions}) do
    %{data: render_many(sessions, __MODULE__, "session.json", as: :session)}
  end

  def render("session.json", %{session: {pid, id}}) do
    %{
      participation_id: id,
      pid: inspect(pid)
    }
  end

  defp maybe_render_survey(data, participation) do
    maybe_render(
      data,
      :survey,
      participation,
      &render_one(&1, SurveyView, "survey.json", as: :survey)
    )
  end

  defp maybe_render_events(data, participation) do
    maybe_render(
      data,
      :events,
      participation,
      &render_many(&1, __MODULE__, "particiption_event.json", as: :event)
    )
  end

  defp maybe_render_verbatims(data, participation) do
    maybe_render(
      data,
      :verbatims,
      participation,
      &render_many(&1, __MODULE__, "verbatim.json", as: :verbatim)
    )
  end

  defp maybe_render_verbatim_logs(data, participation) do
    maybe_render(
      data,
      :verbatim_logs,
      participation,
      &render_many(&1, __MODULE__, "verbatim_log.json", as: :log)
    )
  end

  defp maybe_render_verbatim_categorization(data, verbatim) do
    maybe_render(
      data,
      :verbatim_categorization,
      verbatim,
      &render_one(&1, DemoView, "verbatim_categorization.json", as: :verbatim_categorization)
    )
  end

  defp maybe_render_dlp_finding(data, verbatim) do
    maybe_render(
      data,
      :dlp_finding,
      verbatim,
      &render_one(&1, DemoView, "dlp_finding.json", as: :dlp_finding)
    )
  end
end
