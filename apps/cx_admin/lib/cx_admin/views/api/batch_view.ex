defmodule CXAdmin.API.BatchView do
  @moduledoc false

  use CXAdmin, :view

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "batch.json")
  end

  def render("show.json", %{batch: batch}) do
    %{data: render_one(batch, __MODULE__, "batch.json")}
  end

  def render("batch.json", %{batch: batch}) do
    %{
      id: batch.id,
      name: batch.name,
      log: render_many(batch.log, __MODULE__, "batch_log.json", as: :log),
      inserted_at: batch.inserted_at,
      updated_at: batch.updated_at
    }
  end

  def render("batch_log.json", %{log: log}) do
    %{
      id: log.id,
      event: log.event,
      lvl: log.lvl,
      message: log.message,
      inserted_at: log.inserted_at,
      updated_at: log.updated_at
    }
  end

  def render("json_batch.json", %{count: count}), do: %{data: %{count: count}}
end
