defmodule CXAdmin.API.SurveyView do
  @moduledoc false

  use CXAdmin, :view

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "survey.json")
  end

  def render("show.json", %{survey: survey}) do
    %{data: render_one(survey, __MODULE__, "survey.json")}
  end

  def render("survey.json", %{survey: survey}) do
    %{
      id: survey.id,
      notification_types: survey.notification_types,
      name: survey.name,
      description: survey.description,
      content: survey.content,
      expire_after: survey.expire_after,
      abandonned_after: survey.abandonned_after,
      verbatim_questions: survey.verbatim_questions,
      remind_after: survey.remind_after,
      from_email_name: survey.from_email_name,
      from_email_address: survey.from_email_address,
      tpl_invite: survey.tpl_invite,
      tpl_reminder: survey.tpl_reminder,
      tpl_thanks: survey.tpl_thanks,
      tpl_abandonned: survey.tpl_abandonned,
      sms_invite_tpl: survey.sms_invite_tpl,
      sms_from_number: survey.sms_from_number,
      deleted: survey.deleted,
      inserted_at: survey.inserted_at,
      updated_at: survey.updated_at
    }
  end
end
