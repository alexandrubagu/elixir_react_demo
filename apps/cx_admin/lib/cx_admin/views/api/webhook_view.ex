defmodule CXAdmin.API.WebhookView do
  @moduledoc false

  use CXAdmin, :view

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "webhook.json")
  end

  def render("show.json", %{webhook: webhook}) do
    %{data: render_one(webhook, __MODULE__, "webhook.json")}
  end

  def render("webhook.json", %{webhook: webhook}) do
    %{
      id: webhook.id,
      url: webhook.url,
      data: webhook.data,
      event: webhook.event,
      status: webhook.status,
      attempts: webhook.attempts,
      retry_at: webhook.retry_at,
      delivered_at: webhook.delivered_at,
      failed_at: webhook.failed_at,
      log: render_many(webhook.log, __MODULE__, "webhook_log_entry.json", as: :log),
      inserted_at: webhook.inserted_at,
      updated_at: webhook.updated_at
    }
  end

  def render("webhook_log_entry.json", %{log: log}) do
    %{
      id: log.id,
      type: log.type,
      info: log.info,
      inserted_at: log.inserted_at
    }
  end
end
