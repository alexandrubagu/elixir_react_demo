defmodule CXAdmin.API.BatchEntryView do
  @moduledoc false

  use CXAdmin, :view

  alias CXAdmin.API.BatchView

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "batch_entry.json")
  end

  def render("show.json", %{entry: entry}) do
    %{data: render_one(entry, __MODULE__, "batch_entry.json")}
  end

  def render("batch_entry.json", %{batch_entry: entry}) do
    %{
      id: entry.id,
      notification_type: entry.notification_type,
      status: entry.status,
      params: entry.params,
      ref: entry.ref,
      language: entry.language,
      email_address: entry.email_address,
      sms_number: entry.sms_number,
      invite_id: entry.invite_id,
      mail_id: entry.mail_id,
      sms_id: entry.sms_id,
      batch_id: entry.batch_id,
      survey_id: entry.survey_id,
      log: render_many(entry.log, BatchView, "batch_log.json", as: :log),
      inserted_at: entry.inserted_at,
      updated_at: entry.updated_at
    }
  end
end
