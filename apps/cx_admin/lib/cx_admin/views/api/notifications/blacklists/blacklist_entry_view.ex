defmodule CXAdmin.API.Notifications.Blacklists.BlacklistEntryView do
  @moduledoc false

  use CXAdmin, :view

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "blacklist_entry.json", as: :blacklist_entry)
  end

  def render("show.json", %{blacklist_entry: blacklist_entry}) do
    %{data: render_one(blacklist_entry, __MODULE__, "blacklist_entry.json")}
  end

  def render("blacklist_entry.json", %{blacklist_entry: blacklist_entry}) do
    %{
      id: blacklist_entry.id,
      notification_type: blacklist_entry.notification_type,
      key: blacklist_entry.key,
      reason: blacklist_entry.reason,
      webhook_event_id: blacklist_entry.webhook_event_id,
      inserted_at: blacklist_entry.inserted_at,
      updated_at: blacklist_entry.updated_at
    }
  end
end
