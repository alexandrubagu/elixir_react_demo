defmodule CXAdmin.API.Notifications.SMS.MessageView do
  @moduledoc false

  use CXAdmin, :view

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "message.json", as: :message)
  end

  def render("show.json", %{message: message}) do
    %{data: render_one(message, __MODULE__, "message.json")}
  end

  def render("message.json", %{message: message}) do
    %{
      id: message.id,
      statuses: message.statuses,
      invite_id: message.invite_id,
      inserted_at: message.inserted_at,
      updated_at: message.updated_at
    }
    |> maybe_render(:events, message, &render_many(&1, __MODULE__, "event.json", as: :event))
  end

  def render("event.json", %{event: event}) do
    %{
      id: event.id,
      type: event.type,
      data: event.data,
      sms_id: event.sms_id,
      inserted_at: event.inserted_at,
      updated_at: event.updated_at
    }
  end
end
