defmodule CXAdmin.API.Notifications.Emails.WebhookView do
  @moduledoc false

  use CXAdmin, :view

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "webhook_event.json", as: :event)
  end

  def render("show.json", %{event: event}) do
    %{data: render_one(event, __MODULE__, "webhook_event.json", as: :event)}
  end

  def render("webhook_event.json", %{event: event}) do
    %{
      id: event.id,
      data: event.data,
      status: event.status,
      auto_processing_error: event.auto_processing_error,
      search: event.search,
      inserted_at: event.inserted_at,
      updated_at: event.updated_at
    }
  end
end
