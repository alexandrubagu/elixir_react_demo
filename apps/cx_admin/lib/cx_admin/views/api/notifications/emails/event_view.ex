defmodule CXAdmin.API.Notifications.Emails.EventView do
  @moduledoc false

  use CXAdmin, :view

  def render("event.json", %{event: event}) do
    %{
      id: event.id,
      message_id: event.message_id,
      type: event.type,
      drop_reason: event.drop_reason,
      info: event.info,
      inserted_at: event.inserted_at,
      updated_at: event.updated_at
    }
  end
end
