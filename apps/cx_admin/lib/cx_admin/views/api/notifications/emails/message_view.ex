defmodule CXAdmin.API.Notifications.Emails.MessageView do
  @moduledoc false

  use CXAdmin, :view

  alias CXAdmin.API.Notifications.Emails.EventView

  def render("index.json", %{page: page}) do
    render_paginated(page, __MODULE__, "message.json", as: :message)
  end

  def render("show.json", %{message: message}) do
    %{data: render_one(message, __MODULE__, "message.json")}
  end

  def render("message.json", %{message: message}) do
    %{
      id: message.id,
      invite_id: message.invite_id,
      from_name: message.from_name,
      from_address: message.from_address,
      to_address: message.to_address,
      type: message.type,
      delivery_status: message.delivery_status,
      delivery_drop_reason: message.delivery_drop_reason,
      was_opened: message.was_opened,
      was_unsubscribed: message.was_unsubscribed,
      was_reported_as_spam: message.was_reported_as_spam,
      template_id: message.template_id,
      template_params: message.template_params,
      inserted_at: message.inserted_at,
      updated_at: message.updated_at
    }
    |> maybe_render(:events, message, &render_many(&1, EventView, "event.json"))
  end
end
