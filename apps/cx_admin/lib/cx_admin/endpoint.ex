defmodule CXAdmin.Endpoint do
  use Sentry.PlugCapture
  use Phoenix.Endpoint, otp_app: :cx_admin

  @session_options [
    store: :cookie,
    key: "_cx_admin_key",
    signing_salt: "ldBBMQqc"
  ]

  socket "/cx/admin/socket", CXAdmin.UserSocket,
    websocket: true,
    longpoll: false

  socket "/cx/admin/live", Phoenix.LiveView.Socket,
    websocket: [connect_info: [session: @session_options]]

  plug CXAdmin.Plugs.Health

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phoenix.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/cx/admin",
    from: :cx_admin,
    gzip: false,
    only: ~w(css fonts images js favicon.ico robots.txt)

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
  end

  plug Plug.RequestId
  plug Plug.Logger

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    length: 100_000_000,
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Sentry.PlugContext
  plug Plug.MethodOverride
  plug Plug.Head
  plug Plug.Session, @session_options

  plug CXAdmin.Router

  @doc """
  Callback invoked for dynamically configuring the endpoint.

  It receives the endpoint configuration and checks if
  configuration should be loaded from the system environment.
  """
  def init(_key, config) do
    %URI{host: host} = CX.Config.get_base_url() |> URI.parse()
    {:ok, put_in(config, [:url, :host], host)}
  end
end
