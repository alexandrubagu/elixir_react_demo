defmodule CXAdmin.APIUtils.ErrorView.NotFound do
  @moduledoc false

  alias CX.Errors.NotFoundError

  def error(%NotFoundError{} = error) do
    go_error(
      Exception.message(error),
      Map.take(error, [:context, :resource])
    )
  end

  def error(_) do
    go_error("404 Not found", %{})
  end

  defp go_error(message, info) do
    %{
      error: %{
        type: "not_found",
        message: message,
        info: info
      }
    }
  end
end
