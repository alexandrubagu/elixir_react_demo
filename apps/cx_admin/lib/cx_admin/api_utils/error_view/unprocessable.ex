defmodule CXAdmin.APIUtils.ErrorView.Unprocessable do
  @moduledoc false

  def error(error), do: CXAdmin.APIUtils.ErrorView.Unprocessable.Changeset.error(error)
end
