defmodule CXAdmin.APIUtils.ErrorView.Conflict do
  @moduledoc false

  use CXAdmin.APIUtils.Util

  def error(error) do
    %{
      error: %{
        type: "conflict",
        message: Exception.message(error),
        info: format_info(error)
      }
    }
  end

  defp format_info(error) do
    info = Map.take(error, [:context, :resource, :reason])
    Map.put(info, :details, to_serializable_map(error.details))
  end
end
