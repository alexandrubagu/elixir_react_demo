defmodule CXAdmin.APIUtils.ErrorView.Unprocessable.Changeset do
  @moduledoc false

  use CXAdmin.APIUtils.Util

  require Logger

  def error(changeset) do
    errors =
      Ecto.Changeset.traverse_errors(changeset, fn {_message, opts} = error ->
        {:error,
         %{
           validation: validation_name(opts),
           message: translate_error(error),
           info: to_serializable_map(opts)
         }}
      end)

    %{
      error: %{
        type: :unprocessable,
        message: "Validation failed",
        info: %{
          errors: flatten_errors(errors)
        }
      }
    }
  end

  defp flatten_errors(errors) do
    errors
    |> add_error_location([], [])
    |> List.flatten()
  end

  defp add_error_location(errors, acc, pos)

  defp add_error_location(error_map, acc, pos) when is_map(error_map) do
    for {name, errors_list} <- error_map do
      add_error_location(errors_list, acc, [name | pos])
    end
  end

  defp add_error_location([{:error, _error} | _] = field_errors, acc, pos) do
    for {:error, error} <- field_errors, reduce: acc do
      acc -> [Map.put(error, :location, Enum.reverse(pos)) | acc]
    end
  end

  defp add_error_location(error_list, acc, pos) when is_list(error_list) do
    for {error, list_pos} <- Enum.with_index(error_list) do
      add_error_location(error, acc, [list_pos | pos])
    end
  end

  defp add_error_location(error, acc, _pos) do
    Logger.error(fn ->
      "could not serialize #{inspect(error)} in changeset(view)"
    end)

    acc
  end

  defp translate_error({message, opts}) do
    Regex.replace(~r"%{(\w+)}", message, fn _, key ->
      case Keyword.get(opts, String.to_existing_atom(key), key) do
        x when is_binary(x) -> x
        x when is_atom(x) -> to_string(x)
        x -> inspect(x)
      end
    end)
  end

  defp validation_name(opts) do
    case Keyword.has_key?(opts, :constraint) do
      true -> :constraint
      _ -> opts[:validation]
    end
  end
end
