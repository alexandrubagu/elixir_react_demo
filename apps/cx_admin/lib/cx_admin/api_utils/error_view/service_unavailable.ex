defmodule CXAdmin.APIUtils.ErrorView.ServiceUnavailable do
  @moduledoc false

  use CXAdmin.APIUtils.Util

  def error(error) do
    %{
      error: %{
        type: "service_unavailable",
        message: Exception.message(error),
        info: format_info(error)
      }
    }
  end

  defp format_info(error) do
    info = Map.take(error, [:context, :resource, :reason])
    Map.put(info, :details, to_serializable_map(error.details))
  end
end
