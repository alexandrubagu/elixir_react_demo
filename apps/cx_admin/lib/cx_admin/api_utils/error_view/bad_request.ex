defmodule CXAdmin.APIUtils.ErrorView.BadRequest do
  @moduledoc false

  def error(%{reason: reason}) when is_atom(reason), do: go_error(reason)
  def error(%{reason: reason}) when is_binary(reason), do: go_error(reason)
  def error(_), do: go_error("bad request")

  defp go_error(reason) do
    %{
      error: %{
        type: "bad_request",
        message: "400 Bad Request",
        info: %{reason: reason}
      }
    }
  end
end
