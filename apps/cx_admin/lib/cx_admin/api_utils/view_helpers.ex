defmodule CXAdmin.APIUtils.ViewHelpers do
  @moduledoc """
  Common views.
  """

  @type paginated_data(x) :: %{
          required(:page_number) => non_neg_integer,
          required(:page_size) => non_neg_integer,
          required(:total_entries) => non_neg_integer,
          required(:total_pages) => non_neg_integer,
          required(:entries) => [x]
        }

  @type pagination_info :: %{
          required(:page_number) => non_neg_integer,
          required(:page_size) => non_neg_integer,
          required(:total_entries) => non_neg_integer,
          required(:total_pages) => non_neg_integer
        }

  @type pagination_view :: %{
          required(:data) => [term],
          required(:page_info) => pagination_info
        }

  @spec render_paginated(paginated_data(term), module, binary, Keyword.t()) :: pagination_view
  def render_paginated(paginated_data, view_module, view_name, opts \\ []) do
    %{
      data: Phoenix.View.render_many(paginated_data.entries, view_module, view_name, opts),
      page_info: render_page_info(paginated_data)
    }
  end

  @spec render_page_info(paginated_data(term)) :: pagination_info
  def render_page_info(paginated_data) do
    Map.take(paginated_data, [:page_number, :page_size, :total_entries, :total_pages])
  end

  @spec maybe_render(map(), atom(), map(), (map() -> map())) :: map()
  def maybe_render(data, key, ctx, fun) do
    case Map.get(ctx, key) do
      %Ecto.Association.NotLoaded{} -> data
      x -> Map.put(data, key, fun.(x))
    end
  end
end
