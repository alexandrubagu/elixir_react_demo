defmodule CXAdmin.APIUtils.ErrorView do
  @moduledoc """
  Core error view
  """
  alias __MODULE__

  def render("400.json", error), do: ErrorView.BadRequest.error(error)
  def render("500.json", error), do: ErrorView.InternalServer.error(error)
  def render("401.json", error), do: ErrorView.Unauthorized.error(error)
  def render("403.json", error), do: ErrorView.Forbidden.error(error)
  def render("404.json", error), do: ErrorView.NotFound.error(error)
  def render("409.json", error), do: ErrorView.Conflict.error(error)
  def render("422.json", error), do: ErrorView.Unprocessable.error(error)
  def render("503.json", error), do: ErrorView.ServiceUnavailable.error(error)
end
