defmodule CXAdmin.APIUtils.OAS.CommonSchemas do
  @moduledoc """
  Common schemas used in the api, such as errors and pagination
  """
  use CXAdmin.APIUtils.OAS.Utils

  defmodule PaginationInfo do
    @moduledoc false
    schema(
      object(
        title: "PaginationInfo",
        properties: %{
          page_number: int64(description: "The current page", example: 1),
          page_size: int64(description: "The current page size", example: 20),
          total_entries: int64(description: "The total number of entries", example: 2),
          total_pages: int64(description: "The total number of pages", example: 1)
        }
      )
    )
  end

  defmodule NotFoundErrorResponse do
    @moduledoc false
    schema(
      object(
        title: "NotFoundErrorResponse",
        properties: %{
          error:
            object(
              title: "NotFoundError",
              properties: %{
                type: const("not_found"),
                message: string(description: "Non-translated error message"),
                info:
                  object(
                    title: "NotFoundErrorInfo",
                    properties: %{
                      context: string(description: "Context of the resource that was not found"),
                      resource: string(description: "The resource type that was not found")
                    }
                  )
              }
            )
        }
      )
    )
  end

  defmodule ForbiddenErrorResponse do
    @moduledoc false
    schema(
      object(
        title: "ForbiddenErrorResponse",
        properties: %{
          error:
            object(
              title: "ForbiddenError",
              properties: %{
                type: const("forbidden"),
                message: string(description: "Non-translated error message")
              }
            )
        }
      )
    )
  end

  defmodule UnauthorizedErrorResponse do
    @moduledoc false
    schema(
      object(
        title: "UnauthorizedErrorResponse",
        properties: %{
          error:
            object(
              title: "UnauthorizedError",
              properties: %{
                type: const("unauthorized"),
                message: string(description: "Non-translated error message")
              }
            )
        }
      )
    )
  end

  defmodule ConflictErrorResponse do
    @moduledoc false
    schema(
      object(
        title: "ConflictErrorResponse",
        properties: %{
          error:
            object(
              title: "ConflictError",
              properties: %{
                type: const("conflict"),
                message: string(description: "Non-translated error message"),
                info:
                  object(
                    title: "ConflictErrorInfo",
                    properties: %{
                      context: string(description: "Context of the resource involved"),
                      resource: string(description: "The resource type involved"),
                      reason: string(description: "The reason why the operation is conflicting"),
                      details:
                        object(
                          nullable: true,
                          title: "ConflictErrorDetails",
                          properties: %{},
                          additionalProperties: true
                        )
                    }
                  )
              }
            )
        }
      )
    )
  end

  defmodule FieldValidationError do
    @moduledoc false
    schema(
      object(
        title: "FieldValidationError",
        properties: %{
          validation: string(description: "Validation name"),
          message: string(description: "Non-translated error message"),
          location:
            array(
              items: one_of([string(), int64()]),
              description: "field location of the error"
            ),
          info:
            object(
              title: "FieldValidationErrorInfo",
              description: "Validation specific data, useful for translations",
              properties: %{},
              additionalProperties: true
            )
        }
      )
    )
  end

  defmodule UnprocessableErrorResponse do
    @moduledoc false
    schema(
      object(
        title: "UnprocessableErrorResponse",
        properties: %{
          error:
            object(
              title: "UnprocessableError",
              properties: %{
                type: const("unprocessable"),
                message: string(description: "Non-translated error message"),
                info:
                  object(
                    title: "UnprocessableErrorInfo",
                    properties: %{
                      errors: array(items: FieldValidationError)
                    },
                    additionalProperties: true
                  )
              }
            )
        }
      )
    )
  end

  defmodule ServiceUnavailableErrorResponse do
    @moduledoc false
    schema(
      object(
        title: "ServiceUnavailableErrorResponse",
        properties: %{
          error:
            object(
              title: "ServiceUnavailableError",
              properties: %{
                type: const("service_unavailable"),
                message: string(description: "Non-translated error message"),
                info:
                  object(
                    title: "ServiceUnavailableErrorInfo",
                    properties: %{
                      context: string(description: "Context of the resource involved"),
                      resource: string(description: "The resource type involved"),
                      reason: string(description: "The reason why the operation is conflicting"),
                      details:
                        object(
                          title: "ServiceUnavailableErrorDetails",
                          description: "Additional details on the error",
                          properties: %{},
                          additionalProperties: true
                        )
                    }
                  )
              }
            )
        }
      )
    )
  end
end
