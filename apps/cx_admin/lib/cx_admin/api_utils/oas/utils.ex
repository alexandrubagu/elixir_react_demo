defmodule CXAdmin.APIUtils.OAS.Utils do
  @moduledoc """
  Utilities to make it easier to define open api specs
  """

  alias OpenApiSpex.{Schema, Operation, Parameter, Reference}

  alias CXAdmin.APIUtils.OAS.CommonSchemas, as: Schemas

  defmacro __using__(_) do
    quote do
      import CXAdmin.APIUtils.OAS.Utils
      require OpenApiSpex
      import OpenApiSpex, only: [schema: 1]

      alias OpenApiSpex.{Schema, Operation, Parameter}
    end
  end

  #
  # Data Types
  # ----------------------------------------------------------------------------

  # Number helpers
  def int32(opts \\ []), do: merge(opts, format: :int32) |> integer()
  def int64(opts \\ []), do: merge(opts, format: :int64) |> integer()
  defp integer(opts), do: struct!(Schema, merge(opts, type: :integer))

  def float(opts \\ []), do: merge(opts, format: :float) |> number()
  def double(opts \\ []), do: merge(opts, format: :double) |> integer()
  def number(opts \\ []), do: struct!(Schema, merge(opts, type: :number))

  # String helpers
  def string(opts \\ []), do: struct!(Schema, merge(opts, type: :string))
  def uuid(opts \\ []), do: merge(opts, format: :uuid) |> string()
  def url(opts \\ []), do: merge(opts, format: :url) |> string()
  def byte(opts \\ []), do: merge(opts, format: "byte") |> string()
  def binary(opts \\ []), do: merge(opts, format: "binary") |> string()
  def password(opts \\ []), do: merge(opts, format: "password") |> string()
  def date(opts \\ []), do: merge(opts, format: "date") |> string()
  def date_time(opts \\ []), do: merge(opts, format: "date-time") |> string()
  def const(x) when is_binary(x), do: string(enum: [x])
  def irn(), do: string(format: :irn, description: "Intersoft Resource Notation")

  # Boolean helpers
  def boolean(opts \\ []), do: struct!(Schema, merge(opts, type: :boolean))

  # Array helper
  def array(opts \\ []), do: struct!(Schema, merge(opts, type: :array))

  # Object helper
  def object(opts) do
    properties = Keyword.get(opts, :properties, %{})

    with_required =
      opts
      |> Keyword.put_new_lazy(:required, fn -> Enum.map(properties, &elem(&1, 0)) end)
      |> merge(type: :object)

    struct!(Schema, with_required)
  end

  # Combinator helpers
  def one_of(items, opts \\ []) do
    struct!(Schema, merge(opts, oneOf: items))
  end

  def all_of(items, opts \\ []) do
    struct!(Schema, merge(opts, allOf: items))
  end

  def any_of(items, opts \\ []) do
    struct!(Schema, merge(opts, anyOf: items))
  end

  # boolean flags helpers (see https://github.com/OAI/OpenAPI-Specification/issues/1368#issuecomment-354037150)
  def nullable(schema_ref, opts \\ []) do
    all_of([schema_ref], merge(opts, nullable: true))
  end

  # Reference helper (only use this for recursive data structures and manually these schemas to the spec)
  def ref(schema_ref) when is_atom(schema_ref) do
    case schema_ref.schema().title do
      nil -> raise "Make sure the referenced schema has a title: #{inspect(schema_ref)}"
      title -> ref(title)
    end
  end

  def ref(schema_title) when is_binary(schema_title) do
    %Reference{"$ref": "#/components/schemas/#{schema_title}"}
  end

  def placeholder(name) do
    string(
      description:
        "This concept (#{name}) has not been implemented yet, this type serves as a placeholder"
    )
  end

  #
  # Schema helpers
  # ----------------------------------------------------------------------------
  def single_response(schema_ref) do
    title =
      case schema_ref.schema().title do
        nil -> raise "make sure the schema ref has a title"
        title -> "#{title}Response"
      end

    object(
      title: title,
      properties: %{data: schema_ref}
    )
  end

  def list_response(schema_ref) do
    title =
      case schema_ref.schema().title do
        nil -> raise "make sure the schema ref has a title"
        title -> "#{title}ListResponse"
      end

    object(
      title: title,
      properties: %{data: array(items: schema_ref)}
    )
  end

  def paginated_response(schema_ref) do
    title =
      case schema_ref.schema().title do
        nil -> raise "make sure the schema ref has a title"
        title -> "#{title}PaginatedResponse"
      end

    object(
      title: title,
      properties: %{
        data: array(items: schema_ref),
        page_info: Schemas.PaginationInfo
      }
    )
  end

  defmacro defenum(ecto_module, opts) do
    caller = __CALLER__.module
    title = Keyword.fetch!(opts, :title)
    nullable = Keyword.get(opts, :nullable, false)

    quote bind_quoted: [
            caller: caller,
            title: title,
            nullable: nullable,
            ecto_module: ecto_module
          ] do
      defmodule Module.concat(caller, title) do
        import CXAdmin.APIUtils.OAS.Utils
        @values ecto_module.__valid_values__() |> Enum.filter(&is_binary/1)
        schema(string(title: title, nullable: nullable, enum: @values))
      end
    end
  end

  #
  # Operation helpers
  # ----------------------------------------------------------------------------
  def operation(opts) do
    struct!(Operation, opts)
  end

  @doc """
  Builds the operation id based on the first tag combined with the
  resource and the action
  """
  def operation_id(tags, resource, action) do
    [prefix | _] = tags
    Enum.join([prefix, resource_name(resource), action_name(action)], "-")
  end

  defp resource_name(resource), do: Atom.to_string(resource)
  defp action_name(action), do: Atom.to_string(action)

  #
  # Parameter helpers
  # ----------------------------------------------------------------------------
  def param_in_query(opts) do
    guard_param_name!(opts[:name])
    struct!(Parameter, merge(opts, in: :query, explode: true, style: :deepObject))
  end

  def params_search(_opts \\ []) do
    param_in_query(
      name: :search,
      description: "Apply this search term to the results",
      schema: string()
    )
  end

  def params_page(_opts \\ []) do
    param_in_query(
      name: :page,
      description: "Page number to return",
      schema: int64()
    )
  end

  def params_page_size(_opts \\ []) do
    param_in_query(
      name: :page_size,
      description: "Page size to return (default: 20)",
      schema: int64()
    )
  end

  def params_sorting(title, sortable_fields) do
    param_in_query(
      name: :sort,
      description: "sorting params",
      schema:
        object(
          title: title,
          properties: %{
            field: string(enum: sortable_fields),
            order: string(enum: ["asc", "desc"])
          }
        )
    )
  end

  @doc """
  Generates a identifier in the url path.
  If no name is given, defaults to 'id'
  If no type is given, defaults to `uuid/0`
  """
  @spec params_path_id(Keyword.t()) :: OpenApiSpex.Parameter.t()
  def params_path_id(opts \\ []) do
    name = opts[:name] || :id
    type = opts[:type] || uuid()
    required? = Keyword.get(opts, :required, true)
    guard_param_name!(name)

    %Parameter{name: name, in: :path, required: required?, schema: type}
  end

  defp guard_param_name!(name) do
    unless is_atom(name) do
      raise "parameters names should be expressed as atoms, got: #{inspect(name)}"
    end
  end

  #
  # Response helpers
  # ----------------------------------------------------------------------------

  def no_content(opts \\ [description: "Successfully performed operation"]) do
    struct!(OpenApiSpex.Response, opts)
  end

  def json_response(schema_ref) do
    description =
      case schema_ref.schema().title() do
        nil -> raise "make sure the schema ref has a title"
        title -> "Successful #{title} return"
      end

    %OpenApiSpex.Response{
      description: description,
      content: %{
        "application/json" => %OpenApiSpex.MediaType{
          schema: schema_ref
        }
      }
    }
  end

  defmodule FileDownloadResponse do
    @moduledoc false
    import OpenApiSpex, only: [schema: 1]
    schema(%{title: "FileDownloadResponse", type: :string, format: :binary})
  end

  def file_download(opts) do
    content_type = opts[:content_type] || "application/json"

    %OpenApiSpex.Response{
      description: "File download response",
      content: %{content_type => %OpenApiSpex.MediaType{schema: FileDownloadResponse}}
    }
  end

  def error(name, error_schema_ref) do
    %OpenApiSpex.Response{
      description: String.capitalize(name) <> " error response",
      content: %{
        "application/json" => %OpenApiSpex.MediaType{
          schema: error_schema_ref
        }
      }
    }
  end

  def error_not_found(), do: error("NotFound", Schemas.NotFoundErrorResponse)
  def error_forbidden(), do: error("Forbidden", Schemas.ForbiddenErrorResponse)
  def error_unauthorized(), do: error("Unauthorized", Schemas.UnauthorizedErrorResponse)
  def error_conflict(), do: error("Conflict", Schemas.ConflictErrorResponse)
  def error_unprocessable(), do: error("Unprocessable", Schemas.UnprocessableErrorResponse)

  def error_service_unavailable(),
    do: error("ServiceUnavailable", Schemas.ServiceUnavailableErrorResponse)

  #
  # Request helpers
  # ----------------------------------------------------------------------------
  def request_body_with(schema, opts \\ [media_type: "application/json"])

  def request_body_with([{name_of_ref, schema_ref}], opts) do
    description =
      case schema_ref.schema().description() do
        nil -> raise "make sure the schema ref has a description"
        desc -> desc
      end

    title =
      case schema_ref.schema().title() do
        nil -> raise "make sure the schema ref has a title"
        title -> title <> "RequestBody"
      end

    media_type = Keyword.get(opts, :media_type)

    %OpenApiSpex.RequestBody{
      description: description,
      content: %{
        media_type => %OpenApiSpex.MediaType{
          schema:
            object(
              title: title,
              properties: %{
                name_of_ref => schema_ref
              }
            )
        }
      }
    }
  end

  def request_body_with(schema_ref, opts) do
    description =
      case schema_ref.schema().description() do
        nil -> raise "make sure the schema ref has a description"
        desc -> desc
      end

    media_type = Keyword.get(opts, :media_type)

    %OpenApiSpex.RequestBody{
      description: description,
      content: %{
        media_type => %OpenApiSpex.MediaType{
          schema: schema_ref
        }
      }
    }
  end

  # Removal of duplicated patch and put operations (removes the patch)
  def remove_duplicated_patch_operations(paths) do
    Enum.reduce(paths, %{}, fn
      {url, operation}, acc ->
        if operation.put == operation.patch do
          Map.put(acc, url, %{operation | patch: nil})
        else
          Map.put(acc, url, operation)
        end
    end)
  end

  #
  # Some private helpers
  defp merge(opts1, opts2), do: Keyword.merge(opts1, opts2)
end
