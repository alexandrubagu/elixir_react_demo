defmodule CXAdmin.APIUtils.Util do
  @moduledoc false

  defmacro __using__(_) do
    quote do
      import CXAdmin.APIUtils.Util
    end
  end

  # Struct initialization helpers
  #

  def initialize_id(opts), do: Keyword.put_new_lazy(opts, :id, &Ecto.UUID.generate/0)
  def build_struct!(opts, module), do: struct!(module, opts)

  # Various helpers
  #
  @spec bangify!({:ok, term} | {:error, term}) :: term | no_return
  def bangify!({:ok, x}), do: x
  def bangify!({:error, error}), do: raise_error(error)

  defp raise_error(error) when is_binary(error), do: raise(error)
  defp raise_error(%{__exception__: true} = error), do: raise(error)
  defp raise_error(error), do: raise(inspect(error))

  @spec pop_option!(Keyword.t(), atom) :: {term, Keyword.t()} | no_return
  def pop_option!(opts, key) do
    case Keyword.pop(opts, key) do
      {nil, opts} -> raise "could not fetch #{key} from #{inspect(opts)}"
      any -> any
    end
  end

  @doc """
  Turns a keyword list, struct or map into map that can be serialized by jason.

  This function is mostly used to put additional error details into responses.

  ## Example

    iex> to_serializable_map(test: 1)
    %{test: 1}
    iex> to_serializable_map(test: {1,1})
    %{test: "{1,1"}
    iex> to_serializable_map([])
    %{}

  """
  @spec to_serializable_map(x :: nil | map() | Keyword.t()) :: nil | map()
  def to_serializable_map(data)
  def to_serializable_map([]), do: %{}
  def to_serializable_map(nil), do: nil
  def to_serializable_map(x) when is_map(x), do: serialize_map(x)
  def to_serializable_map([{k, _v} | _] = x) when is_atom(k), do: serialize_map(x)

  defp serialize_map(%Date{} = d), do: Date.to_iso8601(d)
  defp serialize_map(%DateTime{} = dt), do: DateTime.to_iso8601(dt)
  defp serialize_map(%{__struct__: _} = x), do: x |> Map.from_struct() |> serialize_map()
  defp serialize_map(map) when is_map(map), do: :maps.map(fn _, v -> serialize_map(v) end, map)
  defp serialize_map([{k, _v} | _] = x) when is_atom(k), do: x |> Map.new() |> serialize_map()
  defp serialize_map(list) when is_list(list), do: Enum.map(list, &serialize_map/1)
  defp serialize_map(x) when is_tuple(x), do: "#{inspect(x)}"
  defp serialize_map(value), do: value
end
