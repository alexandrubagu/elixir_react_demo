defmodule CXAdmin.APIUtils.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.
  """

  @behaviour Plug

  import Plug.Conn
  import CXAdmin.APIUtils.ErrorView

  alias CX.Errors

  require Logger

  def init(opts), do: opts

  def call(conn, {:error, %Ecto.Changeset{} = changeset}) do
    send_response(conn, 422, render("422.json", changeset))
  end

  def call(conn, {:error, %Errors.NotFoundError{} = error}) do
    send_response(conn, 404, render("404.json", error))
  end

  def call(conn, {:error, %Errors.ForbiddenError{} = error}) do
    Logger.warn("forbidden operation: #{inspect(error)}")
    send_response(conn, 403, render("403.json", error))
  end

  def call(conn, {:error, %Errors.UnauthorizedError{} = error}) do
    send_response(conn, 401, render("401.json", error))
  end

  def call(conn, {:error, %Errors.ConflictError{} = error}) do
    send_response(conn, 409, render("409.json", error))
  end

  def call(conn, {:error, %Errors.ServiceUnavailableError{} = error}) do
    send_response(conn, 503, render("503.json", error))
  end

  def call(conn, {:error, {:invalid_irn, _reason}}) do
    send_response(conn, 400, render("400.json", %{reason: :invalid_irn}))
  end

  defp send_response(conn, http_status, response_body) do
    conn
    |> put_resp_content_type("application/json")
    |> resp(http_status, Jason.encode!(response_body))
  end
end
