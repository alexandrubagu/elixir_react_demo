defmodule CXAdmin.FilteringParams do
  @moduledoc """
  Handling of filtering params
  """

  @behaviour Plug

  alias Plug.Conn

  defmacro __using__(_) do
    quote do
      require Plug, warn: false
      plug CXAdmin.FilteringParams
    end
  end

  @doc false
  @impl Plug
  def init(opts), do: opts

  @doc false
  @impl Plug
  @spec call(Conn.t(), term) :: Conn.t()
  def call(conn, _opts) do
    conn
    |> Conn.fetch_query_params()
    |> assign_sort_params()
    |> assign_pagination_params()
    |> assign_search_params()
    |> assign_filtering_params()
  end

  @spec assign_sort_params(Conn.t()) :: Conn.t()
  defp assign_sort_params(%{query_params: params} = conn) do
    with {:ok, field} <- get_sort_field(params["sort"]),
         {:ok, order} <- get_sort_order(params["sort"]) do
      Conn.assign(conn, :sort, %{field: field, order: order})
    else
      _ -> conn
    end
  end

  defp get_sort_field(%{"field" => name}), do: {:ok, name}
  defp get_sort_field(_sort_params), do: :error

  defp get_sort_order(%{"order" => "asc"}), do: {:ok, :asc}
  defp get_sort_order(%{"order" => "desc"}), do: {:ok, :desc}
  defp get_sort_order(_sort_params), do: :error

  @spec assign_pagination_params(Conn.t()) :: Conn.t()
  defp assign_pagination_params(%{query_params: params} = conn) do
    Conn.merge_assigns(conn,
      page: parse_int(params["page"], 1),
      page_size: parse_int(params["page_size"], 25)
    )
  end

  defp parse_int(input, fallback) when is_binary(input) do
    case Integer.parse(input) do
      {integer, ""} -> integer
      _ -> fallback
    end
  end

  defp parse_int(_int_as_string, fallback), do: fallback

  @spec assign_search_params(Conn.t()) :: Conn.t()
  defp assign_search_params(%{query_params: params} = conn) do
    case params["search"] || "" do
      "" -> conn
      search -> Conn.assign(conn, :search, search)
    end
  end

  @spec assign_filtering_params(Conn.t()) :: Conn.t()
  defp assign_filtering_params(%{query_params: params} = conn) do
    case params["filters"] do
      filters when is_map(filters) -> Conn.assign(conn, :filters, filters)
      _ -> conn
    end
  end

  @doc """
  Returns a url for the current filter param while overriding
  """
  def url(conn, overrides \\ %{}) do
    query_params =
      conn.assigns
      |> Map.take([:sort, :page, :page_size, :search])
      |> Map.merge(overrides)
      |> Plug.Conn.Query.encode()

    "?" <> query_params
  end
end
