defmodule CXAdmin.Stats.AnalyzedVerbatimsBridge do
  @moduledoc """
  Bridges CX.Eventbus VerbatimAnalyzed events onto the channel.

  We keep track of the last 10 changes so that the ui is always filled with
  data (once we had some events)
  """

  use GenServer
  require Logger

  alias CX.EventBus
  alias CX.Verbatims.Processing
  alias CX.Events.Verbatim.VerbatimAnalyzed

  alias CXAdmin.StatsChannel

  #
  # Client API
  #
  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def get_stats() do
    GenServer.call(__MODULE__, :get_stats)
  end

  #
  # Server API
  #
  def init(_) do
    EventBus.subscribe(Processing, __struct__: VerbatimAnalyzed)
    {:ok, []}
  end

  def handle_info({Processing, %VerbatimAnalyzed{} = evt}, state) do
    StatsChannel.emit(evt)
    next_state = Enum.take([evt | state], 10)
    {:noreply, next_state}
  end

  def handle_info({_, _} = e, state) do
    Logger.warn(fn -> "Unknown event: #{inspect(e)}" end)
    {:noreply, state}
  end

  def handle_call(:get_stats, _from, state) do
    {:reply, state, state}
  end
end
