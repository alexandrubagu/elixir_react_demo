defmodule CXAdmin.Stats.State do
  @moduledoc """
   Session
  """

  defstruct stat_name: nil,
            time_period: nil,
            data: %{}

  alias CXAdmin.Stats.{Datasources, TimePeriod}

  def new(stat_name) do
    %__MODULE__{
      stat_name: stat_name,
      time_period: TimePeriod.new()
    }
  end

  def update_time_period(state) do
    time_period = TimePeriod.update(state.time_period)
    %{state | time_period: time_period}
  end

  def update_stats(%{stat_name: stat_name, time_period: period} = state) do
    %{start: start, stop: stop, step: step} = period

    case Datasources.get_stats(stat_name, start, stop, step) do
      {:ok, data} ->
        emit_new_data(stat_name, data)
        update_data(state, data)

      _ ->
        state
    end
  end

  def get_data(state), do: serialize_data(state.data)

  defp emit_new_data(stat_name, data) do
    CXAdmin.StatsChannel.emit({stat_name, serialize_data(data)})
  end

  defp serialize_data(data) do
    to_map =
      data
      |> Map.to_list()
      |> Enum.sort_by(&elem(&1, 0))
      |> Enum.flat_map(&elem(&1, 1))
      |> Enum.map(&convert_dates(&1, ["ts"]))

    %{data: to_map}
  end

  defp update_data(state, data) do
    updated =
      state.data
      |> Map.merge(data)
      |> Map.to_list()
      |> Enum.sort_by(&elem(&1, 0), &>=/2)
      |> Enum.take(TimePeriod.num_intervals(state.time_period))
      |> Enum.into(%{})

    %{state | data: updated}
  end

  defp convert_dates(row, dates) do
    Enum.reduce(dates, row, fn date_key, acc ->
      Map.update!(acc, date_key, &DateTime.from_naive!(&1, "Etc/UTC"))
    end)
  end
end
