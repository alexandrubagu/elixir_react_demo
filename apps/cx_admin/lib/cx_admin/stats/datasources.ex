defmodule CXAdmin.Stats.Datasources do
  @moduledoc """
  Realtime mail dashboard
  """

  alias CX.Repo
  alias CX.Notifications.Emails.Messages.MessageQuery

  def stats_name("mail_type"), do: {:ok, :mail_type}
  def stats_name("mail_event_type"), do: {:ok, :mail_event_type}
  def stats_name("sms_event_type"), do: {:ok, :sms_event_type}
  def stats_name("participation_status"), do: {:ok, :participation_status}
  def stats_name("webhook_status"), do: {:ok, :webhook_status}
  def stats_name(_), do: {:error, :not_found}

  def get_stats(stat_name, start, stop, step) do
    with {:ok, sql} <- sql(stat_name, start, stop, step),
         {:ok, results} <- run_query(sql) do
      {:ok, prepare(results)}
    end
  end

  defp sql(:mail_type, start, stop, step) do
    {:ok, MessageQuery.mails_by_type(start, stop, step)}
  end

  defp sql(:mail_event_type, start, stop, step) do
    {:ok, MessageQuery.events_by_type(start, stop, step)}
  end

  defp sql(:sms_event_type, start, stop, step) do
    sql = ~s"""
    WITH time_series AS (
      SELECT start,
             format('[%s, %s)',start, start + $3::interval)::tsrange AS rng
      FROM generate_series($1::timestamp, $2::timestamp, $3::interval) AS start
    )
    SELECT
      time_series.start AS ts,
      t.name as sms_event_type,
      count(me.id) AS cnt
    FROM time_series
    CROSS JOIN (VALUES
      (1,'queued'),
      (2,'failed'),
      (3,'sent'),
      (4,'delivered'),
      (5,'undelivered'),
      (6,'unsubscribed')
    ) AS t (type_id, name)
    LEFT JOIN sms_events me ON me.inserted_at <@ time_series.rng AND t.type_id = me.type
    GROUP BY time_series.start, t.name
    """

    {:ok, {sql, [start, stop, %Postgrex.Interval{secs: step}]}}
  end

  defp sql(:participation_status, start, stop, step) do
    sql = ~s"""
    WITH time_series AS (
      SELECT start,
             format('[%s, %s)',start, start + $3::interval)::tsrange AS rng
      FROM generate_series($1::timestamp, $2::timestamp, $3::interval) AS start
    )
    SELECT
      time_series.start AS ts,
      ps.name as participation_status,
      count(p.id) AS cnt
    FROM time_series
    CROSS JOIN (VALUES(1,'active'),(2,'abandonned'),(3,'completed')) AS ps (status_id, name)
    LEFT JOIN participations p ON p.inserted_at <@ time_series.rng AND ps.status_id = p.status
    GROUP BY time_series.start, ps.name
    """

    {:ok, {sql, [start, stop, %Postgrex.Interval{secs: step}]}}
  end

  defp sql(:webhook_status, start, stop, step) do
    sql = ~s"""
    WITH time_series AS (
      SELECT start,
             format('[%s, %s)',start, start + $3::interval)::tsrange AS rng
      FROM generate_series($1::timestamp, $2::timestamp, $3::interval) AS start
    )
    SELECT
      time_series.start AS ts,
      ws.name as webhook_status,
      count(w.id) AS cnt
    FROM time_series
    CROSS JOIN (VALUES('queued'),('processing'),('delivered'),('deferred'),('failed')) AS ws (name)
    LEFT JOIN webhooks w ON w.inserted_at <@ time_series.rng AND ws.name = w.status
    GROUP BY time_series.start, ws.name
    """

    {:ok, {sql, [start, stop, %Postgrex.Interval{secs: step}]}}
  end

  defp run_query({sql, args}), do: Repo.query(sql, args)

  defp prepare(%{columns: cols, rows: rows}) do
    rows
    |> Enum.map(&Enum.zip(cols, &1))
    |> Enum.map(&Enum.into(&1, %{}))
    |> Enum.group_by(&Map.get(&1, "ts"))
  end
end
