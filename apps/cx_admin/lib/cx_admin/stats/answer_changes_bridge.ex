defmodule CXAdmin.Stats.AnswerChangesBridge do
  @moduledoc """
  Bridges CX.Eventbus answer changes events onto the channel.

  In terms of performance, this is not ideal.. but by the time
  the volume is so big, the frontend wont be able to handle it anyway.

  We keep track of the last 5 changes so that the ui is always filled with
  data (once we had some events)
  """

  use GenServer
  require Logger

  alias CX.EventBus
  alias CX.Participations.Sessions

  alias CXAdmin.StatsChannel

  alias CX.Events.Participation.AnswerChanged

  #
  # Client API
  #
  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def get_stats() do
    GenServer.call(__MODULE__, :get_stats)
  end

  #
  # Server API
  #
  def init(_) do
    EventBus.subscribe(Sessions, __struct__: AnswerChanged)
    {:ok, []}
  end

  def handle_info({Sessions, %AnswerChanged{} = evt}, state) do
    StatsChannel.emit(evt)
    next_state = Enum.take([evt | state], 10)
    {:noreply, next_state}
  end

  def handle_info({_, _} = e, state) do
    Logger.warn(fn -> "Unknown event: #{inspect(e)}" end)
    {:noreply, state}
  end

  def handle_call(:get_stats, _from, state) do
    {:reply, state, state}
  end
end
