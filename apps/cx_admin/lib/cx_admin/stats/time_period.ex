defmodule CXAdmin.Stats.TimePeriod do
  @moduledoc """
   TimePeriod hold the details about the state of timeframe for a statistics
  """

  defstruct period: 3600,
            start: nil,
            stop: nil,
            step: 300

  def new(period \\ 3600, step \\ 300) do
    now = DateTime.utc_now()

    start =
      now
      |> subtract(period)
      |> floor_interval(step)

    stop = floor_interval(now, step)

    %__MODULE__{
      start: start,
      stop: stop,
      period: period,
      step: step
    }
  end

  def update(%__MODULE__{} = tp) do
    now = DateTime.utc_now()
    start = subtract(tp.stop, tp.step)
    stop = floor_interval(now, tp.step)

    %{tp | start: start, stop: stop}
  end

  def num_intervals(tp) do
    ceiled = :math.ceil(tp.period / tp.step)
    round(ceiled)
  end

  defp subtract(ts, period) do
    DateTime.add(ts, period * -1)
  end

  defp floor_interval(ts, step) do
    floored = div(DateTime.to_unix(ts), step) * step
    DateTime.from_unix!(floored)
  end
end
