defmodule CXAdmin.Stats.ChannelBridge do
  @moduledoc """
  Bridges CX.Eventbus events onto the channel.

  In terms of performance, this is not ideal.. but by the time
  the volume is so big, the frontend wont be able to handle it anyway.
  """

  use GenServer
  require Logger

  alias CX.EventBus
  alias CX.Participations.Sessions
  alias CX.Verbatims.Processing

  alias CXAdmin.StatsChannel

  alias CX.Events.{
    Participation.AnswerChanged,
    Verbatim.VerbatimAnalyzed
  }

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    EventBus.subscribe(Sessions, __struct__: AnswerChanged)
    EventBus.subscribe(Processing, __struct__: VerbatimAnalyzed)
    {:ok, nil}
  end

  def handle_info({Sessions, %AnswerChanged{} = evt}, state) do
    StatsChannel.emit(evt)
    {:noreply, state}
  end

  def handle_info({Processing, %VerbatimAnalyzed{} = evt}, state) do
    StatsChannel.emit(evt)
    {:noreply, state}
  end

  def handle_info({_, _} = e, state) do
    Logger.warn(fn -> "Unknown event: #{inspect(e)}" end)
    {:noreply, state}
  end
end
