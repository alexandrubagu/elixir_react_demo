defmodule CXAdmin.Stats.Supervisor do
  @moduledoc false

  use Supervisor

  @doc """
  Starts the supervisor for stats related processes
  """
  def start_link() do
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc """
  Setup the supervision strategy for all participations
  """
  def init([]) do
    children = [
      CXAdmin.Stats.Registry,
      CXAdmin.Stats.SessionSupervisor,
      CXAdmin.Stats.AnswerChangesBridge,
      CXAdmin.Stats.AnalyzedVerbatimsBridge
    ]

    Supervisor.init(children, strategy: :rest_for_one)
  end
end
