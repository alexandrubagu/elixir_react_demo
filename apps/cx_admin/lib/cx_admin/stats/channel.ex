defmodule CXAdmin.StatsChannel do
  @moduledoc """
  Participation channel
  """
  use CXAdmin, :channel

  alias CXAdmin.Endpoint

  alias CXAdmin.Stats.Session
  alias CXAdmin.Stats.Datasources
  alias CXAdmin.Stats.AnswerChangesBridge
  alias CXAdmin.Stats.AnalyzedVerbatimsBridge

  alias CX.Events.{
    Participation.AnswerChanged,
    Verbatim.VerbatimAnalyzed
  }

  def join("stats", _auth, socket) do
    {:ok, socket}
  end

  def emit(%AnswerChanged{} = evt) do
    Endpoint.broadcast_from!(self(), "stats", "answer_changed", to_map(evt))
  end

  def emit(%VerbatimAnalyzed{} = evt) do
    Endpoint.broadcast_from!(self(), "stats", "verbatim_analyzed", to_map(evt))
  end

  def emit({stat_name, stat}) do
    Endpoint.broadcast_from!(self(), "stats", Atom.to_string(stat_name), stat)
  end

  def handle_in("get_stats", %{"stat_name" => "answer_changed"}, socket) do
    stats = Enum.map(AnswerChangesBridge.get_stats(), &to_map/1)
    {:reply, {:ok, %{data: stats}}, socket}
  end

  def handle_in("get_stats", %{"stat_name" => "verbatim_analyzed"}, socket) do
    stats = Enum.map(AnalyzedVerbatimsBridge.get_stats(), &to_map/1)
    {:reply, {:ok, %{data: stats}}, socket}
  end

  def handle_in("get_stats", %{"stat_name" => stat_name_str}, socket) do
    case Datasources.stats_name(stat_name_str) do
      {:ok, stat_name} ->
        stats = Session.get_stats(stat_name)
        {:reply, {:ok, stats}, socket}

      error ->
        {:reply, error, socket}
    end
  end

  defp to_map(%_{} = x), do: Map.from_struct(x)
end
