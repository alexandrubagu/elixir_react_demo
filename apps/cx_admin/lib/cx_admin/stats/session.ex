defmodule CXAdmin.Stats.Session do
  @moduledoc """
  Session caches stats for a given datasource
  """

  use GenServer

  alias CXAdmin.Stats.State
  alias CXAdmin.Stats.Registry, as: SessionReg

  @doc false
  def child_spec(stat_name) do
    %{
      id: {__MODULE__, stat_name},
      start: {__MODULE__, :start_link, stat_name},
      type: :worker
    }
  end

  defp via(stat_name), do: SessionReg.via(stat_name)

  #
  # Client API
  # ----------------------------------------------------------------------------
  def start_link(stat_name) do
    GenServer.start_link(__MODULE__, stat_name, name: via(stat_name))
  end

  def get_stats(stat_name) do
    GenServer.call(via(stat_name), :get_stats)
  end

  defp init_stats(stat_name) do
    GenServer.cast(via(stat_name), :init_stats)
  end

  #
  # Server API
  # ----------------------------------------------------------------------------
  def init(stat_name) do
    state = State.new(stat_name)
    init_stats(stat_name)
    {:ok, state}
  end

  def handle_cast(:init_stats, state) do
    next_state = State.update_stats(state)
    schedule_update()
    {:noreply, next_state}
  end

  def handle_info(:update_stats, state) do
    next_state =
      state
      |> State.update_time_period()
      |> State.update_stats()

    schedule_update()
    {:noreply, next_state}
  end

  def handle_call(:get_stats, _from, state), do: {:reply, State.get_data(state), state}

  defp schedule_update(wait \\ 5000), do: Process.send_after(self(), :update_stats, wait)
end
