defmodule CXAdmin.Stats.SessionSupervisor do
  @moduledoc """
  Supervision of Session
  """
  use Supervisor

  alias CXAdmin.Stats.Session

  @doc """
  Starts the supervisor for all the related processes
  """
  def start_link(arg \\ []) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  @doc """
  Setup the supervision strategy for all sessions
  """
  @impl true
  def init(_arg) do
    children = [
      {Session, [:mail_type]},
      {Session, [:mail_event_type]},
      {Session, [:sms_event_type]},
      {Session, [:participation_status]},
      {Session, [:webhook_status]}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
