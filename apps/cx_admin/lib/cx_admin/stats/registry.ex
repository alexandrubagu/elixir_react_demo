defmodule CXAdmin.Stats.Registry do
  @moduledoc """
  Handles registering session processes
  """

  @doc false
  def child_spec(_opts) do
    %{
      id: __MODULE__,
      start: {Registry, :start_link, [[keys: :unique, name: __MODULE__]]},
      type: :supervisor
    }
  end

  @doc """
  Generate a via tuple for naming a process through a registry
  Returns {:via, Registry, {__MODULE__, "type-id"}}
  """
  def via(stat_name) do
    {:via, Registry, {__MODULE__, stat_name}}
  end

  @doc """
  Returns the pid of the session process by looking it up via its
  stat_name in the session registry.
  Returns the {:ok, pid} | {:error, :no_such_process}
  """
  def pid(stat_name) do
    __MODULE__
    |> Registry.lookup(stat_name)
    |> case do
      [{pid, nil}] -> {:ok, pid}
      _ -> {:error, :no_such_process}
    end
  end

  @doc """
  Returns the id of the session process by looking it up via its
  pid in the session registry.
  Returns the {:ok, stat_name} | {:error, :no_such_process}
  """
  def id(pid) when is_pid(pid) do
    __MODULE__
    |> Registry.keys(pid)
    |> case do
      [stat_name] -> {:ok, stat_name}
      _ -> {:error, :no_such_process}
    end
  end
end
