defmodule CXAdmin.API.SurveyController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.Surveys

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action) do
    ApiSpec.operation(:surveys, action)
  end

  def index(conn, _params) do
    with {:ok, page} <- Surveys.list_surveys(conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def create(conn, %{"survey" => params}) do
    with {:ok, survey} <- Surveys.create_survey(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.api_survey_path(conn, :show, survey))
      |> render("show.json", survey: survey)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, survey} <- Surveys.get_survey(id) do
      render(conn, "show.json", survey: survey)
    end
  end

  def update(conn, %{"id" => id, "survey" => params}) do
    with {:ok, survey} <- Surveys.get_survey(id),
         {:ok, updated} <- Surveys.update_survey(survey, params) do
      render(conn, "show.json", survey: updated)
    end
  end

  def toggle_delete(conn, %{"id" => id}) do
    with {:ok, survey} <- Surveys.get_survey(id),
         {:ok, updated} <- Surveys.toggle_delete(survey) do
      render(conn, "show.json", survey: updated)
    end
  end
end
