defmodule CXAdmin.API.DemoController do
  @moduledoc false

  use CXAdmin, :controller

  alias CX.AI.Categorizations

  def index(conn, _params) do
    with {:ok, verbatim_categorizations} <- Categorizations.latest_verbatim_categorizations() do
      render(conn, "index.json", verbatim_categorizations: verbatim_categorizations)
    end
  end

  def count_topic_selections(conn, _params) do
    with {:ok, data} <- Categorizations.count_verbatim_categorization_topic_selections() do
      json(conn, %{data: data})
    end
  end

  def get_top_100_word_cloud(conn, _params) do
    with {:ok, data} <- Categorizations.get_top_100_word_cloud() do
      json(conn, %{data: data})
    end
  end
end
