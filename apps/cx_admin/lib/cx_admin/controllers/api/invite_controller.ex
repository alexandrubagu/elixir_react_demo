defmodule CXAdmin.API.InviteController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.Surveys
  alias CX.Invites
  alias CX.InviteService

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action), do: ApiSpec.operation(:invites, action)

  def index(conn, _params) do
    with {:ok, page} <- Invites.list_invites(conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, invite} <- Invites.get_invite(id, preload: [:events, :smses, :email_messages]) do
      render(conn, "show.json", invite: invite)
    end
  end

  def create_and_send(conn, %{"survey_name" => name, "invite" => invite_params}) do
    with {:ok, survey} <- Surveys.get_survey_by(name: name),
         {:ok, created} <- InviteService.create_and_send_invite(survey, invite_params),
         {:ok, invite} <-
           Invites.get_invite(created.id, preload: [:events, :smses, :email_messages]) do
      render(conn, "show.json", invite: invite)
    end
  end

  def update_email_address(conn, %{"id" => id, "email_address" => email_address}) do
    with {:ok, updated} <- InviteService.update_email_address(id, email_address),
         {:ok, invite} <-
           Invites.get_invite(updated.id, preload: [:events, :smses, :email_messages]) do
      render(conn, "show.json", invite: invite)
    end
  end
end
