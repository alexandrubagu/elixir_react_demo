defmodule CXAdmin.API.ParticipationController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.Participations
  alias CX.Verbatims.Processor

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action), do: ApiSpec.operation(:participations, action)

  def index(conn, _params) do
    with {:ok, page} <- Participations.list_participations(conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def show(conn, %{"id" => id}) do
    opts = [preload: [:survey, :events, :verbatims, :verbatim_logs]]

    with {:ok, item} <- Participations.get_participation(id, opts) do
      render(conn, "show.json", participation: item)
    end
  end

  def sessions(conn, _params) do
    render(conn, "sessions.json", sessions: Participations.list_sessions())
  end

  def process_verbatims(conn, _params) do
    json(conn, %{data: Processor.notify_waiting()})
  end

  def verbatim_processor_stats(conn, _params) do
    json(conn, %{data: Processor.get_stats()})
  end
end
