defmodule CXAdmin.API.WebhookController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.Webhooks

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action), do: ApiSpec.operation(:webhooks, action)

  def index(conn, _params) do
    with {:ok, page} <- Webhooks.list(conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, webhook} <- Webhooks.get(id) do
      render(conn, "show.json", webhook: webhook)
    end
  end
end
