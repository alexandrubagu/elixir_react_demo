defmodule CXAdmin.API.Notifications.Blacklists.BlacklistEntryController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.Notifications.Blacklists

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action) do
    ApiSpec.operation(:notification_blacklists, action)
  end

  def index(conn, _params) do
    with {:ok, page} <- Blacklists.list_entries(conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def create(conn, %{"blacklist_entry" => params}) do
    with {:ok, blacklist_entry} <- Blacklists.create_entry(params) do
      conn
      |> put_status(:created)
      |> put_resp_header(
        "location",
        Routes.api_blacklist_entry_path(conn, :show, blacklist_entry)
      )
      |> render("show.json", blacklist_entry: blacklist_entry)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, blacklist_entry} <- Blacklists.get_entry(id) do
      render(conn, "show.json", blacklist_entry: blacklist_entry)
    end
  end

  def update(conn, %{"id" => id, "blacklist_entry" => params}) do
    with {:ok, entry} <- Blacklists.get_entry(id),
         {:ok, updated} <- Blacklists.update_entry(entry, params) do
      render(conn, "show.json", blacklist_entry: updated)
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, entry} <- Blacklists.get_entry(id),
         {:ok, _deleted} <- Blacklists.delete_entry(entry) do
      send_resp(conn, :no_content, "")
    end
  end
end
