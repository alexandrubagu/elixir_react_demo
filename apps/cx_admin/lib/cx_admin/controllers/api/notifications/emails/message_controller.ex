defmodule CXAdmin.API.Notifications.Emails.MessageController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.Notifications
  alias CX.Notifications.Emails.Messages

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action) do
    ApiSpec.operation(:notification_email_messages, action)
  end

  def index(conn, _params) do
    with {:ok, page} <- Messages.list_messages(conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, message} <- Messages.get_message(id, preload: :events) do
      render(conn, "show.json", message: message)
    end
  end

  def retry_email_delivery(conn, %{"id" => id}) do
    with {:ok, _message} <- Notifications.retry_email_delivery(id),
         {:ok, message} <- Notifications.get_email_message(id, preload: :events) do
      render(conn, "show.json", message: message)
    end
  end

  def update_email_address(conn, %{"id" => id, "email_address" => email_address}) do
    with {:ok, _message} <- Notifications.update_email_address(id, email_address),
         {:ok, message} <- Notifications.get_email_message(id, preload: :events) do
      render(conn, "show.json", message: message)
    end
  end
end
