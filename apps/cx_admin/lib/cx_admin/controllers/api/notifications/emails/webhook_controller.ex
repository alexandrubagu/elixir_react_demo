defmodule CXAdmin.API.Notifications.Emails.WebhookController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.Notifications

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action) do
    ApiSpec.operation(:notification_email_webhooks, action)
  end

  def index(conn, _params) do
    with {:ok, page} <- Notifications.list_email_webhook_events(conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, event} <- Notifications.get_email_webhook_event(id) do
      render(conn, "show.json", event: event)
    end
  end

  def manually_process(conn, %{"id" => id, "params" => params}) do
    with {:ok, event} <- Notifications.manually_process_email_webhook_event(id, params) do
      render(conn, "show.json", event: event)
    end
  end

  def manually_ignore(conn, %{"id" => id}) do
    with {:ok, event} <- Notifications.manually_ignore_email_webhook_event(id) do
      render(conn, "show.json", event: event)
    end
  end

  def retry_processing(conn, _params) do
    json(conn, %{data: Notifications.retry_processing_email_webhooks()})
  end

  def retry_processing_stats(conn, _params) do
    json(conn, %{data: Notifications.get_email_webhook_retry_processing_stats()})
  end
end
