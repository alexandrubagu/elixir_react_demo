defmodule CXAdmin.API.Notifications.SMS.MessageController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.Notifications

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action) do
    ApiSpec.operation(:notification_sms_messages, action)
  end

  def index(conn, _params) do
    with {:ok, page} <- Notifications.list_sms_messages(conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, message} <- Notifications.get_sms_message(id, preload: :events) do
      render(conn, "show.json", message: message)
    end
  end
end
