defmodule CXAdmin.API.ParticipationSyncerController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.ParticipationSyncers

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action), do: ApiSpec.operation(:participation_syncers, action)

  def index(conn, _params) do
    with {:ok, page} <- ParticipationSyncers.list_syncers(conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def create(conn, %{"syncer" => params}) do
    with {:ok, syncer} <- ParticipationSyncers.create_syncer(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.api_participation_syncer_path(conn, :show, syncer))
      |> render("show.json", participation_syncer: syncer)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, syncer} <- ParticipationSyncers.get_syncer(id) do
      render(conn, "show.json", participation_syncer: syncer)
    end
  end

  def enable(conn, %{"id" => id}) do
    with {:ok, syncer} <- ParticipationSyncers.enable_syncer(id) do
      render(conn, "show.json", participation_syncer: syncer)
    end
  end

  def disable(conn, %{"id" => id}) do
    with {:ok, syncer} <- ParticipationSyncers.disable_syncer(id) do
      render(conn, "show.json", participation_syncer: syncer)
    end
  end

  def update(conn, %{"id" => id, "syncer" => params}) do
    with {:ok, updated} <- ParticipationSyncers.update_syncer(id, params) do
      render(conn, "show.json", participation_syncer: updated)
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, _deleted} <- ParticipationSyncers.delete_syncer(id) do
      send_resp(conn, :no_content, "")
    end
  end
end
