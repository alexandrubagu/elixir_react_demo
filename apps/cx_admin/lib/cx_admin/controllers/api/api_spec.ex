defmodule CXAdmin.API.ApiSpec do
  @moduledoc """
  Open api specs for the Ingestion context
  """

  @tags ["cx-admin-api"]

  use CXAdmin.APIUtils.OAS.Utils
  alias CXAdmin.API.ApiSpec.Schemas

  #
  # Surveys
  # ----------------------------------------------------------------

  def operation(:surveys, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :surveys, :index),
      description: "Lists surveys",
      parameters: [
        params_page(),
        params_page_size(),
        params_search(),
        params_sorting(
          "SurveySortingParam",
          ~w(name description expire_after abandonned_after inserted_at updated_at)
        ),
        param_in_query(
          name: :filters,
          description: "filtering params",
          schema:
            object(
              title: "SurveyFilteringParams",
              properties: %{
                include_deleted: boolean()
              }
            )
        )
      ],
      responses: %{
        200 => json_response(Schemas.SurveyPaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:surveys, :create) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :surveys, :create),
      description: "Creates a survey",
      parameters: [],
      requestBody: request_body_with(survey: Schemas.CreateSurveyParams),
      responses: %{
        201 => json_response(Schemas.SurveyResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:surveys, :update) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :surveys, :update),
      description: "Updates a survey",
      parameters: [params_path_id()],
      requestBody: request_body_with(survey: Schemas.UpdateSurveyParams),
      responses: %{
        200 => json_response(Schemas.SurveyResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:surveys, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :surveys, :show),
      description: "Shows survey",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.SurveyResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:surveys, :toggle_delete) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :surveys, :toggle_delete),
      description: "Toggles delete flag on survey",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.SurveyResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  #
  # Batches and entries
  # ----------------------------------------------------------------

  def operation(:batches, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batches, :index),
      description: "Lists batches",
      parameters: [
        params_page(),
        params_page_size(),
        params_search(),
        params_sorting(
          "BatchSortingParam",
          ~w(name inserted_at)
        )
      ],
      responses: %{
        200 => json_response(Schemas.BatchPaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:batches, :create) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batches, :create),
      description: "Creates a batch",
      parameters: [],
      requestBody: request_body_with(batch: Schemas.CreateBatchParams),
      responses: %{
        201 => json_response(Schemas.BatchResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:batches, :update) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batches, :update),
      description: "Updates a batch",
      parameters: [params_path_id()],
      requestBody: request_body_with(batch: Schemas.UpdateBatchParams),
      responses: %{
        200 => json_response(Schemas.BatchResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:batches, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batches, :show),
      description: "Shows batch",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.BatchResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:batches, :delete) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batches, :delete),
      description: "Delete batch",
      parameters: [params_path_id()],
      responses: %{
        204 => no_content(),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:batches, :process) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batches, :process),
      description: "Process batch entries",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.BatchResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        409 => error_conflict()
      }
    )
  end

  def operation(:batches, :import_json) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batches, :import_json),
      description: "Import batch entries from json upload",
      parameters: [params_path_id()],
      requestBody:
        request_body_with(Schemas.BatchJsonImportParams, media_type: "multipart/form-data"),
      responses: %{
        200 => json_response(Schemas.BatchJsonImportResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        409 => error_conflict()
      }
    )
  end

  def operation(:batch_entries, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batch_entries, :index),
      description: "Lists batch entries",
      parameters: [
        params_path_id(name: :batch_id),
        params_page(),
        params_page_size(),
        params_search(),
        params_sorting(
          "BatchEntrySortingParam",
          ~w(survey_id batch_id notification_type status ref language)
        )
      ],
      responses: %{
        200 => json_response(Schemas.BatchEntryPaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:batch_entries, :create) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batch_entries, :create),
      description: "Creates a batch entry",
      parameters: [
        params_path_id(name: :batch_id),
        params_path_id(name: :survey_id)
      ],
      requestBody: request_body_with(entry: Schemas.CreateBatchEntryParams),
      responses: %{
        201 => json_response(Schemas.BatchEntryResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:batch_entries, :update) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batch_entries, :update),
      description: "Updates a batch entry",
      parameters: [params_path_id()],
      requestBody: request_body_with(entry: Schemas.UpdateBatchEntryParams),
      responses: %{
        200 => json_response(Schemas.BatchEntryResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:batch_entries, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batch_entries, :show),
      description: "Shows batch entry",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.BatchEntryResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:batch_entries, :delete) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batch_entries, :delete),
      description: "Delete batch entry",
      parameters: [params_path_id()],
      responses: %{
        204 => no_content(),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:batch_entries, :process) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :batch_entries, :process),
      description: "Delete batch entry",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.BatchEntryResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  #
  # Invites
  # ----------------------------------------------------------------

  def operation(:invites, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :invites, :index),
      description: "Lists invites",
      parameters: [
        params_page(),
        params_page_size(),
        params_search(),
        params_sorting(
          "InviteSortingParam",
          ~w(ref language inserted_at updated_at)
        )
      ],
      responses: %{
        200 => json_response(Schemas.InvitePaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:invites, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :invites, :show),
      description: "Shows invite",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.InviteResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:invites, :create_and_send) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :invites, :create_and_send),
      description: "Creates and sends an invite",
      parameters: [
        params_path_id(name: :survey_name, type: string())
      ],
      requestBody: request_body_with(invite: Schemas.CreateInviteParams),
      responses: %{
        200 => json_response(Schemas.InviteResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:invites, :update_email_address) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :invites, :update_email_address),
      description:
        "Updates email_address and returns the invite alongside with attached email messages",
      parameters: [
        params_path_id()
      ],
      requestBody: request_body_with(params: Schemas.InviteUpdateEmailParams),
      responses: %{
        200 => json_response(Schemas.InviteResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        409 => error_conflict(),
        422 => error_unprocessable()
      }
    )
  end

  #
  # Participations
  # ----------------------------------------------------------------

  def operation(:participations, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participations, :index),
      description: "Lists participations",
      parameters: [
        params_page(),
        params_page_size(),
        params_search(),
        params_sorting(
          "ParticipationSortingParam",
          ~w(ref language status verbatims_processed)
        )
      ],
      responses: %{
        200 => json_response(Schemas.ParticipationPaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden()
      }
    )
  end

  def operation(:participations, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participations, :show),
      description: "Shows participation",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.ParticipationResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:participations, :sessions) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participations, :sessions),
      description: "Lists participations sessions",
      parameters: [],
      responses: %{
        200 => json_response(Schemas.ParticipationSessionsResponse),
        401 => error_unauthorized(),
        403 => error_forbidden()
      }
    )
  end

  def operation(:participations, :process_verbatims) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participations, :process_verbatims),
      description: "",
      parameters: [],
      responses: %{
        200 => json_response(Schemas.ProcessVerbatimsResponse),
        401 => error_unauthorized(),
        403 => error_forbidden()
      }
    )
  end

  def operation(:participations, :verbatim_processor_stats) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participations, :verbatim_processor_stats),
      description: "",
      parameters: [],
      responses: %{
        200 => json_response(Schemas.VerbatimProcessorStatsResponse),
        401 => error_unauthorized(),
        403 => error_forbidden()
      }
    )
  end

  #
  # ParticipationSyncers
  # ----------------------------------------------------------------

  def operation(:participation_syncers, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participation_syncers, :index),
      description: "Lists participation syncer",
      parameters: [
        params_page(),
        params_page_size(),
        params_search(),
        params_sorting(
          "ParticipationSyncerSortingParam",
          ~w(id name inserted_at updated_at)
        )
      ],
      responses: %{
        200 => json_response(Schemas.ParticipationSyncerPaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:participation_syncers, :create) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participation_syncers, :create),
      description: "Creates a participation syncer",
      parameters: [],
      requestBody: request_body_with(syncer: Schemas.CreateParticipationSyncerParams),
      responses: %{
        201 => json_response(Schemas.ParticipationSyncerResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:participation_syncers, :update) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participation_syncers, :update),
      description: "Updates a participation syncer",
      parameters: [params_path_id()],
      requestBody: request_body_with(syncer: Schemas.UpdateParticipationSyncerParams),
      responses: %{
        200 => json_response(Schemas.ParticipationSyncerResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:participation_syncers, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participation_syncers, :show),
      description: "Shows a participation syncer",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.ParticipationSyncerResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:participation_syncers, :delete) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participation_syncers, :delete),
      description: "Delete participation syncer",
      parameters: [params_path_id()],
      responses: %{
        204 => no_content(),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:participation_syncers, :enable) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participation_syncers, :enable),
      description: "Enables a participation syncer",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.ParticipationSyncerResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:participation_syncers, :disable) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :participation_syncers, :disable),
      description: "Disables a participation syncer",
      parameters: [params_path_id()],
      responses: %{
        200 => json_response(Schemas.ParticipationSyncerResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  #
  # Blacklists
  # ----------------------------------------------------------------

  def operation(:notification_blacklists, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_blacklists, :index),
      description: "Lists the notification blacklist entries",
      parameters: [
        params_page(),
        params_page_size(),
        params_search(),
        params_sorting(
          "NotificationBlacklistSortingParam",
          ~w(notification_type key reason deleted inserted_at updated_at)
        )
      ],
      responses: %{
        200 => json_response(Schemas.NotificationBlacklistPaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden()
      }
    )
  end

  def operation(:notification_blacklists, :create) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_blacklists, :create),
      description: "Creates a new blacklist entry",
      requestBody: request_body_with(blacklist_entry: Schemas.NotificationBlacklistEntryParams),
      responses: %{
        201 => json_response(Schemas.NotificationBlacklistEntryResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:notification_blacklists, :update) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_blacklists, :update),
      description: "Updates a blacklist entry",
      parameters: [params_path_id()],
      requestBody: request_body_with(blacklist_entry: Schemas.NotificationBlacklistEntryParams),
      responses: %{
        200 => json_response(Schemas.NotificationBlacklistEntryResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:notification_blacklists, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_blacklists, :show),
      description: "Returns the requested blacklist entry",
      parameters: [
        params_path_id()
      ],
      responses: %{
        200 => json_response(Schemas.NotificationBlacklistEntryResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:notification_blacklists, :delete) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_blacklists, :delete),
      description: "Deletes a blacklist entry",
      parameters: [
        params_path_id()
      ],
      responses: %{
        204 => no_content(),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        422 => error_unprocessable()
      }
    )
  end

  #
  # Email messages & webhooks
  # ----------------------------------------------------------------

  def operation(:notification_email_messages, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_email_messages, :index),
      description: "Lists the notification email messages",
      parameters: [
        params_page(),
        params_page_size(),
        params_search(),
        params_sorting(
          "NotificationEmailMessagesSortingParams",
          ~w(invite_id to_address type delivery_status delivery_drop_reason inserted_at updated_at)
        )
      ],
      responses: %{
        200 => json_response(Schemas.NotificationEmailMessagePaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden()
      }
    )
  end

  def operation(:notification_email_messages, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_email_messages, :show),
      description: "Returns the requested email message alongside with attached events",
      parameters: [
        params_path_id()
      ],
      responses: %{
        200 => json_response(Schemas.NotificationEmailMessageResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:notification_email_messages, :retry_email_delivery) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_email_messages, :retry_email_delivery),
      description:
        "Request redelivery and returns the email message alongside with attached events",
      parameters: [
        params_path_id()
      ],
      responses: %{
        200 => json_response(Schemas.NotificationEmailMessageResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        409 => error_conflict()
      }
    )
  end

  def operation(:notification_email_messages, :update_email_address) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_email_messages, :update_email_address),
      description:
        "Updates email_address and returns the email message alongside with attached events",
      parameters: [
        params_path_id()
      ],
      requestBody: request_body_with(params: Schemas.NotificationEmailUpdateEmailParams),
      responses: %{
        200 => json_response(Schemas.NotificationEmailMessageResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:notification_email_webhooks, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_email_webhooks, :index),
      description: "Lists the notification email webhooks",
      parameters: [
        params_page(),
        params_page_size(),
        params_search(),
        params_sorting(
          "NotificationEmailWebhooksSortingParams",
          ~w(status inserted_at created_at)
        )
      ],
      responses: %{
        200 => json_response(Schemas.NotificationEmailWebhookEventPaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden()
      }
    )
  end

  def operation(:notification_email_webhooks, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_email_webhooks, :show),
      description: "Returns the requested email webhook",
      parameters: [
        params_path_id()
      ],
      responses: %{
        200 => json_response(Schemas.NotificationEmailWebhookEventResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  def operation(:notification_email_webhooks, :manually_process) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_email_webhooks, :manually_process),
      description: "Manually processes a webhook entry",
      parameters: [
        params_path_id()
      ],
      requestBody:
        request_body_with(params: Schemas.NotificationEmailWebhookManualHandlingParams),
      responses: %{
        200 => json_response(Schemas.NotificationEmailWebhookEventResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        409 => error_conflict(),
        422 => error_unprocessable()
      }
    )
  end

  def operation(:notification_email_webhooks, :manually_ignore) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_email_webhooks, :manually_ignore),
      description: "Manually mark a webhook entry to be ignored",
      parameters: [
        params_path_id()
      ],
      responses: %{
        200 => json_response(Schemas.NotificationEmailWebhookEventResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        409 => error_conflict()
      }
    )
  end

  def operation(:notification_email_webhooks, :retry_processing) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_email_webhooks, :retry_processing),
      description: "Retries to process mail webhook with requires for manual intervention status",
      responses: %{
        200 => json_response(Schemas.NotificationEmailWebhookRetryProcessingResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        409 => error_conflict()
      }
    )
  end

  def operation(:notification_email_webhooks, :retry_processing_stats) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_email_webhooks, :retry_processing_stats),
      description: "Returns the stats of email webhooks retry processing ",
      responses: %{
        200 => json_response(Schemas.NotificationEmailWebhookRetryProcessingStatsResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found(),
        409 => error_conflict()
      }
    )
  end

  #
  # SMS messages
  # ----------------------------------------------------------------

  def operation(:notification_sms_messages, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_sms_messages, :index),
      description: "Lists the notification sms messages",
      parameters: [
        params_page(),
        params_page_size(),
        params_sorting("NotificationSMSMessagesSortingParams", ~w(inserted_at updated_at))
      ],
      responses: %{
        200 => json_response(Schemas.NotificationSMSMessagePaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden()
      }
    )
  end

  def operation(:notification_sms_messages, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :notification_sms_messages, :show),
      description: "Returns the requested sms message alongside with attached events",
      parameters: [
        params_path_id()
      ],
      responses: %{
        200 => json_response(Schemas.NotificationSMSMessageResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end

  #
  # Webhooks
  # ----------------------------------------------------------------

  def operation(:webhooks, :index) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :webhooks, :index),
      description: "Lists then webhooks",
      parameters: [
        params_page(),
        params_page_size(),
        params_sorting(
          "WebhookSortingParams",
          ~w(event status attempts retry_at delivered_at failed_at inserted_at updated_at)
        ),
        param_in_query(
          name: :filters,
          description: "filtering params",
          schema:
            object(
              title: "WebhookFilteringParams",
              properties: %{
                statuses: array(items: Schemas.Enums.WebhookStatus),
                status: Schemas.Enums.WebhookStatus
              },
              required: []
            )
        )
      ],
      responses: %{
        200 => json_response(Schemas.WebhookPaginatedResponse),
        401 => error_unauthorized(),
        403 => error_forbidden()
      }
    )
  end

  def operation(:webhooks, :show) do
    operation(
      tags: @tags,
      operationId: operation_id(@tags, :webhooks, :show),
      description: "Returns the requested webhook",
      parameters: [
        params_path_id()
      ],
      responses: %{
        200 => json_response(Schemas.WebhookResponse),
        401 => error_unauthorized(),
        403 => error_forbidden(),
        404 => error_not_found()
      }
    )
  end
end
