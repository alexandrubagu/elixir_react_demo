defmodule CXAdmin.API.ApiSpec.Schemas do
  @moduledoc false

  use CXAdmin.APIUtils.OAS.Utils

  defmodule Enums do
    @moduledoc false

    alias CX.Surveys.Survey
    alias CX.Batches.BatchEntry
    alias CX.Invites
    alias CX.Participations
    alias CX.Notifications.Blacklists.BlacklistEntry
    alias CX.Notifications.Emails.Messages
    alias CX.Notifications.Emails.Webhooks
    alias CX.Notifications.SMS
    alias CX.Webhooks.Queue.WebhookStatus
    alias CX.Webhooks.Queue.WebhookLogEntryType

    defenum(Survey.NotificationType, title: "SurveyNotificationType")
    defenum(BatchEntry.BatchEntryStatus, title: "BatchEntryStatus")
    defenum(Invites.Event.Type, title: "InviteEventType")
    defenum(Participations.Participation.Status, title: "ParticipationStatus")
    defenum(Participations.Event.Type, title: "ParticipationEventType")
    defenum(BlacklistEntry.NotificationType, title: "BlacklistEntryNotificationType")
    defenum(BlacklistEntry.BlacklistReason, title: "BlacklistReason")
    defenum(Messages.MessageType, title: "MessageType")
    defenum(Messages.DropReason, title: "DropReason", nullable: true)
    defenum(Messages.DeliveryStatus, title: "DeliveryStatus")
    defenum(Messages.MessageEventType, title: "MessageEventType")
    defenum(Webhooks.EventStatus, title: "WebhookEventStatus")
    defenum(SMS.MessageStatus, title: "SMSMessageStatus")
    defenum(SMS.MessageStatus, title: "SMSMessageEventType")
    defenum(WebhookStatus, title: "WebhookStatus")
    defenum(WebhookLogEntryType, title: "WebhookLogEntryType")
  end

  defmodule Survey do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "Survey",
        description: "Survey",
        properties: %{
          id: uuid(),
          name: string(),
          description: string(nullable: true),
          expire_after: int64(),
          abandonned_after: int64(),
          remind_after: int64(),
          content: object(additionalProperties: true),
          verbatim_questions: array(items: string()),
          notification_types: array(items: Enums.SurveyNotificationType),
          from_email_name: string(nullable: true),
          from_email_address: string(nullable: true),
          tpl_invite: uuid(nullable: true),
          tpl_reminder: uuid(nullable: true),
          tpl_thanks: uuid(nullable: true),
          tpl_abandonned: uuid(nullable: true),
          sms_invite_tpl: string(nullable: true),
          sms_from_number: string(nullable: true),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule CreateSurveyParams do
    @moduledoc false
    schema(
      object(
        title: "CreateSurveyParams",
        description: "The parameters to create a survey",
        properties: %{
          name: string(),
          description: string(nullable: true),
          expire_after: int64(),
          abandonned_after: int64(),
          remind_after: int64(),
          content: object(additionalProperties: true),
          verbatim_questions: array(items: string()),
          notification_types: array(items: Enums.SurveyNotificationType),
          from_email_name: string(nullable: true),
          from_email_address: string(nullable: true),
          tpl_invite: uuid(nullable: true),
          tpl_reminder: uuid(nullable: true),
          tpl_thanks: uuid(nullable: true),
          tpl_abandonned: uuid(nullable: true),
          sms_invite_tpl: string(nullable: true),
          sms_from_number: string(nullable: true)
        }
      )
    )
  end

  defmodule UpdateSurveyParams do
    @moduledoc false
    schema(
      object(
        title: "UpdateSurveyParams",
        description: "The parameters to update a survey",
        properties: %{
          name: string(),
          description: string(nullable: true),
          expire_after: int64(),
          abandonned_after: int64(),
          remind_after: int64(),
          content: object(additionalProperties: true),
          verbatim_questions: array(items: string()),
          notification_types: array(items: Enums.SurveyNotificationType),
          from_email_name: string(nullable: true),
          from_email_address: string(nullable: true),
          tpl_invite: uuid(nullable: true),
          tpl_reminder: uuid(nullable: true),
          tpl_thanks: uuid(nullable: true),
          tpl_abandonned: uuid(nullable: true),
          sms_invite_tpl: string(nullable: true),
          sms_from_number: string(nullable: true)
        },
        required: []
      )
    )
  end

  defmodule SurveyResponse do
    @moduledoc false
    schema(single_response(Survey))
  end

  defmodule SurveyPaginatedResponse do
    @moduledoc false
    schema(paginated_response(Survey))
  end

  defmodule Batch do
    alias CXAdmin.API.ApiSpec.Schemas

    @moduledoc false
    schema(
      object(
        title: "Batch",
        description: "Batch",
        properties: %{
          id: uuid(),
          name: string(),
          log: array(items: Schemas.BatchLog),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule BatchLog do
    alias CX.Batches.Log

    @moduledoc false
    schema(
      object(
        title: "BatchLog",
        description: "BatchLog",
        properties: %{
          id: uuid(),
          event: string(enum: Log.__events__()),
          lvl: string(enum: Log.__levels__()),
          message: string(),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule CreateBatchParams do
    @moduledoc false
    schema(
      object(
        title: "CreateBatchParams",
        description: "The parameters to create a batch",
        properties: %{
          name: string()
        }
      )
    )
  end

  defmodule UpdateBatchParams do
    @moduledoc false
    schema(
      object(
        title: "UpdateBatchParams",
        description: "The parameters to update a batch",
        properties: %{
          name: string()
        },
        required: []
      )
    )
  end

  defmodule BatchJsonImportParams do
    @moduledoc false
    schema(
      object(
        title: "BatchJsonImportParams",
        description: "The parameters to import a json file in a batch",
        properties: %{
          json_file: string(format: :binary)
        }
      )
    )
  end

  defmodule BatchJsonImport do
    @moduledoc false
    schema(
      object(
        title: "BatchJsonImport",
        description: "Successful json import response",
        properties: %{
          count: int32()
        }
      )
    )
  end

  defmodule BatchResponse do
    @moduledoc false
    schema(single_response(Batch))
  end

  defmodule BatchPaginatedResponse do
    @moduledoc false
    schema(paginated_response(Batch))
  end

  defmodule BatchJsonImportResponse do
    @moduledoc false
    schema(single_response(BatchJsonImport))
  end

  defmodule BatchEntry do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas
    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "BatchEntry",
        description: "BatchEntry",
        properties: %{
          id: uuid(),
          notification_type: Enums.SurveyNotificationType,
          status: Enums.BatchEntryStatus,
          params: object(title: "BatchEntryParams", additionalProperties: true),
          ref: string(),
          language: string(),
          email_address: string(nullable: true),
          sms_number: string(nullable: true),
          invite_id: uuid(nullable: true),
          mail_id: uuid(nullable: true),
          sms_id: uuid(nullable: true),
          batch_id: uuid(),
          survey_id: uuid(),
          log: array(items: Schemas.BatchLog),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule CreateBatchEntryParams do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "CreateBatchEntryParams",
        description: "The parameters to create a batch entry",
        properties: %{
          ref: string(),
          language: string(),
          params: object(additionalProperties: true),
          notification_type: Enums.SurveyNotificationType,
          email_address: string(nullable: true),
          sms_number: string(nullable: true)
        }
      )
    )
  end

  defmodule UpdateBatchEntryParams do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "UpdateBatchEntryParams",
        description: "The parameters to update a batch entry",
        properties: %{
          ref: string(),
          language: string(),
          params: object(additionalProperties: true),
          notification_type: Enums.SurveyNotificationType,
          email_address: string(nullable: true),
          sms_number: string(nullable: true)
        },
        required: []
      )
    )
  end

  defmodule BatchEntryResponse do
    @moduledoc false
    schema(single_response(BatchEntry))
  end

  defmodule BatchEntryPaginatedResponse do
    @moduledoc false
    schema(paginated_response(BatchEntry))
  end

  defmodule Invite do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas
    alias CXAdmin.API.ApiSpec.Schemas.Enums

    @properties %{
      id: uuid(),
      batch_id: uuid(nullable: true),
      ref: string(),
      remind_at: date_time(nullable: true),
      language: string(),
      params: object(additionalProperties: true),
      expire_at: date_time(),
      reminder_triggered: boolean(),
      notification_type: Enums.SurveyNotificationType,
      from_email_name: string(nullable: true),
      from_email_address: string(nullable: true),
      email_address: string(nullable: true),
      tpl_invite: uuid(nullable: true),
      tpl_reminder: uuid(nullable: true),
      tpl_thanks: uuid(nullable: true),
      tpl_abandonned: uuid(nullable: true),
      sms_invite_tpl: string(nullable: true),
      sms_dest_number: string(nullable: true),
      sms_from_number: string(nullable: true),
      survey_id: uuid(),
      participation_id: uuid(nullable: true),
      inserted_at: date_time(),
      updated_at: date_time(),
      webhook_id: uuid(nullable: true),
      webhook_params: object(additionalProperties: true, nullable: true),
      webhook_url: string(nullable: true)
    }

    def __properties__(additional), do: Map.merge(@properties, additional)

    schema(
      object(
        title: "Invite",
        description: "Invite",
        properties: @properties
      )
    )
  end

  defmodule InviteDetails do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas

    schema(
      object(
        title: "InviteDetails",
        description: "Invite with details preloaded",
        properties:
          Schemas.Invite.__properties__(%{
            events: array(items: Schemas.InviteEvent),
            smses: array(items: Schemas.NotificationSMSMessage),
            email_messages: array(items: Schemas.NotificationEmailMessage)
          })
      )
    )
  end

  defmodule BrowserInfo do
    @moduledoc false

    schema(
      object(
        title: "BrowserInfo",
        description: "Browser info",
        properties: %{
          device: string(),
          os: string(),
          os_version: string(),
          screen_width: int32(),
          screen_height: int32(),
          browser: string(),
          browser_version: string(),
          browser_useragent: string(),
          browser_width: int32(),
          browser_height: int32()
        }
      )
    )
  end

  defmodule InviteEvent do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas
    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "InviteEvent",
        description: "Invite event",
        properties: %{
          id: uuid(),
          type: Enums.InviteEventType,
          browser_info: Schemas.BrowserInfo,
          invite_id: uuid(),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule CreateMailInviteParams do
    @moduledoc false
    schema(
      object(
        title: "CreateMailInviteParams",
        description: "Params for creating a mail invite",
        properties: %{
          notification_type: string(enum: "mail"),
          email_address: string(),
          language: string(),
          params: object(additionalProperties: true),
          ref: string(),
          webhook_url: string(),
          webhook_params: object(additionalProperties: true)
        },
        required: [
          :notification_type,
          :email_address,
          :language,
          :params,
          :ref
        ]
      )
    )
  end

  defmodule CreateSmsInviteParams do
    @moduledoc false
    schema(
      object(
        title: "CreateSmsInviteParams",
        description: "Params for creating a sms invite",
        properties: %{
          notification_type: string(enum: "sms"),
          sms_dest_number: string(),
          language: string(),
          params: object(additionalProperties: true),
          ref: string(),
          webhook_url: string(),
          webhook_params: object(additionalProperties: true)
        },
        required: [
          :notification_type,
          :sms_dest_number,
          :language,
          :params,
          :ref
        ]
      )
    )
  end

  defmodule CreateNoneInviteParams do
    @moduledoc false
    schema(
      object(
        title: "CreateNoneInviteParams",
        description: "Params for creating a none invite",
        properties: %{
          notification_type: string(enum: "none"),
          language: string(),
          params: object(additionalProperties: true),
          ref: string(),
          webhook_url: string(),
          webhook_params: object(additionalProperties: true)
        },
        required: [
          :notification_type,
          :language,
          :params,
          :ref
        ]
      )
    )
  end

  defmodule CreateInviteParams do
    @moduledoc false
    schema(
      one_of([CreateMailInviteParams, CreateSmsInviteParams, CreateNoneInviteParams],
        title: "CreateInviteParams",
        description: "Params for creating an invite"
      )
    )
  end

  defmodule InviteUpdateEmailParams do
    @moduledoc false
    schema(
      object(
        title: "InviteUpdateEmailParams",
        description: "Params to update an invite email address",
        properties: %{
          email_address: string(),
          expiration_date: date_time()
        },
        required: [:email_address, :expiration_date]
      )
    )
  end

  defmodule InviteResponse do
    @moduledoc false
    schema(single_response(InviteDetails))
  end

  defmodule InvitePaginatedResponse do
    @moduledoc false
    schema(paginated_response(Invite))
  end

  defmodule Participation do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas.Enums

    @properties %{
      id: uuid(),
      status: Enums.ParticipationStatus,
      ref: string(),
      content: object(additionalProperties: true, title: "ParticipationContent"),
      language: string(),
      params: object(additionalProperties: true, title: "ParticipationParams"),
      answers: object(additionalProperties: true, title: "ParticipationAnswers"),
      verbatims_processed: boolean(),
      survey_id: uuid(),
      inserted_at: date_time(),
      updated_at: date_time()
    }

    def __properties__(additional), do: Map.merge(@properties, additional)

    schema(
      object(
        title: "Participation",
        description: "Participation",
        properties: @properties
      )
    )
  end

  defmodule ParticipationWithDetails do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas
    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "ParticipationWithDetails",
        description: "Participation with preloaded details",
        properties:
          Schemas.Participation.__properties__(%{
            events: array(items: Schemas.ParticipationEvent),
            verbatims: array(items: Schemas.Verbatim),
            verbatim_logs: array(items: Schemas.VerbatimLog)
          })
      )
    )
  end

  defmodule ParticipationEvent do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "ParticipationEvent",
        description: "Participation event",
        properties: %{
          id: uuid(),
          data: object(additionalProperties: true, title: "ParticipationEventData"),
          type: Enums.ParticipationEventType,
          participation_id: uuid(),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule Verbatim do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas

    schema(
      object(
        title: "Verbatim",
        description: "Verbatim",
        properties: %{
          id: uuid(),
          question: string(),
          text: string(),
          text_lang: string(),
          text_en: string(),
          sentiment_magnt: float(),
          sentiment_score: float(),
          survey_id: uuid(),
          participation_id: uuid(),
          sentences: array(items: Schemas.Sentence),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule Sentence do
    @moduledoc false

    schema(
      object(
        title: "Sentence",
        description: "Verbatim Sentence",
        properties: %{
          id: uuid(),
          text: string(),
          offset: int32(),
          sentiment_magnt: float(),
          sentiment_score: float(),
          verbatim_id: uuid(),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule VerbatimLog do
    @moduledoc false

    schema(
      object(
        title: "VerbatimLog",
        description: "Verbatim log entry",
        properties: %{
          id: uuid(),
          question: string(nullable: true),
          event: string(),
          lvl: string(),
          message: string(),
          participation_id: uuid(),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule ParticipationSession do
    @moduledoc false

    schema(
      object(
        title: "ParticipationSession",
        description: "Participation realtime session process",
        properties: %{
          participation_id: uuid(),
          pid: string()
        }
      )
    )
  end

  defmodule ProcessVerbatims do
    @moduledoc false

    schema(
      string(
        title: "ProcessVerbatims",
        description: "ProcessVerbatims"
      )
    )
  end

  defmodule VerbatimProcessorLog do
    @moduledoc false

    schema(
      object(
        title: "VerbatimProcessorLog",
        description: "VerbatimProcessorLog",
        properties: %{
          participation: uuid(),
          ts: date_time(),
          status: string()
        }
      )
    )
  end

  defmodule VerbatimProcessorStats do
    @moduledoc false

    schema(
      object(
        title: "VerbatimProcessorStats",
        description: "VerbatimProcessorStats",
        properties: %{
          logs: array(items: VerbatimProcessorLog),
          processed_participation_count: int32(),
          run_count: int32()
        }
      )
    )
  end

  defmodule ProcessVerbatimsResponse do
    @moduledoc false
    schema(single_response(ProcessVerbatims))
  end

  defmodule VerbatimProcessorStatsResponse do
    @moduledoc false
    schema(single_response(VerbatimProcessorStats))
  end

  defmodule ParticipationResponse do
    @moduledoc false
    schema(single_response(ParticipationWithDetails))
  end

  defmodule ParticipationPaginatedResponse do
    @moduledoc false
    schema(paginated_response(Participation))
  end

  defmodule ParticipationSessionsResponse do
    @moduledoc false
    schema(list_response(ParticipationSession))
  end

  defmodule ParticipationSyncerSettings do
    @moduledoc false

    schema(
      object(
        title: "ParticipationSyncerSettings",
        description: "ParticipationSyncerSettings",
        properties: %{
          id: uuid(),
          url: string(),
          interval_sec: int32(),
          invites_after: date_time()
        }
      )
    )
  end

  defmodule ParticipationSyncerState do
    @moduledoc false

    schema(
      object(
        title: "ParticipationSyncerState",
        description: "ParticipationSyncerState",
        properties: %{
          id: uuid(),
          last_run: date_time(),
          participations_after: date_time()
        }
      )
    )
  end

  defmodule ParticipationSyncer do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas

    schema(
      object(
        title: "ParticipationSyncer",
        description: "ParticipationSyncer",
        properties: %{
          id: uuid(),
          name: string(),
          enabled: boolean(),
          settings: ParticipationSyncerSettings,
          state: nullable(ParticipationSyncerState),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule CreateParticipationSyncerParams do
    @moduledoc false
    schema(
      object(
        title: "CreateParticipationSyncerParams",
        description: "The parameters to create a participation syncer",
        properties: %{
          name: string(),
          enabled: boolean(),
          settings: ParticipationSyncerSettings
        }
      )
    )
  end

  defmodule UpdateParticipationSyncerParams do
    @moduledoc false
    schema(
      object(
        title: "UpdateParticipationSyncerParams",
        description: "The parameters to update a participation syncer",
        properties: %{
          settings: ParticipationSyncerSettings
        },
        required: []
      )
    )
  end

  defmodule ParticipationSyncerResponse do
    @moduledoc false
    schema(single_response(ParticipationSyncer))
  end

  defmodule ParticipationSyncerPaginatedResponse do
    @moduledoc false
    schema(paginated_response(ParticipationSyncer))
  end

  defmodule NotificationBlacklistEntry do
    @moduledoc false
    schema(
      object(
        title: "NotificationBlacklistEntry",
        description: "NotificationBlacklistEntry",
        properties: %{
          id: uuid(),
          notification_type: Enums.BlacklistEntryNotificationType,
          key: string(),
          reason: Enums.BlacklistReason,
          webhook_event_id: uuid(nullable: true),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule NotificationBlacklistEntryParams do
    @moduledoc false
    schema(
      object(
        title: "NotificationBlacklistEntryParams",
        description: "The parameters to create a new blacklist entry",
        properties: %{
          notification_type: Enums.BlacklistEntryNotificationType,
          key: string(),
          reason: Enums.BlacklistReason,
          webhook_event_id: uuid()
        },
        required: [:notification_type, :key, :reason]
      )
    )
  end

  defmodule NotificationBlacklistEntryResponse do
    @moduledoc false
    schema(single_response(NotificationBlacklistEntry))
  end

  defmodule NotificationBlacklistPaginatedResponse do
    @moduledoc false
    schema(paginated_response(NotificationBlacklistEntry))
  end

  defmodule NotificationEmailMessageEvent do
    @moduledoc false
    schema(
      object(
        title: "NotificationEmailMessageEvent",
        description: "NotificationEmailMessageEvent",
        properties: %{
          id: uuid(),
          type: Enums.MessageEventType,
          drop_reason: Enums.DropReason,
          info: object(properties: %{}, additionalProperties: true, nullable: true),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule NotificationEmailMessage do
    @moduledoc false
    schema(
      object(
        title: "NotificationEmailMessage",
        description: "NotificationEmailMessage",
        properties: %{
          id: uuid(),
          invite_id: uuid(),
          from_name: string(),
          from_address: string(format: :email),
          to_address: string(format: :email),
          type: Enums.MessageType,
          delivery_status: Enums.DeliveryStatus,
          delivery_drop_reason: Enums.DropReason,
          was_opened: boolean(),
          was_unsubscribed: boolean(),
          was_reported_as_spam: boolean(),
          template_id: uuid(),
          template_params: object(properties: %{}, additionalProperties: true),
          events: array(items: NotificationEmailMessageEvent),
          inserted_at: date_time(),
          updated_at: date_time()
        },
        required: [
          :id,
          :invite_id,
          :from_name,
          :from_address,
          :to_address,
          :type,
          :delivery_status,
          :delivery_drop_reason,
          :was_opened,
          :was_unsubscribed,
          :was_reported_as_spam,
          :template_id,
          :template_params,
          :inserted_at,
          :updated_at
        ]
      )
    )
  end

  defmodule NotificationEmailUpdateEmailParams do
    @moduledoc false
    schema(
      object(
        title: "NotificationEmailUpdateEmailParams",
        description: "Params to update a mail message email address",
        properties: %{
          email_address: string()
        },
        required: [:email_address]
      )
    )
  end

  defmodule NotificationEmailMessagePaginatedResponse do
    @moduledoc false
    schema(paginated_response(NotificationEmailMessage))
  end

  defmodule NotificationEmailMessageResponse do
    @moduledoc false
    schema(single_response(NotificationEmailMessage))
  end

  defmodule NotificationEmailWebhookEvent do
    @moduledoc false
    schema(
      object(
        title: "NotificationEmailWebhookEvent",
        description: "NotificationEmailWebhookEvent",
        properties: %{
          id: uuid(),
          data: object(properties: %{}, additionalProperties: true),
          status: Enums.WebhookEventStatus,
          auto_processing_error: string(nullable: true),
          search: string(),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule NotificationEmailWebhookManualHandlingParams do
    @moduledoc false
    schema(
      object(
        title: "NotificationEmailWebhookManualHandlingParams",
        description: "Params to manually handle a webhook event",
        properties: %{
          mail_id: uuid(),
          event_type: Enums.MessageEventType,
          drop_reason: nullable(Enums.DropReason)
        },
        required: [:mail_id, :event_type]
      )
    )
  end

  defmodule NotificationEmailWebhookEventResponse do
    @moduledoc false
    schema(single_response(NotificationEmailWebhookEvent))
  end

  defmodule NotificationEmailWebhookRetryProcessing do
    @moduledoc false
    schema(
      string(
        title: "NotificationEmailWebhookRetryProcessing",
        description: "NotificationEmailWebhookRetryProcessing"
      )
    )
  end

  defmodule NotificationEmailWebhookRetryProcessingResponse do
    @moduledoc false
    schema(single_response(NotificationEmailWebhookRetryProcessing))
  end

  defmodule NotificationEmailWebhookRetryProcessingLog do
    @moduledoc false
    schema(
      object(
        title: "NotificationEmailWebhookRetryProcessingLog",
        description: "NotificationEmailWebhookRetryProcessingLog",
        properties: %{
          at: date_time(),
          resolved: int32(),
          unresolved: int32()
        }
      )
    )
  end

  defmodule NotificationEmailWebhookRetryProcessingStats do
    @moduledoc false
    schema(
      object(
        title: "NotificationEmailWebhookRetryProcessingStats",
        description: "NotificationEmailWebhookRetryProcessingStats",
        properties: %{
          status: string(),
          logs: array(items: NotificationEmailWebhookRetryProcessingLog)
        }
      )
    )
  end

  defmodule NotificationEmailWebhookRetryProcessingStatsResponse do
    @moduledoc false
    schema(single_response(NotificationEmailWebhookRetryProcessingStats))
  end

  defmodule NotificationEmailWebhookEventPaginatedResponse do
    @moduledoc false
    schema(paginated_response(NotificationEmailWebhookEvent))
  end

  defmodule NotificationSMSMessage do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas
    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "NotificationSMSMessage",
        description: "Notification SMS Message",
        properties: %{
          id: uuid(),
          statuses: array(items: Enums.SMSMessageStatus),
          invite_id: uuid(),
          events: array(items: Schemas.NotificationSMSMessageEvent),
          inserted_at: date_time(),
          updated_at: date_time()
        },
        required: [
          :id,
          :statuses,
          :invite_id,
          :inserted_at,
          :updated_at
        ]
      )
    )
  end

  defmodule NotificationSMSMessageEvent do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "NotificationSMSMessageEvent",
        description: "Notification SMS Message Event",
        properties: %{
          id: uuid(),
          type: Enums.SMSMessageEventType,
          data:
            object(
              properties: %{},
              additionalProperties: true,
              title: "NotificationSMSMessageEventData"
            ),
          sms_id: uuid(),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule NotificationSMSMessagePaginatedResponse do
    @moduledoc false
    schema(paginated_response(NotificationSMSMessage))
  end

  defmodule NotificationSMSMessageResponse do
    @moduledoc false
    schema(single_response(NotificationSMSMessage))
  end

  defmodule WebhookLogEntry do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "Webhook",
        description: "Webhook",
        properties: %{
          id: uuid(),
          type: Enums.WebhookLogEntryType,
          info: object(additionalProperties: true),
          inserted_at: date_time()
        }
      )
    )
  end

  defmodule Webhook do
    @moduledoc false

    alias CXAdmin.API.ApiSpec.Schemas.Enums

    schema(
      object(
        title: "Webhook",
        description: "Webhook",
        properties: %{
          id: uuid(),
          url: string(),
          data: object(additionalProperties: true),
          event: string(),
          status: Enums.WebhookStatus,
          attempts: int32(),
          retry_at: date_time(nullable: true),
          delivered_at: date_time(nullable: true),
          failed_at: date_time(nullable: true),
          log: array(items: WebhookLogEntry),
          inserted_at: date_time(),
          updated_at: date_time()
        }
      )
    )
  end

  defmodule WebhookPaginatedResponse do
    @moduledoc false
    schema(paginated_response(Webhook))
  end

  defmodule WebhookResponse do
    @moduledoc false
    schema(single_response(Webhook))
  end
end
