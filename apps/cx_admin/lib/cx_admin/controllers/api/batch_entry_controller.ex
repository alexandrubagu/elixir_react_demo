defmodule CXAdmin.API.BatchEntryController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.Surveys
  alias CX.Batches

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action), do: ApiSpec.operation(:batch_entries, action)

  def index(conn, %{"batch_id" => batch_id}) do
    with {:ok, batch} <- Batches.get_batch(batch_id),
         {:ok, page} <- Batches.list_batch_entries(batch, conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def create(conn, %{"batch_id" => batch_id, "survey_id" => survey_id, "entry" => params}) do
    with {:ok, batch} <- Batches.get_batch(batch_id),
         {:ok, survey} <- Surveys.get_survey(survey_id),
         {:ok, entry} <- Batches.create_batch_entry(batch, survey, params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.api_batch_entry_path(conn, :show, entry))
      |> render("show.json", entry: entry)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, entry} <- Batches.get_batch_entry(id) do
      render(conn, "show.json", entry: entry)
    end
  end

  def update(conn, %{"id" => id, "entry" => params}) do
    with {:ok, entry} <- Batches.get_batch_entry(id),
         {:ok, updated} <- Batches.update_batch_entry(entry, params) do
      render(conn, "show.json", entry: updated)
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, entry} <- Batches.get_batch_entry(id),
         {:ok, _deleted} <- Batches.delete_batch_entry(entry) do
      send_resp(conn, :no_content, "")
    end
  end

  def process(conn, %{"id" => id}) do
    with {:ok, entry} <- Batches.get_batch_entry(id),
         {:ok, processed} <- Batches.process_batch_entry(entry) do
      render(conn, "show.json", entry: processed)
    else
      {:error, reason} when is_binary(reason) ->
        CX.Errors.conflict(
          context: :batches,
          resource: :batch_entry,
          reason: :processing_problem,
          details: [reason: reason]
        )

      error ->
        error
    end
  end
end
