defmodule CXAdmin.API.BatchController do
  @moduledoc false

  use CXAdmin, :controller
  use CXAdmin.FilteringParams

  alias CXAdmin.API.ApiSpec
  alias CX.Batches

  action_fallback CXAdmin.APIUtils.FallbackController

  def open_api_operation(action), do: ApiSpec.operation(:batches, action)

  def index(conn, _params) do
    with {:ok, page} <- Batches.list_batches(conn.assigns) do
      render(conn, "index.json", page: page)
    end
  end

  def create(conn, %{"batch" => params}) do
    with {:ok, batch} <- Batches.create_batch(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.api_batch_path(conn, :show, batch))
      |> render("show.json", batch: batch)
    end
  end

  def show(conn, %{"id" => id}) do
    with {:ok, batch} <- Batches.get_batch(id) do
      render(conn, "show.json", batch: batch)
    end
  end

  def update(conn, %{"id" => id, "batch" => params}) do
    with {:ok, batch} <- Batches.get_batch(id),
         {:ok, updated} <- Batches.update_batch(batch, params) do
      render(conn, "show.json", batch: updated)
    end
  end

  def delete(conn, %{"id" => id}) do
    with {:ok, batch} <- Batches.get_batch(id),
         {:ok, _deleted} <- Batches.delete_batch(batch) do
      send_resp(conn, :no_content, "")
    end
  end

  def process(conn, %{"id" => id}) do
    with {:ok, batch} <- Batches.get_batch(id),
         {:ok, processed} <- Batches.process_batch(batch) do
      render(conn, "show.json", batch: processed)
    else
      {:error, reason} when is_binary(reason) ->
        CX.Errors.conflict(
          context: :batches,
          resource: :batch,
          reason: :processing_error,
          details: [reason: reason]
        )

      error ->
        error
    end
  end

  def import_json(conn, %{"id" => id, "json_file" => %Plug.Upload{path: json_file}}) do
    with {:ok, batch} <- Batches.get_batch(id),
         {:ok, count} <- Batches.import_batch_entries(batch, json_file) do
      render(conn, "json_batch.json", count: count)
    else
      {:error, reason} when is_binary(reason) ->
        CX.Errors.conflict(
          context: :batches,
          resource: :batch,
          reason: :json_import_problem,
          details: [reason: reason]
        )

      error ->
        error
    end
  end
end
