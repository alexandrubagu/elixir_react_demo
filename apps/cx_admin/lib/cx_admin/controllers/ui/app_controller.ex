defmodule CXAdmin.UI.AppController do
  @moduledoc false

  use CXAdmin, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def redirect_to_ui(conn, _params) do
    redirect(conn, to: "/cx/admin/ui")
  end
end
