defmodule CXAdmin.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use CXAdmin, :controller

  alias CX.Errors

  def call(conn, {:error, %Errors.NotFoundError{}}) do
    conn
    |> put_status(:not_found)
    |> put_view(CXAdmin.ErrorView)
    |> render(:"404")
  end
end
