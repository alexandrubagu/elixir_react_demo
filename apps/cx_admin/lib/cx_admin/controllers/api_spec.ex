defmodule CXAdmin.ApiSpec do
  @moduledoc false
  @behaviour OpenApiSpex.OpenApi

  alias OpenApiSpex.{OpenApi, Server, Info, Paths}
  alias CXAdmin.{Endpoint, Router}
  alias CXAdmin.APIUtils.OAS.Utils

  @impl OpenApi
  def spec do
    OpenApiSpex.resolve_schema_modules(open_api())
  end

  defp open_api do
    %OpenApi{
      servers: [
        Server.from_endpoint(Endpoint)
      ],
      info: %Info{
        title: "Intersoft One platform",
        version: "1.0"
      },
      security: [],
      paths: Router |> Paths.from_router() |> Utils.remove_duplicated_patch_operations()
    }
  end
end
