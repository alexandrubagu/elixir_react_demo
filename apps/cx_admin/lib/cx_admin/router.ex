defmodule CXAdmin.Router do
  use CXAdmin, :router

  pipeline :admin_ui do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :put_layout, {CXAdmin.LayoutView, "admin-ui.html"}
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :api_spec do
    plug :accepts, ["json"]
    plug OpenApiSpex.Plug.PutApiSpec, module: CXAdmin.ApiSpec
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/cx/admin/api/schema.json" do
    pipe_through :api_spec

    get "/", OpenApiSpex.Plug.RenderSpec, []
  end

  scope "/cx/admin/api/swaggerui" do
    pipe_through :browser

    get "/", OpenApiSpex.Plug.SwaggerUI, path: "/cx/admin/api/schema.json"
  end

  scope "/cx/admin/api", CXAdmin.API, as: :api do
    pipe_through :api

    get "/demo/verbatim-categorization/count-topic-selections",
        DemoController,
        :count_topic_selections

    get "/demo/verbatim-categorization/top-100-word-cloud",
        DemoController,
        :get_top_100_word_cloud

    get "/demo/latest-verbatim-categorizations", DemoController, :index

    resources "/surveys", SurveyController, except: [:new, :edit, :delete]
    put "/surveys/:id/toggle-delete", SurveyController, :toggle_delete

    resources "/batches", BatchController, except: [:new, :edit]
    put "/batches/:id/process", BatchController, :process
    post "/batches/:id/import-json", BatchController, :import_json

    get "/batches/:batch_id/batch-entries", BatchEntryController, :index
    post "/batch-entries/:batch_id/:survey_id", BatchEntryController, :create
    get "/batch-entries/:id", BatchEntryController, :show
    delete "/batch-entries/:id", BatchEntryController, :delete
    put "/batch-entries/:id", BatchEntryController, :update
    put "/batch-entries/:id/process", BatchEntryController, :process

    resources "/invites", InviteController, only: [:index, :show]
    post "/invites/:survey_name", InviteController, :create_and_send, as: :invite
    put "/invites/:id/update-email-address", InviteController, :update_email_address, as: :invite

    get "/participations/sessions", ParticipationController, :sessions
    resources "/participations", ParticipationController, only: [:index, :show]
    post "/participations/verbatims/process", ParticipationController, :process_verbatims

    get "/participations/verbatims/processor-stats",
        ParticipationController,
        :verbatim_processor_stats

    resources "/participation-syncers", ParticipationSyncerController, except: [:new, :edit]
    get "/participation-syncers/:id/enable", ParticipationSyncerController, :enable
    get "/participation-syncers/:id/disable", ParticipationSyncerController, :disable

    scope "/notifications", Notifications do
      scope "/blacklists", Blacklists do
        resources "/entries", BlacklistEntryController, except: [:new, :edit]
      end

      scope "/emails", Emails do
        get "/messages", MessageController, :index, as: :email_message
        get "/messages/:id", MessageController, :show, as: :email_message

        get "/messages/:id/retry-email-delivery", MessageController, :retry_email_delivery,
          as: :email_message

        put "/messages/:id/update-email-address", MessageController, :update_email_address,
          as: :email_message

        get "/webhooks/events/retry-processing", WebhookController, :retry_processing,
          as: :email_webhook_event

        get "/webhooks/events/retry-processing-stats", WebhookController, :retry_processing_stats,
          as: :email_webhook_event

        get "/webhooks/events/:id", WebhookController, :show, as: :email_webhook_event

        put "/webhooks/events/:id/manually-process", WebhookController, :manually_process,
          as: :email_webhook_event

        put "/webhooks/events/:id/manually-ignore", WebhookController, :manually_ignore,
          as: :email_webhook_event

        get "/webhooks/events", WebhookController, :index, as: :email_webhook_event
      end

      scope "/sms", SMS do
        get "/messages", MessageController, :index, as: :sms_message
        get "/messages/:id", MessageController, :show, as: :sms_message
      end
    end

    get "/webhooks/:id", WebhookController, :show
    get "/webhooks", WebhookController, :index
  end

  scope "/cx/admin", CXAdmin.UI do
    pipe_through :admin_ui

    get "/ui/*anything", AppController, :index
    get "/", AppController, :redirect_to_ui
  end
end
