defmodule CXAdmin.SentryEventFilter do
  @moduledoc false

  @behaviour Sentry.EventFilter

  @doc """
  These are the exceptions we want to ignore
  """
  def exclude_exception?(%Phoenix.ActionClauseError{}, :plug), do: true
  def exclude_exception?(%Phoenix.Router.NoRouteError{}, :plug), do: true
  def exclude_exception?(%Ecto.Query.CastError{}, :plug), do: true
  def exclude_exception?(%Ecto.CastError{}, :plug), do: true
  def exclude_exception?(_exception, _source), do: false
end
