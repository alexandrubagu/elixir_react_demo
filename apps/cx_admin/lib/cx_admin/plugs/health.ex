defmodule CXAdmin.Plugs.Health do
  @moduledoc false

  import Plug.Conn

  def init(opts), do: opts

  def call(%Plug.Conn{request_path: "/cx/admin/_healthz"} = conn, _opts) do
    conn
    |> put_resp_content_type("application/json")
    |> send_resp(200, ~S({"status": "ok"}))
    |> halt
  end

  def call(conn, _opts) do
    conn
  end
end
