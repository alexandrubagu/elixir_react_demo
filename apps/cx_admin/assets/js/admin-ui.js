import '../css/admin-ui.scss';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './admin-ui/App';

ReactDOM.render(<App />, document.getElementById('root'));
