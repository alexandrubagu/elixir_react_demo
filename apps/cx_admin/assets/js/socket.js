import { Socket } from "phoenix"

export const socket = new Socket("/cx/admin/socket", {});
socket.connect();

export const statsChannel = socket.channel("stats", {})
statsChannel.join()
  .receive("ok", resp => { console.log("Joined stats chan") })
  .receive("error", resp => { console.log("Unable to join stats chan") });