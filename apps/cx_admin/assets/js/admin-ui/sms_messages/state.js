import * as api from './api';
import * as common from '../reducers/common';

const SMS_MESSAGES_FETCHED = 'SMS_MESSAGES_FETCHED';
const SMS_MESSAGES_REQUEST_PENDING = 'SMS_MESSAGES_REQUEST_PENDING';
const SMS_MESSAGES_REQUEST_ERROR = 'SMS_MESSAGES_REQUEST_ERROR';
const SET_SMS_MESSAGES_PAGINATION = 'SET_SMS_MESSAGES_PAGINATION';
const SET_SMS_MESSAGES_SORTING = 'SET_SMS_MESSAGES_SORTING';
const SET_SMS_MESSAGES_SEARCH = 'SET_SMS_MESSAGES_SEARCH';
const SMS_MESSAGE_FETCHED = 'SMS_MESSAGE_FETCHED';

const pendingRequest = () => ({
  type: SMS_MESSAGES_REQUEST_PENDING,
});

const requestFailed = (error) => ({
  type: SMS_MESSAGES_REQUEST_ERROR,
  payload: error,
});

export const Actions = {
  getSmsMessages: () => (dispatch, getState) => {
    const { pagination, sorting, search } = getState().SmsMessages;

    dispatch(pendingRequest());
    return api.getSmsMessages({ pagination, sorting, search }).then(
      (response) => dispatch({ type: SMS_MESSAGES_FETCHED, payload: response }),
      (error) => dispatch(requestFailed(error)),
    );
  },

  getSmsMessage: (id) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.getSmsMessage(id).then(
        (response) =>
          dispatch({ type: SMS_MESSAGE_FETCHED, payload: response.data }),
        (error) => dispatch(errorRequest(error)),
      );
    };
  },

  setPagination: (pagination) => ({
    type: SET_SMS_MESSAGES_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorting) => {
    return {
      type: SET_SMS_MESSAGES_SORTING,
      payload: sorting,
    };
  },

  setSearch: (search) => ({
    type: SET_SMS_MESSAGES_SEARCH,
    payload: search,
  }),
};

const initialState = {
  loading: false,
  pagination: {},
  sorting: {},
  search: '',
  data: [],
  current: {},
  error: {},
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case SMS_MESSAGES_REQUEST_PENDING:
      return { ...state, loading: true };

    case SMS_MESSAGES_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_SMS_MESSAGES_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_SMS_MESSAGES_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_SMS_MESSAGES_SEARCH:
      return common.updateSearch(state, action.payload);

    case SMS_MESSAGES_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case SMS_MESSAGE_FETCHED:
      return { ...state, loading: false, current: action.payload };

    default:
      return state;
  }
};
