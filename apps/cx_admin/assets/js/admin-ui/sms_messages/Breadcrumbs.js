import { breadcrumbs, shortenUUID } from '../components/utils';

const listMessages = {
  path: '/cx/admin/ui/sms/messages',
  breadcrumbName: 'Sms Messages',
};

const showMessage = ({ message }) => ({
  path: `/cx/admin/ui/sms/messages/${message.id}`,
  breadcrumbName: shortenUUID(message.id),
});

export default {
  listMessages: breadcrumbs([listMessages]),
  showMessage: breadcrumbs([listMessages, showMessage]),
};
