import Utils from '../api/utils';

export function getSmsMessages(params) {
  return Utils.request.get('/cx/admin/api/notifications/sms/messages', params);
}

export function getSmsMessage(id) {
  return Utils.request.get(`/cx/admin/api/notifications/sms/messages/${id}`);
}
