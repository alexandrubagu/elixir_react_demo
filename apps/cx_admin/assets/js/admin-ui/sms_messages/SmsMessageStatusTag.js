import React from 'react';
import { Tag } from 'antd';

const color = (status) => {
  switch (status) {
    case 'delivered':
      return 'blue';

    case 'queued':
      return 'blue';

    case 'sent':
      return 'green';

    case 'bounced':
      return 'red';

    case 'undelivered':
      return 'magenta';

    case 'unsubscribed':
      return 'volcano';

    case 'failed':
      return 'red';
  }
};

const SmsMessageStatusTag = (props) => {
  return <Tag color={color(props.status)}>{props.status}</Tag>;
};

export default SmsMessageStatusTag;
