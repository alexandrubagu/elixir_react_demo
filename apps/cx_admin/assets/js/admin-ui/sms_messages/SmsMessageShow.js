import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Card, Layout, Tag, PageHeader, Row, Col } from 'antd';
import { Actions } from './state';
import Breadcrumbs from './Breadcrumbs';
import SmsMessage from './SmsMessage';
import SmsMessageEventTimeline from './SmsMessageEventTimeline';

const SmsMessageShow = (props) => {
  const [breadcrumbs, setBreadcrumbs] = useState([]);

  useEffect(() => {
    props.getSmsMessage(props.match.params.id).then(() => {
      setBreadcrumbs(Breadcrumbs.showMessage({ message: props.current }));
    });
  }, []);

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Sms message details"
          subTitle={`(${props.current.id || ''})`}
          breadcrumb={breadcrumbs}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Row gutter={[16, 24]}>
          <Col lg={{ span: 14 }} md={{ span: 24 }}>
            <Card title="General">
              <SmsMessage message={props.current} />
            </Card>
          </Col>
          <Col lg={{ span: 10 }} md={{ span: 24 }}>
            <Card title="Event timeline">
              <SmsMessageEventTimeline events={props.current.events} />
            </Card>
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  return {
    current: state.SmsMessages.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSmsMessage: (id) => dispatch(Actions.getSmsMessage(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SmsMessageShow);
