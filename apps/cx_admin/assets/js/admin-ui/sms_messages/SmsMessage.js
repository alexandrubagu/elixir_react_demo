import React from 'react';
import { Descriptions } from 'antd';
import { UUIDLink } from '../components/utils';
import Moment from 'react-moment';
import SmsMessageStatusTag from './SmsMessageStatusTag';

const SmsMessage = (props) => {
  return (
    <Descriptions bordered column={1}>
      <Descriptions.Item label="InviteID">
        <UUIDLink prefix="/cx/admin/ui/invites/" id={props.message.invite_id} />
      </Descriptions.Item>

      <Descriptions.Item label="Created">
        <Moment fromNow>{props.message.inserted_at}</Moment>
      </Descriptions.Item>

      <Descriptions.Item label="Last updated">
        <Moment fromNow>{props.message.updated_at}</Moment>
      </Descriptions.Item>
    </Descriptions>
  );
};

export default SmsMessage;
