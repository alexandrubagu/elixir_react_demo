import React from 'react';
import { Tag, Timeline, Row, Col, Typography, Card, Empty } from 'antd';
import SmsMessageStatusTag from './SmsMessageStatusTag';
import Moment from 'react-moment';
import { JSONCode } from '../components/utils';

const SmsMessageEventTimeline = (props) => {
  const events = props.events || [];

  if (events.length == 0) {
    return <Empty description="No events yet" />;
  } else {
    return (
      <Timeline>
        {events.map((event) => (
          <EventTimelineItem event={event} key={event.id} />
        ))}
      </Timeline>
    );
  }
};

const EventTimelineItem = (props) => {
  const event = props.event;

  return (
    <Timeline.Item key={event.id}>
      <Row>
        <Col>
          <SmsMessageStatusTag status={event.type} />
        </Col>
        <Col>
          <Typography.Text disabled>
            <Moment fromNow>{event.inserted_at}</Moment>
          </Typography.Text>
        </Col>
      </Row>
      <Card bordered={false}>
        <JSONCode json={event.data} />
      </Card>
    </Timeline.Item>
  );
};

export default SmsMessageEventTimeline;
