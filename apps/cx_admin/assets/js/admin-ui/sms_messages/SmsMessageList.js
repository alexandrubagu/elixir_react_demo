import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Layout, Card, PageHeader, Table, Form, Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { Actions } from './state';
import { UUIDLink } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';
import Moment from 'react-moment';
import SmsMessageStatusTag from './SmsMessageStatusTag';

const columns = [
  {
    dataIndex: 'id',
    title: 'ID',
    render: (id) => <UUIDLink id={id} prefix="/cx/admin/ui/sms/messages/" />,
  },
  {
    dataIndex: 'invite_id',
    title: 'Invite ID',
    render: (id) => <UUIDLink id={id} prefix="/cx/admin/ui/invites/" />,
  },
  {
    dataIndex: 'statuses',
    title: 'Status',
    render: (statuses) => <SmsMessageStatusTag status={status[0]} />,
  },
  {
    title: 'Last updated',
    dataIndex: 'updated_at',
    sorter: true,
    render: (text) => <Moment fromNow>{text}</Moment>,
  },
];

const SmsMessageList = (props) => {
  useEffect(() => {
    props.getSmsMessages();
  }, []);

  const handleSearch = (e) => {
    Promise.resolve(props.setSearch(e.target.value)).then(props.getSmsMessages);
  };

  const handleTableChange = (pagination, filters, sorter) => {
    Promise.all([
      props.setPagination(pagination),
      props.setSorting(sorter),
    ]).then(props.getSmsMessages);
  };

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Sms message listing"
          breadcrumb={Breadcrumbs.listMessages()}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Card style={{ marginBottom: 24 }}>
          <Form layout="inline">
            <Form.Item key="1" style={{ width: '100%' }}>
              <Input
                prefix={<SearchOutlined className="site-form-item-icon" />}
                placeholder="Search"
                onChange={handleSearch}
              />
            </Form.Item>
          </Form>
        </Card>

        <Card bodyStyle={{ padding: 0 }}>
          <Table
            columns={columns}
            dataSource={props.data}
            pagination={props.pagination}
            loading={props.loading}
            rowKey={(record) => record.id}
            onChange={handleTableChange}
          />
        </Card>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    data: state.SmsMessages.data,
    pagination: state.SmsMessages.pagination,
    loading: state.SmsMessages.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSmsMessages: () => dispatch(Actions.getSmsMessages()),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSearch: (search) => dispatch(Actions.setSearch(search)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SmsMessageList);
