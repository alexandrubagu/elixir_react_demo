import React, { useState, useEffect } from 'react';
import { Form, Button, Input, Switch, Card } from 'antd';
import { errorsAsLookup, getError, isEmpty } from '../components/utils';
import moment from 'moment';

const layout = { labelCol: { span: 7 }, wrapperCol: { span: 18 } };
const tailLayout = { wrapperCol: { span: 24 } };

const ParticipationSyncerForm = (props) => {
  const [form] = Form.useForm();
  const errorsLookup = errorsAsLookup(props.error);
  const [submitted, setSubmitted] = useState(false);
  const [allowEditing, setAllowEditing] = useState(true);

  useEffect(() => {
    if (!isEmpty(props.current)) {
      setAllowEditing(false);
      props.current.settings.invites_after = moment(
        props.current.settings.invites_after,
      ).format('YYYY-MM-DDTHH:mm');
      form.setFieldsValue(props.current);
    }
  }, [props.current]);

  const handleSubmit = (data) => {
    props.onSubmit(data).then(() => setSubmitted(true));
  };

  return (
    <Form {...layout} form={form} onFinish={handleSubmit}>
      <Card title="General">
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true }]}
          {...getError(errorsLookup, 'name', submitted)}
        >
          <Input disabled={!allowEditing} />
        </Form.Item>

        <Form.Item name="enabled" label="Enable" valuePropName="checked">
          <Switch disabled={!allowEditing} />
        </Form.Item>
      </Card>
      <Card title="Settings" style={{ marginTop: 24 }}>
        <Form.Item
          {...tailLayout}
          label="URL"
          name={['settings', 'url']}
          rules={[{ required: true }]}
          {...getError(errorsLookup, 'settings.url', submitted)}
        >
          <Input />
        </Form.Item>

        <Form.Item
          {...tailLayout}
          label="Interval Sec"
          name={['settings', 'interval_sec']}
          rules={[{ required: true }]}
          {...getError(errorsLookup, 'settings.interval_sec', submitted)}
        >
          <Input />
        </Form.Item>

        <Form.Item
          {...tailLayout}
          label="Invites After"
          name={['settings', 'invites_after']}
          rules={[{ required: true }]}
          {...getError(errorsLookup, 'settings.invites_after', submitted)}
        >
          <Input type="datetime-local" />
        </Form.Item>
      </Card>

      <Button type="primary" htmlType="submit">
        Submit
      </Button>
    </Form>
  );
};

export default ParticipationSyncerForm;
