import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Descriptions, Modal, Layout, PageHeader, Card, Button } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Actions } from './state';
import Breadcrumbs from './Breadcrumbs';
import Moment from 'react-moment';
import { success_notification } from '../components/utils';

const UpdateButton = (props) => (
  <Button
    key="edit"
    type="primary"
    onClick={() =>
      props.history.push(
        `/cx/admin/ui/participation-syncers/${props.current.id}/update`,
      )
    }
  >
    Edit
  </Button>
);

const ToggleButton = (props) =>
  props.current.enabled ? (
    <DisableButton {...props} />
  ) : (
    <EnableButton {...props} />
  );

const DisableButton = (props) => (
  <Button
    type="warning"
    onClick={() =>
      Modal.confirm({
        title: `Do you want to disable this syncer ?`,
        icon: <ExclamationCircleOutlined />,
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: () =>
          props
            .disableParticipationSyncer(props.current.id)
            .then(() => success_notification('Syncer was disabled!'))
            .then(() => props.getParticipationSyncer(props.current.id)),
      })
    }
  >
    Disable
  </Button>
);

const EnableButton = (props) => (
  <Button
    type="warning"
    onClick={() =>
      Modal.confirm({
        title: `Do you want to enable this syncer ?`,
        icon: <ExclamationCircleOutlined />,
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: () =>
          props
            .enableParticipationSyncer(props.current.id)
            .then(() => success_notification('Syncer was enabled!'))
            .then(() => props.getParticipationSyncer(props.current.id)),
      })
    }
  >
    Enable
  </Button>
);

const DeleteButton = (props) => (
  <Button
    type="danger"
    onClick={() =>
      Modal.confirm({
        title: 'Confirm deletion',
        content: 'Are you sure you want to delete this ?',
        icon: <ExclamationCircleOutlined />,
        okText: 'Yes',
        okType: 'danger',
        cancelText: 'No',
        onOk: () =>
          props
            .deleteParticipationSyncer(props.current.id)
            .then(() => success_notification('Syncer was deleted!'))
            .then(() =>
              this.props.history.push('/cx/admin/ui/participation-syncers'),
            ),
      })
    }
  >
    Delete
  </Button>
);

const ParticipationSyncerShow = (props) => {
  const [breadcrumbs, setBreadcrumbs] = useState([]);

  useEffect(() => {
    props.getParticipationSyncer(props.match.params.id).then(() => {
      setBreadcrumbs(
        Breadcrumbs.showParticipationSyncer({
          syncer: props.current,
        }),
      );
    });
  }, []);

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="ParticipationSyncer details"
          subTitle={`(${props.current.id || ''})`}
          breadcrumb={breadcrumbs}
          extra={[
            <UpdateButton {...props} />,
            <ToggleButton {...props} />,
            <DeleteButton {...props} />,
          ]}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Card title="General" key="general">
          <Descriptions column={2} bordered>
            <Descriptions.Item label="Name" span={2}>
              {props.current.name}
            </Descriptions.Item>

            <Descriptions.Item label="Enabled" span={2}>
              {props.current.enabled ? 'Yes' : 'No'}
            </Descriptions.Item>

            <Descriptions.Item label="Last updated" span={2}>
              <Moment fromNow={props.current.updated_at} />
            </Descriptions.Item>
          </Descriptions>
        </Card>

        {props.current.settings ? (
          <Card title="Settings" key="settings" style={{ marginTop: 24 }}>
            <Descriptions column={2} bordered>
              <Descriptions.Item label="URL" span={2}>
                {props.current.settings.url}
              </Descriptions.Item>

              <Descriptions.Item label="Interval Sec" span={2}>
                {props.current.settings.interval_sec}
              </Descriptions.Item>

              <Descriptions.Item label="Invites After" span={2}>
                {props.current.settings.invites_after}
              </Descriptions.Item>
            </Descriptions>
          </Card>
        ) : null}

        {props.current.state ? (
          <Card title="State" key="state" style={{ marginTop: 24 }}>
            <Descriptions column={2} bordered>
              <Descriptions.Item label="Last Run" span={2}>
                {props.current.state.last_run}
              </Descriptions.Item>

              <Descriptions.Item label="Participations After" span={2}>
                {props.current.state.participations_after}
              </Descriptions.Item>
            </Descriptions>
          </Card>
        ) : null}
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  return {
    current: state.ParticipationSyncers.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getParticipationSyncer: (id) =>
      dispatch(Actions.getParticipationSyncer(id)),
    deleteParticipationSyncer: (id) =>
      dispatch(Actions.deleteParticipationSyncer(id)),
    enableParticipationSyncer: (id) =>
      dispatch(Actions.enableParticipationSyncer(id)),
    disableParticipationSyncer: (id) =>
      dispatch(Actions.disableParticipationSyncer(id)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ParticipationSyncerShow);
