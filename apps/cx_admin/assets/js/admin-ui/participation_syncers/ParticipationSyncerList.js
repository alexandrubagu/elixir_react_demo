import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import {
  Row,
  Col,
  Table,
  Form,
  Input,
  Button,
  Layout,
  PageHeader,
  Card,
  Modal,
  Space,
} from 'antd';
import {
  SearchOutlined,
  PlusOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';
import { UUIDLink } from '../components/utils';
import Moment from 'react-moment';
import Breadcrumbs from './Breadcrumbs';

const columns = (props) => {
  return [
    {
      title: 'ID',
      dataIndex: 'id',
      render: (id) => (
        <UUIDLink id={id} prefix="/cx/admin/ui/participation-syncers/" />
      ),
    },
    {
      title: 'Name',
      dataIndex: 'name',
      sorter: true,
    },
    {
      title: 'Enabled',
      dataIndex: 'enabled',
      render: (value) => (value ? 'Yes' : 'No'),
    },
    {
      title: 'Last updated',
      dataIndex: 'updated_at',
      sorter: true,
      render: (text) => <Moment fromNow>{text}</Moment>,
    },
  ];
};

const ParticipationSyncerList = (props) => {
  useEffect(() => {
    props.getParticipationSyncers();
  }, []);

  const handleTableChange = (pagination, filters, sorter) => {
    Promise.all([
      props.setPagination(pagination),
      props.setSorting(sorter),
    ]).then(props.getParticipationSyncers);
  };

  const handleSearch = (e) => {
    Promise.resolve(props.setSearch(e.target.value)).then(
      props.getParticipationSyncers,
    );
  };

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Syncer listing"
          breadcrumb={Breadcrumbs.listParticipationSyncers()}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Card style={{ marginBottom: 24 }}>
          <Row gutter={[16, 24]}>
            <Col key="1" flex={'auto'}>
              <Form layout="inline">
                <Form.Item key="1" style={{ width: '100%' }}>
                  <Input
                    prefix={<SearchOutlined className="site-form-item-icon" />}
                    placeholder="Search"
                    value={props.search}
                    onChange={handleSearch}
                  />
                </Form.Item>
              </Form>
            </Col>
            <Col key="2" flex={'none'}>
              <Button
                label="Add"
                type="primary"
                icon={<PlusOutlined />}
                onClick={() =>
                  props.history.push('/cx/admin/ui/participation-syncers/new')
                }
              >
                Create syncer
              </Button>
            </Col>
          </Row>
        </Card>

        <Card bodyStyle={{ padding: 0 }}>
          <Table
            columns={columns(props)}
            dataSource={props.data}
            pagination={props.pagination}
            loading={props.loading}
            rowKey={(record) => record.id}
            onChange={(pagination, filters, sorter) =>
              handleTableChange(pagination, filters, sorter)
            }
          />
        </Card>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.ParticipationSyncers.data,
    pagination: state.ParticipationSyncers.pagination,
    loading: state.ParticipationSyncers.loading,
    search: state.ParticipationSyncers.search,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getParticipationSyncers: () => dispatch(Actions.getParticipationSyncers()),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSearch: (search) => dispatch(Actions.setSearch(search)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ParticipationSyncerList);
