import * as api from './api';
import * as common from '../reducers/common';

const SET_PARTICIPATION_SYNCERS_PAGINATION =
  'SET_PARTICIPATION_SYNCERS_PAGINATION';
const SET_PARTICIPATION_SYNCERS_SORTING = 'SET_PARTICIPATION_SYNCERS_SORTING';
const SET_PARTICIPATION_SYNCERS_SEARCH = 'SET_PARTICIPATION_SYNCERS_SEARCH';

const PARTICIPATION_SYNCERS_REQUEST_PENDING =
  'PARTICIPATION_SYNCERS_REQUEST_PENDING';
const PARTICIPATION_SYNCERS_REQUEST_ERROR =
  'PARTICIPATION_SYNCERS_REQUEST_ERROR';

const PARTICIPATION_SYNCERS_FETCHED = 'PARTICIPATION_SYNCERS_FETCHED';
const PARTICIPATION_SYNCER_FETCHED = 'PARTICIPATION_SYNCER_FETCHED';
const PARTICIPATION_SYNCER_CREATED = 'PARTICIPATION_SYNCER_CREATED';
const PARTICIPATION_SYNCER_UPDATED = 'PARTICIPATION_SYNCER_UPDATED';
const PARTICIPATION_SYNCER_DELETED = 'PARTICIPATION_SYNCER_DELETED';

const pendingRequest = () => ({ type: PARTICIPATION_SYNCERS_REQUEST_PENDING });

const errorRequest = (error) => ({
  type: PARTICIPATION_SYNCERS_REQUEST_ERROR,
  payload: error,
});

const participationSyncersFetched = (result) => ({
  type: PARTICIPATION_SYNCERS_FETCHED,
  payload: result,
});
const participationSyncerFetched = (result) => ({
  type: PARTICIPATION_SYNCER_FETCHED,
  payload: result,
});
const participationSyncerCreated = (result) => ({
  type: PARTICIPATION_SYNCER_CREATED,
  payload: result,
});
const participationSyncerUpdated = (result) => ({
  type: PARTICIPATION_SYNCER_UPDATED,
  payload: result,
});
const participationSyncerDeleted = (result) => ({
  type: PARTICIPATION_SYNCER_DELETED,
  payload: result,
});

export const Actions = {
  setPagination: (pagination) => ({
    type: SET_PARTICIPATION_SYNCERS_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorter) => ({
    type: SET_PARTICIPATION_SYNCERS_SORTING,
    payload: sorter,
  }),

  setSearch: (search) => ({
    type: SET_PARTICIPATION_SYNCERS_SEARCH,
    payload: search,
  }),

  getParticipationSyncers: () => (dispatch, getState) => {
    const { pagination, sorting, search } = getState().ParticipationSyncers;
    dispatch(pendingRequest());
    return api.getParticipationSyncers({ pagination, sorting, search }).then(
      (response) => dispatch(dispatch(participationSyncersFetched(response))),
      (error) => dispatch(errorRequest(error)),
    );
  },

  getParticipationSyncer: (id) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.getParticipationSyncer(id).then(
        (response) => dispatch(participationSyncerFetched(response.data)),
        (error) => dispatch(errorRequest(error)),
      );
    };
  },

  createParticipationSyncer: (params) => (dispatch) => {
    dispatch(pendingRequest());
    return api.createParticipationSyncer(params).then(
      (response) => dispatch(participationSyncerCreated(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  updateParticipationSyncer: (id, params) => (dispatch) => {
    dispatch(pendingRequest());
    return api.updateParticipationSyncer(id, params).then(
      (response) => dispatch(participationSyncerUpdated(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  enableParticipationSyncer: (id) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.enableParticipationSyncer(id).then(
        (response) => dispatch(participationSyncerFetched(response.data)),
        (error) => dispatch(errorRequest(error)),
      );
    };
  },

  disableParticipationSyncer: (id) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.disableParticipationSyncer(id).then(
        (response) => dispatch(participationSyncerFetched(response.data)),
        (error) => dispatch(errorRequest(error)),
      );
    };
  },

  deleteParticipationSyncer: (id) => (dispatch) => {
    dispatch(pendingRequest());
    return api.deleteParticipationSyncer(id).then(
      (response) => dispatch(participationSyncerDeleted(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },
};

const initialState = {
  loading: false,
  pagination: {},
  sorting: {},
  search: '',
  data: [],
  current: {},
  error: {},
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case PARTICIPATION_SYNCERS_REQUEST_PENDING:
      return { ...state, loading: true };

    case PARTICIPATION_SYNCERS_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_PARTICIPATION_SYNCERS_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_PARTICIPATION_SYNCERS_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_PARTICIPATION_SYNCERS_SEARCH:
      return common.updateSearch(state, action.payload);

    case PARTICIPATION_SYNCERS_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case PARTICIPATION_SYNCER_FETCHED:
      return { ...state, loading: false, current: action.payload };

    case PARTICIPATION_SYNCER_CREATED:
      return common.resetSortPaginationSearch({ ...state, error: {} });

    case PARTICIPATION_SYNCER_UPDATED:
      return { ...state, loading: false, error: {} };

    case PARTICIPATION_SYNCER_DELETED:
      return { ...state, loading: false, error: {} };

    default:
      return state;
  }
};
