import { breadcrumbs, shortenUUID } from '../components/utils';

const listParticipationSyncers = {
  path: '/cx/admin/ui/participation-syncers',
  breadcrumbName: 'ParticipationSyncers',
};

const showParticipationSyncer = ({ syncer }) => ({
  path: `/cx/admin/ui/participation-syncers/${syncer.id}`,
  breadcrumbName: syncer.name,
});

const updateParticipationSyncer = ({ syncer }) => ({
  path: `/cx/admin/ui/participation-syncers/${syncer.id}/update`,
  breadcrumbName: 'Edit',
});

const createParticipationSyncer = {
  path: '/cx/admin/ui/participation-syncers/new',
  breadcrumbName: 'New ParticipationSyncer',
};

export default {
  listParticipationSyncers: breadcrumbs([listParticipationSyncers]),
  showParticipationSyncer: breadcrumbs([
    listParticipationSyncers,
    showParticipationSyncer,
  ]),
  updateParticipationSyncer: breadcrumbs([
    listParticipationSyncers,
    showParticipationSyncer,
    updateParticipationSyncer,
  ]),
  createParticipationSyncer: breadcrumbs([
    listParticipationSyncers,
    createParticipationSyncer,
  ]),
};
