import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Layout, PageHeader, Card } from 'antd';
import { Actions } from './state';
import { success_notification, error_notification } from '../components/utils';
import ParticipationSyncerForm from './ParticipationSyncerForm';
import Breadcrumbs from './Breadcrumbs';

const ParticipationSyncerUpdate = (props) => {
  const [breadcrumbs, setBreadcrumbs] = useState([]);

  useEffect(() => {
    props.getParticipationSyncer(props.match.params.id).then(() =>
      setBreadcrumbs(
        Breadcrumbs.updateParticipationSyncer({
          syncer: props.current,
        }),
      ),
    );
  }, []);

  const handleSubmit = (data) =>
    props.updateParticipationSyncer(props.current.id, data).then((result) => {
      if (result.payload.error) {
        error_notification(result.payload.error.message);
      } else {
        success_notification(
          'ParticipationSyncer updated successfully!',
        ).then(() =>
          props.history.push(
            `/cx/admin/ui/participation-syncers/${props.current.id}`,
          ),
        );
      }
    });

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Edit ParticipationSyncer"
          subTitle={`(${props.current.id || ''})`}
          breadcrumb={breadcrumbs}
        />
      </div>
      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Card>
          <ParticipationSyncerForm
            current={props.current}
            onSubmit={handleSubmit}
            error={props.error}
          />
        </Card>
      </Layout.Content>
    </Layout>
  );
};
const mapStateToProps = (state) => {
  return {
    error: state.ParticipationSyncers.error,
    current: state.ParticipationSyncers.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getParticipationSyncer: (id) =>
      dispatch(Actions.getParticipationSyncer(id)),
    updateParticipationSyncer: (id, data) =>
      dispatch(Actions.updateParticipationSyncer(id, data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ParticipationSyncerUpdate);
