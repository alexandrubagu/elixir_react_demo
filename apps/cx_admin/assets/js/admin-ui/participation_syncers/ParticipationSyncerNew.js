import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import ParticipationSyncerForm from './ParticipationSyncerForm';
import { Layout, PageHeader, Card } from 'antd';
import { success_notification, error_notification } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';

const ParticipationSyncerNew = (props) => {
  const handleSubmit = (data) =>
    props.createParticipationSyncer(data).then((result) => {
      if (result.payload.error) {
        error_notification(result.payload.error.message);
      } else {
        success_notification('Survey created successfully!').then(() =>
          props.history.push('/cx/admin/ui/participation-syncers'),
        );
      }
    });

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Create new participation syncer"
          breadcrumb={Breadcrumbs.createParticipationSyncer()}
        />
      </div>
      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Card>
          <ParticipationSyncerForm
            onSubmit={handleSubmit}
            error={props.error}
          />
        </Card>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  return {
    error: state.ParticipationSyncers.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createParticipationSyncer: (data) =>
      dispatch(Actions.createParticipationSyncer(data)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ParticipationSyncerNew);
