import Utils from '../api/utils';

export function getParticipationSyncers(params) {
  return Utils.request.get('/cx/admin/api/participation-syncers', params);
}

export function getParticipationSyncer(id) {
  return Utils.request.get(`/cx/admin/api/participation-syncers/${id}`);
}

export function createParticipationSyncer(params) {
  return Utils.request.post('/cx/admin/api/participation-syncers', {
    syncer: params,
  });
}

export function updateParticipationSyncer(id, params) {
  return Utils.request.put(`/cx/admin/api/participation-syncers/${id}`, {
    syncer: params,
  });
}

export function enableParticipationSyncer(id) {
  return Utils.request.get(`/cx/admin/api/participation-syncers/${id}/enable`);
}

export function disableParticipationSyncer(id) {
  return Utils.request.get(`/cx/admin/api/participation-syncers/${id}/disable`);
}

export function deleteParticipationSyncer(id) {
  return Utils.request.delete(`/cx/admin/api/participation-syncers/${id}`);
}
