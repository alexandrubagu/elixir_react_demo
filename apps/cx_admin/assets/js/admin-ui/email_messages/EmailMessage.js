import React from 'react';
import { Descriptions, Space, Typography } from 'antd';
import { JSONCode, UUIDLink } from '../components/utils';
import Moment from 'react-moment';

const EmailMessage = (props) => {
  const message = props.message || {};

  return (
    <Descriptions bordered column={1}>
      <Descriptions.Item label="InviteID">
        <UUIDLink prefix="/cx/admin/ui/invites/" id={message.invite_id} />
      </Descriptions.Item>

      <Descriptions.Item label="Mail type">{message.type}</Descriptions.Item>

      <Descriptions.Item label="From">
        <Space>
          {message.from_name}{' '}
          <Typography.Text type="secondary">
            ({message.from_address})
          </Typography.Text>
        </Space>
      </Descriptions.Item>

      <Descriptions.Item label="To">{message.to_address}</Descriptions.Item>

      <Descriptions.Item label="Template ID">
        {message.template_id}
      </Descriptions.Item>
      <Descriptions.Item label="Template Params">
        <JSONCode json={message.template_params} />
      </Descriptions.Item>

      <Descriptions.Item label="Created">
        <Moment fromNow>{message.inserted_at}</Moment>
      </Descriptions.Item>

      <Descriptions.Item label="Last updated">
        <Moment fromNow>{message.updated_at}</Moment>
      </Descriptions.Item>
    </Descriptions>
  );
};

export default EmailMessage;
