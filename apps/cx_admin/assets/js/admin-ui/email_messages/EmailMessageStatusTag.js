import React from 'react';
import { Tag } from 'antd';

const color = (status) => {
  switch (status) {
    case 'pending':
      return 'blue';

    case 'queued':
      return 'blue';

    case 'delivered':
      return 'green';

    case 'bounced':
      return 'red';

    case 'deferred':
      return 'magenta';

    case 'dropped':
      return 'red';
  }
};

const EmailMessageStatusTag = (props) => {
  return <Tag color={color(props.status)}>{props.status}</Tag>;
};

export default EmailMessageStatusTag;
