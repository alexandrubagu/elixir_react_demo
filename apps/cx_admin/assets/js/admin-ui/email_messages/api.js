import Utils from '../api/utils';

export function getEmailMessages(params) {
  return Utils.request.get(
    '/cx/admin/api/notifications/emails/messages',
    params,
  );
}

export function getEmailMessage(id) {
  return Utils.request.get(`/cx/admin/api/notifications/emails/messages/${id}`);
}

export function retryEmailMessageDelivery(id) {
  return Utils.request.get(`/cx/admin/api/notifications/emails/messages/${id}/retry-email-delivery`);
}

