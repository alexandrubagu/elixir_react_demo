import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card, Layout, Tag, Button, PageHeader, Row, Col } from 'antd';
import { Actions } from './state';
import Breadcrumbs from './Breadcrumbs';
import EmailMessageEventTimeline from './EmailMessageEventTimeline';
import EmailMessageStatusTag from './EmailMessageStatusTag';
import EmailMessage from './EmailMessage';
import {
  success_notification,
  error_notification,  
} from '../components/utils';
class EmailMessageShow extends Component {
  constructor(props) {
    super(props);
    this.state = { breadcrumbs: [] };
  }

  componentDidMount() {
    this.props.getEmailMessage(this.props.match.params.id).then(() => {
      this.setState({
        breadcrumbs: Breadcrumbs.showMessage({ message: this.props.current }),
      });
    });
  }

  retryEmailDelivery() {
    return this.props
      .retryDelivery(this.props.current.id)
      .then((result) => {
        if (result.payload.error) {
          error_notification(result.payload.error.message);
        } else {
          success_notification('Successfully requested redelivery!');
        }
      });
  }

  render() {
    const current = this.props.current;
    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Email message details"
            subTitle={`(${current.id || ''})`}
            breadcrumb={this.state.breadcrumbs}
            tags={this.getMessageTags(current)}
            extra={this.getMessageActions(current)}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Row gutter={[16, 24]}>
            <Col lg={{ span: 14 }} md={{ span: 24 }}>
              <Card title="General">
                <EmailMessage message={current} />
              </Card>
            </Col>
            <Col lg={{ span: 10 }} md={{ span: 24 }}>
              <Card title="Event timeline">
                <EmailMessageEventTimeline events={current.events} />
              </Card>
            </Col>
          </Row>
        </Layout.Content>
      </Layout>
    );
  }

  getMessageTags(message) {
    let tags = [];

    if (message.delivery_status == 'dropped') {
      tags.push(<Tag color="red">dropped: {message.delivery_drop_reason}</Tag>);
    } else {
      tags.push(<EmailMessageStatusTag status={message.delivery_status} />);
    }

    if (message.was_opened) {
      tags.push(<Tag color="green">opened</Tag>);
    } else {
      tags.push(<Tag>not opened</Tag>);
    }
    if (message.was_unsubscribed) {
      tags.push(<Tag color="volcano">unsubscribed</Tag>);
    }
    if (message.was_reported_as_spam) {
      tags.push(<Tag color="red">reported as spam</Tag>);
    }
    return tags;
  }

  getMessageActions(message) {
    let buttons = [];
    if (['dropped', 'bounced'].includes(message.delivery_status)) {
      buttons.push(<Button type="primary" onClick={() => this.retryEmailDelivery()}>Retry delivery</Button>);
    }
    return buttons;
  }
}

const mapStateToProps = (state) => {
  return {
    current: state.EmailMessages.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getEmailMessage: (id) => dispatch(Actions.getEmailMessage(id)),
    retryDelivery: (id) => dispatch(Actions.retryEmailMessageDelivery(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmailMessageShow);
