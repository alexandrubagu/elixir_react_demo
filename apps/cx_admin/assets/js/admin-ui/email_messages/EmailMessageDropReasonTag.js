import React from 'react';
import { Tag } from 'antd';

const color = (status) => {
  switch (status) {
    case 'invalid_address':
      return 'blue';

    case 'bounced_address':
      return 'blue';

    case 'unsubscribed_address':
      return 'volcano';

    case 'spam_reporting_address':
      return 'red';
  }
};

const EmailMessageDropReasonTag = (props) => {
  if (props.reason) {
    return <Tag color={color(props.reason)}>{props.reason}</Tag>;
  } else {
    return <>--</>;
  }
};

export default EmailMessageDropReasonTag;
