import React from 'react';
import { Tag, Timeline, Row, Col, Typography, Card, Empty, Space } from 'antd';
import Moment from 'react-moment';
import { JSONCode } from '../components/utils';

const EmailMessageEventTimeline = (props) => {
  const events = props.events || [];

  if (events.length == 0) {
    return <Empty description="No events yet" />;
  } else {
    return (
      <Timeline>
        {events.map((event) => (
          <EventTimelineItem event={event} key={event.id} />
        ))}
      </Timeline>
    );
  }
};

const EventTimelineItem = (props) => {
  const event = props.event;

  return (
    <Timeline.Item key={event.id}>
      <Row>
        <Col>
          <TypeTag type={event.type} />
        </Col>
        <Col>
          <Typography.Text disabled>
            <Moment fromNow>{event.inserted_at}</Moment>
          </Typography.Text>
        </Col>
      </Row>
      <Card bordered={false}>
        <TypeDescription type={event.type} />
        <JSONCode json={event.info} />
      </Card>
    </Timeline.Item>
  );
};

const TypeTag = (props) => {
  const [color, label] = color_and_label(props.type);
  return <Tag color={color}>{label}</Tag>;
};

const color_and_label = (type) => {
  switch (type) {
    case 'generated':
      return ['blue', 'generated'];

    case 'queued':
      return ['blue', 'queued'];

    case 'delivered':
      return ['green', 'delivered'];

    case 'bounced':
      return ['volcano', 'bounced'];

    case 'deferred':
      return ['volcano', 'deferred'];

    case 'opened':
      return ['green', 'opened'];

    case 'dropped':
      return ['red', 'dropped'];

    case 'reported_as_spam':
      return ['red', 'reported_as_spam'];

    case 'unsubscribed':
      return ['red', 'unsubscribed'];

    case 'redelivery_requested':
      return ['blue', 'redelivery_requested'];      

    case 'email_address_updated':
      return ['blue', 'email_address_updated'];            
  }
};

const TypeDescription = (props) => {
  switch (props.type) {
    case 'generated':
      return 'The email message was just created';

    case 'queued':
      return 'The email message was handed off to the provider for delivery';

    case 'delivered':
      return 'The email message was successfully delivered';

    case 'bounced':
      return 'The email has bounced';

    case 'deferred':
      return 'Delivery of the email has been delayed, delivery will be attempted later';

    case 'opened':
      return 'Email was opened';

    case 'dropped':
      return 'Email delivery was rejected';

    case 'reported_as_spam':
      return 'The email was reported as being spam';

    case 'unsubscribed':
      return 'This email was unsubscribed upon';

    case 'redelivery_requested':      
      return 'Email redelivery requested, previous statuses were';      

    case 'email_address_updated':      
      return 'Email address updated, previous statuses were';            

    default:
      return 'no info';
  }
};

export default EmailMessageEventTimeline;
