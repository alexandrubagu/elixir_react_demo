import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Layout, Card, PageHeader, Typography, Table, Form, Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { Actions } from './state';
import { UUIDLink } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';
import Moment from 'react-moment';
import EmailMessageStatusTag from './EmailMessageStatusTag';
import EmailMessageDropReasonTag from './EmailMessageDropReasonTag';

class EmailMessageList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columns: [
        {
          dataIndex: 'id',
          title: 'ID',
          render: (id) => (
            <UUIDLink id={id} prefix="/cx/admin/ui/emails/messages/" />
          ),
        },
        {
          dataIndex: 'to_address',
          title: 'To',
          sorter: true,
        },
        {
          title: 'From',
          dataIndex: 'from_name',
          render: (from, message) => (
            <>
              {message.from_name}{' '}
              <Typography.Text type="secondary">
                ({message.from_address})
              </Typography.Text>
            </>
          ),
        },
        {
          dataIndex: 'type',
          title: 'type',
          sorter: true,
        },
        {
          dataIndex: 'delivery_status',
          title: 'Status',
          sorter: true,
          render: (status) => <EmailMessageStatusTag status={status} />,
        },
        {
          dataIndex: 'delivery_drop_reason',
          title: 'Drop Reason',
          sorter: true,
          render: (reason) => <EmailMessageDropReasonTag reason={reason} />,
        },
        {
          title: 'Last updated',
          dataIndex: 'updated_at',
          sorter: true,
          render: (text, record) => <Moment fromNow>{text}</Moment>,
        },
      ],
    };
  }

  componentDidMount() {
    this.props.getEmailMessages();
  }

  handleShowEmailMessage(id) {
    this.props.history.push(`/cx/admin/ui/emails/messages/${id}`);
  }

  handleSearch(e) {
    Promise.resolve(this.props.setSearch(e.target.value)).then(
      this.props.getEmailMessages,
    );
  }

  handleTableChange(pagination, filters, sorter) {
    Promise.all([
      this.props.setPagination(pagination),
      this.props.setSorting(sorter),
    ]).then(this.props.getEmailMessages);
  }

  render() {
    const { data, pagination, loading } = this.props;
    const { columns } = this.state;

    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Email message listing"
            breadcrumb={Breadcrumbs.listMessages()}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card style={{ marginBottom: 24 }}>
            <Form layout="inline">
              <Form.Item key="1" style={{ width: '100%' }}>
                <Input
                  prefix={<SearchOutlined className="site-form-item-icon" />}
                  placeholder="Search"
                  onChange={(search) => this.handleSearch(search)}
                />
              </Form.Item>
            </Form>
          </Card>

          <Card bodyStyle={{ padding: 0 }}>
            <Table
              columns={columns}
              dataSource={data}
              pagination={pagination}
              loading={loading}
              rowKey={(record) => record.id}
              onChange={(pagination, filters, sorter) =>
                this.handleTableChange(pagination, filters, sorter)
              }
            />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    data: state.EmailMessages.data,
    pagination: state.EmailMessages.pagination,
    loading: state.EmailMessages.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getEmailMessages: () => dispatch(Actions.getEmailMessages()),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSearch: (search) => dispatch(Actions.setSearch(search)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmailMessageList);
