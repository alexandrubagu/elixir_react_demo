import * as api from './api';
import * as common from '../reducers/common';

const EMAIL_MESSAGES_FETCHED = 'EMAIL_MESSAGES_FETCHED';
const EMAIL_MESSAGES_REQUEST_PENDING = 'EMAIL_MESSAGES_REQUEST_PENDING';
const EMAIL_MESSAGES_REQUEST_ERROR = 'EMAIL_MESSAGES_REQUEST_ERROR';
const SET_EMAIL_MESSAGES_PAGINATION = 'SET_EMAIL_MESSAGES_PAGINATION';
const SET_EMAIL_MESSAGES_SORTING = 'SET_EMAIL_MESSAGES_SORTING';
const SET_EMAIL_MESSAGES_SEARCH = 'SET_EMAIL_MESSAGES_SEARCH';
const EMAIL_MESSAGE_FETCHED = 'EMAIL_MESSAGE_FETCHED';
const EMAIL_MESSAGE_RETRY_REQUESTED = 'EMAIL_MESSAGE_RETRY_REQUESTED';

const pendingRequest = () => ({
  type: EMAIL_MESSAGES_REQUEST_PENDING,
});

const requestFailed = (error) => ({
  type: EMAIL_MESSAGES_REQUEST_ERROR,
  payload: error,
});

export const Actions = {
  getEmailMessages: () => (dispatch, getState) => {
    const { pagination, sorting, search } = getState().EmailMessages;

    dispatch(pendingRequest());
    return api.getEmailMessages({ pagination, sorting, search }).then(
      (response) =>
        dispatch({ type: EMAIL_MESSAGES_FETCHED, payload: response }),
      (error) => dispatch(requestFailed(error)),
    );
  },

  getEmailMessage: (id) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.getEmailMessage(id).then(
        (response) =>
          dispatch({ type: EMAIL_MESSAGE_FETCHED, payload: response.data }),
        (error) => dispatch(errorRequest(error)),
      );
    };
  },
  
  retryEmailMessageDelivery: (id) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.retryEmailMessageDelivery(id).then(
        (response) =>
          dispatch({ type: EMAIL_MESSAGE_RETRY_REQUESTED, payload: response.data }),
        (error) => dispatch(errorRequest(error)),
      );
    };
  },

  setPagination: (pagination) => ({
    type: SET_EMAIL_MESSAGES_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorting) => {
    return {
      type: SET_EMAIL_MESSAGES_SORTING,
      payload: sorting,
    };
  },

  setSearch: (search) => ({
    type: SET_EMAIL_MESSAGES_SEARCH,
    payload: search,
  }),
};

const initialState = {
  loading: false,
  pagination: {},
  sorting: {},
  search: '',
  data: [],
  current: {},
  error: {},
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case EMAIL_MESSAGES_REQUEST_PENDING:
      return { ...state, loading: true };

    case EMAIL_MESSAGES_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_EMAIL_MESSAGES_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_EMAIL_MESSAGES_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_EMAIL_MESSAGES_SEARCH:
      return common.updateSearch(state, action.payload);

    case EMAIL_MESSAGES_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case EMAIL_MESSAGE_FETCHED:
      return { ...state, loading: false, current: action.payload };

    case EMAIL_MESSAGE_RETRY_REQUESTED:
      return { ...state, loading: false, current: action.payload };       
      
    default:
      return state;
  }
};
