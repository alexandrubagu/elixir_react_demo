import * as State from './state';
import EmailMessageList from './EmailMessageList';
import EmailMessageShow from './EmailMessageShow';
import EmailMessage from './EmailMessage';

export const Actions = State.Actions;
export const Reducer = State.Reducer;
export const List = EmailMessageList;
export const Show = EmailMessageShow;

// Non-container components
export const Message = EmailMessage;
