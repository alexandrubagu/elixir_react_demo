import Utils from '../api/utils';

export function getBatches(params) {
  return Utils.request.get('/cx/admin/api/batches', params);
}

export function getBatch(id) {
  return Utils.request.get(`/cx/admin/api/batches/${id}`);
}

export function createBatch(params) {
  return Utils.request.post('/cx/admin/api/batches', { batch: params });
}

export function updateBatch(id, params) {
  return Utils.request.put(`/cx/admin/api/batches/${id}`, { batch: params });
}

export function deleteBatch(id) {
  return Utils.request.delete(`/cx/admin/api/batches/${id}`);
}

export function processBatch(id) {
  return Utils.request.put(`/cx/admin/api/batches/${id}/process`);
}
