import { breadcrumbs, shortenUUID } from '../components/utils';

const listBatches = {
  path: '/cx/admin/ui/batches',
  breadcrumbName: 'Batches',
};

const showBatch = ({ batch }) => ({
  path: `/cx/admin/ui/batches/${batch.id}`,
  breadcrumbName: batch.name || shortenUUID(batch.id),
});

const createBatch = {
  path: '/cx/admin/ui/batches/new',
  breadcrumbName: 'New Batch',
};

const showEntry = ({ entry }) => ({
  path: '/cx/admin/ui/batch-entries/${id}',
  breadcrumbName: entry.ref,
});

const createEntry = ({ batch }) => ({
  path: `/cx/admin/ui/batches/${batch.id}/new`,
  breadcrumbName: 'New Batch Entry',
});

export default {
  listBatches: breadcrumbs([listBatches]),
  showBatch: breadcrumbs([listBatches, showBatch]),
  createBatch: breadcrumbs([listBatches, createBatch]),
  showEntry: breadcrumbs([listBatches, showBatch, showEntry]),
  createEntry: breadcrumbs([listBatches, showBatch, createEntry]),
};
