import React, { useState, useEffect } from 'react';
import { Form, Button, Input, Row, Col, Divider } from 'antd';
import { errorsAsLookup, getError, isEmpty } from '../components/utils';

const BatchForm = (props) => {
  const [form] = Form.useForm();
  const errorsLookup = errorsAsLookup(props.error);
  const [submitted, setSubmitted] = useState(false);

  useEffect(() => {
    if (!isEmpty(props.current)) {
      form.setFieldsValue(props.current);
    }
  }, [props.current]);

  const handleSubmit = (data) =>
    props.onSubmit(data).then(() => setSubmitted(true));

  return (
    <Form
      labelCol={{ span: 4 }}
      wrapperCol={{ span: 20 }}
      form={form}
      onFinish={handleSubmit}
    >
      <Divider orientation="left">
        <b>General</b>
      </Divider>
      <Row>
        <Col lg={12}>
          <Form.Item
            label="Name"
            name="name"
            rules={[{ required: true }]}
            {...getError(errorsLookup, 'name', submitted)}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>

      <Button type="primary" htmlType="submit">
        Submit
      </Button>
    </Form>
  );
};

export default BatchForm;
