import * as api from './api';
import * as common from '../reducers/common';

const BATCHES_REQUEST_PENDING = 'BATCHES_REQUEST_PENDING';
const BATCHES_REQUEST_ERROR = 'BATCHES_REQUEST_ERROR';
const SET_BATCHES_PAGINATION = 'SET_BATCHES_PAGINATION';
const SET_BATCHES_SORTING = 'SET_BATCHES_SORTING';
const SET_BATCHES_SEARCH = 'SET_BATCHES_SEARCH';
const BATCHES_FETCHED = 'BATCHES_FETCHED';
const BATCH_FETCHED = 'BATCH_FETCHED';
const BATCH_CREATED = 'BATCH_CREATED';
const BATCH_DELETED = 'BATCH_DELETED';
const BATCH_UPDATED = 'BATCH_UPDATED';
const BATCH_PROCESSED = 'BATCH_PROCESSED';

const pendingRequest = () => ({
  type: BATCHES_REQUEST_PENDING,
});

const errorRequest = (error) => ({
  type: BATCHES_REQUEST_ERROR,
  payload: error,
});

const batchesFetched = (result) => ({ type: BATCHES_FETCHED, payload: result });
const batchFetched = (result) => ({ type: BATCH_FETCHED, payload: result });
const batchCreated = (result) => ({ type: BATCH_CREATED, payload: result });
const batchUpdated = (result) => ({ type: BATCH_UPDATED, payload: result });
const batchDeleted = (result) => ({ type: BATCH_DELETED, payload: result });
const batchProcessed = (result) => ({ type: BATCH_PROCESSED, payload: result });

export const Actions = {
  setPagination: (pagination) => ({
    type: SET_BATCHES_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorter) => ({
    type: SET_BATCHES_SORTING,
    payload: sorter,
  }),

  setSearch: (search) => ({
    type: SET_BATCHES_SEARCH,
    payload: search,
  }),

  getBatches: () => (dispatch, getState) => {
    const { pagination, sorting, search } = getState().Batches;
    dispatch(pendingRequest());
    return api.getBatches({ pagination, sorting, search }).then(
      (response) => dispatch(batchesFetched(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  getBatch: (id) => (dispatch) => {
    dispatch(pendingRequest());
    return api.getBatch(id).then(
      (response) => dispatch(batchFetched(response.data)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  createBatch: (params) => (dispatch) => {
    dispatch(pendingRequest());
    return api.createBatch(params).then(
      (response) => dispatch(batchCreated(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  updateBatch: (id, params) => (dispatch) => {
    dispatch(pendingRequest());
    return api.updateBatch(id, params).then(
      (response) => dispatch(batchUpdated(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  deleteBatch: (batch) => (dispatch) => {
    dispatch(pendingRequest());
    return api.deleteBatch(batch.id).then(
      (response) => dispatch(batchDeleted(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  processBatch: (batch) => (dispatch) => {
    dispatch(pendingRequest());
    return api.processBatch(batch.id).then(
      (response) => dispatch(batchProcessed(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },
};

const initialState = {
  loading: false,
  pagination: {},
  sorting: {},
  search: '',
  data: [],
  current: {},
  error: {},
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case BATCHES_REQUEST_PENDING:
      return { ...state, loading: true };

    case BATCHES_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_BATCHES_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_BATCHES_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_BATCHES_SEARCH:
      return common.updateSearch(state, action.payload);

    case BATCHES_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case BATCH_FETCHED:
      return { ...state, loading: false, current: action.payload };

    case BATCH_CREATED:
      return { ...state, loading: false, error: {} };

    case BATCH_UPDATED:
      return { ...state, loading: false, error: {} };

    case BATCH_DELETED:
      return { ...state, loading: false, error: {} };

    case BATCH_PROCESSED:
      return {
        ...state,
        loading: false,
        error: {},
        current: action.payload.data,
      };

    default:
      return state;
  }
};
