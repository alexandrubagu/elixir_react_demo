import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import BatchForm from './BatchForm';
import { Layout, PageHeader, Card } from 'antd';
import { success_notification, error_notification } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';

class BatchCreation extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(data) {
    return this.props.createBatch(data).then((result) => {
      if (result.payload.error) {
        error_notification(result.payload.error.message);
      } else {
        success_notification('Batch created successfully!').then(() =>
          this.props.history.push(
            `/cx/admin/ui/batches/${result.payload.data.id}`,
          ),
        );
      }
      this.setState({ submitted: true });
    });
  }

  render() {
    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Create new batch"
            breadcrumb={Breadcrumbs.createBatch()}
          />
        </div>
        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card>
            <BatchForm onSubmit={this.handleSubmit} error={this.props.error} />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    error: state.Batches.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createBatch: (params) => dispatch(Actions.createBatch(params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BatchCreation);
