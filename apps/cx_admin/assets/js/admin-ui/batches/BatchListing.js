import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import { Link } from 'react-router-dom';
import {
  Layout,
  Row,
  Col,
  Space,
  Table,
  Form,
  Input,
  Button,
  Modal,
  Card,
  PageHeader,
} from 'antd';

import {
  SearchOutlined,
  PlusOutlined,
  ExclamationCircleOutlined,
} from '@ant-design/icons';

import Moment from 'react-moment';
import { success_notification } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';

class BatchListing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columns: [
        {
          title: 'ID',
          dataIndex: 'id',
          render: (text, record) => {
            const label = text.substring(text.length - 7, text.length);
            return <Link to={`/cx/admin/ui/batches/${text}`}>{label}</Link>;
          },
        },
        {
          title: 'Name',
          dataIndex: 'name',
          sorter: true,
        },
        {
          title: 'Last updated',
          dataIndex: 'updated_at',
          sorter: true,
          render: (text, record) => <Moment fromNow>{text}</Moment>,
        },
      ],
    };

    this.showDeleteModal = this.showDeleteModal.bind(this);
    this.handleTableChange = this.handleTableChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentDidMount() {
    this.props.getBatches();
  }

  showDeleteModal(entry) {
    Modal.confirm({
      title: `Do you want to delete this batch?`,
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () =>
        this.props
          .deleteBatch(entry)
          .then(() => success_notification('Batch deleted!'))
          .then(() => this.props.getBatches()),
    });
  }

  handleTableChange(pagination, filters, sorter) {
    Promise.all([
      this.props.setPagination(pagination),
      this.props.setSorting(sorter),
    ]).then(this.props.getBatches);
  }

  handleSearch(e) {
    Promise.resolve(this.props.setSearch(e.target.value)).then(
      this.props.getBatches,
    );
  }

  render() {
    const { data, pagination, loading, search } = this.props;
    const { columns } = this.state;

    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Batch listing"
            breadcrumb={Breadcrumbs.listBatches()}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card style={{ marginBottom: 24 }}>
            <Row gutter={[16, 24]}>
              <Col key="1" flex={'auto'}>
                <Form layout="inline">
                  <Form.Item key="1" style={{ width: '100%' }}>
                    <Input
                      prefix={
                        <SearchOutlined className="site-form-item-icon" />
                      }
                      placeholder="Search"
                      value={search}
                      onChange={this.handleSearch}
                    />
                  </Form.Item>
                </Form>
              </Col>
              <Col key="2" flex={'none'}>
                <Button
                  label="Add"
                  type="primary"
                  icon={<PlusOutlined />}
                  onClick={() =>
                    this.props.history.push('/cx/admin/ui/batches/new')
                  }
                >
                  Create batch
                </Button>
              </Col>
            </Row>
          </Card>

          <Card bodyStyle={{ padding: 0 }}>
            <Table
              columns={columns}
              dataSource={data}
              pagination={pagination}
              loading={loading}
              onChange={this.handleTableChange}
              rowKey={(record) => record.id}
            />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.Batches.data,
    pagination: state.Batches.pagination,
    loading: state.Batches.loading,
    search: state.Batches.search,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBatches: () => dispatch(Actions.getBatches()),
    deleteBatch: (entry) => dispatch(Actions.deleteBatch(entry)),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSearch: (search) => dispatch(Actions.setSearch(search)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BatchListing);
