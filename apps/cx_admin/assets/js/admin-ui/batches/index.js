import { Actions, Reducer } from './state';
import BatchListing from './BatchListing';
import BatchCreation from './BatchCreation';
import BatchDetails from './BatchDetails';
import BatchLogTimeline from './BatchLogTimeline';
import Breadcrumbs from './Breadcrumbs';

export default {
  Actions,
  Reducer,
  List: BatchListing,
  New: BatchCreation,
  Show: BatchDetails,
  LogTimeline: BatchLogTimeline,
  Breadcrumbs: Breadcrumbs,
};
