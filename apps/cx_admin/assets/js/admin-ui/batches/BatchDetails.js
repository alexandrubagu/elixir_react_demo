import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import { Actions as BatchEntryActions } from '../batch_entries/state';
import { Layout, PageHeader, Tabs, Modal, Card, Button } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { success_notification, error_notification } from '../components/utils';
import BatchLogTimeline from './BatchLogTimeline';
import BatchEntries from '../batch_entries';
import Breadcrumbs from './Breadcrumbs';

const BatchDetails = (props) => {
  const [id, _] = useState(props.match.params.id);

  useEffect(() => {
    props.getBatch(id);
  }, []);

  const showProcessModal = () =>
    Modal.confirm({
      title: `Do you want to process all batch entries ?`,
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () =>
        props
          .processBatch(props.batch)
          .then((response) => {
            if (response.payload.error) {
              return error_notification(response.payload.error.message);
            }
            const latestBatchStats = response.payload.data.log.find(
              (logEntry) => logEntry.event == 'batch_stats',
            );
            success_notification(`Batch processed ${latestBatchStats.message}`);
          })
          .then(() => props.getBatchEntries(id)),
    });

  const showDeleteModal = () =>
    Modal.confirm({
      title: `Do you want to delete this batch ?`,
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () =>
        this.props
          .deleteBatch(props.batch)
          .then(() => success_notification('Batch entry deleted!'))
          .then(() => props.history.push('/cx/admin/ui/batches')),
    });

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Batch details"
          subTitle={`(${props.batch.id || ''})`}
          breadcrumb={Breadcrumbs.showBatch({ batch: props.batch })}
          extra={[
            <Button type="danger" key="delete" onClick={showDeleteModal}>
              Delete Batch
            </Button>,
            <Button type="warning" key="process" onClick={showProcessModal}>
              Process Batch
            </Button>,
          ]}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Tabs>
          <Tabs.TabPane tab="Entries" key="entries">
            <Card>
              <BatchEntries.List batch_id={id} history={props.history} />
            </Card>
          </Tabs.TabPane>
          <Tabs.TabPane tab="Logs" key="logs">
            <Card>
              <BatchLogTimeline logs={props.batch.log} />
            </Card>
          </Tabs.TabPane>
        </Tabs>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  return {
    batch: state.Batches.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBatch: (id) => dispatch(Actions.getBatch(id)),
    deleteBatch: (batch) => dispatch(Actions.deleteBatch(batch)),
    processBatch: (batch) => dispatch(Actions.processBatch(batch)),
    getBatchEntries: (batchId) =>
      dispatch(BatchEntryActions.getBatchEntries(batchId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BatchDetails);
