import React from 'react';
import { Tag, Timeline, Row, Col, Typography, Card, Empty } from 'antd';
import Moment from 'react-moment';

const BatchLogTimeline = (props) => {
  const logs = props.logs || [];

  if (logs.length == 0) {
    return <Empty description="No logs stored" />;
  } else {
    return (
      <Timeline>
        {logs.map((entry) => (
          <LogTimelineItem entry={entry} key={entry.id} />
        ))}
      </Timeline>
    );
  }
};

const LogTimelineItem = (props) => {
  const entry = props.entry;

  return (
    <Timeline.Item key={entry.id}>
      <Row gutter={8}>
        <Col>
          <LevelTag level={entry.lvl} />
        </Col>
        <Col>
          <strong>{entry.event}</strong>
        </Col>
        <Col>
          <Typography.Text disabled>
            <Moment fromNow>{entry.inserted_at}</Moment>
          </Typography.Text>
        </Col>
      </Row>
      <Card bordered={false}>
        <code>{entry.message}</code>
      </Card>
    </Timeline.Item>
  );
};

const LevelTag = (props) => {
  const [color, label] = color_and_label(props.level);
  return <Tag color={color}>{label}</Tag>;
};

const color_and_label = (level) => {
  switch (level) {
    case 'info':
      return ['blue', 'Info'];

    case 'error':
      return ['error', 'Error'];

    case 'warn':
      return ['warning', 'Warn'];
  }
};

export default BatchLogTimeline;
