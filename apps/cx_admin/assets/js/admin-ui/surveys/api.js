import Utils from '../api/utils';

export function getSurveys(params) {
  return Utils.request.get('/cx/admin/api/surveys', params);
}

export function getSurvey(id) {
  return Utils.request.get(`/cx/admin/api/surveys/${id}`);
}

export function createSurvey(params) {
  return Utils.request.post('/cx/admin/api/surveys', {
    survey: params,
  });
}

export function updateSurvey(id, params) {
  return Utils.request.put(`/cx/admin/api/surveys/${id}`, {
    survey: params,
  });
}

export function toggleDeleteSurvey(id, params) {
  return Utils.request.put(`/cx/admin/api/surveys/${id}/toggle-delete`, {
    survey: params,
  });
}
