import * as State from './state';

import SurveyList from './SurveyList';
import SurveyNew from './SurveyNew';
import SurveyShow from './SurveyShow';
import SurveyUpdate from './SurveyUpdate';

export const Actions = State.Actions;
export const Reducer = State.Reducer;
export const List = SurveyList;
export const New = SurveyNew;
export const Show = SurveyShow;
export const Update = SurveyUpdate;
