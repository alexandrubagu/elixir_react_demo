import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Layout, PageHeader, Card } from 'antd';
import { Actions } from './state';
import { success_notification, error_notification } from '../components/utils';
import SurveyForm from './SurveyForm';
import Breadcrumbs from './Breadcrumbs';

class SurveyUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = { breadcrumbs: {} };
  }

  componentDidMount() {
    this.props.getSurvey(this.props.match.params.id).then(() => {
      this.setState({
        breadcrumbs: Breadcrumbs.updateSurvey({ survey: this.props.current }),
      });
    });
  }

  handleSubmit(data) {
    return this.props
      .updateSurvey(this.props.current.id, data)
      .then((result) => {
        if (result.payload.error) {
          error_notification(result.payload.error.message);
        } else {
          success_notification('Survey updated successfully!').then(() =>
            this.props.history.push(
              `/cx/admin/ui/surveys/${this.props.current.id}`,
            ),
          );
        }
      });
  }

  render() {
    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Edit survey"
            subTitle={`(${this.props.current.id || ''})`}
            breadcrumb={this.state.breadcrumbs}
          />
        </div>
        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card>
            <SurveyForm
              current={this.props.current}
              onSubmit={(data) => this.handleSubmit(data)}
              error={this.props.error}
            />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    error: state.Surveys.error,
    current: state.Surveys.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSurvey: (id) => dispatch(Actions.getSurvey(id)),
    updateSurvey: (id, data) => dispatch(Actions.updateSurvey(id, data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveyUpdate);
