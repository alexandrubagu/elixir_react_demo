import * as api from './api';
import * as common from '../reducers/common';

const SET_SURVEYS_PAGINATION = 'SET_SURVEYS_PAGINATION';
const SET_SURVEYS_SORTING = 'SET_SURVEYS_SORTING';
const SET_SURVEYS_SEARCH = 'SET_SURVEYS_SEARCH';
const SET_SURVEY_FILTERS = 'SET_SURVEY_FILTERS';

const SURVEYS_REQUEST_PENDING = 'SURVEYS_REQUEST_PENDING';
const SURVEYS_REQUEST_ERROR = 'SURVEYS_REQUEST_ERROR';

const SURVEYS_FETCHED = 'SURVEYS_FETCHED';
const SURVEY_FETCHED = 'SURVEY_FETCHED';
const SURVEY_CREATED = 'SURVEY_CREATED';
const SURVEY_UPDATED = 'SURVEY_UPDATED';
const TOGGLE_SURVEY_DELETED = 'TOGGLE_SURVEY_DELETED';

const pendingRequest = () => ({ type: SURVEYS_REQUEST_PENDING });

const errorRequest = (error) => ({
  type: SURVEYS_REQUEST_ERROR,
  payload: error,
});

const surveysFetched = (result) => ({ type: SURVEYS_FETCHED, payload: result });
const surveyFetched = (result) => ({ type: SURVEY_FETCHED, payload: result });
const surveyCreated = (result) => ({ type: SURVEY_CREATED, payload: result });
const surveyUpdated = (result) => ({ type: SURVEY_UPDATED, payload: result });
const surveyToggled = (result) => ({
  type: TOGGLE_SURVEY_DELETED,
  payload: result,
});

export const Actions = {
  setPagination: (pagination) => ({
    type: SET_SURVEYS_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorter) => ({
    type: SET_SURVEYS_SORTING,
    payload: sorter,
  }),

  setSearch: (search) => ({
    type: SET_SURVEYS_SEARCH,
    payload: search,
  }),

  setFilters: (filters) => ({
    type: SET_SURVEY_FILTERS,
    payload: filters,
  }),

  getSurveys: () => (dispatch, getState) => {
    const { pagination, sorting, search, filters } = getState().Surveys;
    dispatch(pendingRequest());
    return api.getSurveys({ pagination, sorting, search, filters }).then(
      (response) => dispatch(dispatch(surveysFetched(response))),
      (error) => dispatch(errorRequest(error)),
    );
  },

  getSurvey: (id) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.getSurvey(id).then(
        (response) => dispatch(surveyFetched(response.data)),
        (error) => dispatch(errorRequest(error)),
      );
    };
  },

  createSurvey: (params) => (dispatch) => {
    dispatch(pendingRequest());
    return api.createSurvey(params).then(
      (response) => dispatch(surveyCreated(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  updateSurvey: (id, params) => (dispatch) => {
    dispatch(pendingRequest());
    return api.updateSurvey(id, params).then(
      (response) => dispatch(surveyUpdated(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  toggleDeleteSurvey: (id, params) => (dispatch) => {
    dispatch(pendingRequest());
    return api.toggleDeleteSurvey(id, params).then(
      (response) => dispatch(surveyToggled(response.data)),
      (error) => dispatch(errorRequest(error)),
    );
  },
};

const initialState = {
  loading: false,
  pagination: {},
  sorting: {},
  filters: {},
  search: '',
  data: [],
  current: {},
  error: {},
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case SURVEYS_REQUEST_PENDING:
      return { ...state, loading: true };

    case SURVEYS_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_SURVEYS_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_SURVEYS_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_SURVEY_FILTERS:
      return { ...state, filters: action.payload };

    case SET_SURVEYS_SEARCH:
      return common.updateSearch(state, action.payload);

    case SURVEYS_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case SURVEY_FETCHED:
      return { ...state, loading: false, current: action.payload };

    case SURVEY_CREATED:
      return common.resetSortPaginationSearch({ ...state, error: {} });

    case SURVEY_UPDATED:
      return { ...state, loading: false, error: {} };

    case TOGGLE_SURVEY_DELETED:
      return { ...state, loading: false, current: action.payload };

    default:
      return state;
  }
};
