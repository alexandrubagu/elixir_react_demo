import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import SurveyForm from './SurveyForm';
import { Layout, PageHeader, Card } from 'antd';
import { success_notification, error_notification } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';

class SurveyNew extends Component {
  handleSubmit(data) {
    this.props.createSurvey(data).then((result) => {
      if (result.payload.error) {
        error_notification(result.payload.error.message);
      } else {
        success_notification('Survey created successfully!').then(() =>
          this.props.history.push('/cx/admin/ui/surveys'),
        );
      }
      this.setState({ submitted: true });
    });
  }

  render() {
    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Create new survey"
            breadcrumb={Breadcrumbs.createSurvey()}
          />
        </div>
        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card>
            <SurveyForm
              onSubmit={(data) => this.handleSubmit(data)}
              error={this.props.error}
            />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    error: state.Surveys.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createSurvey: (breadcrumbs) => dispatch(Actions.createSurvey(breadcrumbs)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveyNew);
