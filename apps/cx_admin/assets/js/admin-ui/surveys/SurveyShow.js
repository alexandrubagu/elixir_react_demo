import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Descriptions,
  Tag,
  Layout,
  PageHeader,
  Card,
  Row,
  Col,
  Empty,
  Button,
} from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { Actions } from './state';
import { JSONCode, MailTemplate } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';
import Moment from 'react-moment';

const ToggleDelete = (props) => {
  return props.current.deleted ? (
    <Button
      type="warning"
      onClick={() => props.toggleDeleteSurvey(props.current.id)}
    >
      Undelete
    </Button>
  ) : (
    <Button
      type="danger"
      onClick={() => props.toggleDeleteSurvey(props.current.id)}
    >
      Delete
    </Button>
  );
};

const SurveyShow = (props) => {
  const [breadcrumbs, setBreadcrumbs] = useState([]);

  useEffect(() => {
    props.getSurvey(props.match.params.id).then(() =>
      setBreadcrumbs(
        Breadcrumbs.showSurvey({
          survey: props.current,
        }),
      ),
    );
  }, []);

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Survey details"
          subTitle={`(${props.current.id || ''})`}
          breadcrumb={breadcrumbs}
          extra={[
            <ToggleDelete {...props} key="toggleDelete" />,
            <Button
              key="edit"
              type="primary"
              icon={<EditOutlined />}
              onClick={() =>
                props.history.push(
                  `/cx/admin/ui/surveys/${props.current.id}/update`,
                )
              }
            >
              Edit survey
            </Button>,
          ]}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Card title="General" key="general">
          <Descriptions column={2} bordered>
            <Descriptions.Item label="Name" span={2}>
              {props.current.name}
            </Descriptions.Item>

            <Descriptions.Item label="Description" span={2}>
              {props.current.description}
            </Descriptions.Item>

            <Descriptions.Item label="NotificationTypes" span={2}>
              {(props.current.notification_types || []).map((type, i) => (
                <Tag key={i}>{type}</Tag>
              ))}
            </Descriptions.Item>

            <Descriptions.Item label="Content" span={2}>
              <JSONCode json={props.current.content} />
            </Descriptions.Item>

            <Descriptions.Item label="Verbatim Questions" span={2}>
              <JSONCode json={props.current.verbatim_questions} />
            </Descriptions.Item>

            <Descriptions.Item label="Expire After (ms)">
              {props.current.expire_after}
            </Descriptions.Item>

            <Descriptions.Item label="Abandonned After (ms)">
              {props.current.abandonned_after}
            </Descriptions.Item>

            <Descriptions.Item label="Last updated" span={2}>
              <Moment fromNow={props.current.updated_at} />
            </Descriptions.Item>
          </Descriptions>
        </Card>

        <Row gutter={[16, 24]}>
          <Col lg={{ span: 12 }} md={{ span: 24 }}>
            <Card title="Mail Notification Details" style={{ marginTop: 24 }}>
              <MailDetails survey={props.current} />
            </Card>
          </Col>
          <Col lg={{ span: 12 }} md={{ span: 24 }}>
            <Card title="SMS Notification Details" style={{ marginTop: 24 }}>
              <SmsDetails survey={props.current} />
            </Card>
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
};

const MailDetails = (props) => {
  const survey = props.survey || {};
  const notification_types = props.survey.notification_types || [];

  if (!notification_types.includes('mail')) {
    return <Empty description="Mail notification not configured" />;
  } else {
    return (
      <Descriptions column={1} bordered>
        <Descriptions.Item label="From Name">
          {survey.from_email_name}
        </Descriptions.Item>
        <Descriptions.Item label="From Address">
          {survey.from_email_address}
        </Descriptions.Item>
        <Descriptions.Item label="Template Invite">
          <MailTemplate template={survey.tpl_invite} />
        </Descriptions.Item>
        <Descriptions.Item label="Template Reminder">
          <MailTemplate template={survey.tpl_reminder} />
        </Descriptions.Item>
        <Descriptions.Item label="Template Thanks">
          <MailTemplate template={survey.tpl_thanks} />
        </Descriptions.Item>
        <Descriptions.Item label="Template Abandonned">
          <MailTemplate template={survey.tpl_abandonned} />
        </Descriptions.Item>
        <Descriptions.Item label="Send Reminder After (ms)">
          {survey.remind_after}
        </Descriptions.Item>
      </Descriptions>
    );
  }
};

const SmsDetails = (props) => {
  const survey = props.survey || {};
  const notification_types = props.survey.notification_types || [];

  if (!notification_types.includes('sms')) {
    return <Empty description="SMS notification not configured" />;
  } else {
    return (
      <Descriptions column={1} bordered>
        <Descriptions.Item label="SMS Invite Template">
          {survey.sms_invite_tpl}
        </Descriptions.Item>
        <Descriptions.Item label="SMS From Number">
          {survey.sms_from_number}
        </Descriptions.Item>
      </Descriptions>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    current: state.Surveys.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSurvey: (id) => dispatch(Actions.getSurvey(id)),
    toggleDeleteSurvey: (id) => dispatch(Actions.toggleDeleteSurvey(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveyShow);
