import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import {
  Row,
  Col,
  Table,
  Form,
  Input,
  Button,
  Tag,
  Layout,
  PageHeader,
  Card,
  Switch,
} from 'antd';
import { SearchOutlined, PlusOutlined } from '@ant-design/icons';
import { UUIDLink } from '../components/utils';
import Moment from 'react-moment';
import Breadcrumbs from './Breadcrumbs';

class SurveyList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columns: [
        {
          title: 'ID',
          dataIndex: 'id',
          render: (id) => <UUIDLink id={id} prefix="/cx/admin/ui/surveys/" />,
        },
        {
          title: 'Name',
          dataIndex: 'name',
          sorter: true,
        },
        {
          title: 'Description',
          dataIndex: 'description',
          sorter: true,
        },
        {
          title: 'Notifications',
          dataIndex: 'notification_types',
          render: (notification_types) => (
            <>
              {notification_types.map((type, i) => {
                return (
                  <Tag key={i} className="notification-tag">
                    {type}
                  </Tag>
                );
              })}
            </>
          ),
        },
        {
          title: 'Last updated',
          dataIndex: 'updated_at',
          sorter: true,
          render: (text, record) => <Moment fromNow>{text}</Moment>,
        },
        {
          title: 'Status',
          dataIndex: 'deleted',
          render: (text) =>
            text ? (
              <Tag color="red" className="status-tag">
                deleted
              </Tag>
            ) : (
              <Tag color="green">active</Tag>
            ),
        },
      ],
    };
  }

  componentDidMount() {
    this.props.getSurveys();
  }

  handleTableChange(pagination, filters, sorter) {
    Promise.all([
      this.props.setPagination(pagination),
      this.props.setSorting(sorter),
    ]).then(this.props.getSurveys);
  }

  handleSearch(e) {
    Promise.resolve(this.props.setSearch(e.target.value)).then(
      this.props.getSurveys,
    );
  }

  toggleDeletedSurveys(value) {
    if (value) {
      Promise.resolve(this.props.setFilters({ include_deleted: [value] })).then(
        this.props.getSurveys,
      );
    } else {
      Promise.resolve(this.props.setFilters({})).then(this.props.getSurveys);
    }
  }

  render() {
    const { data, pagination, loading, search } = this.props;
    const { columns } = this.state;

    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Survey listing"
            breadcrumb={Breadcrumbs.listSurveys()}
            extra={[
              <Switch
                checkedChildren="Hide deleted"
                unCheckedChildren="Show deleted"
                onChange={(value) => this.toggleDeletedSurveys(value)}
                defaultChecked={!!this.props.filters.include_deleted}
              />,
            ]}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card style={{ marginBottom: 24 }}>
            <Row gutter={[16, 24]}>
              <Col key="1" flex={'auto'}>
                <Form layout="inline">
                  <Form.Item key="1" style={{ width: '100%' }}>
                    <Input
                      prefix={
                        <SearchOutlined className="site-form-item-icon" />
                      }
                      placeholder="Search"
                      value={search}
                      onChange={(search) => this.handleSearch(search)}
                    />
                  </Form.Item>
                </Form>
              </Col>
              <Col key="2" flex={'none'}>
                <Button
                  label="Add"
                  type="primary"
                  icon={<PlusOutlined />}
                  onClick={() =>
                    this.props.history.push('/cx/admin/ui/surveys/new')
                  }
                >
                  Create survey
                </Button>
              </Col>
            </Row>
          </Card>

          <Card bodyStyle={{ padding: 0 }}>
            <Table
              columns={columns}
              dataSource={data}
              pagination={pagination}
              loading={loading}
              rowKey={(record) => record.id}
              onChange={(pagination, filters, sorter) =>
                this.handleTableChange(pagination, filters, sorter)
              }
              rowClassName={(record) => (record.deleted ? 'row-deleted' : '')}
              className="survey-table"
            />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.Surveys.data,
    pagination: state.Surveys.pagination,
    loading: state.Surveys.loading,
    search: state.Surveys.search,
    filters: state.Surveys.filters,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSurveys: () => dispatch(Actions.getSurveys()),
    deleteSurvey: (entry) => dispatch(Actions.deleteSurvey(entry)),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSearch: (search) => dispatch(Actions.setSearch(search)),
    setFilters: (filters) => dispatch(Actions.setFilters(filters)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SurveyList);
