import React, { useState, useEffect } from 'react';
import { Form, Button, Input, Row, Col, Switch, Divider, Space } from 'antd';
import {
  errorsAsLookup,
  getError,
  validateIsJSONObject,
  validateIsJSONList,
  isEmpty,
} from '../components/utils';

const layout = { labelCol: { span: 7 }, wrapperCol: { span: 18 } };
const tailLayout = { wrapperCol: { span: 24 } };
const initialValues = { content: '{}', verbatim_questions: '[]' };

const SurveyForm = (props) => {
  const [form] = Form.useForm();
  const errorsLookup = errorsAsLookup(props.error);
  const [submitted, setSubmitted] = useState(false);
  const [mailFieldsEnabled, toggleMailFields] = useState(false);
  const [smsFieldsEnabled, toggleSmsFields] = useState(false);

  useEffect(() => {
    if (!isEmpty(props.current)) {
      props.current.content = JSON.stringify(props.current.content);
      props.current.verbatim_questions = JSON.stringify(
        props.current.verbatim_questions,
      );
      props.current.notification_types.map((type, i) => {
        if (type == 'mail') {
          toggleMailFields(true);
        }
        if (type == 'sms') {
          toggleSmsFields(true);
        }
      });
      form.setFieldsValue(props.current);
    }
  }, [props.current]);

  const handleSubmit = (data) => {
    const prepared_data = {
      ...data,
      notification_types: enabledNotificationTypes(),
      content: JSON.parse(data.content),
      verbatim_questions: JSON.parse(data.verbatim_questions),
    };

    props.onSubmit(prepared_data).then(() => setSubmitted(true));
  };

  const enabledNotificationTypes = () => {
    let enabled = [
      { name: 'mail', enabled: mailFieldsEnabled },
      { name: 'sms', enabled: smsFieldsEnabled },
    ]
      .filter((type) => type.enabled)
      .map((type) => type.name);
    return enabled;
  };

  return (
    <Form
      {...layout}
      initialValues={initialValues}
      form={form}
      onFinish={handleSubmit}
    >
      <Divider orientation="left">
        <b>General Survey config</b>
      </Divider>
      <Row>
        <Col lg={12}>
          <Form.Item
            label="Name"
            name="name"
            rules={[{ required: true }]}
            {...getError(errorsLookup, 'name', submitted)}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Description"
            name="description"
            rules={[{ required: true }]}
            {...getError(errorsLookup, 'description', submitted)}
          >
            <Input />
          </Form.Item>
        </Col>
        <Col lg={12}>
          <Form.Item
            {...tailLayout}
            label="Expire After"
            name="expire_after"
            help="Timeout in milliseconds after which an invite for this survey expires"
            rules={[{ required: true }]}
            {...getError(errorsLookup, 'expire_after', submitted)}
          >
            <Input />
          </Form.Item>

          <Form.Item
            {...tailLayout}
            label="Abandonned After"
            name="abandonned_after"
            help="Timeout in milliseconds after which a inactive participation is considered abandonned."
            rules={[{ required: true }]}
            {...getError(errorsLookup, 'abandonned_after', submitted)}
          >
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col lg={12}>
          <Form.Item
            {...tailLayout}
            label="Content"
            name="content"
            rules={[{ required: true }, { validator: validateIsJSONObject }]}
            {...getError(errorsLookup, 'content', true)}
          >
            <Input.TextArea rows={4} />
          </Form.Item>

          <Form.Item
            {...tailLayout}
            label="Verbatim questions"
            name="verbatim_questions"
            rules={[{ required: true }, { validator: validateIsJSONList }]}
            {...getError(errorsLookup, 'verbatim_questions', submitted)}
          >
            <Input.TextArea rows={4} />
          </Form.Item>
        </Col>
      </Row>
      <Divider orientation="left">
        <b>Notification types</b>
      </Divider>

      <Row gutter={[16, 24]}>
        <Col lg={12}>
          <Space style={{ marginBottom: 16 }}>
            <span>Enable mail notifications:</span>
            <Switch
              checked={mailFieldsEnabled}
              onChange={(value) => toggleMailFields(value)}
            />
          </Space>
          <Form.Item
            label="Remind After (msec)"
            name="remind_after"
            rules={[{ required: mailFieldsEnabled }]}
            {...getError(errorsLookup, 'remind_after', submitted)}
          >
            <Input disabled={!mailFieldsEnabled} />
          </Form.Item>
          <Form.Item
            label="Email From Name"
            name="from_email_name"
            rules={[{ required: mailFieldsEnabled }]}
            {...getError(errorsLookup, 'from_email_name', submitted)}
          >
            <Input disabled={!mailFieldsEnabled} />
          </Form.Item>
          <Form.Item
            label="Email From Address"
            name="from_email_address"
            rules={[{ required: mailFieldsEnabled }]}
            {...getError(errorsLookup, 'from_email_address', submitted)}
          >
            <Input disabled={!mailFieldsEnabled} />
          </Form.Item>
          <Form.Item
            label="Invite Email Template ID"
            name="tpl_invite"
            rules={[{ required: mailFieldsEnabled }]}
            {...getError(errorsLookup, 'tpl_invite', submitted)}
          >
            <Input disabled={!mailFieldsEnabled} />
          </Form.Item>
          <Form.Item
            label="Reminder Email Template ID"
            name="tpl_reminder"
            rules={[]}
            {...getError(errorsLookup, 'tpl_reminder', submitted)}
          >
            <Input disabled={!mailFieldsEnabled} />
          </Form.Item>
          <Form.Item
            label="Thanks Email Template ID"
            name="tpl_thanks"
            rules={[]}
            {...getError(errorsLookup, 'tpl_thanks', submitted)}
          >
            <Input disabled={!mailFieldsEnabled} />
          </Form.Item>
          <Form.Item
            label="Abandonned Email Template ID"
            name="tpl_abandonned"
            rules={[]}
            {...getError(errorsLookup, 'tpl_abandonned', submitted)}
          >
            <Input disabled={!mailFieldsEnabled} />
          </Form.Item>
        </Col>

        <Col span={12}>
          <Space style={{ marginBottom: 16 }}>
            <span>Enable sms notifications:</span>
            <Switch
              checked={smsFieldsEnabled}
              onChange={(value) => toggleSmsFields(value)}
            />
          </Space>

          <Form.Item
            label="Sms From Number"
            name="sms_from_number"
            rules={[{ required: smsFieldsEnabled }]}
            {...getError(errorsLookup, 'sms_from_number', submitted)}
          >
            <Input disabled={!smsFieldsEnabled} />
          </Form.Item>

          <Form.Item
            label="Sms Invite Template"
            name="sms_invite_tpl"
            rules={[{ required: smsFieldsEnabled }]}
            {...getError(errorsLookup, 'sms_invite_tpl', submitted)}
          >
            <Input disabled={!smsFieldsEnabled} />
          </Form.Item>
        </Col>
      </Row>

      <Button type="primary" htmlType="submit">
        Submit
      </Button>
    </Form>
  );
};

export default SurveyForm;
