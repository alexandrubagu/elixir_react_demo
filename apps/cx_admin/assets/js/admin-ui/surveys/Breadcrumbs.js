import { breadcrumbs, shortenUUID } from '../components/utils';

const listSurveys = {
  path: '/cx/admin/ui/surveys',
  breadcrumbName: 'Surveys',
};

const showSurvey = ({ survey }) => ({
  path: `/cx/admin/ui/surveys/${survey.id}`,
  breadcrumbName: survey.name,
});

const updateSurvey = ({ survey }) => ({
  path: `/cx/admin/ui/surveys/${survey.id}/update`,
  breadcrumbName: 'Edit',
});

const createSurvey = {
  path: '/cx/admin/ui/surveys/new',
  breadcrumbName: 'New Survey',
};

export default {
  listSurveys: breadcrumbs([listSurveys]),
  showSurvey: breadcrumbs([listSurveys, showSurvey]),
  updateSurvey: breadcrumbs([listSurveys, showSurvey, updateSurvey]),
  createSurvey: breadcrumbs([listSurveys, createSurvey]),
};
