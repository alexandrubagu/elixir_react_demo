import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  Table,
  Form,
  Input,
  Layout,
  Card,
  PageHeader,
  Descriptions,
} from 'antd';
import { Actions } from './state';
import { UUIDLink, CodeBlock } from '../components/utils';
import EventStatusTag from './EventStatusTag';
import RetryProcessor from './RetryProcessor';
import Moment from 'react-moment';

const columns = [
  {
    dataIndex: 'id',
    title: 'ID',
    render: (id) => (
      <UUIDLink prefix="/cx/admin/ui/emails/webhooks/events/" id={id} />
    ),
  },
  {
    dataIndex: 'status',
    title: 'Status',
    render: (status) => <EventStatusTag status={status} />,
  },
  {
    title: 'Last updated',
    dataIndex: 'updated_at',
    sorter: true,
    render: (text, record) => <Moment fromNow>{text}</Moment>,
  },
];
const RequireInterventionEventList = (props) => {
  useEffect(() => {
    props.setFilters({ status: ['requires_manual_intervention'] });
    props.getEmailWebhookEvents();
    props.getEmailWebhookRetryProcessingStats();
  }, []);

  const handleTableChange = (pagination, filters, sorter) => {
    Promise.all([
      this.props.setPagination(pagination),
      this.props.setSorting(sorter),
    ]).then(this.props.getEmailWebhookEvents);
  };

  return (
    <Row gutter={[16, 24]}>
      <Col span={18}>
        <Card bodyStyle={{ padding: 0 }}>
          <Table
            columns={columns}
            dataSource={props.data}
            pagination={props.pagination}
            loading={props.loading}
            rowKey={(event) => event.id}
            onChange={handleTableChange}
            expandable={{
              expandedRowRender: (event) => (
                <Descriptions bordered column={1}>
                  <Descriptions.Item label="Data">
                    {event.key}
                    <CodeBlock content={JSON.stringify(event.data)} />
                  </Descriptions.Item>

                  <Descriptions.Item label="Auto Processing Error">
                    <CodeBlock content={event.auto_processing_error} />
                  </Descriptions.Item>
                </Descriptions>
              ),
            }}
          />
        </Card>
      </Col>
      <Col span={6}>
        <RetryProcessor />
      </Col>
    </Row>
  );
};

const mapStateToProps = (state) => {
  return {
    pagination: state.EmailWebhookEvents.pagination,
    loading: state.EmailWebhookEvents.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getEmailWebhookEvents: () => dispatch(Actions.getEmailWebhookEvents()),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
    setFilters: (filters) => dispatch(Actions.setFilters(filters)),
    getEmailWebhookRetryProcessingStats: () =>
      dispatch(Actions.getEmailWebhookRetryProcessingStats()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RequireInterventionEventList);
