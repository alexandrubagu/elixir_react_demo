import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  Table,
  Form,
  Input,
  Layout,
  Card,
  PageHeader,
  Button,
  Tabs,
  Descriptions,
  Tag,
  Badge,
} from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { Actions } from './state';
import { UUIDLink, CodeBlock } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';
import RetryProcessor from './RetryProcessor';
import EventStatusTag from './EventStatusTag';
import Moment from 'react-moment';

const columns = [
  {
    dataIndex: 'id',
    title: 'ID',
    render: (id) => (
      <UUIDLink prefix="/cx/admin/ui/emails/webhooks/events/" id={id} />
    ),
  },
  {
    dataIndex: 'status',
    title: 'Status',
    sorter: true,
    filters: [
      {
        value: 'waiting_for_processing',
        text: 'Waiting for processing',
      },
      {
        value: 'automatically_processed',
        text: 'Automatically processed',
      },
      {
        value: 'requires_manual_intervention',
        text: 'Requires manual intervention',
      },
      {
        value: 'manually_processed',
        text: 'Manually processed',
      },
      {
        value: 'manually_ignored',
        text: 'Manually ignored',
      },
    ],
    filterMultiple: false,
    render: (status) => <EventStatusTag status={status} />,
  },
  {
    title: 'Data',
    dataIndex: 'data',
    ellipsis: true,
    // Dont use JSONCode here, since popup will not work
    render: (data) => <CodeBlock content={JSON.stringify(data)} />,
  },
  {
    dataIndex: 'auto_processing_error',
    title: 'Auto Processing Error',
    ellipsis: true,
    sorter: true,
  },
  {
    title: 'Last updated',
    dataIndex: 'updated_at',
    sorter: true,
    render: (text, record) => <Moment fromNow>{text}</Moment>,
  },
];

const EventList = (props) => {
  useEffect(() => {
    props.getEmailWebhookEvents();
  }, []);

  const handleTableChange = (pagination, filters, sorter) => {
    Promise.all([
      props.setPagination(pagination),
      props.setSorting(sorter),
      props.setFilters(filters),
    ]).then(props.getEmailWebhookEvents);
  };

  return (
    <Card bodyStyle={{ padding: 0 }}>
      <Table
        columns={columns}
        dataSource={props.data}
        pagination={props.pagination}
        loading={props.loading}
        rowKey={(record) => record.id}
        onChange={handleTableChange}
      />
    </Card>
  );
};

const mapStateToProps = (state) => {
  return {
    pagination: state.EmailWebhookEvents.pagination,
    loading: state.EmailWebhookEvents.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getEmailWebhookEvents: () => dispatch(Actions.getEmailWebhookEvents()),
    ignoreEmailWebhookEvent: (event) =>
      dispatch(Actions.ignoreEmailWebhookEvent(event)),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
    setFilters: (filters) => dispatch(Actions.setFilters(filters)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventList);
