import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  Table,
  Form,
  Input,
  Layout,
  Card,
  PageHeader,
  Button,
  Tabs,
  Descriptions,
  Tag,
  Badge,
} from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { Actions } from './state';
import Breadcrumbs from './Breadcrumbs';
import AllEventList from './AllEventList';
import RequireInterventionEventList from './RequireInterventionEventList';

const EventList = (props) => {
  const fetchData = (tabKey) => {
    if (tabKey == 'all') {
      props.setFilters({});
      props.getEmailWebhookEvents();
    }

    if (tabKey == 'manual') {
      props.setFilters({ status: ['requires_manual_intervention'] });
      props.getEmailWebhookEvents();
    }
  };

  const handleSearch = (e) => {
    Promise.resolve(props.setSearch(e.target.value)).then(
      props.getEmailWebhookEvents,
    );
  };

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Email webhook event listing"
          breadcrumb={Breadcrumbs.listEvents()}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Card style={{ marginBottom: 24 }}>
          <Form layout="inline">
            <Form.Item key="1" style={{ width: '100%' }}>
              <Input
                prefix={<SearchOutlined className="site-form-item-icon" />}
                placeholder="Search"
                value={props.search}
                onChange={handleSearch}
              />
            </Form.Item>
          </Form>
        </Card>

        <Tabs onChange={fetchData}>
          <Tabs.TabPane tab="Failed" key="manual">
            <RequireInterventionEventList data={props.data} />
          </Tabs.TabPane>
          <Tabs.TabPane tab="All" key="all">
            <AllEventList data={props.data} />
          </Tabs.TabPane>
        </Tabs>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.EmailWebhookEvents.data,
    search: state.EmailWebhookEvents.search,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getEmailWebhookEvents: () => dispatch(Actions.getEmailWebhookEvents()),
    setSearch: (search) => dispatch(Actions.setSearch(search)),
    setFilters: (filters) => dispatch(Actions.setFilters(filters)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventList);
