import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Descriptions, Button, Layout, PageHeader, Card, Modal } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Actions } from './state';
import {
  success_notification,
  error_notification,
  JSONCode,
} from '../components/utils';
import Breadcrumbs from './Breadcrumbs';
import EmailWebhookEventStatusTag from './EventStatusTag';
import ManualProcessing from './ManualProcessing';

const requiresManualIntervention = (event) =>
  event.status == 'requires_manual_intervention';

class EventDetails extends Component {
  constructor(props) {
    super(props);
    this.state = { breadcrumbs: {} };
  }

  componentDidMount() {
    this.props.getEvent(this.props.match.params.id).then(() =>
      this.setState({
        breadcrumbs: Breadcrumbs.showEvent({ event: this.props.event }),
      }),
    );
  }

  showIgnoreModal() {
    Modal.confirm({
      title: 'Ignore this event ?',
      content:
        'Are you sure you want to ignore this webhook event without any further processing ?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () => {
        this.props
          .ignoreEvent(this.props.event.id)
          .then(() => success_notification('Webhook event ignored!'))
          .then(() => this.props.getEvent(this.props.event.id));
      },
    });
  }

  handleManualProcessing(data) {
    this.props.processEvent(this.props.event.id, data).then((result) => {
      if (result.payload.error) {
        error_notification(result.payload.error.message);
      } else {
        success_notification('Webhook processed successfully!').then(() =>
          this.props.getEvent(this.props.event.id),
        );
      }
    });
  }

  render() {
    const event = this.props.event || {};

    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Email webhook event"
            breadcrumb={this.state.breadcrumbs}
            tags={<EmailWebhookEventStatusTag status={event.status} />}
            extra={
              requiresManualIntervention(event) && (
                <Button
                  type="danger"
                  onClick={() => this.showIgnoreModal(event.id)}
                >
                  Ignore
                </Button>
              )
            }
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card style={{ marginBottom: 24 }}>
            <Descriptions column={1} bordered>
              <Descriptions.Item label="ID">{event.id}</Descriptions.Item>
              <Descriptions.Item label="Status">
                {event.status}
              </Descriptions.Item>
              <Descriptions.Item label="Auto Processing Error">
                <code>{event.auto_processing_error}</code>
              </Descriptions.Item>
              <Descriptions.Item label="Data">
                <JSONCode json={event.data} />
              </Descriptions.Item>
            </Descriptions>
          </Card>
          {requiresManualIntervention(event) && (
            <ManualProcessing
              event={event}
              onProcessed={(data) => this.handleManualProcessing(data)}
            />
          )}
        </Layout.Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    event: state.EmailWebhookEvents.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getEvent: (id) => dispatch(Actions.getEmailWebhookEvent(id)),
    ignoreEvent: (id) => dispatch(Actions.ignoreEmailWebhookEvent(id)),
    processEvent: (id, params) =>
      dispatch(Actions.processEmailWebhookEvent(id, params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EventDetails);
