import React from 'react';
import { Tag } from 'antd';

const color = (status) => {
  switch (status) {
    case 'waiting_for_processing':
      return 'blue';

    case 'automatically_processed':
      return 'green';

    case 'manually_processed':
      return 'green';

    case 'requires_manual_intervention':
      return 'red';
  }
};

const EmailWebhookEventStatusTag = (props) => {
  return <Tag color={color(props.status)}>{props.status}</Tag>;
};

export default EmailWebhookEventStatusTag;
