import * as State from './state';
import EventList from './EventList';
import EventDetails from './EventDetails';

export const Actions = State.Actions;
export const Reducer = State.Reducer;
export const List = EventList;
export const Show = EventDetails;
