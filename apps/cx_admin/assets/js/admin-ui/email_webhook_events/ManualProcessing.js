import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import {
  Card,
  Typography,
  Button,
  Table,
  Row,
  Col,
  Input,
  Form,
  Select,
} from 'antd';

import { isEmpty } from '../components/utils';
import { SearchOutlined } from '@ant-design/icons';
import EmailMessageStatusTag from '../email_messages/EmailMessageStatusTag';
import EmailMessageDropReasonTag from '../email_messages/EmailMessageDropReasonTag';
import Moment from 'react-moment';

const { Option } = Select;

const ManualProcessing = (props) => {
  const [state, setState] = useState({
    mail_id: null,
    drop_reason: null,
    event_type: null,
  });

  const messageSelected = (id) => setState({ ...state, mail_id: id });
  const typeChanged = (data) => setState({ ...state, ...data });

  const canSubmit = () => {
    return (
      state.mail_id != null &&
      state.event_type != null &&
      (state.drop_reason != null || state.event_type != 'dropped')
    );
  };

  useEffect(() => {
    if (!isEmpty(props.event)) {
      props.autoFindInvolvedMails(props.event);
    }
  }, [props.event.id]);

  return (
    <Card title="Manually process">
      <FindMail
        involvedMails={props.involvedMails}
        involvedSearch={props.involvedSearch}
        onSearch={props.manuallyFindInvolvedMails}
        onChange={messageSelected}
      />

      <ChooseEventType onChange={typeChanged} />

      <Row justify="end" style={{ marginTop: 24, padding: 16 }}>
        <Col>
          <Button
            type="primary"
            disabled={!canSubmit()}
            onClick={() => props.onProcessed(state)}
          >
            Manually process
          </Button>
        </Col>
      </Row>
    </Card>
  );
};

const FindMail = (props) => {
  const [state, setState] = useState({ selected: [] });

  const selectMail = (ids) => {
    props.onChange(ids[0]);
    return setState({ ...state, selected: ids });
  };

  const columns = [
    {
      dataIndex: 'to_address',
      title: 'To',
    },
    {
      title: 'From',
      dataIndex: 'from_name',
      render: (from, message) => (
        <>
          {message.from_name}{' '}
          <Typography.Text type="secondary">
            ({message.from_address})
          </Typography.Text>
        </>
      ),
    },
    {
      dataIndex: 'type',
      title: 'type',
    },
    {
      dataIndex: 'delivery_status',
      title: 'Status',
      render: (status) => <EmailMessageStatusTag status={status} />,
    },
    {
      dataIndex: 'delivery_drop_reason',
      title: 'Drop Reason',
      render: (reason) => <EmailMessageDropReasonTag reason={reason} />,
    },
    {
      title: 'Last updated',
      dataIndex: 'updated_at',
      render: (text, record) => <Moment fromNow>{text}</Moment>,
    },
  ];

  return (
    <Form labelCol={{ span: 4 }} wrapperCol={{ span: 20 }}>
      <Form.Item label="Mail referenced in event" name="mail_referenced">
        <Row style={{}}>
          <Col span={24}>
            <Input
              prefix={<SearchOutlined className="site-form-item-icon" />}
              placeholder="Search"
              value={props.involvedSearch}
              onChange={(e) => props.onSearch(e.target.value)}
            />
          </Col>
        </Row>
        <Row style={{ marginTop: 24, padding: 16 }}>
          <Col span={24}>
            <Table
              columns={columns}
              dataSource={props.involvedMails}
              rowKey={(message) => message.id}
              pagination={false}
              rowSelection={{
                type: 'radio',
                selectedRowKeys: state.selected,
                onChange: selectMail,
              }}
            />
          </Col>
        </Row>
      </Form.Item>
    </Form>
  );
};

const ChooseEventType = (props) => {
  const [state, setState] = useState({
    event_type: null,
    drop_reason: null,
  });

  const updateState = (data) => {
    const next_state = { ...state, ...data };
    setState(next_state);
    props.onChange(next_state);
  };

  const typeChanged = (type) => {
    if (type != 'dropped') {
      updateState({ drop_reason: null });
    }
    return updateState({ event_type: type });
  };

  const reasonChanged = (reason) => updateState({ drop_reason: reason });

  return (
    <>
      <Form
        id="choose_event_type"
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
      >
        <Form.Item
          label="Event Type"
          name="event_type"
          rules={[{ required: true }]}
        >
          <Select onChange={typeChanged}>
            <Option value="delivered">Delivered</Option>
            <Option value="bounced">Bounced</Option>
            <Option value="deferred">Deferred</Option>
            <Option value="opened">Opened</Option>
            <Option value="dropped">Dropped</Option>
            <Option value="reported_as_spam">Spam</Option>
            <Option value="unsubscribed">Unsubscribed</Option>
          </Select>
        </Form.Item>
        {state.event_type == 'dropped' && (
          <Form.Item
            label="Drop Reason"
            name="drop_reason"
            rules={[{ required: true }]}
          >
            <Select onChange={reasonChanged}>
              <Option value="invalid_address">Invalid</Option>
              <Option value="bounced_address">Bounced</Option>
              <Option value="unsubscribed_address">Unsubscribed</Option>
              <Option value="spam_reporting_address">Spam</Option>
            </Select>
          </Form.Item>
        )}
      </Form>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    event: state.EmailWebhookEvents.current,
    involvedSearch: state.EmailWebhookEvents.involvedSearch,
    involvedMails: state.EmailWebhookEvents.involvedMails,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getEvent: (id) => dispatch(Actions.getEmailWebhookEvent(id)),
    ignoreEvent: (id) => dispatch(Actions.ignoreEmailWebhookEvent(id)),
    autoFindInvolvedMails: (event) =>
      dispatch(Actions.autoFindInvolvedMails(event)),
    manuallyFindInvolvedMails: (search) =>
      dispatch(Actions.manuallyFindInvolvedMails(search)),
    processEvent: (id, data) =>
      dispatch(Actions.processEmailWebhookEvent(id, data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ManualProcessing);
