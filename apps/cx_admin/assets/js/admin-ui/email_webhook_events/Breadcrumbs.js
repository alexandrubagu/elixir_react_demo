import { breadcrumbs } from '../components/utils';

const listEvents = {
  path: '/cx/admin/ui/emails/webhooks/events',
  breadcrumbName: 'Email Webhook Events',
};

const showEvent = ({ event }) => ({
  path: `/cx/admin/ui/emails/webhooks/events/${event.id}`,
  breadcrumbName: event.id,
});

export default {
  listEvents: breadcrumbs([listEvents]),
  showEvent: breadcrumbs([listEvents, showEvent]),
};
