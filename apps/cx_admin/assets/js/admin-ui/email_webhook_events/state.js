import * as api from './api';
import * as mail_api from '../email_messages/api';
import * as common from '../reducers/common';

const EMAIL_WEBHOOK_EVENTS_FETCHED = 'EMAIL_WEBHOOK_EVENTS_FETCHED';
const EMAIL_WEBHOOK_EVENTS_REQUEST_PENDING =
  'EMAIL_WEBHOOK_EVENTS_REQUEST_PENDING';
const EMAIL_WEBHOOK_EVENTS_REQUEST_ERROR = 'EMAIL_WEBHOOK_EVENTS_REQUEST_ERROR';
const SET_EMAIL_WEBHOOK_EVENTS_PAGINATION =
  'SET_EMAIL_WEBHOOK_EVENTS_PAGINATION';
const SET_EMAIL_WEBHOOK_EVENTS_SORTING = 'SET_EMAIL_WEBHOOK_EVENTS_SORTING';
const SET_EMAIL_WEBHOOK_EVENTS_SEARCH = 'SET_EMAIL_WEBHOOK_EVENTS_SEARCH';
const SET_EMAIL_WEBHOOK_EVENTS_FILTERS = 'SET_EMAIL_WEBHOOK_EVENTS_FILTERS';
const EMAIL_WEBHOOK_EVENT_FETCHED = 'EMAIL_WEBHOOK_EVENT_FETCHED';
const EMAIL_WEBHOOK_EVENT_IGNORED = 'EMAIL_WEBHOOK_EVENT_IGNORED';
const EMAIL_WEBHOOK_EVENT_PROCESSED = 'EMAIL_WEBHOOK_EVENT_PROCESSED';
const EMAIL_WEBHOOK_EVENT_FOUND_INVOLVED_EMAILS =
  'EMAIL_WEBHOOK_EVENT_FOUND_INVOLVED_EMAILS';

const MAIL_WEBHOOK_EVENT_PROCESSING_RETRIED =
  'MAIL_WEBHOOK_EVENT_PROCESSING_RETRIED';

const EMAIL_WEBHOOK_EVENT_PROCESSING_STATS_FETCHED =
  'EMAIL_WEBHOOK_EVENT_PROCESSING_STATS_FETCHED';

const pendingRequest = () => ({
  type: EMAIL_WEBHOOK_EVENTS_REQUEST_PENDING,
});

const requestFailed = (error) => ({
  type: EMAIL_WEBHOOK_EVENTS_REQUEST_ERROR,
  payload: error,
});

const eventsFetched = (payload) => ({
  type: EMAIL_WEBHOOK_EVENTS_FETCHED,
  payload: payload,
});

const eventFetched = (event) => ({
  type: EMAIL_WEBHOOK_EVENT_FETCHED,
  payload: event,
});

const eventIgnored = (event) => ({
  type: EMAIL_WEBHOOK_EVENT_IGNORED,
  payload: event,
});

const eventProcessed = (event) => ({
  type: EMAIL_WEBHOOK_EVENT_PROCESSED,
  payload: event,
});

const foundInvolvedEmails = (search, mails) => ({
  type: EMAIL_WEBHOOK_EVENT_FOUND_INVOLVED_EMAILS,
  payload: { search: search || '', mails: mails },
});

const InvolvedMailParams = {
  pagination: { current: 1, pageSize: 5 },
  sorting: { order: 'descend', field: 'inserted_at' },
};

const autoDetermineInvolvedMailParams = (event) => {
  let params = { ...InvolvedMailParams };

  if ('email' in event.data || {}) {
    params.search = event.data.email;
  }

  return params;
};

const emailWebhookRetryProcessingRetried = (payload) => ({
  type: MAIL_WEBHOOK_EVENT_PROCESSING_RETRIED,
  payload: payload,
});

const emailWebhookRetryProcessingStatsFetched = (payload) => ({
  type: EMAIL_WEBHOOK_EVENT_PROCESSING_STATS_FETCHED,
  payload: payload,
});

export const Actions = {
  setPagination: (pagination) => ({
    type: SET_EMAIL_WEBHOOK_EVENTS_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorting) => {
    return {
      type: SET_EMAIL_WEBHOOK_EVENTS_SORTING,
      payload: sorting,
    };
  },

  setSearch: (search) => ({
    type: SET_EMAIL_WEBHOOK_EVENTS_SEARCH,
    payload: search,
  }),

  setFilters: (filters) => ({
    type: SET_EMAIL_WEBHOOK_EVENTS_FILTERS,
    payload: filters,
  }),

  getEmailWebhookEvents: () => (dispatch, getState) => {
    const {
      pagination,
      sorting,
      search,
      filters,
    } = getState().EmailWebhookEvents;

    dispatch(pendingRequest());
    return api
      .getEmailWebhookEvents({ pagination, sorting, search, filters })
      .then(
        (response) => dispatch(eventsFetched(response)),
        (error) => dispatch(requestFailed(error)),
      );
  },

  getEmailWebhookEvent: (id) => (dispatch) => {
    dispatch(pendingRequest());
    return api.getEmailWebhookEvent(id).then(
      (response) => dispatch(eventFetched(response.data)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  ignoreEmailWebhookEvent: (id) => (dispatch) => {
    dispatch(pendingRequest());
    return api.ignoreEmailWebhookEvent(id).then(
      (response) => dispatch(eventIgnored(response.data)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  processEmailWebhookEvent: (id, data) => (dispatch) => {
    dispatch(pendingRequest());
    return api.processEmailWebhookEvent(id, data).then(
      (response) => dispatch(eventProcessed(response.data)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  autoFindInvolvedMails: (event) => (dispatch) => {
    dispatch(pendingRequest());
    const params = autoDetermineInvolvedMailParams(event);
    return mail_api.getEmailMessages(params).then(
      (response) => dispatch(foundInvolvedEmails(params.search, response.data)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  manuallyFindInvolvedMails: (search) => (dispatch) => {
    dispatch(pendingRequest());
    const params = { ...InvolvedMailParams, search: search };
    return mail_api.getEmailMessages(params).then(
      (response) => dispatch(foundInvolvedEmails(params.search, response.data)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  retryProcessingEmailWebhooks: () => (dispatch) => {
    dispatch(pendingRequest());
    return api.retryProcessingEmailWebhooks().then(
      () => dispatch(emailWebhookRetryProcessingRetried()),
      (error) => dispatch(requestFailed(error)),
    );
  },

  getEmailWebhookRetryProcessingStats: () => (dispatch) => {
    dispatch(pendingRequest());
    return api.getEmailWebhookRetryProcessingStats().then(
      (response) =>
        dispatch(emailWebhookRetryProcessingStatsFetched(response.data)),
      (error) => dispatch(requestFailed(error)),
    );
  },
};

const initialState = {
  loading: false,
  pagination: {},
  sorting: {},
  search: '',
  filters: {},
  data: [],
  current: {},
  error: {},
  involvedSearch: '',
  involvedMails: [],
  retryProcessingStats: {},
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case EMAIL_WEBHOOK_EVENTS_REQUEST_PENDING:
      return { ...state, loading: true };

    case EMAIL_WEBHOOK_EVENTS_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_EMAIL_WEBHOOK_EVENTS_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_EMAIL_WEBHOOK_EVENTS_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_EMAIL_WEBHOOK_EVENTS_SEARCH:
      return common.updateSearch(state, action.payload);

    case SET_EMAIL_WEBHOOK_EVENTS_FILTERS:
      return { ...state, filters: action.payload };

    case EMAIL_WEBHOOK_EVENTS_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case EMAIL_WEBHOOK_EVENT_FETCHED:
      return { ...state, loading: false, current: action.payload };

    case EMAIL_WEBHOOK_EVENT_IGNORED:
      return { ...state, loading: false, errors: {} };

    case EMAIL_WEBHOOK_EVENT_FOUND_INVOLVED_EMAILS:
      return {
        ...state,
        loading: false,
        errors: {},
        involvedSearch: action.payload.search,
        involvedMails: action.payload.mails,
      };

    case EMAIL_WEBHOOK_EVENT_PROCESSED:
      return {
        ...state,
        loading: false,
        errors: {},
        involvedMails: [],
        involvedSearch: '',
      };

    case EMAIL_WEBHOOK_EVENT_PROCESSING_STATS_FETCHED:
      return { ...state, loading: false, retryProcessingStats: action.payload };

    case MAIL_WEBHOOK_EVENT_PROCESSING_RETRIED:
      return { ...state, loading: false };

    default:
      return state;
  }
};
