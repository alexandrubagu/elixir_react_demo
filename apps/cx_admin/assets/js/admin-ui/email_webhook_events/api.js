import Utils from '../api/utils';

export function getEmailWebhookEvents(params) {
  return Utils.request.get(
    '/cx/admin/api/notifications/emails/webhooks/events',
    params,
  );
}

export function getEmailWebhookEvent(id) {
  return Utils.request.get(
    `/cx/admin/api/notifications/emails/webhooks/events/${id}`,
  );
}

export function ignoreEmailWebhookEvent(id) {
  return Utils.request.put(
    `/cx/admin/api/notifications/emails/webhooks/events/${id}/manually-ignore`,
  );
}

export function processEmailWebhookEvent(id, data) {
  return Utils.request.put(
    `/cx/admin/api/notifications/emails/webhooks/events/${id}/manually-process`,
    { params: data },
  );
}

export function retryProcessingEmailWebhooks() {
  return Utils.request.get(
    '/cx/admin/api/notifications/emails/webhooks/events/retry-processing',
  );
}

export function getEmailWebhookRetryProcessingStats() {
  return Utils.request.get(
    '/cx/admin/api/notifications/emails/webhooks/events/retry-processing-stats',
  );
}
