import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  Card,
  Button,
  Empty,
  Tag,
  Badge,
  Timeline,
  Typography,
} from 'antd';
import { Actions } from './state';
import Moment from 'react-moment';

const RetryProcessorLogs = (props) => {
  const logs = props.logs || [];

  if (logs.length == 0) {
    return <Empty description="No logs yet" />;
  } else {
    return (
      <Timeline>
        {logs.map((log, i) => (
          <RetryProcessorLog log={log} key={i} />
        ))}
      </Timeline>
    );
  }
};

const RetryProcessorLog = (props) => {
  const log = props.log;

  return (
    <Timeline.Item key={log.id}>
      <Row gutter={8}>
        <Col>
          <Typography.Text disabled>
            <Moment fromNow>{log.at}</Moment>
          </Typography.Text>
        </Col>
      </Row>
      <Card bordered={false}>
        <span>
          Retry job ran successfully, resolved: <strong>{log.resolved}</strong>,
          was unable to resolve: <strong>{log.unresolved}</strong>
        </span>
      </Card>
    </Timeline.Item>
  );
};

const RetryProcessor = (props) => {
  useEffect(() => {
    props.getEmailWebhookRetryProcessingStats();
  }, []);

  const delay = () =>
    new Promise((resolve) => setTimeout(() => resolve('done'), 500));

  const retryProcessing = () =>
    props
      .retryProcessingEmailWebhooks()
      .then(delay)
      .then(props.getEmailWebhookRetryProcessingStats);

  return (
    <Card
      title={
        <div>
          <h4>Retry Processor</h4>
          <Tag>
            <Badge status="success" /> {props.stats.status}
          </Tag>
        </div>
      }
      style={{ marginBottom: 24 }}
      extra={[
        <Button type="primary" onClick={retryProcessing}>
          Launch retry job
        </Button>,
      ]}
    >
      <RetryProcessorLogs logs={props.stats.logs || []} />
    </Card>
  );
};

const mapStateToProps = (state) => {
  return {
    stats: state.EmailWebhookEvents.retryProcessingStats,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    retryProcessingEmailWebhooks: () =>
      dispatch(Actions.retryProcessingEmailWebhooks()),
    getEmailWebhookRetryProcessingStats: () =>
      dispatch(Actions.getEmailWebhookRetryProcessingStats()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RetryProcessor);
