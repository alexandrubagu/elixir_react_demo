import 'core-js';
import 'regenerator-runtime/runtime';
import React, { useState } from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import { syncHistoryWithStore } from 'react-router-redux';
import { Layout } from 'antd';

import SocketContext from './context/socket';
import { socket, statsChannel } from '../socket';
import state from './reducers';

import Navigation from './navigation';

const browserHistory = createBrowserHistory();
const store = createStore(state, applyMiddleware(thunk, createLogger));
const history = syncHistoryWithStore(browserHistory, store);

const App = (props) => {
  return (
    <SocketContext.Provider value={{ socket: socket, channel: statsChannel }}>
      <Provider store={store}>
        <Router history={history}>
          <Layout className="app-layout">
            <Navigation />
          </Layout>
        </Router>
      </Provider>
    </SocketContext.Provider>
  );
};

export default App;
