import Utils from '../api/utils';

export const getParticipations = (params) =>
  Utils.request.get('/cx/admin/api/participations', params);

export const getParticipation = (id) =>
  Utils.request.get(`/cx/admin/api/participations/${id}`);

export const getSessions = () =>
  Utils.request.get(`/cx/admin/api/participations/sessions`);

export const getLatestVerbatimCategorizations = () =>
  Utils.request.get(`/cx/admin/api/demo/latest-verbatim-categorizations`);

export const countVerbatimCategorizationTopicSelections = () =>
  Utils.request.get(
    `/cx/admin/api/demo/verbatim-categorization/count-topic-selections`,
  );

export const countVerbatimCategorizationWordSelections = () =>
  Utils.request.get(
    `/cx/admin/api/demo/verbatim-categorization/top-100-word-cloud`,
  );

export const processVerbatims = () =>
  Utils.request.post(`/cx/admin/api/participations/verbatims/process`);

export const getVerbatimProcessingStats = () =>
  Utils.request.get(`/cx/admin/api/participations/verbatims/processor-stats`);
