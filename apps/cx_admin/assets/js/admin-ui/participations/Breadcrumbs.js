import { breadcrumbs } from '../components/utils';

const listParticipations = {
  path: '/cx/admin/ui/participations',
  breadcrumbName: 'Participations',
};

const showParticipation = ({ participation }) => ({
  path: `/cx/admin/ui/participations/${participation.id}`,
  breadcrumbName: participation.ref,
});

const listSessions = {
  path: '/cx/admin/ui/participations',
  breadcrumbName: 'Sessions',
};

const administration = {
  path: '/cx/admin/ui/participations/verbatim-categorization/administration',
  breadcrumbName: 'Administration',
};

export default {
  listParticipations: breadcrumbs([listParticipations]),
  showParticipation: breadcrumbs([listParticipations, showParticipation]),
  listSessions: breadcrumbs([listParticipations, listSessions]),
  verbatimAdministration: breadcrumbs([listParticipations, administration]),
};
