import React, { useEffect, useState } from 'react';
import {
  Card,
  Row,
  Col,
  Empty,
  Timeline,
  Typography,
  Descriptions,
  Tag,
} from 'antd';
import { getLatestVerbatimCategorizations } from './api';
import Moment from 'react-moment';
import { UUIDLink } from '../components/utils';

const LogTimelineItem = (props) => {
  const entry = props.entry;

  return (
    <Timeline.Item key={entry.id}>
      <Row gutter={8}>
        <Col>
          <Typography.Text disabled>
            <Moment fromNow>{entry.inserted_at}</Moment>
          </Typography.Text>
        </Col>
        <Col>
          {entry.topic_selections.map((topic, i) => (
            <Tag color="blue" key={i}>
              {topic}
            </Tag>
          ))}
        </Col>
      </Row>

      <blockquote>
        <p className="quotation">{entry.text_en}</p>
        <footer>
          — participation:{' '}
          <UUIDLink
            prefix="/cx/admin/ui/participations/"
            id={entry.participation_id}
          />
        </footer>
      </blockquote>
    </Timeline.Item>
  );
};

const LatestVerbatimCategorizationTimeline = (props) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    getLatestVerbatimCategorizations().then((response) =>
      setData(response.data),
    );
  }, []);

  return (
    <Card title="Latest verbatim categorization">
      {data.length == 0 ? (
        <Empty description="No analyzed verbatims yet" />
      ) : (
        <Timeline>
          {data.map((entry) => (
            <LogTimelineItem entry={entry} key={entry.id} />
          ))}
        </Timeline>
      )}
    </Card>
  );
};

export default LatestVerbatimCategorizationTimeline;
