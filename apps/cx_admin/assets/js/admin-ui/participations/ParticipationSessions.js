import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Layout, Table, Form, Input, Card, PageHeader } from 'antd';
import { Actions } from './state';
import Breadcrumbs from './Breadcrumbs';
import { UUIDLink } from '../components/utils';

const columns = [
  {
    title: 'Participation ID',
    dataIndex: 'participation_id',
    render: (text, record) => (
      <UUIDLink prefix="/cx/admin/ui/participations/" id={text} />
    ),
  },
  {
    title: 'PID',
    dataIndex: 'pid',
  },
];

const ParticipationSessions = (props) => {
  useEffect(() => {
    props.getSessions();
  }, [props.data]);

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Participation Sesssions"
          breadcrumb={Breadcrumbs.listSessions()}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Card style={{ marginBottom: 24 }}>
          <Table
            columns={columns}
            dataSource={props.sessions}
            pagination={false}
            loading={props.loading}
          />
        </Card>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  return {
    sessions: state.Participations.sessions,
    loading: state.Participations.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getSessions: () => dispatch(Actions.getSessions()),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ParticipationSessions);
