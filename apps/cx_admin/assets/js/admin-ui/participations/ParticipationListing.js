import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Layout, Table, Form, Input, Card, PageHeader } from 'antd';
import ParticipationStatusTag from './ParticipationStatusTag';
import { SearchOutlined } from '@ant-design/icons';
import { Actions } from './state';
import Breadcrumbs from './Breadcrumbs';
import Moment from 'react-moment';

class ParticipationListing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columns: [
        {
          title: 'ID',
          dataIndex: 'id',
          render: (text, record) => {
            const label = text.substring(text.length - 7, text.length);
            return (
              <Link to={`/cx/admin/ui/participations/${text}`}>{label}</Link>
            );
          },
        },
        {
          title: 'Ref',
          dataIndex: 'ref',
          sorter: true,
        },
        {
          title: 'Lang',
          dataIndex: 'language',
          sorter: true,
        },
        {
          title: 'Status',
          dataIndex: 'status',
          sorter: true,
          render: (text, record) => <ParticipationStatusTag status={text} />,
        },
        {
          title: 'Last updated',
          dataIndex: 'updated_at',
          sorter: true,
          render: (text, record) => <Moment fromNow>{text}</Moment>,
        },
      ],
    };

    this.handleTableChange = this.handleTableChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentDidMount() {
    this.props.getParticipations();
  }

  handleTableChange(pagination, filters, sorter) {
    Promise.all([
      this.props.setPagination(pagination),
      this.props.setSorting(sorter),
    ]).then(this.props.getParticipations);
  }

  handleSearch(e) {
    Promise.resolve(this.props.setSearch(e.target.value)).then(
      this.props.getParticipations,
    );
  }

  render() {
    const { data, pagination, loading, search } = this.props;
    const { columns } = this.state;

    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Participations listing"
            breadcrumb={Breadcrumbs.listParticipations()}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card style={{ marginBottom: 24 }}>
            <Form layout="inline">
              <Form.Item key="1" style={{ width: '100%' }}>
                <Input
                  size="large"
                  value={search}
                  prefix={<SearchOutlined className="site-form-item-icon" />}
                  placeholder="Search"
                  onChange={this.handleSearch}
                />
              </Form.Item>
            </Form>
          </Card>

          <Card bodyStyle={{ padding: 0 }}>
            <Table
              columns={columns}
              dataSource={data}
              pagination={pagination}
              loading={loading}
              rowKey={(record) => record.id}
              onChange={this.handleTableChange}
            />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.Participations.data,
    pagination: state.Participations.pagination,
    loading: state.Participations.loading,
    search: state.Participations.search,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getParticipations: () => dispatch(Actions.getParticipations()),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSearch: (search) => dispatch(Actions.setSearch(search)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ParticipationListing);
