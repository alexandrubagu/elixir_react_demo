import React, { useEffect, useState } from 'react';
import { Card, Empty } from 'antd';
import {
  Chart,
  Geom,
  Tooltip,
  Coordinate,
  Legend,
  Axis,
  Interaction,
  G2,
  registerShape,
} from 'bizcharts';
import _ from 'lodash';
import DataSet from '@antv/data-set';
import { countVerbatimCategorizationWordSelections } from './api';

function getTextAttrs(cfg) {
  return _.assign({}, cfg.style, {
    fontSize: cfg.data.size,
    text: cfg.data.text,
    textAlign: 'center',
    fontFamily: cfg.data.font,
    fill: cfg.color,
    textBaseline: 'Alphabetic',
  });
}
registerShape('point', 'cloud', {
  draw(cfg, container) {
    const attrs = getTextAttrs(cfg);
    const textShape = container.addShape('text', {
      attrs: _.assign(attrs, {
        x: cfg.x,
        y: cfg.y,
      }),
    });
    if (cfg.data.rotate) {
      G2.Util.rotate(textShape, (cfg.data.rotate * Math.PI) / 180);
    }
    return textShape;
  },
});

const scale = {
  x: {
    nice: false,
  },
  y: {
    nice: false,
  },
};

const VerbatimCategorizationWordCloud = (props) => {
  const [dataset, setDataset] = useState(false);

  useEffect(() => {
    countVerbatimCategorizationWordSelections().then((response) => {
      if (response.data.length > 0) {
        let dataset = new DataSet.View().source(response.data);

        let [min, max] = dataset.range('count');
        if (min == max) {
          max = min + 1;
        }

        dataset.transform({
          type: 'tag-cloud',
          fields: ['word', 'count'],
          size: [600, 500],
          font: 'Verdana',
          padding: 0,
          timeInterval: 5000, // max execute time
          rotate() {
            let random = ~~(Math.random() * 4) % 4;
            if (random === 2) {
              random = 0;
            }
            return random * 90; // 0, 90, 270
          },
          fontSize(d) {
            if (d.count) {
              return ((d.count - min) / (max - min)) * (40 - 12) + 12;
            }
            return 0;
          },
        });

        setDataset(dataset);
      }
    });
  }, []);
  return (
    <Card title="Verbatim Word Cloud">
      {dataset ? (
        <Chart
          height={600}
          data={dataset.rows}
          scale={scale}
          padding={0}
          autoFit={true}
        >
          <Tooltip showTitle={false} />
          <Coordinate reflect="y" />
          <Axis name="x" visible={false} />
          <Axis name="y" visible={false} />
          <Legend visible={false} />
          <Geom
            type="point"
            position="x*y"
            color="word"
            shape="cloud"
            tooltip="count*word"
          />
          <Interaction type="element-active" />
        </Chart>
      ) : (
        <Empty description="No analyzed verbatims yet" />
      )}
    </Card>
  );
};

export default VerbatimCategorizationWordCloud;
