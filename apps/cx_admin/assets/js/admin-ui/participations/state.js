import * as api from './api';
import * as common from '../reducers/common';

const PARTICIPATIONS_REQUEST_PENDING = 'PARTICIPATIONS_REQUEST_PENDING';
const PARTICIPATIONS_REQUEST_ERROR = 'PARTICIPATIONS_REQUEST_ERROR';
const PARTICIPATIONS_FETCHED = 'PARTICIPATIONS_FETCHED';
const PARTICIPATION_FETCHED = 'PARTICIPATION_FETCHED';
const SESSIONS_FETCHED = 'SESSIONS_FETCHED';
const VERBATIMS_PROCESSING_STARTED = 'VERBATIMS_PROCESSING_STARTED';
const VERBATIMS_PROCESSING_STATS_FETCHED = 'VERBATIMS_PROCESSING_STATS_FETCHED';

const SET_PARTICIPATIONS_PAGINATION = 'SET_PARTICIPATIONS_PAGINATION';
const SET_PARTICIPATIONS_SORTING = 'SET_PARTICIPATIONS_SORTING';
const SET_PARTICIPATIONS_SEARCH = 'SET_PARTICIPATIONS_SEARCH';
const SET_PARTICIPATIONS_FILTERS = 'SET_PARTICIPATIONS_FILTERS';

const pendingRequest = () => ({
  type: PARTICIPATIONS_REQUEST_PENDING,
});

const requestFailed = (error) => ({
  type: PARTICIPATIONS_REQUEST_ERROR,
  payload: error,
});

const participationsFetched = (result) => ({
  type: PARTICIPATIONS_FETCHED,
  payload: result,
});

const participationFetched = (result) => ({
  type: PARTICIPATION_FETCHED,
  payload: result,
});

const sessionsFetched = (result) => ({
  type: SESSIONS_FETCHED,
  payload: result,
});

const verbatimProcessingStarted = (result) => ({
  type: VERBATIMS_PROCESSING_STARTED,
  payload: result,
});

const verbatimProcessingStatsFetched = (result) => ({
  type: VERBATIMS_PROCESSING_STATS_FETCHED,
  payload: result,
});

export const Actions = {
  getParticipations: () => (dispatch, getState) => {
    const { pagination, sorting, search, filters } = getState().Participations;

    dispatch(pendingRequest());
    return api.getParticipations({ pagination, sorting, search, filters }).then(
      (response) => dispatch(participationsFetched(response)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  getParticipation: (id) => (dispatch) => {
    dispatch(pendingRequest());
    return api.getParticipation(id).then(
      (response) => dispatch(participationFetched(response.data)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  getSessions: () => (dispatch) => {
    dispatch(pendingRequest());
    return api.getSessions().then(
      (response) => dispatch(sessionsFetched(response)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  processVerbatims: () => (dispatch) => {
    dispatch(pendingRequest());
    return api.processVerbatims().then(
      (response) => dispatch(verbatimProcessingStarted(response)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  getVerbatimProcessingStats: () => (dispatch) => {
    dispatch(pendingRequest());
    return api.getVerbatimProcessingStats().then(
      (response) => dispatch(verbatimProcessingStatsFetched(response)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  setPagination: (pagination) => ({
    type: SET_PARTICIPATIONS_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorting) => {
    return {
      type: SET_PARTICIPATIONS_SORTING,
      payload: sorting,
    };
  },

  setSearch: (search) => ({
    type: SET_PARTICIPATIONS_SEARCH,
    payload: search,
  }),

  setFilters: (filters) => ({
    type: SET_PARTICIPATIONS_FILTERS,
    payload: filters,
  }),
};

const initialState = {
  loading: false,
  pagination: {},
  sorting: {},
  search: '',
  filters: {},
  data: [],
  current: {},
  sessions: [],
  error: {},
  verbatimProcessingStats: [],
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case PARTICIPATIONS_REQUEST_PENDING:
      return { ...state, loading: true };

    case PARTICIPATIONS_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_PARTICIPATIONS_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_PARTICIPATIONS_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_PARTICIPATIONS_SEARCH:
      return common.updateSearch(state, action.payload);

    case PARTICIPATIONS_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case PARTICIPATION_FETCHED:
      return { ...state, loading: false, current: action.payload };

    case SET_PARTICIPATIONS_FILTERS:
      return { ...state, filters: action.payload };

    case SESSIONS_FETCHED:
      return { ...state, loading: false, sessions: action.payload.data };

    case VERBATIMS_PROCESSING_STARTED:
      return { ...state, loading: false };

    case VERBATIMS_PROCESSING_STATS_FETCHED:
      return {
        ...state,
        loading: false,
        verbatimProcessingStats: action.payload.data,
      };

    default:
      return state;
  }
};
