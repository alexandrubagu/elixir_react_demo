import React, { useEffect, useState } from 'react';
import {
  Chart,
  Axis,
  Tooltip,
  Coordinate,
  Interval,
  Interaction,
  Legend,
} from 'bizcharts';
import { Card, Empty } from 'antd';
import { countVerbatimCategorizationTopicSelections } from './api';

const VerbatimCategorizationChart = (props) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    countVerbatimCategorizationTopicSelections().then((response) =>
      setData(response.data),
    );
  }, []);

  return (
    <Card title="Verbatim Topic Distribution" style={{ marginBottom: 24 }}>
      {data.length == 0 ? (
        <Empty description="No analyzed verbatims yet" />
      ) : (
        <Chart height={400} data={data} autoFit>
          <Coordinate type="theta" radius={0.75} />
          <Tooltip showTitle={false} />
          <Axis visible={false} />
          <Legend position="top" />
          <Interval
            position="count"
            adjust="stack"
            color="topic"
            style={{
              lineWidth: 1,
              stroke: '#fff',
            }}
            label={[
              'count',
              {
                content: (data) => {
                  return `${data.topic}: ${data.count}`;
                },
              },
            ]}
          />
          <Interaction type="element-single-selected" />
        </Chart>
      )}
    </Card>
  );
};

export default VerbatimCategorizationChart;
