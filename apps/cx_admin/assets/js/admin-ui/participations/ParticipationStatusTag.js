import React from 'react';
import { Tag } from 'antd';

const color = (status) => {
  switch (status) {
    case 'active':
      return 'processing';

    case 'abandonned':
      return 'warning';

    case 'completed':
      return 'success';

    default:
      return state;
  }
};

const ParticipationStatusTag = (props) => {
  return <Tag color={color(props.status)}>{props.status}</Tag>;
};

export default ParticipationStatusTag;
