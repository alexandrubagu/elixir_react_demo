import React from 'react';
import { Tag, Timeline, Row, Col, Typography, Card, Empty, Space } from 'antd';
import Moment from 'react-moment';
import { JSONCode } from '../components/utils';

const color_and_label = (type) => {
  switch (type) {
    case 'started':
      return ['processing', 'Started'];

    case 'abandonned':
      return ['warning', 'Abandonned'];

    case 'completed':
      return ['success', 'Completed'];

    case 'resumed':
      return ['processing', 'Resumed'];

    case 'session_closed':
      return ['warning', 'Session closed'];
  }
};

export const TypeTag = (props) => {
  const [color, label] = color_and_label(props.type);
  return <Tag color={color}>{label}</Tag>;
};

export const EventTimeline = (props) => {
  const events = props.events || [];
  if (events.length == 0) {
    return <Empty description="No events yet" />;
  } else {
    return (
      <Timeline>
        {events.map((event) => (
          <EventTimelineItem event={event} key={event.id} />
        ))}
      </Timeline>
    );
  }
};

const EventTimelineItem = (props) => {
  const event = props.event;

  return (
    <Timeline.Item key={event.id}>
      <Row>
        <Col>
          <TypeTag type={event.type} />
        </Col>
        <Col>
          <Typography.Text disabled>
            <Moment fromNow>{event.inserted_at}</Moment>
          </Typography.Text>
        </Col>
      </Row>
      <Card bordered={false}>
        <Description event={event} />
      </Card>
    </Timeline.Item>
  );
};

const Description = (props) => {
  switch (props.event.type) {
    case 'started':
      return <span>A participation session was started</span>;

    case 'abandonned':
      return <span>The session was abandonned (timer expired)</span>;

    case 'completed':
      return (
        <Space>
          <span>The participation completed, result: </span>
          <JSONCode json={props.data} />
        </Space>
      );

    case 'resumed':
      return <span>The participation was resumed</span>;

    case 'session_closed':
      return <span>The participation session closed</span>;
  }
};
