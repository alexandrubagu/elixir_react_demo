import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Descriptions,
  Layout,
  PageHeader,
  Card,
  Tabs,
  Space,
  Row,
  Col,
} from 'antd';
import ParticipationStatusTag from './ParticipationStatusTag';
import { EventTimeline } from './ParticipationEvent';
import VerbatimDetails from './VerbatimDetails';
import { Actions } from './state';
import { JSONCode, UUIDLink } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';
import Moment from 'react-moment';

class ParticipationDetails extends Component {
  constructor(props) {
    super(props);
    this.state = { breadcrumbs: {} };
  }

  componentDidMount() {
    this.props.getParticipation(this.props.match.params.id).then(() => {
      this.setState({
        breadcrumbs: Breadcrumbs.showParticipation({
          participation: this.props.current,
        }),
      });
    });
  }

  render() {
    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Participation details"
            subTitle={`(${this.props.current.id || ''})`}
            tags={<ParticipationStatusTag status="active" />}
            breadcrumb={this.state.breadcrumbs}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Row gutter={[16, 24]}>
            <Col span={16}>
              <Card title="Participation details">
                <GeneralDetails participation={this.props.current} />
              </Card>
              <Card title="Verbatim details" style={{ marginTop: 24 }}>
                <VerbatimDetails participation={this.props.current} />
              </Card>
            </Col>
            <Col span={8}>
              <Card title="Event timeline">
                <EventTimeline events={this.props.current.events} />
              </Card>
            </Col>
          </Row>
        </Layout.Content>
      </Layout>
    );
  }
}

const GeneralDetails = (props) => {
  const participation = props.participation;

  return (
    <Space direction="vertical" size="large" style={{ width: '100%' }}>
      <Descriptions title="General" column={2} bordered>
        <Descriptions.Item label="Ref" span={2}>
          {participation.ref}
        </Descriptions.Item>

        <Descriptions.Item label="Survey">
          <UUIDLink
            prefix="/cx/admin/ui/surveys/"
            id={participation.survey_id}
          />
        </Descriptions.Item>

        <Descriptions.Item label="Language">
          {participation.language}
        </Descriptions.Item>

        <Descriptions.Item label="Params" span={2}>
          <JSONCode json={participation.params} />
        </Descriptions.Item>

        <Descriptions.Item label="Content" span={2}>
          <JSONCode json={participation.content} />
        </Descriptions.Item>

        <Descriptions.Item label="Answers" span={2}>
          <JSONCode json={participation.answers} />
        </Descriptions.Item>

        <Descriptions.Item label="Created">
          <Moment fromNow>{participation.inserted_at}</Moment>
        </Descriptions.Item>

        <Descriptions.Item label="Last updated">
          <Moment fromNow>{participation.updated_at}</Moment>
        </Descriptions.Item>
      </Descriptions>
    </Space>
  );
};

const mapStateToProps = (state) => {
  return {
    current: state.Participations.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getParticipation: (id) => dispatch(Actions.getParticipation(id)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ParticipationDetails);
