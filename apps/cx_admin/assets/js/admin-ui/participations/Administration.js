import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  Table,
  Layout,
  PageHeader,
  Tabs,
  Card,
  Form,
  Input,
} from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import Moment from 'react-moment';
import { UUIDLink } from '../components/utils';
import { Actions } from './state';
import Breadcrumbs from './Breadcrumbs';
import VerbatimProcessor from './VerbatimProcessor';

const columns = [
  {
    title: 'ID',
    dataIndex: 'id',
    render: (text) => (
      <UUIDLink prefix="/cx/admin/ui/participations/" id={text} />
    ),
  },
  {
    title: 'Ref',
    dataIndex: 'ref',
    sorter: true,
  },
  {
    title: 'Lang',
    dataIndex: 'language',
    sorter: true,
  },
  {
    title: 'Last updated',
    dataIndex: 'updated_at',
    sorter: true,
    render: (text) => <Moment fromNow>{text}</Moment>,
  },
];

const Administration = (props) => {
  useEffect(() => {
    props.setFilters({ verbatims_processed: [false] });
    props.getParticipations();

    return () => {
      props.setFilters({});
      props.getParticipations();
    };
  }, []);

  const handleSearch = (e) => {
    Promise.resolve(props.setSearch(e.target.value)).then(
      props.getEmailWebhookEvents,
    );
  };

  const handleTableChange = (pagination, filters, sorter) => {
    Promise.all([
      this.props.setPagination(pagination),
      this.props.setFilters(filters),
    ]).then(this.props.getParticipations());
  };

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Verbatims Administration"
          breadcrumb={Breadcrumbs.verbatimAdministration()}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Tabs>
          <Tabs.TabPane tab="Pending Processing Verbatims" key="pending">
            <Row gutter={[16, 24]}>
              <Col span={18}>
                <Card style={{ marginBottom: 24 }}>
                  <Form layout="inline">
                    <Form.Item key="1" style={{ width: '100%' }}>
                      <Input
                        prefix={
                          <SearchOutlined className="site-form-item-icon" />
                        }
                        placeholder="Search"
                        value={props.search}
                        onChange={handleSearch}
                      />
                    </Form.Item>
                  </Form>
                </Card>
                <Table
                  columns={columns}
                  dataSource={props.data}
                  pagination={props.pagination}
                  loading={props.loading}
                  rowKey={(record) => record.id}
                  onChange={handleTableChange}
                />
              </Col>
              <Col span={6}>
                <VerbatimProcessor />
              </Col>
            </Row>
          </Tabs.TabPane>
        </Tabs>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.Participations.data,
    pagination: state.Participations.pagination,
    loading: state.Participations.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getParticipations: () => dispatch(Actions.getParticipations()),
    setFilters: (filters) => dispatch(Actions.setFilters(filters)),
    setSearch: (search) => dispatch(Actions.setSearch(filters)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Administration);
