import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  Card,
  Button,
  Empty,
  Timeline,
  Typography,
  Modal,
} from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Actions } from './state';
import Moment from 'react-moment';
import { UUIDLink, success_notification } from '../components/utils';

const VerbatimProcessorLogs = (props) => {
  const logs = props.logs || [];

  if (logs.length == 0) {
    return <Empty description="No logs yet" />;
  } else {
    return (
      <Timeline>
        {logs.map((log, i) => (
          <VerbatimProcessorLog log={log} key={i} />
        ))}
      </Timeline>
    );
  }
};

const VerbatimProcessorLog = (props) => {
  const log = props.log;

  return (
    <Timeline.Item key={log.participation}>
      <Row gutter={8}>
        <Col>
          <Typography.Text disabled>
            <Moment fromNow>{log.ts}</Moment>
          </Typography.Text>
        </Col>
      </Row>
      <Card bordered={false}>
        <span>
          Participation{' '}
          <UUIDLink
            prefix="/cx/admin/ui/participations/"
            id={log.participation}
          />{' '}
          was {log.status} <Moment fromNow>{log.ts}</Moment>
        </span>
      </Card>
    </Timeline.Item>
  );
};

const VerbatimProcessor = (props) => {
  useEffect(() => {
    props.getVerbatimProcessingStats();
  }, []);

  const delay = () =>
    new Promise((resolve) => setTimeout(() => resolve('done'), 500));

  const processVerbatims = () =>
    props.processVerbatims().then(delay).then(props.getVerbatimProcessingStats);

  const processingModal = (_event) => {
    Modal.confirm({
      title: 'Run processing ?',
      content:
        'This will run a job to translate all participations, perform sentiment analysis and topic categorization ?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () => {
        processVerbatims()
          .then(() => success_notification('Verbatim Processor started !'))
          .then(() => {
            props.getVerbatimProcessingStats();
            props.setFilters({ verbatims_processed: [false] });
            props.getParticipations();
          });
      },
    });
  };

  return (
    <Card
      title={<h4>Verbatims Processor</h4>}
      style={{ marginBottom: 24 }}
      extra={[
        <Button type="primary" onClick={processingModal}>
          Run processing
        </Button>,
      ]}
    >
      <VerbatimProcessorLogs logs={props.stats.logs || []} />
    </Card>
  );
};

const mapStateToProps = (state) => {
  return {
    stats: state.Participations.verbatimProcessingStats,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getParticipations: () => dispatch(Actions.getParticipations()),
    setFilters: (filters) => dispatch(Actions.setFilters(filters)),
    processVerbatims: () => dispatch(Actions.processVerbatims()),
    getVerbatimProcessingStats: () =>
      dispatch(Actions.getVerbatimProcessingStats()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(VerbatimProcessor);
