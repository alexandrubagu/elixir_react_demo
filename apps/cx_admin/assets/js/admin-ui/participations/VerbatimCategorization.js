import React from 'react';
import { connect } from 'react-redux';
import { Row, Col, Layout, PageHeader, Button } from 'antd';
import { SettingOutlined } from '@ant-design/icons';
import VerbatimCategorizationChart from './VerbatimCategorizationChart';
import LatestVerbatimCategorizationTimeline from './LatestVerbatimCategorizationTime';
import VerbatimCategorizationWordCloud from './VerbatimCategorizationWordCloud';

const VerbatimCategorization = (props) => {
  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Verbatim analysis overview"
          extra={[
            <Button
              shape="circle"
              icon={<SettingOutlined />}
              onClick={() =>
                props.history.push(
                  `/cx/admin/ui/participations/verbatim-categorization/administration`,
                )
              }
              key={'administration'}
            />,
          ]}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Row gutter={[16, 24]}>
          <Col span={14}>
            <VerbatimCategorizationChart />
            <VerbatimCategorizationWordCloud />
          </Col>
          <Col span={10}>
            <LatestVerbatimCategorizationTimeline />
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
};

export default connect()(VerbatimCategorization);
