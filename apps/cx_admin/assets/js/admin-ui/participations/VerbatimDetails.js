import React from 'react';
import {
  Row,
  Col,
  Card,
  Result,
  Descriptions,
  Typography,
  Space,
  Progress,
  Empty,
  Rate,
} from 'antd';
import { HistoryOutlined } from '@ant-design/icons';
import { FrownTwoTone, MehTwoTone, SmileTwoTone } from '@ant-design/icons';

const VerbatimDetails = (props) => {
  const processed = props.participation.verbatims_processed || false;
  const verbatims = props.participation.verbatims || [];

  return (
    <Card>
      {!processed && (
        <Result
          icon={<HistoryOutlined />}
          title="Processing of verbatims has not completed yet"
          subTitle="Please check later to see processed verbatim results"
        />
      )}
      {processed && verbatims.length == 0 && (
        <Empty description="No Verbatims available" />
      )}
      {processed &&
        verbatims.map((verbatim, idx) => (
          <VerbatimDetail verbatim={verbatim} key={idx} />
        ))}
    </Card>
  );
};
const Circle = () => (
  <svg width="1em" height="1em" fill="currentColor" viewBox="0 0 1024 1024">
    <circle
      cx="512"
      cy="512"
      r="400"
      stroke="currentColor"
      strokeWidth="100"
      fill="white"
    />
    Sorry, your browser does not support inline SVG.
  </svg>
);

const likelihoodLookup = {
  LIKELIHOOD_UNSPECIFIED: { value: 1, color: '#c3c9c4' },
  VERY_UNLIKELY: { value: 2, color: '#ff341e' },
  UNLIKELY: { value: 3, color: '#ff992b' },
  POSSIBLE: { value: 4, color: '#ffce35' },
  LIKELY: { value: 5, color: '#86d12e' },
  VERY_LIKELY: { value: 6, color: '#00be2c' },
};

const VerbatimDetail = (props) => {
  const verbatim = props.verbatim;
  const topic_probabilities =
    verbatim.verbatim_categorization &&
    verbatim.verbatim_categorization.topic_probabilities;

  return (
    <Descriptions
      column={2}
      title={`Question "${verbatim.question}"`}
      bordered
      style={{ marginBottom: 24 }}
    >
      <Descriptions.Item label="Original Text" span={2}>
        {verbatim.text}
      </Descriptions.Item>
      {verbatim.dlp_finding ? (
        <Descriptions.Item label="Redacted Text" span={2}>
          {verbatim.dlp_finding.text_dlp}
        </Descriptions.Item>
      ) : (
        <Descriptions.Item label="Translated Text" span={2}>
          {verbatim.text_en}
        </Descriptions.Item>
      )}
      <Descriptions.Item label="Sentiment" span={2}>
        <SentimentDetails
          score={verbatim.sentiment_score}
          magnitude={verbatim.sentiment_magnt}
        />
      </Descriptions.Item>
      <Descriptions.Item label="Topic classification" span={2}>
        {topic_probabilities ? (
          <table>
            {topic_probabilities.map((item, i) => (
              <tbody key={i}>
                <tr>
                  <td width={200}>{item.topic}</td>
                  <td>
                    <Progress
                      size="small"
                      percent={
                        Math.round(parseFloat(item.probability) * 10000) / 100
                      }
                    />
                  </td>
                </tr>
              </tbody>
            ))}
          </table>
        ) : (
          <Empty description="No data" />
        )}
      </Descriptions.Item>
      <Descriptions.Item label="Data Loss Analysis" span={2}>
        {verbatim.dlp_finding ? (
          <table>
            {verbatim.dlp_finding.probabilities.map((probability, i) => (
              <tbody key={i}>
                <tr>
                  <td width={110}>{probability.info_type}</td>
                  <td width={100}>
                    <Rate
                      disabled
                      count={6}
                      value={likelihoodLookup[probability.likelihood].value}
                      style={{
                        color: likelihoodLookup[probability.likelihood].color,
                      }}
                      character={<Circle />}
                    />
                  </td>
                  <td width={200}>
                    <span>{probability.likelihood}</span>
                  </td>
                </tr>
              </tbody>
            ))}
          </table>
        ) : (
          <Empty description="No data" />
        )}
      </Descriptions.Item>
    </Descriptions>
  );
};

const SentimentDetails = (props) => {
  const score = props.score;
  const magnitude = props.magnitude;

  return (
    <SentimentIcon score={score}>
      <Typography.Text type="secondary">
        (score: {score} / magn: {magnitude})
      </Typography.Text>
    </SentimentIcon>
  );
};

export const SentimentIcon = (props) => {
  if (props.score < -1) {
    return (
      <Space>
        <FrownTwoTone twoToneColor="#eb2f96" style={{ fontSize: '1.5rem' }} />
        {props.children}
      </Space>
    );
  }
  if (props.score > 1) {
    return (
      <Space>
        <SmileTwoTone twoToneColor="#52c41a" style={{ fontSize: '1.5rem' }} />
        {props.children}
      </Space>
    );
  }
  return (
    <Space>
      <MehTwoTone style={{ fontSize: '1.5rem' }} />
      {props.children}
    </Space>
  );
};

export default VerbatimDetails;
