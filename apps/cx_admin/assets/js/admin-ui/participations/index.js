import * as State from './state';
import ParticipationListing from './ParticipationListing';
import ParticipationDetails from './ParticipationDetails';
import ParticipationSessions from './ParticipationSessions';
import VerbatimCategorization from './VerbatimCategorization';
import Administration from './Administration';

export const Actions = State.Actions;
export const Reducer = State.Reducer;
export const Listing = ParticipationListing;
export const Details = ParticipationDetails;
export const Sessions = ParticipationSessions;
export const VerbatimCategorizationOverview = VerbatimCategorization;
export const VerbatimAdministration = Administration;
