import React from 'react';
import { withRouter } from 'react-router';
import { Switch, Route } from 'react-router-dom';
import { Layout } from 'antd';

import SideMenu from './SideMenu';
import * as BlacklistEntries from '../blacklist_entries';
import * as SmsMessages from '../sms_messages';
import * as EmailMessages from '../email_messages';
import * as EmailWebhookEvents from '../email_webhook_events';
import * as Invites from '../invites';
import Batches from '../batches';
import BatchEntries from '../batch_entries';
import * as Participation from '../participations';
import * as Surveys from '../surveys';
import * as ParticipationSyncers from '../participation_syncers';
import Stats from '../stats/Show';
import * as Webhooks from '../webhooks';

const Navigation = (props) => {
  return (
    <>
      <Layout.Sider width={230} theme="dark">
        <div
          className="logo"
          onClick={(e) => {
            props.history.push('/cx/admin/ui');
            e.preventDefault();
          }}
        ></div>
        <SideMenu />
      </Layout.Sider>

      <Switch>
        <Route
          path="/cx/admin/ui/batches/:id/batch-entries/new"
          component={BatchEntries.New}
        />

        <Route
          path="/cx/admin/ui/batch-entries/:id"
          component={BatchEntries.Show}
        />

        <Route path="/cx/admin/ui/batches/new" component={Batches.New} />
        <Route path="/cx/admin/ui/batches/:id" component={Batches.Show} />
        <Route path="/cx/admin/ui/batches" component={Batches.List} />

        <Route
          path="/cx/admin/ui/blacklist/entries/new"
          component={BlacklistEntries.Creation}
        />

        <Route
          path="/cx/admin/ui/blacklist/entries/:id/update"
          component={BlacklistEntries.Update}
        />

        <Route
          path="/cx/admin/ui/blacklist/entries/:id"
          component={BlacklistEntries.Show}
        />

        <Route
          path="/cx/admin/ui/blacklist/entries"
          component={BlacklistEntries.Listing}
          exact
        />

        <Route
          path="/cx/admin/ui/sms/messages/:id"
          component={SmsMessages.Show}
        />

        <Route
          path="/cx/admin/ui/sms/messages"
          component={SmsMessages.List}
          exact
        />

        <Route
          path="/cx/admin/ui/emails/messages/:id"
          component={EmailMessages.Show}
        />

        <Route
          path="/cx/admin/ui/emails/messages"
          component={EmailMessages.List}
          exact
        />

        <Route
          path="/cx/admin/ui/emails/webhooks/events/:id"
          component={EmailWebhookEvents.Show}
        />

        <Route
          path="/cx/admin/ui/emails/webhooks/events"
          component={EmailWebhookEvents.List}
          exact
        />

        <Route path="/cx/admin/ui/surveys/new" component={Surveys.New} />

        <Route
          path="/cx/admin/ui/surveys/:id/update"
          component={Surveys.Update}
        />

        <Route path="/cx/admin/ui/surveys/:id" component={Surveys.Show} />

        <Route path="/cx/admin/ui/surveys" component={Surveys.List} exact />

        <Route path="/cx/admin/ui/invites/:id" component={Invites.Show} />

        <Route path="/cx/admin/ui/invites" component={Invites.List} exact />

        <Route
          path="/cx/admin/ui/participation-sessions"
          component={Participation.Sessions}
        />

        <Route
          path="/cx/admin/ui/participations/verbatim-categorization/administration"
          component={Participation.VerbatimAdministration}
        />

        <Route
          path="/cx/admin/ui/participations/verbatim-categorization"
          component={Participation.VerbatimCategorizationOverview}
        />

        <Route
          path="/cx/admin/ui/participations/:id"
          component={Participation.Details}
        />

        <Route
          path="/cx/admin/ui/participations"
          component={Participation.Listing}
          exact
        />

        <Route
          path="/cx/admin/ui/participation-syncers/new"
          component={ParticipationSyncers.New}
        />

        <Route
          path="/cx/admin/ui/participation-syncers/:id/update"
          component={ParticipationSyncers.Update}
        />

        <Route
          path="/cx/admin/ui/participation-syncers/:id"
          component={ParticipationSyncers.Show}
        />

        <Route
          path="/cx/admin/ui/participation-syncers"
          component={ParticipationSyncers.List}
        />

        <Route path="/cx/admin/ui/webhooks/:id" component={Webhooks.Show} />

        <Route path="/cx/admin/ui/webhooks" component={Webhooks.List} />

        <Route path="/cx/admin/ui" component={Stats} exact />
      </Switch>
    </>
  );
};

export default withRouter(Navigation);
