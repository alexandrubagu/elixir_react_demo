import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'antd';
import { withRouter } from 'react-router';

import {
  NotificationOutlined,
  MessageOutlined,
  ReconciliationOutlined,
  ThunderboltOutlined,
  RocketOutlined,
  ToTopOutlined,
} from '@ant-design/icons';

const regexActiveMenuItems = [
  { regex: new RegExp('/cx/admin/ui/batches(.*?)'), key: 'batches' },
  { regex: new RegExp('/cx/admin/ui/surveys(.*?)'), key: 'surveys' },
  { regex: new RegExp('/cx/admin/ui/invites(.*?)'), key: 'invites' },
  {
    regex: new RegExp('/cx/admin/ui/participation-sessions(.*?)'),
    key: 'participations.sessions',
  },
  {
    regex: new RegExp('/cx/admin/ui/participation-syncers(.*?)'),
    key: 'participations.syncers',
  },
  {
    regex: new RegExp(
      '/cx/admin/ui/participations/verbatim-categorization(.*?)',
    ),
    key: 'verbatim.categorization',
  },
  {
    regex: new RegExp('/cx/admin/ui/participations(.*?)'),
    key: 'participations.entries',
  },
  {
    regex: new RegExp('/cx/admin/ui/blacklist/entries(.*?)'),
    key: 'notifications.blacklist_entries',
  },
  {
    regex: new RegExp('/cx/admin/ui/sms/messages(.*?)'),
    key: 'notifications.sms_messages',
  },
  {
    regex: new RegExp('/cx/admin/ui/emails/messages(.*?)'),
    key: 'notifications.email_messages',
  },
  {
    regex: new RegExp('/cx/admin/ui/emails/webhooks/events(.*?)'),
    key: 'notifications.email_webhook_events',
  },
  {
    regex: new RegExp('/cx/admin/ui/webhooks(.*?)'),
    key: 'webhooks',
  },
];

const SideMenu = (props) => {
  const [activeMenuItem, setActiveMenuItem] = useState(['']);

  useEffect(() => {
    let [activeMenuItem, ...reset] = regexActiveMenuItems.reduce(
      (acc, item) => {
        if (props.location.pathname.match(item.regex)) {
          acc.push(item.key);
        }
        return acc;
      },
      [],
    );

    setActiveMenuItem([activeMenuItem]);
  }, [props.location.pathname]);
  return (
    <Menu
      mode="inline"
      selectedKeys={activeMenuItem}
      defaultOpenKeys={['notifications', 'participations']}
      theme="dark"
      style={{ height: '100%', borderRight: 0 }}
    >
      <Menu.Item key="batches" icon={<ThunderboltOutlined />}>
        <Link to="/cx/admin/ui/batches">Batches</Link>
      </Menu.Item>
      <Menu.Item key="surveys" icon={<ReconciliationOutlined />}>
        <Link to="/cx/admin/ui/surveys">Surveys</Link>
      </Menu.Item>
      <Menu.Item key="invites" icon={<MessageOutlined />}>
        <Link to="/cx/admin/ui/invites">Invites</Link>
      </Menu.Item>
      <Menu.SubMenu
        key="participations"
        icon={<RocketOutlined />}
        title="Participations"
      >
        <Menu.Item key="participations.entries">
          <Link to="/cx/admin/ui/participations">Entries</Link>
        </Menu.Item>

        <Menu.Item key="participations.sessions">
          <Link to="/cx/admin/ui/participation-sessions">Sessions</Link>
        </Menu.Item>

        <Menu.Item key="participations.syncers">
          <Link to="/cx/admin/ui/participation-syncers">Syncers</Link>
        </Menu.Item>

        <Menu.Item key="verbatim.categorization">
          <Link to="/cx/admin/ui/participations/verbatim-categorization">
            Verbatim analysis
          </Link>
        </Menu.Item>
      </Menu.SubMenu>
      <Menu.SubMenu
        key="notifications"
        icon={<NotificationOutlined />}
        title="Notifications"
      >
        <Menu.Item key="notifications.blacklist_entries">
          <Link to="/cx/admin/ui/blacklist/entries">Blacklist Entries</Link>
        </Menu.Item>
        <Menu.Item key="notifications.sms_messages">
          <Link to="/cx/admin/ui/sms/messages">Sms Messages</Link>
        </Menu.Item>
        <Menu.Item key="notifications.email_messages">
          <Link to="/cx/admin/ui/emails/messages">Email Messages</Link>
        </Menu.Item>
        <Menu.Item key="notifications.email_webhook_events">
          <Link to="/cx/admin/ui/emails/webhooks/events">
            Email Webhooks Events
          </Link>
        </Menu.Item>
      </Menu.SubMenu>
      <Menu.Item key="webhooks" icon={<ToTopOutlined />}>
        <Link to="/cx/admin/ui/webhooks">Webhooks</Link>
      </Menu.Item>
    </Menu>
  );
};

export default withRouter(SideMenu);
