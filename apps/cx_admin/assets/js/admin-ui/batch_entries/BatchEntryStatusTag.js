import React from 'react';
import { Tag } from 'antd';

const color = (status) => {
  switch (status) {
    case 'unprocessed':
      return 'blue';

    case 'processed':
      return 'green';

    case 'invite_failed':
      return 'red';

    case 'mail_failed':
      return 'red';

    case 'sms_failed':
      return 'red';
  }
};

const BatchEntryStatusTag = (props) => {
  return <Tag color={color(props.status)}>{props.status}</Tag>;
};

export default BatchEntryStatusTag;
