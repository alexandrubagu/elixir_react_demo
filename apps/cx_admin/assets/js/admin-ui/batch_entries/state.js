import * as api from './api';
import * as common from '../reducers/common';

const BATCH_ENTRIES_REQUEST_PENDING = 'BATCH_ENTRIES_REQUEST_PENDING';
const BATCH_ENTRIES_REQUEST_ERROR = 'BATCH_ENTRIES_REQUEST_ERROR';

const SET_BATCH_ENTRIES_PAGINATION = 'SET_BATCH_ENTRY_PAGINATION';
const SET_BATCH_ENTRIES_SORTING = 'SET_BATCH_ENTRY_SORTING';
const SET_BATCH_ENTRIES_SEARCH = 'SET_BATCH_ENTRY_SEARCH';

const BATCH_ENTRIES_FETCHED = 'BATCH_ENTRIES_FETCHED';
const BATCH_ENTRY_FETCHED = 'BATCH_ENTRY_FETCHED';
const BATCH_ENTRY_CREATED = 'BATCH_ENTRY_CREATED';
const BATCH_ENTRY_DELETED = 'BATCH_ENTRY_DELETED';
const BATCH_ENTRY_PROCESSED = 'BATCH_ENTRY_PROCESSED';
const BATCH_ENTRIES_UPLOADED = 'BATCH_ENTRIES_UPLOADED';

const pendingRequest = () => ({
  type: BATCH_ENTRIES_REQUEST_PENDING,
});

const errorRequest = (error) => ({
  type: BATCH_ENTRIES_REQUEST_ERROR,
  payload: error,
});

const entriesFetched = (result) => ({
  type: BATCH_ENTRIES_FETCHED,
  payload: result,
});

const entryFetched = (result) => ({
  type: BATCH_ENTRY_FETCHED,
  payload: result,
});

const entryCreated = (result) => ({
  type: BATCH_ENTRY_CREATED,
  payload: result,
});

const entryDeleted = (result) => ({
  type: BATCH_ENTRY_DELETED,
  payload: result,
});

const entryProcessed = (result) => ({
  type: BATCH_ENTRY_PROCESSED,
  payload: result,
});

const entriesUploaded = (result) => ({
  type: BATCH_ENTRIES_UPLOADED,
  payload: result,
});

export const Actions = {
  setPagination: (pagination) => ({
    type: SET_BATCH_ENTRIES_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorter) => ({
    type: SET_BATCH_ENTRIES_SORTING,
    payload: sorter,
  }),

  setSearch: (search) => ({
    type: SET_BATCH_ENTRIES_SEARCH,
    payload: search,
  }),

  getBatchEntries: (batchId) => (dispatch, getState) => {
    const { pagination, sorting, search } = getState().BatchEntries;
    dispatch(pendingRequest());
    return api.getBatchEntries(batchId, { pagination, sorting, search }).then(
      (response) => dispatch(entriesFetched(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  getBatchEntry: (entryId) => (dispatch) => {
    dispatch(pendingRequest());
    return api.getBatchEntry(entryId).then(
      (response) => dispatch(entryFetched(response.data)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  deleteBatchEntry: (id) => (dispatch) => {
    dispatch(pendingRequest());
    return api.deleteBatchEntry(id).then(
      (response) => dispatch(entryDeleted(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  createBatchEntry: (batchId, surveyId, params) => (dispatch) => {
    dispatch(pendingRequest());
    return api.createBatchEntry(batchId, surveyId, params).then(
      (response) => dispatch(entryCreated(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  processBatchEntry: (id) => (dispatch) => {
    dispatch(pendingRequest());
    return api.processBatchEntry(id).then(
      (response) => dispatch(entryProcessed(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },

  uploadBatchEntries: (batchId, data) => (dispatch) => {
    dispatch(pendingRequest());
    return api.uploadBatchEntries(batchId, data).then(
      (response) => dispatch(entriesUploaded(response)),
      (error) => dispatch(errorRequest(error)),
    );
  },
};

const initialState = {
  loading: false,
  pagination: { pageSize: 15 },
  sorting: {},
  search: '',
  data: [],
  current: {},
  error: {},
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case BATCH_ENTRIES_REQUEST_PENDING:
      return { ...state, loading: true };

    case BATCH_ENTRIES_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_BATCH_ENTRIES_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_BATCH_ENTRIES_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_BATCH_ENTRIES_SEARCH:
      return common.updateSearch(state, action.payload);

    case BATCH_ENTRIES_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case BATCH_ENTRY_FETCHED:
      return { ...state, loading: false, current: action.payload };

    case BATCH_ENTRY_CREATED:
      return { ...state, loading: false, error: {} };

    case BATCH_ENTRY_DELETED:
      return { ...state, loading: false, error: {} };

    case BATCH_ENTRY_PROCESSED:
      return { ...state, loading: false, error: {} };

    default:
      return state;
  }
};
