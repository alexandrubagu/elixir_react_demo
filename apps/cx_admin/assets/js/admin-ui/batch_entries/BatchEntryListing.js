import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  Row,
  Col,
  Form,
  Input,
  Card,
  Layout,
  Button,
  Space,
  Table,
  Modal,
} from 'antd';
import { SearchOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { Actions } from './state';
import BatchEntryStatusTag from './BatchEntryStatusTag';
import { success_notification, error_notification } from '../components/utils';

class BatchEntryListing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columns: [
        {
          title: 'ID',
          dataIndex: 'id',
          render: (text, record) => {
            const label = text.substring(text.length - 7, text.length);
            return (
              <Link to={`/cx/admin/ui/batch-entries/${text}`}>{label}</Link>
            );
          },
        },
        {
          dataIndex: 'ref',
          title: 'Ref',
          sorter: true,
        },
        {
          title: 'To',
          sorter: false,
          render: (entry, text) => {
            if (entry.notification_type == 'mail') {
              return entry.email_address;
            }
            if (entry.notification_type == 'sms') {
              return entry.sms_number;
            }
          },
        },
        {
          dataIndex: 'language',
          title: 'Language',
          sorter: true,
        },
        {
          dataIndex: 'notification_type',
          title: 'Notification',
          sorter: true,
        },
        {
          dataIndex: 'status',
          title: 'Status',
          sorter: true,
          render: (status) => <BatchEntryStatusTag status={status} />,
        },
        {
          title: 'Actions',
          key: 'actions',
          render: (text, record) => (
            <Space size="middle">
              {record.status != 'processed' && (
                <Button
                  type="warning"
                  onClick={() => this.showProcessModal(record.id)}
                >
                  Process
                </Button>
              )}

              {record.status != 'processed' && (
                <Button
                  type="danger"
                  onClick={() => this.showDeleteModal(record.id)}
                >
                  Delete
                </Button>
              )}
            </Space>
          ),
        },
      ],
    };
  }

  componentDidMount() {
    this.props.getBatchEntries(this.props.batch_id);
  }

  handleShowBatchEntry(id) {
    this.props.history.push(`/cx/admin/ui/batch-entries/${id}`);
  }

  showProcessModal(id) {
    Modal.confirm({
      title: `Do you want to process this batch entry ?`,
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () =>
        this.props
          .processBatchEntry(id)
          .then((response) => {
            if (response.payload.error) {
              return error_notification(response.payload.error.message);
            }
            success_notification('Batch entry processed!');
          })
          .then(() => this.props.getBatchEntries(this.props.batch_id)),
    });
  }

  showDeleteModal(id) {
    Modal.confirm({
      title: `Do you want to delete this batch entry ?`,
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () =>
        this.props
          .deleteBatchEntry(id)
          .then(() => success_notification('Batch entry deleted!'))
          .then(() => this.props.getBatchEntries(this.props.batch_id)),
    });
  }

  handleTableChange(pagination, filters, sorter) {
    Promise.all([
      this.props.setPagination(pagination),
      this.props.setSorting(sorter),
    ]).then(this.props.getBatchEntries(this.props.batch_id));
  }

  handleSearch(e) {
    Promise.resolve(this.props.setSearch(e.target.value)).then(
      this.props.getBatchEntries(this.props.batch_id),
    );
  }

  handleAddEntry() {
    this.props.history.push(
      `/cx/admin/ui/batches/${this.props.batch_id}/batch-entries/new`,
    );
  }

  render() {
    const { entries, pagination, search, loading } = this.props;

    return (
      <Layout.Content>
        <Card style={{ marginBottom: 24 }}>
          <Row gutter={[16, 24]}>
            <Col flex={'auto'}>
              <Form layout="inline">
                <Form.Item style={{ width: '100%' }}>
                  <Input
                    prefix={<SearchOutlined className="site-form-item-icon" />}
                    placeholder="Search"
                    value={search}
                    onChange={(e) => this.handleSearch(e)}
                  />
                </Form.Item>
              </Form>
            </Col>
            <Col flex={'none'}>
              <Space align="horizontal">
                <Button type="primary" onClick={() => this.handleAddEntry()}>
                  Add Entry
                </Button>
              </Space>
            </Col>
          </Row>
        </Card>

        <Card bodyStyle={{ padding: 0 }}>
          <Table
            columns={this.state.columns}
            dataSource={entries}
            pagination={pagination}
            loading={loading}
            rowKey={(record) => record.id}
            onChange={(pagination, filters, sorter) =>
              this.handleTableChange(pagination, filters, sorter)
            }
          />
        </Card>
      </Layout.Content>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.BatchEntries.loading,
    entries: state.BatchEntries.data,
    pagination: state.BatchEntries.pagination,
    search: state.BatchEntries.search,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBatchEntries: (batchId) => dispatch(Actions.getBatchEntries(batchId)),
    deleteBatchEntry: (id) => dispatch(Actions.deleteBatchEntry(id)),
    processBatchEntry: (id) => dispatch(Actions.processBatchEntry(id)),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSearch: (search) => dispatch(Actions.setSearch(search)),
    setSorting: (sorting) => dispatch(Actions.setSorting(sorting)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BatchEntryListing);
