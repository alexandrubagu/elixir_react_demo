import Utils from '../api/utils';

export function getBatchEntries(batchId, params) {
  return Utils.request.get(
    `/cx/admin/api/batches/${batchId}/batch-entries`,
    params,
  );
}

export function getBatchEntry(id) {
  return Utils.request.get(`/cx/admin/api/batch-entries/${id}`);
}

export function deleteBatchEntry(id) {
  return Utils.request.delete(`/cx/admin/api/batch-entries/${id}`);
}

export function createBatchEntry(batchId, surveyId, params) {
  return Utils.request.post(
    `/cx/admin/api/batch-entries/${batchId}/${surveyId}`,
    {
      entry: params,
    },
  );
}

export function processBatchEntry(id) {
  return Utils.request.put(`/cx/admin/api/batch-entries/${id}/process`);
}

export function uploadBatchEntries(id, data) {
  return Utils.request.upload(`/cx/admin/api/batches/${id}/import-json`, data);
}
