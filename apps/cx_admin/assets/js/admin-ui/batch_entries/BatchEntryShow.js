import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import {
  Card,
  Col,
  Descriptions,
  Layout,
  PageHeader,
  Row,
  Space,
  Tabs,
} from 'antd';
import { ClockCircleOutlined } from '@ant-design/icons';
import { UUIDLink, JSONCode, isEmpty } from '../components/utils';
import Batches from '../batches';
import BatchEntryStatusTag from './BatchEntryStatusTag';

class BatchEntryShow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      breadcrumbs: [],
    };
  }
  componentDidMount() {
    let id = this.props.match.params.id;
    this.props.getBatchEntry(id).then(() => {
      const batch = isEmpty(this.props.currentBatch)
        ? { id: this.props.current.batch_id }
        : this.props.currentBatch;

      this.setState({
        breadcrumbs: Batches.Breadcrumbs.showEntry({
          batch: batch,
          entry: this.props.current,
        }),
      });
    });
  }

  render() {
    const { current } = this.props;
    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Batch entry details"
            subTitle={this.props.current.ref}
            tags={<BatchEntryStatusTag status={this.props.current.status} />}
            breadcrumb={this.state.breadcrumbs}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Row gutter={[16, 24]}>
            <Col lg={{ span: 14 }} md={{ span: 24 }}>
              <Card title="General" key="general">
                <GeneralDetails entry={current} />
              </Card>
              <Card
                title={
                  <Space>
                    <span>Notification</span>
                    <span>{current.notification_type}</span>
                  </Space>
                }
                key="notification"
                style={{ marginTop: 24 }}
              >
                <NotificationDetails entry={current} />
              </Card>
            </Col>
            <Col lg={{ span: 10 }} md={{ span: 24 }}>
              <Card title="Log timeline" key="logs">
                <Batches.LogTimeline logs={current.log} />
              </Card>
            </Col>
          </Row>
        </Layout.Content>
      </Layout>
    );
  }
}

const GeneralDetails = (props) => {
  const entry = props.entry || {};

  return (
    <Descriptions bordered column={2}>
      <Descriptions.Item label="Invite">
        {entry.invite_id ? (
          <UUIDLink prefix="/cx/admin/ui/invites/" id={entry.invite_id} />
        ) : (
          <Space>
            <ClockCircleOutlined />
            Not created yet !
          </Space>
        )}
      </Descriptions.Item>

      <Descriptions.Item label="Survey">
        <UUIDLink prefix="/cx/admin/ui/surveys/" id={entry.survey_id} />
      </Descriptions.Item>

      <Descriptions.Item label="Ref" span={2}>
        {entry.ref}
      </Descriptions.Item>

      <Descriptions.Item label="Language" span={2}>
        {entry.language}
      </Descriptions.Item>

      <Descriptions.Item label="Params" span={2}>
        <JSONCode json={entry.params} />
      </Descriptions.Item>
    </Descriptions>
  );
};

const NotificationDetails = (props) => {
  switch (props.entry.notification_type) {
    case 'sms':
      return <SMSDetails entry={props.entry} />;

    default:
      return <EmailDetails entry={props.entry} />;
  }
};

const EmailDetails = (props) => {
  const entry = props.entry || {};

  return (
    <Descriptions bordered column={1}>
      <Descriptions.Item label="Email Address">
        {entry.email_address}
      </Descriptions.Item>
      <Descriptions.Item label="Mail Message">
        {entry.mail_id ? (
          <UUIDLink prefix="/cx/admin/ui/emails/messages/" id={entry.mail_id} />
        ) : (
          <Space>
            <ClockCircleOutlined />
            Not created yet !
          </Space>
        )}
      </Descriptions.Item>
    </Descriptions>
  );
};

const SMSDetails = (props) => {
  const entry = props.entry || {};

  return (
    <Descriptions bordered column={1}>
      <Descriptions.Item label="SMS Number">
        {entry.sms_number}
      </Descriptions.Item>

      <Descriptions.Item label="SMS">
        {entry.sms_id ? (
          <span>No ui for sms messages yet: {entry.sms_id}</span>
        ) : (
          <Space>
            <ClockCircleOutlined />
            Not sent yet !
          </Space>
        )}
      </Descriptions.Item>
    </Descriptions>
  );
};

const mapStateToProps = (state) => {
  return {
    currentBatch: state.Batches.current,
    current: state.BatchEntries.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBatchEntry: (id) => dispatch(Actions.getBatchEntry(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BatchEntryShow);
