import { Actions, Reducer } from './state';
import BatchEntryShow from './BatchEntryShow';
import BatchEntryListing from './BatchEntryListing';
import BatchEntryCreation from './BatchEntryCreation';

export default {
  Actions,
  Reducer,
  Show: BatchEntryShow,
  List: BatchEntryListing,
  New: BatchEntryCreation,
};
