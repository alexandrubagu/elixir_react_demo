import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import {
  Layout,
  PageHeader,
  Card,
  Tabs,
  Form,
  Select,
  Input,
  Button,
} from 'antd';
import { Actions as SurveyActions } from '../surveys';
import {
  success_notification,
  error_notification,
  validateIsJSONObject,
  errorsAsLookup,
  getError,
} from '../components/utils';

import Batches from '../batches';

const { Option } = Select;
const layout = { labelCol: { span: 2 }, wrapperCol: { span: 12 } };

class BatchCreation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      batchId: this.props.match.params.id,
      selectedNotificationType: null,
      breadcrumbs: [],
      fileList: {},
    };
  }

  componentDidMount() {
    this.setState({
      breadcrumbs: Batches.Breadcrumbs.createEntry({
        batch: { id: this.state.batchId },
      }),
    });
  }

  handleSearchForSurveys(search) {
    Promise.all([this.props.setSurveysSearch(search), this.props.getSurveys()]);
  }

  handleSubmit(data) {
    const { batch_id, survey_id, ...rest } = data;
    let params = { ...rest, params: JSON.parse(rest.params) };
    return this.props
      .createBatchEntry(batch_id, survey_id, params)
      .then((result) => {
        if (result.payload.error) {
          error_notification(result.payload.error.message);
        } else {
          success_notification('Batch Entry created successfully!').then(() =>
            this.props.history.push(
              `/cx/admin/ui/batches/${this.state.batchId}`,
            ),
          );
        }
      });
  }

  handleUpload(data) {
    const formData = new FormData();
    formData.append('json_file', this.state.fileList[0]);

    this.props
      .uploadBatchEntries(this.state.batchId, formData)
      .then((result) => {
        if (result.payload.error) {
          error_notification(
            `${result.payload.error.message} ${result.payload.error.info.details.reason}`,
          );
        } else {
          success_notification(
            `${result.payload.data.count} batch entries created successfully!`,
          ).then(() =>
            this.props.history.push(
              `/cx/admin/ui/batches/${this.state.batchId}`,
            ),
          );
        }
      });
  }

  render() {
    const errorsLookup = errorsAsLookup(this.props.error);
    let notification_types = this.props.survey.notification_types || [];

    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="New Batch Entry"
            breadcrumb={this.state.breadcrumbs}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Tabs>
            <Tabs.TabPane tab="Manual Batch Entry" key="manual">
              <Card>
                <Form
                  {...layout}
                  id="manual"
                  name="manual"
                  initialValues={{ batch_id: this.state.batchId, params: '{}' }}
                  onFinish={(data) => this.handleSubmit(data)}
                >
                  <Form.Item
                    label="Batch"
                    name="batch_id"
                    rules={[{ required: true }]}
                    {...getError(errorsLookup, 'batch_id', true)}
                  >
                    <Input value={this.state.batchId} disabled />
                  </Form.Item>

                  <Form.Item
                    label="Survey"
                    name="survey_id"
                    rules={[{ required: true }]}
                    {...getError(errorsLookup, 'survey_id', true)}
                  >
                    <Select
                      showSearch
                      placeholder="Select a survey"
                      optionFilterProp="children"
                      onSearch={(e) => this.handleSearchForSurveys(e)}
                      onSelect={this.props.getSurvey}
                    >
                      {this.props.surveys.map((survey, i) => (
                        <Option key={i} value={survey.id}>
                          {survey.name}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Params"
                    name="params"
                    rules={[
                      { required: true },
                      { validator: validateIsJSONObject },
                    ]}
                    {...getError(errorsLookup, 'params', true)}
                  >
                    <Input.TextArea rows={7} />
                  </Form.Item>

                  <Form.Item
                    label="Ref"
                    name="ref"
                    rules={[{ required: true }]}
                    {...getError(errorsLookup, 'ref', true)}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Language"
                    name="language"
                    rules={[{ required: true }]}
                    {...getError(errorsLookup, 'language', true)}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Notification Type"
                    name="notification_type"
                    rules={[{ required: true }]}
                    {...getError(errorsLookup, 'notification_type', true)}
                  >
                    <Select
                      onSelect={(type) =>
                        this.setState({ selectedNotificationType: type })
                      }
                    >
                      {notification_types.map((type, i) => (
                        <Option key={i} value={type}>
                          {type}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>

                  {this.state.selectedNotificationType == 'mail' ? (
                    <Form.Item
                      label="Email Address"
                      name="email_address"
                      rules={[{ required: true }]}
                      {...getError(errorsLookup, 'email_address', true)}
                    >
                      <Input />
                    </Form.Item>
                  ) : this.state.selectedNotificationType == 'sms' ? (
                    <Form.Item
                      label="SMS Number"
                      name="sms_number"
                      rules={[{ required: true }]}
                      {...getError(errorsLookup, 'sms_number', true)}
                    >
                      <Input />
                    </Form.Item>
                  ) : null}

                  <Button type="primary" htmlType="submit">
                    Submit
                  </Button>
                </Form>
              </Card>
            </Tabs.TabPane>
            <Tabs.TabPane tab="Upload JSON" key="upload">
              <Card>
                <Form
                  {...layout}
                  id="upload"
                  name="upload"
                  onFinish={(e) => this.handleUpload(e)}
                >
                  <Form.Item
                    label="JSON File"
                    name="file"
                    rules={[{ required: true }]}
                  >
                    <Input
                      type="file"
                      onChange={(e) =>
                        this.setState({ fileList: e.target.files })
                      }
                    />
                  </Form.Item>

                  <Button type="primary" htmlType="submit">
                    Upload
                  </Button>
                </Form>
              </Card>
            </Tabs.TabPane>
          </Tabs>
        </Layout.Content>
      </Layout>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    error: state.BatchEntries.error,
    surveys: state.Surveys.data,
    survey: state.Surveys.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createBatchEntry: (batchId, surveyId, params) =>
      dispatch(Actions.createBatchEntry(batchId, surveyId, params)),
    getSurveys: () => dispatch(SurveyActions.getSurveys()),
    getSurvey: (id) => dispatch(SurveyActions.getSurvey(id)),
    setSurveysSearch: (search) => dispatch(SurveyActions.setSearch(search)),
    uploadBatchEntries: (batchId, data) =>
      dispatch(Actions.uploadBatchEntries(batchId, data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BatchCreation);
