import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import {
  Card,
  Col,
  Descriptions,
  Layout,
  PageHeader,
  Row,
  Space,
  Button,
  Modal,
} from 'antd';
import { EditOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { UUIDLink, success_notification } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';
import NotificationTypeTag from './NotificationTypeTag';
import Moment from 'react-moment';

class BlacklistEntryShow extends Component {
  constructor(props) {
    super(props);
    this.state = { breadcrumbs: [] };
  }

  componentDidMount() {
    this.props.getEntry(this.props.match.params.id).then(() =>
      this.setState({
        breadcrumbs: Breadcrumbs.showEntry({ entry: this.props.entry }),
      }),
    );
  }

  showDeleteModal() {
    Modal.confirm({
      title: 'Confirm deletion',
      content: 'Are you sure you want to delete this blacklist entry ?',
      icon: <ExclamationCircleOutlined />,
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk: () =>
        this.props
          .deleteEntry(this.props.entry)
          .then(() => success_notification('Blacklist entry deleted!'))
          .then(() =>
            this.props.history.push('/cx/admin/ui/blacklist/entries'),
          ),
    });
  }

  render() {
    const { entry } = this.props;

    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Blacklist entry details"
            subTitle={`(${entry.id})`}
            breadcrumb={this.state.breadcrumbs}
            tags={<NotificationTypeTag type={entry.notification_type} />}
            extra={[
              <Button
                key="edit"
                type="primary"
                icon={<EditOutlined />}
                onClick={() =>
                  this.props.history.push(
                    `/cx/admin/ui/blacklist/entries/${entry.id}/update`,
                  )
                }
              >
                Edit entry
              </Button>,
              <Button
                type="danger"
                key="delete"
                onClick={() => this.showDeleteModal()}
              >
                Delete entry
              </Button>,
            ]}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Row gutter={[16, 24]}>
            <Col span={24}>
              <Card title="General" key="general">
                <Descriptions bordered column={2}>
                  <Descriptions.Item label="Key">{entry.key}</Descriptions.Item>

                  <Descriptions.Item label="Reason">
                    {entry.reason}
                  </Descriptions.Item>

                  <Descriptions.Item label="Source" span={2}>
                    {entry.webhook_event_id ? (
                      <Space>
                        Webhook event:
                        <UUIDLink
                          prefix="/cx/admin/ui/emails/webhooks/events/"
                          id={entry.webhook_event_id}
                        />
                      </Space>
                    ) : (
                      <Space>Manually added</Space>
                    )}
                  </Descriptions.Item>

                  <Descriptions.Item label="Created" span={2}>
                    <Moment fromNow>{entry.inserted_at}</Moment>
                  </Descriptions.Item>

                  <Descriptions.Item label="Last updated" span={2}>
                    <Moment fromNow>{entry.updated_at}</Moment>
                  </Descriptions.Item>
                </Descriptions>
              </Card>
            </Col>
          </Row>
        </Layout.Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => ({
  entry: state.BlacklistEntries.current,
});

const mapDispatchToProps = (dispatch) => ({
  getEntry: (id) => dispatch(Actions.getBlacklistEntry(id)),
  deleteEntry: (entry) => dispatch(Actions.deleteBlacklistEntry(entry)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BlacklistEntryShow);
