import { breadcrumbs } from '../components/utils';

const listEntries = {
  path: '/cx/admin/ui/blacklist/entries',
  breadcrumbName: 'Blacklist Entries',
};

const showEntry = ({ entry }) => ({
  path: `/cx/admin/ui/blacklist/entries/${entry.id}`,
  breadcrumbName: entry.key,
});

const updateEntry = ({ entry }) => ({
  path: `/cx/admin/ui/blacklist/entries/${entry.id}/update`,
  breadcrumbName: 'Edit',
});

const createEntry = {
  path: `/cx/admin/ui/blacklist/entries/new`,
  breadcrumbName: 'New Blacklist Entry',
};

export default {
  listEntries: breadcrumbs([listEntries]),
  showEntry: breadcrumbs([listEntries, showEntry]),
  updateEntry: breadcrumbs([listEntries, showEntry, updateEntry]),
  createEntry: breadcrumbs([listEntries, createEntry]),
};
