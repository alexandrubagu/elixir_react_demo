import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Layout, Card, PageHeader } from 'antd';
import { Actions } from './state';
import { success_notification, error_notification } from '../components/utils';
import BlacklistEntryForm from './BlacklistEntryForm';
import Breadcrumbs from './Breadcrumbs';

class BlacklistEntryUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = { submitted: false, breadcrumbs: {} };
  }

  componentDidMount(props) {
    this.props.getEntry(this.props.match.params.id).then(() => {
      this.setState({
        breadcrumbs: Breadcrumbs.updateEntry({ entry: this.props.entry }),
      });
    });
  }

  handleSubmit(data) {
    return this.props
      .updateEntry(this.props.entry.id, data)
      .then((result) => {
        if (result.payload.error) {
          error_notification(result.payload.error.message);
        } else {
          success_notification(
            'Blacklist Entry updated successfully!',
          ).then(() =>
            this.props.history.push(
              `/cx/admin/ui/blacklist/entries/${this.props.entry.id}`,
            ),
          );
        }
      })
      .then(() => this.setState({ submitted: true }));
  }

  render() {
    const entry = this.props.entry || {};
    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Edit blacklist entry"
            subTitle={`(${entry.id || ''})`}
            breadcrumb={Breadcrumbs.updateEntry({ entry: entry })}
          />
        </div>
        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card>
            <BlacklistEntryForm
              entry={entry}
              onSubmit={(data) => this.handleSubmit(data)}
              error={this.props.error}
            />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    error: state.BlacklistEntries.error,
    entry: state.BlacklistEntries.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getEntry: (id) => dispatch(Actions.getBlacklistEntry(id)),
    updateEntry: (id, params) =>
      dispatch(Actions.updateBlacklistEntry(id, params)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BlacklistEntryUpdate);
