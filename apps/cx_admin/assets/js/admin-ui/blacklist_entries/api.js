import Utils from '../api/utils';

export function getBlacklistEntries(params) {
  return Utils.request.get(
    '/cx/admin/api/notifications/blacklists/entries',
    params,
  );
}

export function getBlacklistEntry(id) {
  return Utils.request.get(
    `/cx/admin/api/notifications/blacklists/entries/${id}`,
  );
}

export function updateBlacklistEntry(id, params) {
  return Utils.request.put(
    `/cx/admin/api/notifications/blacklists/entries/${id}`,
    { blacklist_entry: params },
  );
}

export function deleteBlacklistEntry(id) {
  return Utils.request.delete(
    `/cx/admin/api/notifications/blacklists/entries/${id}`,
  );
}

export function createBlacklistEntry(params) {
  return Utils.request.post(`/cx/admin/api/notifications/blacklists/entries`, {
    blacklist_entry: params,
  });
}
