import React, { useState, useEffect } from 'react';
import { Form, Button, Input, Radio } from 'antd';
import { errorsAsLookup, getError, isEmpty } from '../components/utils';

const verticalRadioStyle = {
  display: 'block',
  height: '30px',
  lineHeight: '30px',
};

const BlacklistEntryForm = (props) => {
  const [form] = Form.useForm();
  const errorsLookup = errorsAsLookup(props.error);
  const [submitted, setSubmitted] = useState(false);

  useEffect(() => {
    if (!isEmpty(props.entry)) {
      form.setFieldsValue(props.entry);
    }
  }, [props.entry]);

  const handleSubmit = (data) =>
    props.onSubmit(data).then(() => setSubmitted(true));

  return (
    <Form
      form={form}
      labelCol={{ span: 5 }}
      wrapperCol={{ span: 15 }}
      name="blacklist_entry"
      onFinish={handleSubmit}
    >
      <Form.Item
        label="Notification Type"
        name="notification_type"
        rules={[{ required: true }]}
        {...getError(errorsLookup, 'notification_type', submitted)}
      >
        <Radio.Group>
          <Radio value="mail">Mail</Radio>
          <Radio value="sms">SMS</Radio>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        label="Key"
        name="key"
        help="Email address or SMS number to use as key for blacklist lookups"
        rules={[{ required: true }]}
        {...getError(errorsLookup, 'key', submitted)}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Reason for blacklisting"
        name="reason"
        rules={[{ required: true }]}
        {...getError(errorsLookup, 'reason', submitted)}
      >
        <Radio.Group>
          <Radio style={verticalRadioStyle} value="invalid_address">
            Invalid address
          </Radio>
          <Radio style={verticalRadioStyle} value="bounced_address">
            Bounced
          </Radio>
          <Radio style={verticalRadioStyle} value="unsubscribed_address">
            Unsubscribed
          </Radio>
          <Radio style={verticalRadioStyle} value="spam_reporting_address">
            Reported as spam
          </Radio>
        </Radio.Group>
      </Form.Item>

      <Button type="primary" htmlType="submit">
        Submit
      </Button>
    </Form>
  );
};

export default BlacklistEntryForm;
