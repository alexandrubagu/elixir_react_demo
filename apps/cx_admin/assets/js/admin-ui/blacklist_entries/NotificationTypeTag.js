import React from 'react';
import { Tag } from 'antd';

const color = (status) => {
  switch (status) {
    case 'mail':
      return 'blue';

    case 'sms':
      return 'green';
  }
};

const NotificationTypeTag = (props) => {
  return <Tag color={color(props.type)}>{props.type}</Tag>;
};

export default NotificationTypeTag;
