import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Layout, Card, PageHeader } from 'antd';
import { Actions } from './state';
import { success_notification, error_notification } from '../components/utils';
import BlacklistEntryForm from './BlacklistEntryForm';
import Breadcrumbs from './Breadcrumbs';

class BlacklistEntryCreation extends Component {
  constructor(props) {
    super(props);
    this.state = { submitted: false };
  }

  handleSubmit(data) {
    return this.props
      .createEntry(data)
      .then((result) => {
        if (result.payload.error) {
          error_notification(result.payload.error.message);
        } else {
          success_notification(
            'Blacklist Entry created successfully!',
          ).then(() =>
            this.props.history.push('/cx/admin/ui/blacklist/entries'),
          );
        }
      })
      .then(() => this.setState({ submitted: true }));
  }

  render() {
    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Create new blacklist entry"
            breadcrumb={Breadcrumbs.createEntry()}
          />
        </div>
        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card>
            <BlacklistEntryForm
              onSubmit={(data) => this.handleSubmit(data)}
              error={this.props.error}
            />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    error: state.BlacklistEntries.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    createEntry: (params) => dispatch(Actions.createBlacklistEntry(params)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BlacklistEntryCreation);
