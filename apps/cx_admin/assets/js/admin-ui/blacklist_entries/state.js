import * as api from './api';
import * as common from '../reducers/common';

const BLACKLIST_ENTRIES_REQUEST_PENDING = 'BLACKLIST_ENTRIES_REQUEST_PENDING';
const BLACKLIST_ENTRIES_REQUEST_ERROR = 'BLACKLIST_ENTRIES_REQUEST_ERROR';
const SET_BLACKLIST_ENTRIES_PAGINATION = 'SET_BLACKLIST_ENTRY_PAGINATION';
const SET_BLACKLIST_ENTRIES_SORTING = 'SET_BLACKLIST_ENTRY_SORTING';
const SET_BLACKLIST_ENTRIES_SEARCH = 'SET_BLACKLIST_ENTRY_SEARCH';
const BLACKLIST_ENTRIES_FETCHED = 'BLACKLIST_ENTRIES_FETCHED';
const BLACKLIST_ENTRY_FETCHED = 'BLACKLIST_ENTRY_FETCHED';
const BLACKLIST_ENTRY_CREATED = 'BLACKLIST_ENTRY_CREATED';
const BLACKLIST_ENTRY_UPDATED = 'BLACKLIST_ENTRY_UPDATED';
const BLACKLIST_ENTRY_DELETED = 'BLACKLIST_ENTRY_DELETED';

const pendingRequest = () => ({
  type: BLACKLIST_ENTRIES_REQUEST_PENDING,
});

const requestFailed = (error) => ({
  type: BLACKLIST_ENTRIES_REQUEST_ERROR,
  payload: error,
});

const entriesFetched = (entry) => ({
  type: BLACKLIST_ENTRIES_FETCHED,
  payload: entry,
});

const entryFetched = (entry) => ({
  type: BLACKLIST_ENTRY_FETCHED,
  payload: entry,
});

export const Actions = {
  getBlacklistEntries: () => (dispatch, getState) => {
    const { pagination, sorting, search } = getState().BlacklistEntries;
    dispatch(pendingRequest());
    return api.getBlacklistEntries({ pagination, sorting, search }).then(
      (response) => dispatch(entriesFetched(response)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  getBlacklistEntry: (id) => (dispatch, getState) => {
    dispatch(pendingRequest());
    return api.getBlacklistEntry(id).then(
      (response) => dispatch(entryFetched(response.data)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  deleteBlacklistEntry: (entry) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.deleteBlacklistEntry(entry.id).then(
        (response) =>
          dispatch({ type: BLACKLIST_ENTRY_DELETED, payload: response }),
        (error) => dispatch(errorRequest(error)),
      );
    };
  },

  createBlacklistEntry: (params) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.createBlacklistEntry(params).then(
        (response) =>
          dispatch({ type: BLACKLIST_ENTRY_CREATED, payload: response }),
        (error) => dispatch(requestFailed(error)),
      );
    };
  },

  updateBlacklistEntry: (id, params) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.updateBlacklistEntry(id, params).then(
        (response) =>
          dispatch({ type: BLACKLIST_ENTRY_UPDATED, payload: response }),
        (error) => dispatch(requestFailed(error)),
      );
    };
  },

  setPagination: (pagination) => ({
    type: SET_BLACKLIST_ENTRIES_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorting) => {
    return {
      type: SET_BLACKLIST_ENTRIES_SORTING,
      payload: sorting,
    };
  },

  setSearch: (search) => ({
    type: SET_BLACKLIST_ENTRIES_SEARCH,
    payload: search,
  }),
};

const initialState = {
  loading: false,
  pagination: {},
  sorting: {},
  search: '',
  data: [],
  current: {},
  error: {},
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case BLACKLIST_ENTRIES_REQUEST_PENDING:
      return { ...state, loading: true };

    case BLACKLIST_ENTRIES_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_BLACKLIST_ENTRIES_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_BLACKLIST_ENTRIES_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_BLACKLIST_ENTRIES_SEARCH:
      return common.updateSearch(state, action.payload);

    case BLACKLIST_ENTRIES_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case BLACKLIST_ENTRY_FETCHED:
      return { ...state, loading: false, current: action.payload };

    case BLACKLIST_ENTRY_CREATED:
    case BLACKLIST_ENTRY_DELETED:
      return { ...state, loading: false, errors: {} };

    default:
      return state;
  }
};
