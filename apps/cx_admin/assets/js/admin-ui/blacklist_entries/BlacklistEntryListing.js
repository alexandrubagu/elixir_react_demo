import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Row,
  Col,
  Table,
  Form,
  Input,
  Button,
  Layout,
  PageHeader,
  Card,
} from 'antd';

import { SearchOutlined, PlusOutlined } from '@ant-design/icons';
import { Actions } from './state';
import Breadcrumbs from './Breadcrumbs';
import NotificationTypeTag from './NotificationTypeTag';
import Moment from 'react-moment';

class BlacklistEntryListing extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columns: [
        {
          dataIndex: 'key',
          title: 'Key',
          sorter: true,
          render: (key, record) => (
            <Link to={`/cx/admin/ui/blacklist/entries/${record.id}`}>
              {key}
            </Link>
          ),
        },
        {
          dataIndex: 'notification_type',
          title: 'NotificationType',
          sorter: true,
          render: (type) => <NotificationTypeTag type={type} />,
        },
        {
          dataIndex: 'reason',
          title: 'Reason',
          sorter: true,
        },
        {
          title: 'Last updated',
          dataIndex: 'updated_at',
          sorter: true,
          render: (text, record) => <Moment fromNow>{text}</Moment>,
        },
      ],
    };
  }

  componentDidMount() {
    this.props.getBlacklistEntries();
  }

  handleTableChange(pagination, filters, sorter) {
    Promise.all([
      this.props.setPagination(pagination),
      this.props.setSorting(sorter),
    ]).then(this.props.getBlacklistEntries);
  }

  handleSearch(e) {
    Promise.resolve(this.props.setSearch(e.target.value)).then(
      this.props.getBlacklistEntries,
    );
  }

  render() {
    const { data, pagination, loading, search } = this.props;
    const { columns } = this.state;

    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Blacklist entries listing"
            breadcrumb={Breadcrumbs.listEntries()}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card style={{ marginBottom: 24 }}>
            <Row gutter={[16, 24]}>
              <Col key="1" flex={'auto'}>
                <Form layout="inline">
                  <Form.Item key="1" style={{ width: '100%' }}>
                    <Input
                      prefix={
                        <SearchOutlined className="site-form-item-icon" />
                      }
                      placeholder="Search"
                      value={search}
                      onChange={(search) => this.handleSearch(search)}
                    />
                  </Form.Item>
                </Form>
              </Col>
              <Col key="2" flex={'none'}>
                <Button
                  label="Add"
                  type="primary"
                  icon={<PlusOutlined />}
                  onClick={() =>
                    this.props.history.push(
                      '/cx/admin/ui/blacklist/entries/new',
                    )
                  }
                >
                  Create entry
                </Button>
              </Col>
            </Row>
          </Card>

          <Card bodyStyle={{ padding: 0 }}>
            <Table
              columns={columns}
              dataSource={data}
              pagination={pagination}
              loading={loading}
              rowKey={(record) => record.id}
              onChange={(pagination, filters, sorter) =>
                this.handleTableChange(pagination, filters, sorter)
              }
            />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.BlacklistEntries.data,
    pagination: state.BlacklistEntries.pagination,
    loading: state.BlacklistEntries.loading,
    search: state.BlacklistEntries.search,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getBlacklistEntries: () => dispatch(Actions.getBlacklistEntries()),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSearch: (search) => dispatch(Actions.setSearch(search)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BlacklistEntryListing);
