import React from 'react';
const SocketContext = React.createContext();
export const withSocketContext = (module) => {
  module.contextType = SocketContext;
  return module;
};
export default SocketContext;
