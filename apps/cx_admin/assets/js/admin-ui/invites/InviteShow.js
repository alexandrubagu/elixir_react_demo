import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import {
  Row,
  Col,
  Descriptions,
  Layout,
  PageHeader,
  Card,
  Typography,
  Space,
  Tabs,
  Empty,
  Timeline,
  Tooltip,
} from 'antd';
import {
  InfoCircleOutlined,
  WarningOutlined,
  CloseCircleOutlined,
  CheckCircleOutlined,
} from '@ant-design/icons';
import { JSONCode, UUIDLink } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';
import Moment from 'react-moment';

const sorter = (a, b) => {
  let t1 = new Date(a.inserted_at).getTime();
  let t2 = new Date(b.inserted_at).getTime();
  return t1 > t2 ? -1 : 1;
};

const capitalizeFirstLetter = (string) =>
  string.charAt(0).toUpperCase() + string.slice(1);

const InviteShow = (props) => {
  const [breadcrumbs, setBreadcrumbs] = useState([]);
  const current = props.current;

  useEffect(() => {
    props.getInvite(props.match.params.id).then(() => {
      setBreadcrumbs(Breadcrumbs.showInvite({ invite: props.current }));
    });
  }, []);

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Invite details"
          subTitle={`(${current.id || ''})`}
          breadcrumb={breadcrumbs}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Tabs>
          <Tabs.TabPane tab="General" key="general">
            <GeneralDetails invite={current} />
          </Tabs.TabPane>
          <Tabs.TabPane
            tab={`Notification ${current.notification_type}`}
            key="natification"
          >
            <NotificationDetails invite={current} />
          </Tabs.TabPane>
        </Tabs>
      </Layout.Content>
    </Layout>
  );
};

const GeneralDetails = (props) => {
  const invite = props.invite || {};

  return (
    <Card>
      <Descriptions column={2} bordered>
        <Descriptions.Item label="Ref" span={2}>
          {invite.ref}
        </Descriptions.Item>

        <Descriptions.Item label="Batch">
          <UUIDLink prefix="/cx/admin/ui/batches/" id={invite.batch_id} />
        </Descriptions.Item>

        <Descriptions.Item label="Survey">
          <UUIDLink prefix="/cx/admin/ui/surveys/" id={invite.survey_id} />
        </Descriptions.Item>

        <Descriptions.Item label="Participation">
          {invite.participation_id || 'No participation'}
        </Descriptions.Item>

        <Descriptions.Item label="Expiry">
          <Moment fromNow>{invite.expire_at}</Moment>
        </Descriptions.Item>

        <Descriptions.Item label="Params" span={2}>
          <JSONCode json={invite.params} />
        </Descriptions.Item>

        <Descriptions.Item label="Language" span={2}>
          {invite.language}
        </Descriptions.Item>

        <Descriptions.Item label="Created">
          <Moment fromNow>{invite.inserted_at}</Moment>
        </Descriptions.Item>

        <Descriptions.Item label="Last updated">
          <Moment fromNow>{invite.updated_at}</Moment>
        </Descriptions.Item>
      </Descriptions>
    </Card>
  );
};

const NotificationDetails = (props) => {
  const invite = props.invite || {};
  const notification_type = invite.notification_type || 'mail';

  if (notification_type == 'mail') {
    return <EmailDetails invite={invite} />;
  } else {
    return <SMSDetails invite={invite} />;
  }
};

const EmailDetails = (props) => {
  const invite = props.invite || {};

  const getInviteEmailMessageByType = (invite, type) =>
    invite.email_messages.filter((data) => data.type == type)[0];

  const sortEmailMessageEvents = (invite) => {
    let events = invite.email_messages.reduce((acc, email_message) => {
      let x = email_message.events.map((data) => ({
        ...data,
        email_message_type: email_message.type,
      }));
      return acc.concat(x);
    }, []);

    events.sort(sorter);

    return events;
  };

  return (
    <Row gutter={[16, 24]}>
      <Col span={16}>
        <Card style={{ marginBottom: 24 }}>
          <Descriptions column={2} bordered>
            <Descriptions.Item label="From">
              <Space>
                {invite.from_email_name}{' '}
                <Typography.Text type="secondary">
                  ({invite.from_email_address})
                </Typography.Text>
              </Space>
            </Descriptions.Item>
            <Descriptions.Item label="To">
              {invite.email_address}
            </Descriptions.Item>
          </Descriptions>
        </Card>
        {invite.tpl_invite ? (
          <Card title="Invite" style={{ marginBottom: 24 }}>
            <EmailMessageTypeDetails
              message={getInviteEmailMessageByType(invite, 'invite')}
            />
          </Card>
        ) : null}
        {invite.tpl_reminder ? (
          <Card title="Remember" style={{ marginBottom: 24 }}>
            <EmailMessageTypeDetails
              message={getInviteEmailMessageByType(invite, 'remember')}
            />
          </Card>
        ) : null}
        {invite.tpl_abandonned ? (
          <Card title="Abandonned" style={{ marginBottom: 24 }}>
            <EmailMessageTypeDetails
              message={getInviteEmailMessageByType(invite, 'abandonned')}
            />
          </Card>
        ) : null}
        {invite.tpl_thanks ? (
          <Card title="Thanks">
            <EmailMessageTypeDetails
              message={getInviteEmailMessageByType(invite, 'thanks')}
            />
          </Card>
        ) : null}
      </Col>
      <Col span={8}>
        <Card>
          <EmailMessageEventsTimeline events={sortEmailMessageEvents(invite)} />
        </Card>
      </Col>
    </Row>
  );
};

const EmailMessageEventsTimeline = (props) => {
  const events = props.events || [];

  if (events.length == 0) {
    return <Empty description="No events yet" />;
  } else {
    return (
      <Timeline>
        {events.map((event) => (
          <EmailMessageEventTimelineItem event={event} key={event.id} />
        ))}
      </Timeline>
    );
  }
};

const EmailMessageEventTimelineItem = (props) => {
  const config = {
    invite: { icon: <InfoCircleOutlined />, color: 'grey' },
    remember: { icon: <WarningOutlined />, color: 'red' },
    abandonned: { icon: <CloseCircleOutlined />, color: 'red' },
    thanks: { icon: <CheckCircleOutlined />, color: 'green' },
  };
  const event = props.event;
  const eventConfig = config[event.email_message_type];

  return (
    <Timeline.Item
      key={event.id}
      dot={eventConfig.icon}
      color={eventConfig.color}
    >
      <Row gutter={8}>
        <Col>
          <a href="">{capitalizeFirstLetter(event.email_message_type)}</a>{' '}
          {event.type == 'dropped' ? (
            <Tooltip placement="top" title={event.drop_reason}>
              {event.type}
            </Tooltip>
          ) : (
            event.type
          )}
        </Col>
        <Col>
          <Typography.Text disabled>
            <Moment fromNow>{event.inserted_at}</Moment>
          </Typography.Text>
        </Col>
      </Row>
    </Timeline.Item>
  );
};

const EmailMessageTypeDetails = (props) => {
  const message = props.message;

  if (message) {
    return (
      <Descriptions bordered column={1}>
        <Descriptions.Item label="From">
          <Space>
            {message.from_name}{' '}
            <Typography.Text type="secondary">
              ({message.from_address})
            </Typography.Text>
          </Space>
        </Descriptions.Item>

        <Descriptions.Item label="To">{message.to_address}</Descriptions.Item>

        <Descriptions.Item label="Template ID">
          {message.template_id}
        </Descriptions.Item>
        <Descriptions.Item label="Template Params">
          <JSONCode json={message.template_params} />
        </Descriptions.Item>

        <Descriptions.Item label="Created">
          <Moment fromNow>{message.inserted_at}</Moment>
        </Descriptions.Item>

        <Descriptions.Item label="Last updated">
          <Moment fromNow>{message.updated_at}</Moment>
        </Descriptions.Item>
      </Descriptions>
    );
  } else {
    return <Empty description="No email send yet" />;
  }
};

const SMSDetails = (props) => {
  const invite = props.invite || {};

  const sortSMSEvents = (invite) => {
    let events = invite.smses.events;
    events.sort(sorter);
    return events;
  };

  return (
    <Row gutter={[16, 24]}>
      <Col span={16}>
        <Card style={{ marginBottom: 24 }}>
          <Descriptions column={2} bordered>
            <Descriptions.Item label="SMS From">
              {invite.sms_from_number}
            </Descriptions.Item>
            <Descriptions.Item label="SMS To">
              {invite.sms_dest_number}
            </Descriptions.Item>
          </Descriptions>
        </Card>
      </Col>
      <Col span={8}>
        <Card>
          <SMSTimeline events={sortSMSEvents(invite)} />
        </Card>
      </Col>
    </Row>
  );
};

const SMSTimeline = (props) => {
  const events = props.events || [];

  if (events.length == 0) {
    return <Empty description="No events yet" />;
  } else {
    return (
      <Timeline>
        {events.map((event) => (
          <SMSEventTimelineItem event={event} key={event.id} />
        ))}
      </Timeline>
    );
  }
};

const SMSEventTimelineItem = (props) => {
  const config = {
    queued: { icon: <InfoCircleOutlined />, color: 'grey' },
    failed: { icon: <CloseCircleOutlined />, color: 'red' },
    sent: { icon: <InfoCircleOutlined />, color: 'grey' },
    delivered: { icon: <CheckCircleOutlined />, color: 'green' },
    undelivered: { icon: <CloseCircleOutlined />, color: 'red' },
    unsubscribed: { icon: <CloseCircleOutlined />, color: 'red' },
  };

  const event = props.event;
  const eventConfig = config[event.type];

  return (
    <Timeline.Item
      key={event.id}
      dot={eventConfig.icon}
      color={eventConfig.color}
    >
      <Row gutter={8}>
        <Col>
          <a href="">{capitalizeFirstLetter(event.email_message_type)}</a>{' '}
          {event.type}
        </Col>
        <Col>
          <Typography.Text disabled>
            <Moment fromNow>{event.inserted_at}</Moment>
          </Typography.Text>
        </Col>
      </Row>
    </Timeline.Item>
  );
};

const mapStateToProps = (state) => {
  return {
    current: state.Invites.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getInvite: (id) => dispatch(Actions.getInvite(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InviteShow);
