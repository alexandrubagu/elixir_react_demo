import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from './state';
import { Table, Form, Input, Layout, PageHeader, Card } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import { UUIDLink } from '../components/utils';
import Breadcrumbs from './Breadcrumbs';
import Moment from 'react-moment';

class InviteList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      columns: [
        {
          dataIndex: 'id',
          title: 'ID',
          render: (id) => <UUIDLink id={id} prefix="/cx/admin/ui/invites/" />,
        },
        {
          dataIndex: 'ref',
          title: 'Ref',
          sorter: true,
        },
        {
          dataIndex: 'notification_type',
          title: 'Notification',
        },
        {
          dataIndex: 'email_address',
          title: 'Email',
        },
        {
          dataIndex: 'language',
          title: 'Lang',
          sorter: true,
        },
        {
          title: 'Last updated',
          dataIndex: 'updated_at',
          sorter: true,
          render: (text, record) => <Moment fromNow>{text}</Moment>,
        },
      ],
    };
  }

  componentDidMount() {
    this.props.getInvites();
  }

  handleShowInvite(id) {
    this.props.history.push(`/cx/admin/ui/invites/${id}`);
  }

  handleSearch(e) {
    Promise.resolve(this.props.setSearch(e.target.value)).then(
      this.props.getInvites,
    );
  }

  handleTableChange(pagination, filters, sorter) {
    Promise.all([
      this.props.setPagination(pagination),
      this.props.setSorting(sorter),
    ]).then(this.props.getInvites);
  }

  render() {
    const { data, pagination, loading, search } = this.props;
    const { columns } = this.state;

    return (
      <Layout>
        <div className="site-layout-background">
          <PageHeader
            className="site-page-header"
            title="Invite listing"
            breadcrumb={Breadcrumbs.listInvites()}
          />
        </div>

        <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
          <Card style={{ marginBottom: 24 }}>
            <Form>
              <Form.Item style={{ width: '100%' }}>
                <Input
                  prefix={<SearchOutlined className="site-form-item-icon" />}
                  placeholder="Search"
                  value={search}
                  onChange={(search) => this.handleSearch(search)}
                />
              </Form.Item>
            </Form>
          </Card>

          <Card bodyStyle={{ padding: 0 }}>
            <Table
              columns={columns}
              dataSource={data}
              pagination={pagination}
              loading={loading}
              rowKey={(record) => record.id}
              onChange={(pagination, filters, sorter) =>
                this.handleTableChange(pagination, filters, sorter)
              }
            />
          </Card>
        </Layout.Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    data: state.Invites.data,
    pagination: state.Invites.pagination,
    loading: state.Invites.loading,
    search: state.Invites.search,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getInvites: () => dispatch(Actions.getInvites()),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSearch: (search) => dispatch(Actions.setSearch(search)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(InviteList);
