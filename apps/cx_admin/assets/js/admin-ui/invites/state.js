import * as api from './api';
import * as common from '../reducers/common';

export const INVITES_REQUEST_PENDING = 'INVITES_REQUEST_PENDING';
export const INVITES_REQUEST_ERROR = 'INVITES_REQUEST_ERROR';
export const SET_INVITES_PAGINATION = 'SET_INVITES_PAGINATION';
export const SET_INVITES_SORTING = 'SET_INVITES_SORTING';
export const SET_INVITES_SEARCH = 'SET_INVITES_SEARCH';
export const INVITES_FETCHED = 'INVITES_FETCHED';
export const INVITE_FETCHED = 'INVITE_FETCHED';

const pendingRequest = () => ({ type: INVITES_REQUEST_PENDING });

const errorRequest = (error) => ({
  type: INVITES_REQUEST_ERROR,
  payload: error,
});

const invitesFetched = (result) => ({ type: INVITES_FETCHED, payload: result });
const inviteFetched = (result) => ({ type: INVITE_FETCHED, payload: result });

export const Actions = {
  setPagination: (pagination) => ({
    type: SET_INVITES_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorter) => ({
    type: SET_INVITES_SORTING,
    payload: sorter,
  }),

  setSearch: (search) => ({
    type: SET_INVITES_SEARCH,
    payload: search,
  }),

  getInvites: () => (dispatch, getState) => {
    const { pagination, sorting, search } = getState().Invites;
    dispatch(pendingRequest());
    return api.getInvites({ pagination, sorting, search }).then(
      (response) => dispatch(dispatch(invitesFetched(response))),
      (error) => dispatch(errorRequest(error)),
    );
  },

  getInvite: (id) => {
    return (dispatch) => {
      dispatch(pendingRequest());
      return api.getInvite(id).then(
        (response) => dispatch(inviteFetched(response.data)),
        (error) => dispatch(errorRequest(error)),
      );
    };
  },
};

const initialState = {
  loading: false,
  pagination: {},
  sorting: {},
  search: '',
  data: [],
  current: {},
  error: {},
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case INVITES_REQUEST_PENDING:
      return { ...state, loading: true };

    case INVITES_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_INVITES_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_INVITES_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_INVITES_SEARCH:
      return common.updateSearch(state, action.payload);

    case INVITES_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case INVITE_FETCHED:
      return { ...state, loading: false, current: action.payload };

    default:
      return state;
  }
};
