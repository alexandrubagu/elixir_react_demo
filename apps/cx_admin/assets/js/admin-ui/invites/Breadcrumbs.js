import { breadcrumbs } from '../components/utils';

const listInvites = {
  path: '/cx/admin/ui/invites',
  breadcrumbName: 'Invites',
};

const showInvite = ({ invite }) => ({
  path: `/cx/admin/ui/invites/${invite.id}`,
  breadcrumbName: invite.ref,
});

export default {
  listInvites: breadcrumbs([listInvites]),
  showInvite: breadcrumbs([listInvites, showInvite]),
};
