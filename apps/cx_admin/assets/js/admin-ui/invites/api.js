import Utils from '../api/utils';

export function getInvites(params) {
  return Utils.request.get('/cx/admin/api/invites', params);
}

export function getInvite(id) {
  return Utils.request.get(`/cx/admin/api/invites/${id}`);
}
