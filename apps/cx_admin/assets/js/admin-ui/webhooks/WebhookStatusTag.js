import React from 'react';
import { Tag } from 'antd';

const color = (status) => {
  switch (status) {
    case 'queued':
      return 'blue';

    case 'processing':
      return 'green';

    case 'delivered':
      return 'green';

    case 'deferred':
      return 'red';

    case 'failed':
      return 'red';
  }
};

const WebhookStatusTag = (props) => {
  return <Tag color={color(props.status)}>{props.status}</Tag>;
};

export default WebhookStatusTag;
