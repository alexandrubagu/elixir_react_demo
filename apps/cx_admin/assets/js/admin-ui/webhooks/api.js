import Utils from '../api/utils';

export function getWebhooks(params) {
  return Utils.request.get('/cx/admin/api/webhooks', params);
}

export function getWebhook(id) {
  return Utils.request.get(`/cx/admin/api/webhooks/${id}`);
}
