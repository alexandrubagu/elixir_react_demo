import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Card, Layout, PageHeader, Row, Col, Descriptions } from 'antd';
import Moment from 'react-moment';
import { Actions } from './state';
import Breadcrumbs from './Breadcrumbs';
import WebhookLogTimeline from './WebhookLogTimeline';
import WebhookStatusTag from './WebhookStatusTag';
import { JSONCode } from '../components/utils';

const WebhookShow = (props) => {
  const [breadcrumbs, setBreadcrumbs] = useState([]);

  useEffect(() => {
    props.getWebhook(props.match.params.id).then(() => {
      setBreadcrumbs(Breadcrumbs.showWebhook({ webhook: props.current }));
    });
  }, []);

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Webhook details"
          subTitle={`(${props.current.id || ''})`}
          breadcrumb={breadcrumbs}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Row gutter={[16, 24]}>
          <Col lg={{ span: 14 }} md={{ span: 24 }}>
            <Card title="General">
              <Descriptions bordered column={1}>
                <Descriptions.Item label="URL">
                  {props.current.url}
                </Descriptions.Item>
                <Descriptions.Item label="Created At">
                  <Moment fromNow>{props.current.inserted_at}</Moment>
                </Descriptions.Item>
                <Descriptions.Item label="Data">
                  <JSONCode json={props.current.data} />
                </Descriptions.Item>
              </Descriptions>
            </Card>

            <Card
              title={
                <>
                  Delivery <WebhookStatusTag status={props.current.status} />
                </>
              }
              style={{ marginTop: 24 }}
            >
              <Descriptions bordered column={1}>
                <Descriptions.Item label="Attempts">
                  {props.current.attempts}
                </Descriptions.Item>

                {props.current.status == 'deferred' && (
                  <>
                    <Descriptions.Item label="Last Attempt">
                      <Moment fromNow>{props.current.updated_at}</Moment>
                    </Descriptions.Item>

                    <Descriptions.Item label="Next Attempt">
                      <Moment fromNow>{props.current.retry_at}</Moment>
                    </Descriptions.Item>
                  </>
                )}

                {props.current.status == 'delivered' && (
                  <Descriptions.Item label="Delivered At">
                    <Moment fromNow>{props.current.delivered_at}</Moment>
                  </Descriptions.Item>
                )}

                {props.current.status == 'failed' && (
                  <Descriptions.Item label="Failed At">
                    <Moment fromNow>{props.current.failed_at}</Moment>
                  </Descriptions.Item>
                )}
              </Descriptions>
            </Card>
          </Col>
          <Col lg={{ span: 10 }} md={{ span: 24 }}>
            <Card title="Webhook log">
              <WebhookLogTimeline log={props.current.log} />
            </Card>
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  return {
    current: state.Webhooks.current,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getWebhook: (id) => dispatch(Actions.getWebhook(id)),
    retryDelivery: (id) => dispatch(Actions.retryEmailMessageDelivery(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WebhookShow);
