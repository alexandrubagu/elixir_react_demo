import { breadcrumbs } from '../components/utils';

const listWebhooks = {
  path: '/cx/admin/ui/webhooks',
  breadcrumbName: 'Webhooks',
};

const showWebhook = ({ webhook }) => ({
  path: `/cx/admin/ui/webhooks/${webhook.id}`,
  breadcrumbName: webhook.id,
});

export default {
  listWebhooks: breadcrumbs([listWebhooks]),
  showWebhook: breadcrumbs([listWebhooks, showWebhook]),
};
