import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Layout, PageHeader, Tabs } from 'antd';
import { Actions } from './state';
import Breadcrumbs from './Breadcrumbs';
import DisplayWebhooks from './DisplayWebhook';

const WebhookList = (props) => {
  const fetchPending = () => {
    props.setFilters({ statuses: ['deferred', 'queued', 'processing'] });
    props.getWebhooks();
  };

  const fetchFailed = () => {
    props.setFilters({ status: ['failed'] });
    props.getWebhooks();
  };

  const fetchDelivered = () => {
    props.setFilters({ status: ['delivered'] });
    props.getWebhooks();
  };

  useEffect(() => {
    fetchFailed();
  }, []);

  const fetchData = (tabKey) => {
    tabKey == 'failed' && fetchFailed();
    tabKey == 'pending' && fetchPending();
    tabKey == 'delivered' && fetchDelivered();
  };

  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader
          className="site-page-header"
          title="Webhook listing"
          breadcrumb={Breadcrumbs.listWebhooks()}
        />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Tabs onChange={fetchData}>
          <Tabs.TabPane tab="Failed" key="failed">
            <DisplayWebhooks state="failed" data={props.data} />
          </Tabs.TabPane>
          <Tabs.TabPane tab="Pending" key="pending">
            <DisplayWebhooks state="pending" data={props.data} />
          </Tabs.TabPane>
          <Tabs.TabPane tab="Delivered" key="delivered">
            <DisplayWebhooks state="delivered" data={props.data} />
          </Tabs.TabPane>
        </Tabs>
      </Layout.Content>
    </Layout>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.Webhooks.data,
    search: state.Webhooks.search,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getWebhooks: () => dispatch(Actions.getWebhooks()),
    setFilters: (filters) => dispatch(Actions.setFilters(filters)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WebhookList);
