import React from 'react';
import { Tag, Timeline, Row, Col, Typography, Card, Empty } from 'antd';
import Moment from 'react-moment';
import { JSONCode } from '../components/utils';

const WebhookLogTimeline = (props) => {
  const log = props.log || [];

  if (log.length == 0) {
    return <Empty description="No log entries yet" />;
  } else {
    return (
      <Timeline>
        {log.map((entry) => (
          <WebhookLogTimelineItem entry={entry} key={entry.id} />
        ))}
      </Timeline>
    );
  }
};

const WebhookLogTimelineItem = (props) => {
  const entry = props.entry;

  return (
    <Timeline.Item key={entry.id}>
      <Row>
        <Col>
          <TypeTag type={entry.type} />
        </Col>
        <Col>
          <Typography.Text disabled>
            <Moment fromNow>{entry.inserted_at}</Moment>
          </Typography.Text>
        </Col>
      </Row>
      <Card bordered={false}>
        <TypeDescription type={entry.type} />

        <JSONCode json={entry.info} />
      </Card>
    </Timeline.Item>
  );
};

const TypeTag = (props) => {
  const [color, label] = color_and_label(props.type);
  return <Tag color={color}>{label}</Tag>;
};

const color_and_label = (type) => {
  switch (type) {
    case 'queued':
      return ['blue', 'queued'];

    case 'delivered':
      return ['green', 'delivered'];

    case 'deferred':
      return ['volcano', 'deferred'];

    case 'processing':
      return ['green', 'processing'];

    case 'dropped':
      return ['red', 'dropped'];

    case 'reported_as_spam':
      return ['red', 'reported_as_spam'];

    case 'unsubscribed':
      return ['red', 'unsubscribed'];

    case 'redelivery_requested':
      return ['blue', 'redelivery_requested'];

    case 'email_address_updated':
      return ['blue', 'email_address_updated'];
  }
};

const TypeDescription = (props) => {
  const message = statusDescription(props.type);
  return <p>{message}</p>;
};

const statusDescription = (type) => {
  switch (type) {
    case 'queued':
      return 'The webhook was just added to the queue and no processing was attempted yet';

    case 'delivered':
      return 'The webhook was successfully delivered.';

    case 'processing':
      return 'The webhook is picked for processing.';

    case 'deferred':
      return 'Delivery of the webhook failed and delivery will be re-attempted at a later moment.';

    case 'failed':
      return 'Delivery of the webhook failed and no further attempts will be made.';

    default:
      return 'no info';
  }
};

export default WebhookLogTimeline;
