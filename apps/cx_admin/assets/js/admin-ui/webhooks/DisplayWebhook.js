import React from 'react';
import { connect } from 'react-redux';
import { Table, Card } from 'antd';
import Moment from 'react-moment';
import { Actions } from './state';
import { UUIDLink } from '../components/utils';
import WebhookStatusTag from './WebhookStatusTag';
import { PresetColorTypes } from 'antd/lib/_util/colors';

const webhookFailedTableColumns = [
  {
    dataIndex: 'id',
    title: 'ID',
    render: (id) => <UUIDLink prefix="/cx/admin/ui/webhooks/" id={id} />,
  },
  {
    dataIndex: 'url',
    title: 'URL',
  },
  {
    dataIndex: 'event',
    title: 'Event',
    sorter: true,
  },

  {
    dataIndex: 'log',
    title: 'Reason',
    render: (log) => {
      if (Array.isArray(log) && log.length > 1) {
        return log[0].type;
      }
      return '-';
    },
  },
  {
    title: 'Failed At',
    dataIndex: 'failed_at',
    sorter: true,
    render: (failed_at) =>
      failed_at ? <Moment fromNow>{failed_at}</Moment> : '-',
  },
];

const webhookPendingTableColumns = [
  {
    dataIndex: 'id',
    title: 'ID',
    render: (id) => <UUIDLink prefix="/cx/admin/ui/webhooks/" id={id} />,
  },
  {
    dataIndex: 'url',
    title: 'URL',
  },
  {
    dataIndex: 'event',
    title: 'Event',
    sorter: true,
  },
  {
    dataIndex: 'status',
    title: 'Status',
    sorter: true,
    render: (status) => <WebhookStatusTag status={status} />,
  },
  {
    title: 'Attempts',
    dataIndex: 'attempts',
    sorter: true,
  },
  {
    title: 'Retry',
    dataIndex: 'retry_at',
    sorter: true,
    render: (retry_at) =>
      retry_at ? <Moment fromNow>{retry_at}</Moment> : '-',
  },
];

const webhookDeliveredTableColumns = [
  {
    dataIndex: 'id',
    title: 'ID',
    render: (id) => <UUIDLink prefix="/cx/admin/ui/webhooks/" id={id} />,
  },
  {
    dataIndex: 'url',
    title: 'URL',
  },
  {
    dataIndex: 'event',
    title: 'Event',
    sorter: true,
  },
  {
    title: 'Delivered At',
    dataIndex: 'delivered_at',
    sorter: true,
    render: (delivered_at) =>
      delivered_at ? <Moment fromNow>{delivered_at}</Moment> : '-',
  },
];

const getTableColumnByState = (state) => {
  switch (state) {
    case 'failed':
      return webhookFailedTableColumns;
    case 'pending':
      return webhookPendingTableColumns;
    case 'delivered':
      return webhookDeliveredTableColumns;
  }
};

const DisplayWebhooks = (props) => {
  const handleTableChange = (pagination, filters, sorter) => {
    Promise.all([
      props.setPagination(pagination),
      props.setSorting(sorter),
    ]).then(props.getWebhooks);
  };

  return (
    <Card bodyStyle={{ padding: 0 }}>
      <Table
        columns={getTableColumnByState(props.state)}
        dataSource={props.data}
        pagination={props.pagination}
        loading={props.loading}
        rowKey={(record) => record.id}
        onChange={handleTableChange}
      />
    </Card>
  );
};

const mapStateToProps = (state) => {
  return {
    pagination: state.Webhooks.pagination,
    loading: state.Webhooks.loading,
    data: state.Webhooks.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getWebhooks: () => dispatch(Actions.getWebhooks()),
    setPagination: (page) => dispatch(Actions.setPagination(page)),
    setSorting: (field, order) => dispatch(Actions.setSorting(field, order)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayWebhooks);
