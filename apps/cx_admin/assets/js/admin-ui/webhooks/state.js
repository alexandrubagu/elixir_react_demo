import * as api from './api';
import * as common from '../reducers/common';

const WEBHOOKS_FETCHED = 'WEBHOOKS_FETCHED';
const WEBHOOK_FETCHED = 'WEBHOOK_FETCHED';
const WEBHOOKS_REQUEST_PENDING = 'WEBHOOKS_REQUEST_PENDING';
const WEBHOOKS_REQUEST_ERROR = 'WEBHOOKS_REQUEST_ERROR';
const SET_WEBHOOKS_PAGINATION = 'SET_WEBHOOKS_PAGINATION';
const SET_WEBHOOKS_SORTING = 'SET_WEBHOOKS_SORTING';
const SET_WEBHOOKS_SEARCH = 'SET_WEBHOOKS_SEARCH';
const SET_WEBHOOKS_FILTERS = 'SET_WEBHOOKS_FILTERS';

const pendingRequest = () => ({
  type: WEBHOOKS_REQUEST_PENDING,
});

const requestFailed = (error) => ({
  type: WEBHOOKS_REQUEST_ERROR,
  payload: error,
});

const webhooksFetched = (payload) => ({
  type: WEBHOOKS_FETCHED,
  payload: payload,
});

const webhookFetched = (event) => ({
  type: WEBHOOK_FETCHED,
  payload: event,
});

export const Actions = {
  setPagination: (pagination) => ({
    type: SET_WEBHOOKS_PAGINATION,
    payload: pagination,
  }),

  setSorting: (sorting) => {
    return {
      type: SET_WEBHOOKS_SORTING,
      payload: sorting,
    };
  },

  setSearch: (search) => ({
    type: SET_WEBHOOKS_SEARCH,
    payload: search,
  }),

  setFilters: (filters) => ({
    type: SET_WEBHOOKS_FILTERS,
    payload: filters,
  }),

  getWebhooks: () => (dispatch, getState) => {
    const { pagination, sorting, search, filters } = getState().Webhooks;

    dispatch(pendingRequest());
    return api.getWebhooks({ pagination, sorting, search, filters }).then(
      (response) => dispatch(webhooksFetched(response)),
      (error) => dispatch(requestFailed(error)),
    );
  },

  getWebhook: (id) => (dispatch) => {
    dispatch(pendingRequest());
    return api.getWebhook(id).then(
      (response) => dispatch(webhookFetched(response.data)),
      (error) => dispatch(requestFailed(error)),
    );
  },
};

const initialState = {
  loading: false,
  pagination: {},
  sorting: {},
  search: '',
  filters: {},
  data: [],
  current: {},
  error: {},
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case WEBHOOKS_REQUEST_PENDING:
      return { ...state, loading: true };

    case WEBHOOKS_REQUEST_ERROR:
      return { ...state, loading: false, error: action.payload };

    case SET_WEBHOOKS_PAGINATION:
      return common.updatePagination(state, action.payload);

    case SET_WEBHOOKS_SORTING:
      return common.updateSorting(state, action.payload);

    case SET_WEBHOOKS_SEARCH:
      return common.updateSearch(state, action.payload);

    case SET_WEBHOOKS_FILTERS:
      return { ...state, filters: action.payload };

    case WEBHOOKS_FETCHED:
      return { ...state, loading: false, ...action.payload };

    case WEBHOOK_FETCHED:
      return { ...state, loading: false, current: action.payload };

    default:
      return state;
  }
};
