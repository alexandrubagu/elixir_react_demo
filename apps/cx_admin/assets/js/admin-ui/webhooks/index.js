import * as State from './state';
import WebhookList from './WebhookList';
import WebhookShow from './WebhookShow';

export const Actions = State.Actions;
export const Reducer = State.Reducer;
export const List = WebhookList;
export const Show = WebhookShow;
