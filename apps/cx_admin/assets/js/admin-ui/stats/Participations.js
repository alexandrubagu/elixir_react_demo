import React from 'react';
import { Card, Row, Col } from 'antd';
import SessionStats from './SessionStats';
import SessionActivity from './SessionActivity';
import SentimentAnalysis from './SentimentAnalysis';

const Participations = (props) => {
  return (
    <Card title="Participations" className="participation-sessions-stats">
      <Row gutter={[16, 24]}>
        <Col span={4}>
          <SessionStats />
        </Col>
        <Col span={20}>
          <Row>
            <Col span={24}>
              <SessionActivity />
            </Col>
            <Col span={24} style={{ marginTop: 24 }}>
              <SentimentAnalysis />
            </Col>
          </Row>
        </Col>
      </Row>
    </Card>
  );
};

export default Participations;
