import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card } from 'antd';
import NotificationChart, { extractValues } from './NotificationChart';
import { Actions } from './state';
import { withSocketContext } from '../context/socket';

const colorsMap = (type) => {
  switch (type) {
    case 'queued':
      return '#a6cee3';
    case 'failed':
      return '#1f78b4';
    case 'sent':
      return '#e31a1c';
    case 'delivered':
      return '#fb9a99';
    case 'undelivered':
      return '#33a02c';
    case 'unsubscribed':
      return '#999999';
    default:
      return '#ff7f00';
  }
};

class SmsHealth extends Component {
  componentDidMount() {
    this.props.subscribeSmsHealthStats(this.context.channel);
  }

  componentWillUnmount() {
    this.props.unsubscribeSmsHealthStats(this.context.channel);
  }

  render() {
    return (
      <Card title="Sms Health" style={{ marginTop: 24, marginBottom: 10 }}>
        <NotificationChart
          data={this.props.data}
          colorKey="sms_event_type"
          colorsMap={colorsMap}
        />
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: extractValues(state.Stats.smsHealthData),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    subscribeSmsHealthStats: (channel) =>
      dispatch(Actions.subscribeSmsHealthStats(channel)),
    unsubscribeSmsHealthStats: (channel) =>
      dispatch(Actions.unsubscribeSmsHealthStats(channel)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withSocketContext(SmsHealth));
