const MAIL_HEALTH_STATS_RECEIVED = 'MAIL_HEALTH_STATS_RECEIVED';
const MAIL_ACTIVITY_STATS_RECEIVED = 'MAIL_ACTIVITY_STATS_RECEIVED';
const SMS_HEALTH_STATS_RECEIVED = 'SMS_HEALTH_STATS_RECEIVED';
const PARTICIPATION_SESSION_STATS_RECEIVED =
  'PARTICIPATION_SESSION_STATS_RECEIVED';
const WEBHOOK_STATS_RECEIVED = 'WEBHOOK_STATS_RECEIVED';
const INITIAL_ANSWER_CHANGES = 'INITIAL_ANSWER_CHANGES';
const ANSWER_CHANGED = 'ANSWER_CHANGED';
const INITIAL_ANALYZED_VERBATIMS = 'INITIAL_ANALYZED_VERBATIMS';
const VERBATIM_ANALYZED = 'VERBATIM_ANALYZED';

const mailEventTypes = [
  'generated',
  'queued',
  'delivered',
  'bounced',
  'deferred',
  'opened',
  'dropped',
  'reported_as_spam',
  'unsubscribed',
];

const mailTypes = ['invite', 'remember', 'thanks', 'abandonned'];

const smsEventTypes = [
  'queued',
  'failed',
  'sent',
  'delivered',
  'undelivered',
  'unsubscribed',
];

const participationSessionTypes = ['active', 'completed', 'abandonned'];

const webhookTypes = [
  'queued',
  'processing',
  'delivered',
  'deferred',
  'failed',
];

const initialData = (types) => {
  const state = {};
  types.forEach((e) => {
    state[e] = {};
  });
  return state;
};

const updateData = (data, newData, statName, types) => {
  for (let idx = 0; idx < newData.length; idx++) {
    const element = newData[idx];
    data[element[statName]][element.ts] = element;
  }

  types.forEach((series) => {
    const ts = [];
    for (const key in data[series]) {
      if (data[series].hasOwnProperty(key)) {
        ts.push(data[series][key]);
      }
    }
    const latest = ts
      .sort((a, b) => new Date(a.ts) - new Date(b.ts))
      .slice(-12)
      .reduce((acc, i) => {
        acc[i.ts] = i;
        return acc;
      }, {});
    data[series] = latest;
  });

  return data;
};

export const Actions = {
  subscribeMailHealthStats: (channel) => (dispatch, getState) => {
    let statName = 'mail_event_type';
    channel
      .push('get_stats', { stat_name: statName }, 15000)
      .receive('ok', (stats) =>
        dispatch({
          type: MAIL_HEALTH_STATS_RECEIVED,
          payload: updateData(
            getState().Stats.mailHealthData,
            stats.data,
            statName,
            mailEventTypes,
          ),
        }),
      )
      .receive('error', (reason) => console.log('get_stats failed', reason))
      .receive('timeout', () => console.log('Networking issue...'));

    channel.on(statName, (stats) =>
      dispatch({
        type: MAIL_HEALTH_STATS_RECEIVED,
        payload: updateData(
          getState().Stats.mailHealthData,
          stats.data,
          statName,
          mailEventTypes,
        ),
      }),
    );
  },
  unsubscribeMailHealthStats: (channel) => () => channel.off('mail_event_type'),
  subscribeMailActivityStats: (channel) => (dispatch, getState) => {
    let statName = 'mail_type';
    channel
      .push('get_stats', { stat_name: statName }, 15000)
      .receive('ok', (stats) =>
        dispatch({
          type: MAIL_ACTIVITY_STATS_RECEIVED,
          payload: updateData(
            getState().Stats.mailActivityData,
            stats.data,
            statName,
            mailTypes,
          ),
        }),
      )
      .receive('error', (reason) => console.log('get_stats failed', reason))
      .receive('timeout', () => console.log('Networking issue...'));

    channel.on(statName, (stats) =>
      dispatch({
        type: MAIL_ACTIVITY_STATS_RECEIVED,
        payload: updateData(
          getState().Stats.mailActivityData,
          stats.data,
          statName,
          mailTypes,
        ),
      }),
    );
  },
  unsubscribeMailActivityStats: (channel) => () => channel.off('mail_type'),
  subscribeSmsHealthStats: (channel) => (dispatch, getState) => {
    let statName = 'sms_event_type';
    channel
      .push('get_stats', { stat_name: statName }, 15000)
      .receive('ok', (stats) =>
        dispatch({
          type: SMS_HEALTH_STATS_RECEIVED,
          payload: updateData(
            getState().Stats.smsHealthData,
            stats.data,
            statName,
            smsEventTypes,
          ),
        }),
      )
      .receive('error', (reason) => console.log('get_stats failed', reason))
      .receive('timeout', () => console.log('Networking issue...'));

    channel.on(statName, (stats) =>
      dispatch({
        type: SMS_HEALTH_STATS_RECEIVED,
        payload: updateData(
          getState().Stats.smsHealthData,
          stats.data,
          statName,
          smsEventTypes,
        ),
      }),
    );
  },
  unsubscribeSmsHealthStats: (channel) => () => channel.off('sms_event_type'),
  subscribeParticipationSessionStats: (channel) => (dispatch, getState) => {
    let statName = 'participation_status';
    channel
      .push('get_stats', { stat_name: statName }, 15000)
      .receive('ok', (stats) =>
        dispatch({
          type: PARTICIPATION_SESSION_STATS_RECEIVED,
          payload: updateData(
            getState().Stats.participationSessionData,
            stats.data,
            statName,
            participationSessionTypes,
          ),
        }),
      )
      .receive('error', (reason) => console.log('get_stats failed', reason))
      .receive('timeout', () => console.log('Networking issue...'));

    channel.on(statName, (stats) =>
      dispatch({
        type: PARTICIPATION_SESSION_STATS_RECEIVED,
        payload: updateData(
          getState().Stats.participationSessionData,
          stats.data,
          statName,
          participationSessionTypes,
        ),
      }),
    );
  },
  unsubscribeParticipationSessionStats: (channel) => () =>
    channel.off('participation_status'),
  subscribeWebhookHealthStats: (channel) => (dispatch, getState) => {
    let statName = 'webhook_status';
    channel
      .push('get_stats', { stat_name: statName }, 15000)
      .receive('ok', (stats) =>
        dispatch({
          type: WEBHOOK_STATS_RECEIVED,
          payload: updateData(
            getState().Stats.webhookData,
            stats.data,
            statName,
            webhookTypes,
          ),
        }),
      )
      .receive('error', (reason) => console.log('get_stats failed', reason))
      .receive('timeout', () => console.log('Networking issue...'));

    channel.on(statName, (stats) =>
      dispatch({
        type: WEBHOOK_STATS_RECEIVED,
        payload: updateData(
          getState().Stats.webhookData,
          stats.data,
          statName,
          webhookTypes,
        ),
      }),
    );
  },
  unsubscribeWebhookHealthStats: (channel) => () =>
    channel.off('webhook_status'),
  subscribeAnswerChangedStats: (channel) => (dispatch) => {
    let statName = 'answer_changed';
    channel
      .push('get_stats', { stat_name: statName }, 15000)
      .receive('ok', (stats) =>
        dispatch({
          type: INITIAL_ANSWER_CHANGES,
          payload: stats.data,
        }),
      )
      .receive('error', (reason) => console.log('get_stats failed', reason))
      .receive('timeout', () => console.log('Networking issue...'));

    channel.on(statName, (answerChanged) =>
      dispatch({
        type: ANSWER_CHANGED,
        payload: answerChanged,
      }),
    );
  },
  unsubscribeAnswerChangedStats: (channel) => () =>
    channel.off('answer_changed'),
  subscribeVerbatimAnalyzedStats: (channel) => (dispatch) => {
    let statName = 'verbatim_analyzed';
    channel
      .push('get_stats', { stat_name: statName }, 15000)
      .receive('ok', (initialVerbatims) =>
        dispatch({
          type: INITIAL_ANALYZED_VERBATIMS,
          payload: initialVerbatims.data,
        }),
      )
      .receive('error', (reason) => console.log('get_stats failed', reason))
      .receive('timeout', () => console.log('Networking issue...'));

    channel.on(statName, (verbatimAnalyzed) => {
      dispatch({
        type: VERBATIM_ANALYZED,
        payload: verbatimAnalyzed,
      });
    });
  },
  unsubscribeVerbatimAnalyzedStats: (channel) => () =>
    channel.off('verbatim_analyzed'),
};

const initialState = {
  mailEventTypes: mailEventTypes,
  mailTypes: mailTypes,
  smsEventTypes: smsEventTypes,
  participationSessionTypes: participationSessionTypes,
  mailHealthData: initialData(mailEventTypes),
  mailActivityData: initialData(mailTypes),
  smsHealthData: initialData(smsEventTypes),
  participationSessionData: initialData(participationSessionTypes),
  webhookData: initialData(webhookTypes),
  answerChanges: [],
  sentiments: [],
};

export const Reducer = (state = initialState, action) => {
  switch (action.type) {
    case MAIL_HEALTH_STATS_RECEIVED:
      return { ...state, mailHealthData: action.payload };

    case MAIL_ACTIVITY_STATS_RECEIVED:
      return { ...state, mailActivityData: action.payload };

    case PARTICIPATION_SESSION_STATS_RECEIVED:
      return { ...state, participationSessionData: action.payload };

    case INITIAL_ANSWER_CHANGES:
      return { ...state, answerChanges: action.payload };

    case ANSWER_CHANGED:
      return {
        ...state,
        answerChanges: appendAnswerChanged(state.answerChanges, action.payload),
      };

    case INITIAL_ANALYZED_VERBATIMS:
      return { ...state, sentiments: initializeSentiments(action.payload) };

    case VERBATIM_ANALYZED:
      return {
        ...state,
        sentiments: appendAnalyzedVerbatim(state.sentiments, action.payload),
      };

    default:
      return state;
  }
};

const appendAnswerChanged = (previous, change) => {
  previous.unshift(change);
  return previous.slice(0, 10);
};

const initializeSentiments = (verbatims) =>
  verbatims.flatMap(mapVerbatimToSentiments).slice(0, 10);

const appendAnalyzedVerbatim = (previousSentiments, verbatim) => {
  let newSentiments = mapVerbatimToSentiments(verbatim);
  return newSentiments.reverse().concat(previousSentiments).slice(0, 10);
};

const mapVerbatimToSentiments = (verbatim) =>
  verbatim.sentiments.map((sentiment) => {
    sentiment.id = verbatim.participation_id;
    return sentiment;
  });
