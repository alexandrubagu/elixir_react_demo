import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card } from 'antd';
import NotificationChart, { extractValues } from './NotificationChart';
import { Actions } from './state';
import { withSocketContext } from '../context/socket';

const colorsMap = (type) => {
  switch (type) {
    case 'queued':
      return '#a6cee3';
    case 'processing':
      return '#e31a1c';
    case 'delivered':
      return '#fb9a99';
    case 'deferred':
      return '#33a02c';
    case 'failed':
      return '#1f78b4';
    default:
      return '#ff7f00';
  }
};

class WebhookHealth extends Component {
  componentDidMount() {
    this.props.subscribeWebhookHealthStats(this.context.channel);
  }

  componentWillUnmount() {
    this.props.unsubscribeWebhookHealthStats(this.context.channel);
  }

  render() {
    return (
      <Card title="Webhook Health" style={{ marginTop: 24, marginBottom: 10 }}>
        <NotificationChart
          data={this.props.data}
          colorKey="webhook_status"
          colorsMap={colorsMap}
        />
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: extractValues(state.Stats.webhookData),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    subscribeWebhookHealthStats: (channel) =>
      dispatch(Actions.subscribeWebhookHealthStats(channel)),
    unsubscribeWebhookHealthStats: (channel) =>
      dispatch(Actions.unsubscribeWebhookHealthStats(channel)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withSocketContext(WebhookHealth));
