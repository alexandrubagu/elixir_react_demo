import React, { useContext, useEffect } from 'react';
import { Card, Table } from 'antd';
import { connect } from 'react-redux';
import Moment from 'react-moment';
import SocketContext from '../context/socket';
import { Actions } from './state';
import { UUIDLink } from '../components/utils';

const columns = [
  {
    title: 'Label',
    dataIndex: 'question',
    render: (text, record) => {
      let label = record.question + ' changed to ' + record.answer;
      return label.length > 50 ? label.slice(0, 50) + '...' : label;
    },
  },
  {
    title: 'Participation',
    dataIndex: 'id',
    render: (text) => (
      <UUIDLink prefix="/cx/admin/ui/participations/" id={text} />
    ),
  },
  {
    title: 'Time',
    dataIndex: 'ts',
    render: (text, record) => <Moment fromNow>{text}</Moment>,
  },
];

const SessionActivity = (props) => {
  const context = useContext(SocketContext);

  useEffect(() => {
    props.subscribeAnswerChangedStats(context.channel);

    return () => {
      props.unsubscribeAnswerChangedStats(context.channel);
    };
  }, []);

  return (
    <Card title="Participation activity" size="small">
      <Table
        size="small"
        columns={columns}
        dataSource={props.answerChanges}
        pagination={false}
      />
    </Card>
  );
};

const mapStateToProps = (state) => {
  return {
    answerChanges: state.Stats.answerChanges,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    subscribeAnswerChangedStats: (channel) =>
      dispatch(Actions.subscribeAnswerChangedStats(channel)),
    unsubscribeAnswerChangedStats: (channel) =>
      dispatch(Actions.unsubscribeAnswerChangedStats(channel)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SessionActivity);
