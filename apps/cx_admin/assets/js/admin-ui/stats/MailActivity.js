import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card } from 'antd';
import NotificationChart, { extractValues } from './NotificationChart';
import { Actions } from './state';
import { withSocketContext } from '../context/socket';

const colorsMap = (type) => {
  switch (type) {
    case 'invite':
      return '#253494';
    case 'remember':
      return '#2c7fb8';
    case 'thanks':
      return '#41b6c4';
    case 'abandonned':
      return '#a1dab4';
    default:
      return '#ffffcc';
  }
};

class MailActivity extends Component {
  componentDidMount() {
    this.props.subscribeMailActivityStats(this.context.channel);
  }

  componentWillUnmount() {
    this.props.unsubscribeMailActivityStats(this.context.channel);
  }

  render() {
    return (
      <Card title="Mail Activity" style={{ marginTop: 24 }}>
        <NotificationChart
          data={this.props.data}
          colorKey="mail_type"
          colorsMap={colorsMap}
        />
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: extractValues(state.Stats.mailActivityData),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    subscribeMailActivityStats: (channel) =>
      dispatch(Actions.subscribeMailActivityStats(channel)),
    unsubscribeMailActivityStats: (channel) =>
      dispatch(Actions.unsubscribeMailActivityStats(channel)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withSocketContext(MailActivity));
