import React, { useEffect } from 'react';
import { Row, Col, Card, Layout, PageHeader } from 'antd';
import MailHealth from './MailHealth';
import MailActivity from './MailActivity';
import SmsHealth from './SmsHealth';
import WebhookHealth from './WebhookHealth';
import Participations from './Participations';

const Stats = (props) => {
  return (
    <Layout>
      <div className="site-layout-background">
        <PageHeader className="site-page-header" title="Notification Stats" />
      </div>

      <Layout.Content style={{ padding: 24, margin: 0, minHeight: 280 }}>
        <Row gutter={[16, 24]}>
          <Col span={12}>
            <MailHealth />
            <MailActivity />
            <SmsHealth />
            <WebhookHealth />
          </Col>
          <Col span={12}>
            <Participations />
          </Col>
        </Row>
      </Layout.Content>
    </Layout>
  );
};

export default Stats;
