import React, { useContext, useEffect } from 'react';
import { Card, Table } from 'antd';
import { SmileOutlined, MehOutlined, FrownOutlined } from '@ant-design/icons';
import { green, red, grey } from '@ant-design/colors';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import SocketContext from '../context/socket';
import { Actions } from './state';
import { UUIDLink } from '../components/utils';
import { SentimentIcon } from '../participations/VerbatimDetails';

const columns = [
  {
    title: 'Label',
    dataIndex: 'label',
    render: (text, record) =>
      record.text_en.length > 50
        ? record.text_en.slice(0, 50) + '...'
        : record.text_en,
  },
  {
    title: 'Participation',
    dataIndex: 'id',
    render: (text, record) => (
      <UUIDLink prefix="/cx/admin/ui/participations/" id={text} />
    ),
  },
  {
    title: 'Score',
    dataIndex: 'score',
    render: (text, record) => <SentimentIcon score={record.sentiment_score} />,
  },
];

const SentimentAnalysis = (props) => {
  const context = useContext(SocketContext);

  useEffect(() => {
    props.subscribeVerbatimAnalyzedStats(context.channel);

    return () => {
      props.unsubscribeVerbatimAnalyzedStats(context.channel);
    };
  }, []);

  return (
    <Card title="Sentiment analysis" size="small">
      <Table
        size="small"
        columns={columns}
        dataSource={props.sentiments}
        pagination={false}
      />
    </Card>
  );
};

const mapStateToProps = (state) => {
  return {
    sentiments: state.Stats.sentiments,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    subscribeVerbatimAnalyzedStats: (channel) =>
      dispatch(Actions.subscribeVerbatimAnalyzedStats(channel)),
    unsubscribeVerbatimAnalyzedStats: (channel) =>
      dispatch(Actions.unsubscribeVerbatimAnalyzedStats(channel)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SentimentAnalysis);
