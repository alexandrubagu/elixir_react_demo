import React from 'react';
import moment from 'moment';
import { Chart, Legend, Axis, Geom, Interaction, Tooltip } from 'bizcharts';

const config = {
  height: 200,
  autoFit: true,
  scale: {
    cnt: {
      min: 0,
      max: 30,
      tickInterval: 10,
      alias: 'Value',
    },
  },
};

const NotificationChart = (props) => {
  return (
    <Chart {...config} data={props.data}>
      <Tooltip shared={true} showMarkers={false} />
      <Legend position="top" />
      <Axis
        name="ts"
        label={{ formatter: (text) => moment(text).format('HH:mm') }}
      />

      <Axis name="value" title={{ style: { fill: '#8C8C8C', fontSize: 14 } }} />
      <Interaction type="active-region" />
      <Geom
        type="interval"
        position="ts*cnt"
        adjust="stack"
        color={[props.colorKey, (val) => props.colorsMap(val)]}
      />
    </Chart>
  );
};

export const extractValues = (data) => {
  return Object.entries(data).reduce((acc, [key, object]) => {
    return acc.concat(Object.values(object));
  }, []);
};

export default NotificationChart;
