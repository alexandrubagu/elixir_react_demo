import React, { useEffect, useContext } from 'react';
import { connect } from 'react-redux';
import SocketContext from '../context/socket';
import { extractValues } from './NotificationChart';
import { Actions } from './state';

const sumStatus = (series, data) => {
  return data.reduce((acc, value) => {
    return value.participation_status === series ? acc + value.cnt : acc;
  }, 0);
};

const SessionStats = (props) => {
  const context = useContext(SocketContext);

  useEffect(() => {
    props.subscribeParticipationSessionStats(context.channel);

    return () => {
      props.unsubscribeParticipationSessionStats(context.channel);
    };
  }, []);

  return (
    <dl className="participation-counters">
      {props.participationSessionTypes.map((type, index) => (
        <div key={index}>
          <dt>{sumStatus(type, props.participationSessionData)}</dt>
          <dd>{type}</dd>
        </div>
      ))}
    </dl>
  );
};

const mapStateToProps = (state) => {
  return {
    participationSessionTypes: state.Stats.participationSessionTypes,
    participationSessionData: extractValues(
      state.Stats.participationSessionData,
    ),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    subscribeParticipationSessionStats: (channel) =>
      dispatch(Actions.subscribeParticipationSessionStats(channel)),
    unsubscribeParticipationSessionStats: (channel) =>
      dispatch(Actions.unsubscribeParticipationSessionStats(channel)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SessionStats);
