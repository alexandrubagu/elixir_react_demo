import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Card } from 'antd';
import NotificationChart, { extractValues } from './NotificationChart';
import { Actions } from './state';
import { withSocketContext } from '../context/socket';

const colorsMap = (type) => {
  switch (type) {
    case 'generated':
      return '#a6cee3';
    case 'delivered':
      return '#1f78b4';
    case 'bounced':
      return '#e31a1c';
    case 'deferred':
      return '#fb9a99';
    case 'opened':
      return '#33a02c';
    case 'reported_as_spam':
      return '#666666';
    case 'unsubscribed':
      return '#999999';
    default:
      return '#ff7f00';
  }
};

class MailHealth extends Component {
  componentDidMount() {
    this.props.subscribeMailHealthStats(this.context.channel);
  }

  componentWillUnmount() {
    this.props.unsubscribeMailHealthStats(this.context.channel);
  }

  render() {
    return (
      <Card title="Mail Health">
        <NotificationChart
          data={this.props.data}
          colorKey="mail_event_type"
          colorsMap={colorsMap}
        />
      </Card>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    data: extractValues(state.Stats.mailHealthData),
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    subscribeMailHealthStats: (channel) =>
      dispatch(Actions.subscribeMailHealthStats(channel)),
    unsubscribeMailHealthStats: (channel) =>
      dispatch(Actions.unsubscribeMailHealthStats(channel)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withSocketContext(MailHealth));
