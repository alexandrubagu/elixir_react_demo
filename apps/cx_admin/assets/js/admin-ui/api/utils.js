import qs from 'qs';

const defaultOptions = {
  cache: 'no-cache',
  headers: { 'Content-Type': 'application/json' },
};

const throwIfError = (response) => {
  if (response.error) {
    throw { error: response.error };
  }
  return response;
};

const to_api_query_params = (params) => {
  let api_params = {};
  api_params = maybe_add_search(params.search, api_params);
  api_params = maybe_add_pagination(params.pagination, api_params);
  api_params = maybe_add_sorting(params.sorting, api_params);
  return maybe_add_filters(params.filters, api_params);
};

const maybe_add_search = (search, api_params) => {
  if (search) {
    api_params.search = search;
  }

  return api_params;
};

const maybe_add_pagination = (pagination, api_params) => {
  if (typeof pagination === 'object') {
    api_params.page = pagination.current;
    api_params.page_size = pagination.pageSize;
  }

  return api_params;
};

const maybe_add_sorting = (sorting, api_params) => {
  if (typeof sorting === 'object') {
    if (sorting.order == 'ascend') {
      api_params.sort = { field: sorting.field, order: 'asc' };
    }

    if (sorting.order == 'descend') {
      api_params.sort = { field: sorting.field, order: 'desc' };
    }
  }
  return api_params;
};

const maybe_add_filters = (filters, api_params) => {
  if (typeof filters === 'object') {
    api_params.filters = Object.keys(filters).reduce((acc, key) => {
      if (Array.isArray(filters[key])) {
        acc[key] = filters[key].length > 1 ? filters[key] : filters[key][0];
      }
      return acc;
    }, {});
  }
  return api_params;
};

export default {
  request: {
    get: async function (url, params = {}) {
      let query_params = '';
      if (Object.keys(params).length > 0) {
        query_params =
          '?' +
          qs.stringify(to_api_query_params(params), {
            arrayFormat: 'brackets',
            encode: false,
          });
      }

      return fetch(url + query_params)
        .then((response) => response.json())
        .then((response) => throwIfError(response))
        .then((response) => {
          if (typeof response.page_info === 'object') {
            response.pagination = {
              pageSize: response.page_info.page_size,
              current: response.page_info.page_number,
              total: response.page_info.total_entries,
            };
            delete response.page_info;
          }

          return response;
        });
    },
    post: async function (url, data = {}) {
      return fetch(url, {
        ...defaultOptions,
        method: 'POST',
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((response) => throwIfError(response));
    },
    put: async function (url, data = {}) {
      return fetch(url, {
        ...defaultOptions,
        method: 'PUT',
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((response) => throwIfError(response));
    },
    delete: async function (url) {
      return fetch(url, {
        ...defaultOptions,
        method: 'DELETE',
      });
    },
    upload: async function (url, data) {
      return fetch(url, {
        method: 'POST',
        body: data,
      })
        .then((response) => response.json())
        .then((response) => throwIfError(response));
    },
  },
};
