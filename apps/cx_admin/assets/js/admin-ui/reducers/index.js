import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import * as SmsMessages from '../sms_messages/state';
import * as EmailMessages from '../email_messages/state';
import * as EmailWebhookEvents from '../email_webhook_events';
import * as Surveys from '../surveys';
import * as Invites from '../invites';
import Batches from '../batches';
import BatchEntries from '../batch_entries';
import * as Participations from '../participations/state';
import * as BlacklistEntries from '../blacklist_entries/state';
import * as Stats from '../stats';
import * as ParticipationSyncers from '../participation_syncers/state';
import * as Webhooks from '../webhooks/state';

const state = combineReducers({
  routing: routerReducer,
  BlacklistEntries: BlacklistEntries.Reducer,
  SmsMessages: SmsMessages.Reducer,
  EmailMessages: EmailMessages.Reducer,
  EmailWebhookEvents: EmailWebhookEvents.Reducer,
  Surveys: Surveys.Reducer,
  Invites: Invites.Reducer,
  Participations: Participations.Reducer,
  Batches: Batches.Reducer,
  BatchEntries: BatchEntries.Reducer,
  Participations: Participations.Reducer,
  Stats: Stats.Reducer,
  ParticipationSyncers: ParticipationSyncers.Reducer,
  Webhooks: Webhooks.Reducer,
});

export default state;
