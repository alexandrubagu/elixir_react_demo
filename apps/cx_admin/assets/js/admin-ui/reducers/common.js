export const updatePagination = (state, pagination) => {
  return { ...state, pagination: pagination };
};

export const updateSorting = (state, sorting) => {
  const nextState = { ...state, sorting };
  return nextState.sorting.field != state.sorting.field ||
    nextState.sorting.order != state.sorting.order
    ? resetPagination(nextState)
    : nextState;
};

export const updateSearch = (state, search) => {
  const nextState = { ...state, search: search };
  return nextState.search != state.search
    ? resetPagination(nextState)
    : nextState;
};

const resetPagination = (state) => {
  const { pageSize } = state.pagination;
  return { ...state, pagination: { pageSize: pageSize } };
};

export const resetSortPaginationSearch = (state) => {
  return resetPagination({ ...state, sorting: {}, search: '' });
};
