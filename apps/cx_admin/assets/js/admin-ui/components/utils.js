import React from 'react';
import { Link } from 'react-router-dom';
import { notification, Space } from 'antd';
import { CloseOutlined } from '@ant-design/icons';
import JSONPretty from 'react-json-pretty';

export const success_notification = (message) =>
  Promise.resolve(notification['success']({ message: message }));

export const error_notification = (message) =>
  Promise.resolve(notification['error']({ message: message }));

export const errorsAsLookup = (error) => {
  if (Object.keys(error).length > 0 && error.type == 'unprocessable') {
    return error.error.info.errors.reduce((acc, error) => {
      acc[error.location.join('.')] = error.message;
      return acc;
    }, {});
  }

  return {};
};

export const getError = (lookup, key, submitted) => {
  if (!!lookup[key] && submitted) {
    return {
      hasFeedback: true,
      validateStatus: 'error',
      help: lookup[key],
    };
  } else {
    return {};
  }
};

const home_breadcrumb = { path: '/cx/admin/ui', breadcrumbName: 'Home' };

export const breadcrumbs = (breadcrumbs) => (args) => {
  let routes = breadcrumbs.map((breadcrumb) => {
    if (typeof breadcrumb === 'function') {
      return breadcrumb(args || {});
    }
    return breadcrumb;
  });
  return {
    routes: [home_breadcrumb].concat(routes),
    itemRender: breadcrumbRenderer,
  };
};

export const breadcrumbRenderer = (route, params, routes, paths) => {
  const last = routes.indexOf(route) === routes.length - 1;
  return last ? (
    <span>{route.breadcrumbName}</span>
  ) : (
    <Link to={route.path}>{route.breadcrumbName}</Link>
  );
};

export const validateIsJSONObject = (rule, value, callback) => {
  const errorMessage = 'invalid JSON Object';
  try {
    let result = JSON.parse(value);
    Object.prototype.toString.call(result) == '[object Object]'
      ? callback()
      : callback(errorMessage);
  } catch (e) {
    callback(errorMessage);
  }
};

export const validateIsJSONList = (rule, value, callback) => {
  const errorMessage = 'invalid JSON List';
  try {
    let result = JSON.parse(value);
    Array.isArray(result) ? callback() : callback(errorMessage);
  } catch (e) {
    callback(errorMessage);
  }
};

export const isEmpty = (obj) =>
  obj == null ||
  typeof obj == 'undefined' ||
  (Object.keys(obj).length === 0 && obj.constructor === Object);

export const shortenUUID = (uuid) => {
  uuid = uuid || '';
  return uuid.substring(uuid.length - 7, uuid.length);
};

export const UUIDLink = (props) => {
  const id = props.id || '';
  const prefix = props.prefix || '';

  const label = shortenUUID(id);
  return <Link to={`${prefix}${id}`}>{label}</Link>;
};

export const CodeBlock = (props) => {
  const content = props.content || '';
  return (
    <code style={{ wordWrap: 'break-word', wordBreak: 'break-word' }}>
      {content}
    </code>
  );
};

export const JSONCode = (props) => (
  <JSONPretty
    data={props.json}
    theme={{
      main: 'line-height:1.3;color:#586e75;overflow:auto;',
      error: 'line-height:1.3;color:#586e75;overflow:auto;',
      key: 'color:#2aa198;',
      string: 'color:#cb4b16;',
      value: 'color:#85990;',
      boolean: 'color:#85990;',
    }}
  />
);

export const MailTemplate = (props) => {
  if (props.template != null || props.template != undefined) {
    return props.template;
  } else {
    return (
      <Space>
        <CloseOutlined />
        template not set
      </Space>
    );
  }
};
