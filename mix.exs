defmodule CX.Umbrella.Mixfile do
  @moduledoc false
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      start_permanent: Mix.env() == :prod,
      deps: deps(),

      # Coveralls (also needs to be in each app)
      test_coverage: [tool: ExCoveralls],

      # Dialyzer
      dialyzer: [
        plt_add_deps: :app_tree,
        plt_add_apps: [:mix],
        flags: [:race_conditions, :no_opaque]
      ],

      # Releases
      releases: [
        cx: [
          version: "0.0.1",
          cookie: "[<W@532sI*qb*GcBM:|eK_2y*@[F&$n@mQbEaS2@gR[5u}j>[WMMoYAO!ceYO(>t",
          include_executables_for: [:unix],
          steps: [:assemble, :tar],
          applications: [
            runtime_tools: :permanent,
            cx: :permanent,
            cx_admin: :permanent,
            cx_api: :permanent
          ]
        ]
      ]
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options.
  #
  # Dependencies listed here are available only for this project
  # and cannot be accessed from applications inside the apps folder
  defp deps do
    [
      # Dev/Test dependencies
      {:mix_test_watch, "~> 1.0", only: :dev, runtime: false},
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev, :test], runtime: false},
      {:excoveralls, "~> 0.10", only: :test}
    ]
  end
end
